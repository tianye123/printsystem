<?php
class administrator extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function tableName()
    {
        return '{{administrator}}';
    }
     public function rules()
    {
        return array(
            array("username", "required", "message" => "用户名不能为空"),
            array("password", "required", "message" => "密码不能为空"),
        );
    }

}
