<?php

class terninalStateController extends Controller {

    public function filterIsSessionWrong($filterChain) {
        if (isset(Yii::app()->session['username']) && (!empty(Yii::app()->session['username']))) {
            $filterChain->run();
        } else {
            $this->redirect(array('login/login'));
        }
    }

    public function filters() {
        return array('IsSessionWrong');
    }

    public function actionterninalState() {
        $username = Yii::app()->session['username'];

        $print_model = printor::model();

        $print_info = $print_model->findAll();

        if ($print_info) {
            $crt = count($print_info);
            $per = 10;
            $page = new page($crt, $per);
            $sql = "select * from tbl_printor order by printorId asc $page->limit";
            $print_list = $page->fpage(array(0, 3, 4, 5, 6, 7));
            $print_info = $print_model->findAllBySql($sql);
        } else {
            $print_list = "";
        }
        $this->renderPartial('terninalState', array('username' => $username, 'print_info' => $print_info, 'print_list' => $print_list));
    }

    public function actionterninalPicture($machineId) {
        $username = Yii::app()->session['username'];
        $machineId = base64_decode($machineId);

        $printor_model = printor::model();
        $printor_info = $printor_model->find(array('condition' => "machineId = '$machineId'"));

        if ($printor_info->picture) {
            $picture_array = explode(",", $printor_info->picture);
        } else {
            $picture_array = array();
        }
        $print_name = $printor_info->printorName;

        $this->renderPartial('terninalPicture', array('username' => $username, 'picture_array' => $picture_array, "print_name" => $print_name));
    }

}
