<?php

class loginController extends Controller {

    public $defaultAction = 'Login';

    public function actionLogin() {

        $username = Yii::app()->session['username'];

        $cookie = Yii::app()->request->cookies["username"];
        if ($cookie) {
            $cookie_username = $cookie->value;
        } else {
            $cookie_username = "";
        }
        $cookies = Yii::app()->request->cookies["password"];
        if ($cookies) {
            $cookie_password = $cookies->value;
        } else {
            $cookie_password = "";
        }

        $administrator_model = administrator::model();
        $store_model = store::model();
        $store_info = $store_model->findAll();
        $this->renderPartial('Login', array('administrator_model' => $administrator_model, 'store_info' => $store_info, 'username' => $username, 'cookie_username' => $cookie_username, 'cookie_password' => $cookie_password));
    }

//用户登录
    public function actionuserLogin() {

        $user_model = new user();

        $loginnam = $_POST['loginUsername']; //用户名
        $loginps = md5($_POST['loginPassword']); //密码
        $remember = $_POST['remeber']; //是否记住密码
//        $logcaptcha = new LoginForm();
//        $logcaptcha->captcha = $_POST['verifyCode'];
        $user_infos = $user_model->find(array('condition' => "username= '$loginnam' AND userpsw ='$loginps'")); //判断用户是否存在？ 
        $user_infoq = $user_model->find(array('condition' => "phone= '$loginnam' AND userpsw ='$loginps'")); //判断手机号用户是否存在？ 

        if (count($user_infos) != 0) {
            Yii::app()->session['username'] = $loginnam; //重新赋值
            if ($remember == "true") {
                $cookie = new CHttpCookie('username', $loginnam);
                $cookie->expire = time() + 60 * 60 * 24 * 30;  //有限期30天
                Yii::app()->request->cookies['username'] = $cookie; //用户名

                $cookies = new CHttpCookie('password', base64_encode($_POST['loginPassword']));
                $cookies->expire = time() + 60 * 60 * 24 * 30;  //有限期30天
                Yii::app()->request->cookies['password'] = $cookies; //密码
            } else {
                $cookiea = Yii::app()->request->getCookies();
                unset($cookiea['username']);
                unset($cookiea['password']);

                $cookie = new CHttpCookie('username', "");
                $cookie->expire = time() - 1;  //有限期30天
                Yii::app()->request->cookies['username'] = $cookie; //用户名

                $cookies = new CHttpCookie('password', "");
                $cookies->expire = time() - 1;  //有限期30天
                Yii::app()->request->cookies['password'] = $cookies; //密码
            }

            $json = '{"data":"success"}';
            echo $json;
        } else if (count($user_infos) == 0 && count($user_infoq) != 0) {
            Yii::app()->session['username'] = $user_infoq->username; //重新赋值
            if ($remember == "true") {
                $cookie = new CHttpCookie('username', $loginnam);
                $cookie->expire = time() + 60 * 60 * 24 * 30;  //有限期30天
                Yii::app()->request->cookies['username'] = $cookie; //用户名

                $cookies = new CHttpCookie('password', base64_encode($_POST['loginPassword']));
                $cookies->expire = time() + 60 * 60 * 24 * 30;  //有限期30天
                Yii::app()->request->cookies['password'] = $cookies; //密码
            } else {
                $cookiea = Yii::app()->request->getCookies();
                unset($cookiea['username']);
                unset($cookiea['password']);

                $cookie = new CHttpCookie('username', "");
                $cookie->expire = time() - 1;  //有限期30天
                Yii::app()->request->cookies['username'] = $cookie; //用户名

                $cookies = new CHttpCookie('password', "");
                $cookies->expire = time() - 1;  //有限期30天
                Yii::app()->request->cookies['password'] = $cookies; //密码
            }

            $json = '{"data":"success"}';
            echo $json;
        } else if (count($user_infos) == 0 && count($user_infoq) == 0) {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    //注册
    public function actionuserRegister() {
        $user_model = new user();
        $record_model = new record();
        $integralDetails_model = new integralDetails();

        $registerUsername = $_POST['registerUsername']; //用户名
        $registerPassword = md5($_POST['registerPassword']); //密码
        $registerPhone = $_POST['registerPhone']; //手机
        $registerEmail = $_POST['registerEmail']; //邮箱
        $phonecode = $_POST['phonecode']; //手机验证码
        $storeid = $_POST['school']; //所属学校



        $user_infos = $user_model->find(array('condition' => "username= '$registerUsername'")); //判断用户是否存在？ 
        if ($user_infos) {
            $json = '{"data":"false-exist"}';
            echo $json;
        } else {

            if ($phonecode == Yii::app()->session['phoneRegistercheckma']) {
                $user_model->username = $registerUsername;
                $user_model->userpsw = $registerPassword;
                $user_model->phone = $registerPhone;
                $user_model->email = $registerEmail;
                $user_model->_storeid = $storeid;
                date_default_timezone_set('PRC');

                $user_model->registertime = date('Y-m-d H:i:s');
//                $user_model->integration = 200;

                if ($user_model->save()) {
                    $record_model->userid = $user_model->userid;
                    $record_model->points = 200;
                    $integralDetails_model->_userid = $user_model->userid;
                    $integralDetails_model->addIntegral = 200;
                    $integralDetails_model->reduceIntegral = 0;
                    $integralDetails_model->happentime = date('Y-m-d H:i:s');
                    $integralDetails_model->happenInfo = "注册送积分";


                    if ($record_model->save() && $integralDetails_model->save()) {
                        Yii::app()->session['username'] = $registerUsername;
                        unset(Yii::app()->session['phoneRegistercheckma']);
                        $json = '{"data":"success"}';
                        echo "$json";
                    }
                } else {
                    $json = '{"data":"false-error"}';
                    echo "$json";
                }
            } else {
                $json = '{"data":"phonecode_false"}';
                echo "$json";
            }
        }
    }

    //注册发送验证码
    public function actionuserRegisterphone() {
        $registerPhone = $_POST['registerPhone'];
        $user_model = user::model();
        $user_infos = $user_model->find(array('condition' => "phone= '$registerPhone'")); //判断手机是否注册？
        if (isset(Yii::app()->session['phoneRegistercheckma'])) {
            unset(Yii::app()->session['phoneRegistercheckma']);
        }
        $phoneRegistercheckma = mt_rand(100000, 999999);
        Yii::app()->session['phoneRegistercheckma'] = $phoneRegistercheckma;
        $user = 'cqutprint'; //短信接口用户名 $user
        $pwd = '112233'; //短信接口密码 $pwd
        $mobiles = $registerPhone; //说明：取用户输入的手号
        $contents = "亲爱的用户，您此次注册的验证码为" . $phoneRegistercheckma . "，验证码很重要，打死都不要告诉任何人哦，谢谢！"; //说明：取用户输入的短信内容
        $chid = 0; //通道ID
        $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
        file_get_contents($sendMessage);
        $json = '{"data":"success"}';
        echo $json;
    }

    //注册判断手机是否存在
    public function actionCheckphone() {
        $registerPhone = $_POST['registerPhone'];
        $user_model = user::model();
        $user_infos = $user_model->find(array('condition' => "phone= '$registerPhone'")); //判断手机是否注册？
        if (isset($user_infos)) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    public function actionloginOut() {
        if (isset(Yii::app()->session['username'])) {
            unset(Yii::app()->session['username']);
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    //找回密码
    public function actionFindpsw() {
        $this->renderPartial('Findpsw');
    }

    public function actioncheckpsw() {
        $FindUsername = $_POST['FindUsername'];

        $user_model = user::model();
        $user_infos = $user_model->find(array('condition' => "username= '$FindUsername'")); //判断用户是否存在？ 

        require('./css/jy/lib/class.geetestlib.php');
        $PRIVATE_KEY = "562c1e19b5080bcd4893989beaf9db9b";
        $geetest = new GeetestLib($PRIVATE_KEY);
        $validate_response = $geetest->validate(@$_POST ['geetest_challenge'], @$_POST ['geetest_validate'], @$_POST ['geetest_seccode']);

        if ($validate_response && $user_infos) {
            Yii::app()->session['FindUsername'] = $FindUsername;
            echo "<script>parent.location.href='./index.php?r=login/findPsw_2';</script>";
        } else if (!$user_infos) {
            echo '<script>parent.$("#FindUsername").css("border", "2px red solid");parent.$("#username_error").text("没有此用户！");</script>';
        } else {
            
        }
    }

    public function actionfindPsw_2() {

        $FindUsername = Yii::app()->session['FindUsername'];
        $user_model = user::model();
        $user_infos = $user_model->find(array('condition' => "username= '$FindUsername'")); //判断用户是否存在？ 


        $this->renderPartial('findPsw_2', array("user_infos" => $user_infos));
    }

    public function actionemailCheck() {
        $this->renderPartial('emailCheck');
    }

    //发送邮箱验证码
    public function actionemailCheckSend() {


        $FindUsername = Yii::app()->session['FindUsername'];
        $user_model = user::model();
        $user_infos = $user_model->find(array('condition' => "username= '$FindUsername'"));
        require "./css/sendEmail/email.class.php";
        $checkma = base64_encode(mt_rand(100000, 999999));


//******************** 配置信息 ********************************
        $smtpserver = "smtp.163.com"; //SMTP服务器
        $smtpserverport = 25; //SMTP服务器端口
        $smtpusermail = "13330290051@163.com"; //SMTP服务器的用户邮箱
        $smtpemailto = $user_infos->email; //发送给谁
        $smtpuser = "13330290051@163.com"; //SMTP服务器的用户帐号
        $smtppass = "guaxixiya"; //SMTP服务器的用户密码
        $mailtitle = "找回密码"; //邮件主题
        date_default_timezone_set('PRC');
        $mailcontent = "<p>您好：</p><p>您正在使用邮箱验证找回密码，请点击以下链接重置密码。如果您的浏览器不支持直接点击链接地址，请将下面的地址复制到您的浏览器地址栏中，然后点击“转到”按钮。(注：只能在本平台找回密码的的浏览器中打开，给您带来的不便，敬请谅解！)</p><a href='http://www.cqutprint.com?r=login/emailCheckO&checkma=$checkma'>http://www.cqutprint.com?r=login/emailCheckO&checkma=$checkma</a><p>重庆颇闰科技有限公司</p><p>" . date('Y-m-d H:i:s') . "</p></h1>"; //邮件内容
        $mailtype = "HTML"; //邮件格式（HTML/TXT）,TXT为文本邮件
//************************ 配置信息 ****************************
        $smtp = new smtp($smtpserver, $smtpserverport, true, $smtpuser, $smtppass); //这里面的一个true是表示使用身份验证,否则不使用身份验证.
        $smtp->debug = false; //是否显示发送的调试信息
        $state = $smtp->sendmail($smtpemailto, $smtpusermail, $mailtitle, $mailcontent, $mailtype);

        if ($state == "") {
            $json = '{"data":"false"}';
            echo $json;
        } else {
            Yii::app()->session['checkma'] = $checkma;
            $json = '{"data":"success"}';
            echo $json;
        }
    }

    //点击链接访问
    public function actionemailCheckOK() {
        $this->renderPartial('emailCheckOK');
    }

    public function actionemailCheckO($checkma) {
        if ($checkma == Yii::app()->session['checkma']) {
            $this->redirect(array('login/emailCheckOK'));
        } else {
            echo "验证失败，请重试！";
        }
    }

    public function actionresetPassword() {
        $newpsw = $_POST['newpsw'];
        $FindUsername = Yii::app()->session['FindUsername'];
        $user_model = user::model();
        $user_infos = $user_model->find(array('condition' => "username= '$FindUsername'"));

        if (isset(Yii::app()->session['phonecheckma']) || isset(Yii::app()->session['checkma'])) {
            $user_infos->userpsw = md5($newpsw);
            if ($user_infos->save()) {
                unset(Yii::app()->session['FindUsername']);
                unset(Yii::app()->session['phonecheckma']);
                unset(Yii::app()->session['checkma']);
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        } else {
            $json = '{"data":"error"}';
            echo $json;
        }
    }

    public function actionphoneCheck() {
        $this->renderPartial('phoneCheck');
    }

    public function actionphoneCheckSend() {

        $FindUsername = Yii::app()->session['FindUsername'];
        $user_model = user::model();
        $user_infos = $user_model->find(array('condition' => "username= '$FindUsername'"));

        $phonecheckma = mt_rand(100000, 999999);
        Yii::app()->session['phonecheckma'] = $phonecheckma;

        $user = 'cqutprint'; //短信接口用户名 $user
        $pwd = '112233'; //短信接口密码 $pwd
        $mobiles = $user_infos->phone; //说明：取用户输入的手号
        $contents = "亲爱的用户，您此次重置密码的验证码为" . $phonecheckma . ",验证码很重要，打死都不要告诉任何人哦，谢谢！"; //说明：取用户输入的短信内容
        $chid = 0; //通道ID
        $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
        file_get_contents($sendMessage);
        $json = '{"data":"success"}';
        echo $json;
    }

    public function actioncheckPhonema() {
        $phonecheck = $_POST['phonecheck'];
        if ($phonecheck == Yii::app()->session['phonecheckma']) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"error"}';
            echo $json;
        }
    }

    public function actionphoneCheckOK() {
        $this->renderPartial('phoneCheckOK');
    }

    //验证码
    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'height' => 40,
                'width' => 60,
                'minLength' => 4,
                'maxLength' => 4,
                'backColor' => 0xD1E1F1,
                'foreColor' => 0xf46229,
            //'transparent' => true,
        ));
    }

    public function actionprotocol() {
        $this->renderPartial('protocol');
    }

    public function actiontext() {
        $this->renderPartial('text');
    }

}
