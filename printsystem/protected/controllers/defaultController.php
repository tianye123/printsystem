<?php

class defaultController extends Controller {

    public function actionIndex() {
        $user_model = new user();
        $attachment_model = new attachment();
        $usermessage_model = usermessage::model();
        $business_model = business::model();

        $usernamess = Yii::app()->session['username'];
        $notPrint = array();
        $yesPrint = array();
        $moreFile = array();
        $orderInfo = array();


        if ($usernamess) {
            $user_infoss = $user_model->find(array('condition' => "username = '$usernamess'"));

////支付未打印
//            $user_notattachment = $business_model->findAll(array('condition' => "_userid='$user_infoss->userid' AND iscomplete = 0 AND ispay =1 AND isrefund =0", 'limit' => 4, 'order' => "businessid DESC")); //支付未打印
//            if ($user_notattachment) {
//                foreach ($user_notattachment as $X => $Z) {
//                    $attachment_messs = $attachment_model->find(array('condition' => "attachmentid = $Z->_attachmentid"));
//                    array_push($notPrint, array("attachmentid" => $attachment_messs->attachmentid, "attachmentname" => $attachment_messs->attachmentname, "fileNumber" => $attachment_messs->filenumber, "printNumbers" => $Z->printNumbers, "paidMoney" => $Z->paidMoney, "paytime" => $Z->paytime));
//                }
//            }
//上传文件
            $user_attachment = $attachment_model->findAll(array('condition' => "_userid = '$user_infoss->userid' AND isdelete = 1 ", 'order' => "attachmentid DESC"));
            if ($user_attachment) {
                foreach ($user_attachment as $K => $V) {
                    array_push($moreFile, array("attachmentid" => $V->attachmentid, "attachmentname" => $V->attachmentname, "filenumber" => $V->filenumber, "loadtime" => $V->uploadtime,));
                }
            }
//订单信息
            $user_orderinfo = $business_model->findAll(array('condition' => "_userid = '$user_infoss->userid' AND isdelete = 0 AND isrefund =0", 'order' => "businessid DESC"));
            if ($user_orderinfo) {
                foreach ($user_orderinfo as $K => $V) {
                    array_push($orderInfo, array("orderId" => $V->orderId, "payType" => $V->payType, "paidMoney" => $V->paidMoney, "placeOrdertime" => $V->placeOrdertime, "ispay" => $V->ispay));
                }
            }

//            //已完成打印文件
//            $user_yesattachment = $business_model->findAll(array('condition' => "_userid=$user_infoss->userid AND iscomplete = 1 AND ispay =1 AND isrefund =0", 'limit' => 4, 'order' => "businessid DESC")); //支付打印
//            if ($user_yesattachment) {
//                foreach ($user_yesattachment as $K => $V) {
//                    $attachment_messsx = $attachment_model->find(array('condition' => "attachmentid = $V->_attachmentid"));
//                    array_push($yesPrint, array("attachmentid" => $attachment_messs->attachmentid, "attachmentname" => $attachment_messs->attachmentname, "fileNumber" => $attachment_messs->filenumber, "printNumbers" => $V->printNumbers, "paidMoney" => $V->paidMoney, "printTime" => $V->printTime));
//                }
//            }
        }
//dropDownList
        $store_model = store::model();
        $store_info = $store_model->findAll();
        $store_list = array(); //学校选择
        $store_list[0] = '---选择学校---';
        foreach ($store_info as $K => $V) {
            $store_list[$V->storeid] = $V->storename;
        }

        if (isset($_POST["user"])) {//如果不为空，则执行一下操作
            $username = $_POST['user']['username']; //用户名
//$phone = $_POST['user']['phone'];//电话号码
            $user_infos = $user_model->find(array('condition' => "username = '$username'")); //判断用户名是否存在？
//用户名不存在
            if (!$user_infos) {
                foreach ($_POST['user'] as $_k => $_l) {
                    if ($_k == 'userpsw')
                        $user_model->userpsw = md5($_l);
                    else
                        $user_model->$_k = $_l;
                }
                $user_model->registertime = date('Y-m-d'); //time();
//如果学校不为空的话，则保存进去
                if (isset($_POST["store"])) {
                    $user_model->_storeid = $_POST['store']['storename'];
                } else {
                    $user_model->_storeid = "";
                }
                if ($user_model->save()) {
                    Yii::app()->session['username'] = $_POST['user']['username'];
                    echo '<script>alert("亲，注册成功，请点击确认登录！");parent.location.href="./index.php";</script>';

//$this->redirect("./index.php");
                } else {
                    echo '<script>alert("亲，很遗憾，注册失败了！")</script>';
                }
            } else {
                foreach ($_POST['user'] as $_k => $_l) {
                    if ($_k == 'userpsw')
                        $user_infos->userpsw = md5($_l);
                    else
                        $user_infos->$_k = $_l;
                }
//如果学校不为空的话，则保存进去
                if (isset($_POST["store"])) {
                    $user_infos->_storeid = $_POST['store']['storename'];
                } else {
                    $user_infos->_storeid = "";
                }
                if ($user_infos->save()) {
                    unset(Yii::app()->session['username']);
                    Yii::app()->session['username'] = $_POST['user']['username'];
                    echo '<script>alert("亲，修改成功！点击确定跳转..."); parent.location.href="./index.php";</script>';
                } else {
                    echo '<script>alert("亲，很遗憾，修改失败了！")</script>';
                }
            }
        }
        $this->render('index', array('user_model' => $user_model, 'attachment_model' => $attachment_model, 'store_model' => $store_model, 'store_list' => $store_list, 'notPrint' => $notPrint, 'yesPrint' => $yesPrint, 'orderInfo' => $orderInfo, 'usermessage_model' => $usermessage_model, 'moreFile' => $moreFile));
    }

//验证码
    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'height' => 40,
                'width' => 70,
                'minLength' => 4,
                'maxLength' => 4,
                'backColor' => 0xE4D6C9,
                'foreColor' => 0xf46229,
            //'transparent' => true,
        ));
    }

//登录验证
    public function actionlog() {

        $user_model = new user();

        $loginnam = $_POST['loginname']; //用户名
        $loginps = md5($_POST['loginpsw']); //密码
        $logcaptcha = new LoginForm();
        $logcaptcha->captcha = $_POST['verifyCode'];
        $user_infos = $user_model->find(array('condition' => "username= '$loginnam' AND userpsw ='$loginps'")); //判断用户是否存在？ 
//验证码
        if ($user_infos && $logcaptcha->validate()) {
            unset(Yii::app()->session['username']);
            Yii::app()->session['username'] = $loginnam; //重新赋值
            echo "success";
        }
//不存在
        else if ($logcaptcha->validate() != 1) {
            echo'亲，验证码错了额！';
        } else {
            echo'亲，用户名或者密码错了额！';
        }
    }

    public function actiontttt() {
//        $str = file_get_contents('php://input');
//        $str = file_get_contents('formData');
        $paras = $_GET['formData'];
        echo $paras;
    }

    public function actiontest() {
        echo dirname(Yii::app()->BasePath);

//        $username = Yii::app()->session['username'];
//
//        $administrator_model = administrator::model();
//        $this->render('test', array('administrator_model' => $administrator_model, 'username' => $username));
    }

    public function actionlogins() {
        $username = Yii::app()->session['username'];

        $administrator_model = administrator::model();
        $this->renderPartial('logins', array('administrator_model' => $administrator_model, 'username' => $username));
    }

    public function actionloginfo() {
        $username = Yii::app()->session['username'];
        if ($username) {
            $user_model = user::model();
            $store_model = store::model();
            $attachment_model = attachment::model();
            $business_model = business::model();

            $user_info = $user_model->find(array('condition' => "username= '$username'"));
            $user_school = $store_model->find(array('condition' => "storeid=$user_info->_storeid"));

//有附件
            $arr = array(
                'username' => $user_info->username,
                'phone' => $user_info->phone,
                'school' => $user_school->storename,
                'storelogo' => $user_school->storelogo,
//                    'userattachment' => $user_attachment->attachmentname,
//                    'loadtime' => $user_attachment->uploadtime,
//                'contact' => array(
//                    'email' => 'zhuoweida@163.com',
//                    'website' => 'http://zhuoweida.blog.tianya.cn',
//                )                    
            );



//            //最新未打印文件
//            $user_notattachment = $business_model->findAll(array('condition' => "_userid='$user_info->userid' AND iscomplete = 0 AND ispay =1 AND isrefund =0", 'limit' => 4, 'order' => "businessid DESC")); //支付未打印
//            if ($user_notattachment) {
//                $b = array();
//                foreach ($user_notattachment as $X => $Z) {
//                    $attachment_messs = $attachment_model->find(array('condition' => "attachmentid = $Z->_attachmentid"));
//                    array_push($b, array("userattachment" => $attachment_messs->attachmentname, "ispay" => $Z->ispay, "paytime" => $Z->paytime, "attachmentid" => $attachment_messs->attachmentid));
//                }
//                array_push($arr, $b);
//            }
//最新上传文件
//            $user_attachment = $attachment_model->findAll(array('condition' => "_userid = '$user_info->userid' AND isdelete = 1 ", 'limit' => 4, 'order' => "attachmentid DESC"));
//            if ($user_attachment) {
//                $d = array();
//                foreach ($user_attachment as $K => $V) {
//                    array_push($d, array("userattachment" => $V->attachmentname, "loadtime" => $V->uploadtime, "attachmentid" => $V->attachmentid));
//                }
//                array_push($arr, $d);
//            }
//最新订单
//            //已完成打印文件
//            $user_yesattachment = $business_model->findAll(array('condition' => "_userid=$user_info->userid AND iscomplete = 1 AND ispay =1 AND isrefund =0", 'limit' => 4, 'order' => "businessid DESC")); //支付打印
//            if ($user_yesattachment) {
//                $c = array();
//                foreach ($user_yesattachment as $K => $V) {
//                    $attachment_messsx = $attachment_model->find(array('condition' => "attachmentid = $V->_attachmentid"));
//                    array_push($c, array("userattachment" => $attachment_messsx->attachmentname, "ispay" => $V->ispay, "paytime" => $V->paytime, "attachmentid" => $attachment_messsx->attachmentid));
//                }
//                array_push($arr, $c);
//            }

            $json_string = json_encode($arr);
            echo $json_string; //json格式的字符串
        } else
            echo 'false';
    }

//注销
    public function actionlogout() {
        unset(Yii::app()->session['username']);
        $this->redirect("./index.php");
    }

//session是否存在判断
    public function actionsessioncheck() {
//如果session已存在，则跳转到登录
        if (Yii::app()->session['username']) {
            echo 'success';
        } else {
            echo 'false';
        }
    }

//判断用户名是否存在
    public function actionloginnamecheck() {
        $user_model = new user();

        $loginnam = $_POST['username']; //用户名       

        $user_infos = $user_model->find(array('condition' => "username= '$loginnam'")); //判断用户名是否存在？
//用户名存在
        if ($user_infos) {
            echo 'false';
        } else {
            echo 'success';
        }
    }

//获取需要修改者的信息
    public function actionlogininfo() {
        $username = Yii::app()->session['username'];
        if ($username) {
            $user_model = user::model();

            $user_info = $user_model->find(array('condition' => "username= '$username'"));

            $arr = array(
                'username' => $user_info->username,
                'phone' => $user_info->phone,
                'school' => $user_info->_storeid,
//                'contact' => array(
//                    'email' => 'zhuoweida@163.com',
//                    'website' => 'http://zhuoweida.blog.tianya.cn',
//                )
            );
            $json_string = json_encode($arr);
            echo $json_string; //json格式的字符串
        } else
            echo 'false';
    }

//上传文件
    public function actionuploadfile() {

        $attachment_model = new attachment();
        $user_model = user::model();
        $username = Yii::app()->session['username'];

        if ($username) {

            $user_info = $user_model->find(array('condition' => "username= '$username'"));

//$user_fileid = $user_file->attachmentid;
//$newId = $author_model->attributes['id'];
            date_default_timezone_set('PRC');
            $fileName = $user_info->userid . '-' . date('YmdHis');

            $attachment_file = CUploadedFile::getInstance($attachment_model, 'attachmentfile'); //文件本来的名字           
            if ($attachment_file != null) {
                $attachment_after = $attachment_file->getExtensionName(); //文件的后缀名 docx
//判断是否为word或者pdf
                if ($attachment_after == 'doc' || $attachment_after == 'docx' || $attachment_after == 'pdf') {
                    $attachment_file->saveAs('./assets/userfile/' . $fileName . "." . $attachment_after); //$author_model->attachmentfile
                    require 'PageInfo.php';
//$road = 'D:\Program Files\XAMPP\htdocs\printor\printsystem\assets\userfile\\' . $fileName . "." . $attachment_after;
                    $road = dirname(Yii::app()->basePath) . "\assets\userfile\\" . $fileName . "." . $attachment_after;

                    $filenumber = PageInfo::getPages($road, $attachment_after);
//把信息存入attachment表
                    $attachment_model->attachmentname = $attachment_file; //原名
                    $attachment_model->attachmentfile = $fileName . "." . $attachment_after; //新名
                    $attachment_model->_userid = $user_info->userid;
                    $attachment_model->filetype = $attachment_after;
                    $attachment_model->uploadtime = date('Y-m-d H:i:s');
                    $attachment_model->filenumber = (int) $filenumber;
                    $attachment_model->isdelete = 1;

                    if ($attachment_model->save()) {
                        echo '<script>'
                        . 'alert("亲，文件上传成功了！");'
//                        . 'parent.spinner.spin();'
//                        . 'parent.document.getElementById("bg").style.display = "none";'
//                        . 'parent.document.getElementById("show").style.display = "none";'
                        . 'parent.location.href="./index.php";'
                        . '</script>';
                    } else {
                        echo '<script>alert("亲，文件上传失败了,请重新上传！"); '
                        . 'parent.spinner.spin();'
                        . 'parent.document.getElementById("bg").style.display = "none";'
                        . 'parent.document.getElementById("show").style.display = "none";'
                        . '</script>';
                    }
                } else {
                    echo'<script>alert("亲，只支持word或者pdf文件，请重新上传！"); '
                    . 'parent.spinner.spin();'
                    . 'parent.document.getElementById("bg").style.display = "none";'
                    . 'parent.document.getElementById("show").style.display = "none";'
                    . '</script>';
                }
            } else {
                echo'<script>alert("亲，请先选择需要上传的文件！"); '
                . 'parent.spinner.spin();'
                . 'parent.document.getElementById("bg").style.display = "none";'
                . 'parent.document.getElementById("show").style.display = "none";'
                . '</script>';
            }
        } else {
            echo '<script>alert("亲，找不到你了，需要重新登录了！");</script>'; //session 不存在
        }
    }

//删除未打印文件
    public function actionnotDel() {
        $attachment_model = attachment::model();
        $attachmentid = $_POST['attachmentid']; //ID

        $attachment_info = $attachment_model->findByPk($attachmentid);
        $filename = './assets/userfile/' . $attachment_info->attachmentfile;
        $attachment_info->isdelete = 0;
        if (is_file($filename)) {
            if (unlink($filename) && $attachment_info->save()) {
                echo 'success';
            } else {
                echo '文件删除失败，权限不够';
            }
        } else {
            if ($attachment_info->save())
                echo 'success';
            else {
                echo'文件删除失败,请联系管理员。';
            }
        }
    }

//反馈信息存储
    public function actionfeedBack() {

        if (isset($_POST['usermessage'])) {
            $usermessage = $_POST['usermessage']['content'];
            $usermessage_model = new usermessage();
            $user_model = user::model();
            $usernamess = Yii::app()->session['username'];
            if ($usernamess) {
                $user_info = $user_model->find(array('condition' => "username='$usernamess'"));
                $userid = $user_info->userid;
            } else
                $userid = 0;
            $usermessage_model->pmessageid = 0;
            $usermessage_model->_userid = $userid;
            $usermessage_model->happentime = date('Y-m-d H:i:s');
            $usermessage_model->content = $usermessage;
            if ($usermessage_model->save()) {
                echo"<script>alert('亲爱的用户，我们已经接收到你的留言，感谢你的支持。'); parent.location.href = './index.php';</script>";
            } else {
                echo"<script>alert('亲爱的用户，非常抱歉，此功能已暂时关闭，我们将尽快开启。');</script>";
            }
        }
    }

//支付成功处理函数
    public function actionPayment($out_trade_no) {

        $business_model = business::model();

        $business_info = $business_model->find(array('condition' => "orderId=$out_trade_no"));

        $business_info->ispay = 1;
        $business_info->paytime = date('Y-m-d H:i:s'); //支付时间

        if ($attachment_info->save() && $business_info->save()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
