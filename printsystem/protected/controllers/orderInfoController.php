<?php

class orderInfoController extends Controller {

    public function filterIsSessionWrong($filterChain) {
        if (isset(Yii::app()->session['username']) && (!empty(Yii::app()->session['username']))) {
            $filterChain->run();
        } else {
            $this->redirect(array('login/login'));
        }
    }

    public function filters() {
        return array('IsSessionWrong');
    }

    public function actionorderInfo() {
        $user_model = user::model();
        $business_model = business::model();
        $subbusiness_model = subbusiness::model();
        $username = Yii::app()->session['username'];
        $Notpaid = array(); //未支付
        $Yetpaid = array(); //已支付
        $Yetprint = array(); //已打印
        $Notpaids = ""; //未支付
        $Yetpaids = ""; //已支付
        $Yetprints = ""; //已打印
        $user_infoss = $user_model->find(array('condition' => "username = '$username'"));
        $business_info = $business_model->findAll(array('condition' => "_userid = '$user_infoss->userid' AND isdelete = 0", 'order' => "businessid DESC"));
        foreach ($business_info as $k => $l) {
            //所有支付与否 1 全支付 0 不全支付
            $allpayed = 1;
            //0未打印完成
            $allprinted = 1;
            $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessId = '$l->businessid' AND isdelete = 0"));
            foreach ($subbusiness_info as $h => $j) {
                if ($j->isPay == 0) {//0未支付 
                    $allpayed = 0;
                }
                if ($j->status == 0 || $j->status == 2)
                    $allprinted = 0;

//                if ($j->isPay == 0 && $j->isdelete == 0) {//未支付
//                    $Notpaids .= $j->_businessId . ",";
//                    array_push($Notpaid, $j->_businessId);
//                } else if ($j->isPay == 1 && $j->isdelete == 0 && $j->status != 1) {//已支付未打印
//                    $Yetpaids .= $j->_businessId . ",";
//                    array_push($Yetpaid, $j->_businessId);
//                } else if ($j->isPay == 1 && $j->isdelete == 0 && $j->status == 1) {//已打印
//                    $Yetprints .= $j->_businessId . ",";
//                    array_push($Yetprint, $j->_businessId);
//                }
            }
            if ($allprinted == 1) {
                $Yetprints .= $j->_businessId . ",";
                array_push($Yetprint, $j->_businessId);
            } else {
                if ($allpayed == 1) {
                    $Yetpaids .= $j->_businessId . ",";
                    array_push($Yetpaid, $j->_businessId);
                } else {
                    $Notpaids .= $j->_businessId . ",";
                    array_push($Notpaid, $j->_businessId);
                }
            }
        }
        $Notpaids = substr($Notpaids, 0, strlen($Notpaids) - 1);
        $Yetpaids = substr($Yetpaids, 0, strlen($Yetpaids) - 1);
        $Yetprints = substr($Yetprints, 0, strlen($Yetprints) - 1);

        if (strlen($Notpaids) == 0) {
            array_push($Notpaid, -1);
        }
        if (strlen($Yetpaids) == 0) {
            array_push($Yetpaid, -1);
        }
        if (strlen($Yetprints) == 0) {
            array_push($Yetprint, -1);
        }

        $Notpaidcriteria = new CDbCriteria;
        $Notpaidcriteria->compare('businessid', $Notpaid);
        $NotpaidList = $business_model->findAll($Notpaidcriteria);

        $Yetpaidcriteria = new CDbCriteria;
        $Yetpaidcriteria->compare('businessid', $Yetpaid);
        $YetpaidList = $business_model->findAll($Yetpaidcriteria);

        $Yetprintcriteria = new CDbCriteria;
        $Yetprintcriteria->compare('businessid', $Yetprint);
        $YetprintList = $business_model->findAll($Yetprintcriteria);

        //未支付
        if ($NotpaidList) {
            $crt = count($NotpaidList);
            $per = 10;
            $page = new page($crt, $per);
            $sql = "select * from tbl_business where businessid in  (" . $Notpaids . ") order by businessid desc $page->limit";
            $Notpaid_list = $page->fpage(array(0, 3, 4, 5, 6, 7));
            $NotpaidList = $business_model->findAllBySql($sql);
        } else {
            $Notpaid_list = "";
        }
        //未打印
        if ($YetpaidList) {
            $crt = count($YetpaidList);
            $per = 10;
            $page = new page($crt, $per);
            $sql = "select * from tbl_business where businessid in  (" . $Yetpaids . ") order by businessid desc $page->limit";
            $Yetpaid_list = $page->fpage(array(0, 3, 4, 5, 6, 7));
            $YetpaidList = $business_model->findAllBySql($sql);
        } else {
            $Yetpaid_list = "";
        }
        //已打印
        if ($YetprintList) {
            $crt = count($YetprintList);
            $per = 10;
            $page = new page($crt, $per);
            $sql = "select * from tbl_business where businessid in  (" . $Yetprints . ") order by businessid desc $page->limit";
            $Yetprint_list = $page->fpage(array(0, 3, 4, 5, 6, 7));
            $YetprintList = $business_model->findAllBySql($sql);
        } else {
            $Yetprint_list = "";
        }
        $this->render('orderInfo', array('username' => $username, 'Notpaid_list' => $Notpaid_list, 'NotpaidList' => $NotpaidList, 'Yetpaid_list' => $Yetpaid_list, 'YetpaidList' => $YetpaidList, 'Yetprint_list' => $Yetprint_list, 'YetprintList' => $YetprintList));
    }

}
