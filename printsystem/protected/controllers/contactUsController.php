<?php

class contactUsController extends Controller {

    public function filterIsSessionWrong($filterChain) {
        if (isset(Yii::app()->session['username']) && (!empty(Yii::app()->session['username']))) {
            $filterChain->run();
        } else {
            $this->redirect(array('login/login'));
        }
    }

    public function filters() {
        return array('IsSessionWrong');
    }

    public function actionContactUs() {
        $username = Yii::app()->session['username'];
        $this->renderPartial('ContactUs', array('username' => $username));
    }

}
