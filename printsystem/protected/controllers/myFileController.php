<?php

class myFileController extends Controller {

    public function filterIsSessionWrong($filterChain) {
        if (isset(Yii::app()->session['username']) && (!empty(Yii::app()->session['username']))) {
            $filterChain->run();
        } else {
            $this->redirect(array('login/login'));
        }
    }

    public function filters() {
        return array('IsSessionWrong');
    }

    public function actionmyFile() {

        $attachment_model = attachment::model();
        $user_model = user::model();
        $username = Yii::app()->session['username'];

        $FileList = array();

        $user_infos = $user_model->find(array('condition' => "username = '$username'"));
        $user_attachment = $attachment_model->findAll(array('condition' => "_userid = '$user_infos->userid' AND isdelete = 1 ", 'order' => "attachmentid DESC"));

        if ($user_attachment) {
            $crt = count($user_attachment);
            $per = 10;
            $page = new page($crt, $per);
            $sql = "select *from tbl_attachment where _userid=$user_infos->userid AND isdelete=1 order by attachmentid desc $page->limit ";
            $page_list = $page->fpage(array(0, 3, 4, 5, 6, 7));
            $FileList1 = $attachment_model->findAllBySql($sql);
            foreach ($FileList1 as $K => $V) {
                array_push($FileList, array("attachmentid" => $V->attachmentid, "attachmentname" => $V->attachmentname, "filetype" => $V->filetype, "filenumber" => $V->filenumber, "loadtime" => $V->uploadtime, "attachmentfile" => $V->attachmentfile));
            }
        } else {
            $page_list = "";
        }
        $this->renderPartial('myFile', array("page_list" => $page_list, "FileList" => $FileList, "username" => $username));
    }

    public function actionmyFileDetail($businessid) {
        $username = Yii::app()->session['username'];

        $businessidd = base64_decode($businessid);

        $attachment_model = attachment::model();
        $user_model = user::model();
        $record_model = record::model();
        $businssid_model = business::model();
        $subbusiness_model = subbusiness::model();
        $businessid_info = $businssid_model->find(array("condition" => "businessid=$businessidd AND isdelete = 0"));
        $user_infos = $user_model->find(array('condition' => "username = '$username'"));

        $integration = (int) $record_model->find(array("condition" => "userid = '$user_infos->userid'"))->points;

        $attachmentArray = array();

        $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId=$businessidd AND isdelete = 0"));
        //所有支付与否
        $allpayed = 1;
        //0未打印完成
        $allprinted = 1;
        if ($subbusiness_info) {
            foreach ($subbusiness_info as $l => $y) {
                $attachmentId = $y->_attachmentId;
                $attachinfo = $attachment_model->find(array('condition' => "attachmentid = $attachmentId"));
                if ($y->isPay == 0)
                    $allpayed = 0; //未支付 
                if ($y->status == 0 || $y->status == 2)
                    $allprinted = 0;
                array_push($attachmentArray, array("attachmentId" => $attachmentId, "attachmentname" => $attachinfo->attachmentname, "printNumber" => $y->printNumbers, "paidMoney" => $y->paidMoney, "printSet" => $y->printSet, "payType" => $y->payType, "isPay" => $y->isPay, "status" => $y->status, "marchineId" => $y->marchineId, "subbusinessId" => $y->subbusinessId, "isrefund" => $y->isrefund));
            }
        }

        if ($allprinted == 1) {
            $sta = 0; //属于已打印列表
        } else {
            if ($allpayed == 1) {
                $sta = 1; //属于已支付列表
            } else {
                $sta = 2; //属于未支付列表
            }
        }

        $this->renderPartial('myFileDetail', array("businessid_info" => $businessid_info, "sta" => $sta, "attachmentArray" => $attachmentArray, "username" => $username, "integration" => $integration));
    }

//下订单
    public function actionplaceOrder() {

        $business_model = new business();


        $usernamess = Yii::app()->session['username'];
        $user_model = user::model();
        $attachment_model = attachment::model();
        $user_infoss = $user_model->find(array('condition' => "username = '$usernamess'"));

        $attachmentidd = $_POST['attachmentidd']; //打印文件ID
        $printnumbers = $_POST['printnumbers']; //打印文件份数
        $printPages = $_POST['printPages']; //打印页码

        date_default_timezone_set('PRC');

        $orderNum = date('YmdHis') . mt_rand(100000, 999999); //生成20位订单号 20150901094439392811123
        $orderNums = $business_model->find(array('condition' => "orderId = '$orderNum'")); //判断订单号是否唯一

        while ($orderNums) {
            $orderNum = date('YmdHis') . mt_rand(100000, 999999);
            $orderNums = $business_model->find(array('condition' => "orderId = '$orderNum'")); //判断订单号是否唯一
        }

        $attachmentid = explode(',', $attachmentidd); //文件ID 1,2,3,4
        $printList = explode(',', $printnumbers); //每个文件打印的份数 1,2,2,2
        $filePages = explode('*', $printPages); //每个文件打印的页码  1,2,3-5*5-9,10,11*1,5,8

        $money = 0.00; //总金额
        $money_one = 0.00; //订单中单个文件所需的金额
        $page = 0; //总页数  用于打折
        $page_one = 0; //单个文件页数  用于打折

        foreach ($printList as $l => $y) {
            $filePage = explode(',', $filePages[$l]);
            foreach ($filePage as $z => $x) {
                if (is_numeric($x)) {
                    $page += (int) $y;
//                    $money += (int) $y * 0.2;
                } else {
                    $filePag = explode('-', $x);
                    $filePa = $filePag[1] - $filePag[0] + 1;
                    $page += (int) $y * $filePa;
//                    $money += (int) $y * (int) $filePa * 0.2;
                }
            }
        }
        if ($page > 0 && $page < 20) {
            $money = $page * 0.2;
        } else if ($page >= 20 && $page < 50) {
            $money = $page * 0.18;
        } else if ($page >= 50) {
            $money = $page * 0.15;
        }
        //存入订单
        $business_model->_userid = $user_infoss->userid;
        $business_model->orderId = $orderNum; //订单号
        $business_model->paidMoney = $money; //支付总金额
        $business_model->placeOrdertime = date('Y-m-d H:i:s', time()); //下定单时间
        $business_model->_storeid = $user_infoss->_storeid;
        $business_model->isdelete = 0;
        $business_model->save();
        foreach ($printList as $l => $y) {
            $filePage = explode(',', $filePages[$l]);
            foreach ($filePage as $z => $x) {
                if (is_numeric($x)) {
//                    $money_one+= (int) $y * 0.2;
                    $page_one +=(int) $y;
                } else {
                    $filePag = explode('-', $x);
                    $filePa = $filePag[1] - $filePag[0] + 1;
//                    $money_one+= (int) $y * (int) $filePa * 0.2;
                    $page_one+=(int) $y * (int) $filePa;
                }
            }

            if ($page > 0 && $page < 20) {
                $money_one = $page_one * 0.2;
            } else if ($page >= 20 && $page < 50) {
                $money_one = $page_one * 0.18;
            } else if ($page >= 50) {
                $money_one = $page_one * 0.15;
            }

            $subbusiness_model = new subbusiness();
            //存入子订单，存储订单详细信息
            $subbusiness_model->_businessId = $business_model->businessid;
            $subbusiness_model->_attachmentId = $attachmentid[$l];
            $subbusiness_model->paidMoney = $money_one;
            $subbusiness_model->printNumbers = $y;
            $subbusiness_model->printSet = $filePages[$l]; //打印页码
            $subbusiness_model->isdelete = 0;
            $subbusiness_model->isrefund = 0;

            $money_one = 0.00;
            $page_one = 0;

            $subbusiness_model->save();
        }


        $this->redirect(array('myFile/myFileDetail', 'businessid' => base64_encode($business_model->businessid)));
    }

    //检验打印设置  最新版，暂时没用！
    function chkPrintSet($filenum, $printset) {
        $exp = '/^[1-9]([0-9])*([,\-][1-9]([0-9])*)*$/';
        $exp2 = '/\-\d+\-/';
        $num = 0;
        if (preg_match($exp, $printset)) {
            if (!preg_match($exp2, $printset)) {
                $arr = split(',', $printset);
                foreach ($arr as $val) {
                    if (preg_match('/^\d+$/', $val) && intval($val) > 0 && intval($val) <= $filenum) {
                        $num = $num + 1;
                    } else if (preg_match('/^(\d+)\-(\d+)$/', $val, $matches)) {
                        $n1 = intval($matches[1]);
                        $n2 = intval($matches[2]);
                        if ($n1 <= $filenum && $n2 <= $filenum) {
                            $num = $num + abs($n2 - $n1) + 1;
                        } else {
                            return 0;
                        }
                    } else {
                        return 0;
                    }
                }
                return $num;
            }
        }
        return 0;
    }

    //判断传上来的数据格式是否正确 暂时未用
    private function actioncheck($number) {
        $exp = '/^[1-9]([0-9])*([,\-][1-9]([0-9])*)*$/';
        $exp2 = '/\-\d+\-/';

        $num = 0;
        if (preg_match($exp, $number)) {
            if (!preg_match($exp2, $number)) {
                $arr = split(',', $number);
                foreach ($arr as $val) {
                    if (preg_match('/^\d+$/', $val) && intval($val) > 0) {
                        $num = $num + 1;
                    } else if (preg_match('/^(\d+)\-(\d+)$/', $val, $matches)) {
                        $n1 = intval($matches[1]);
                        $n2 = intval($matches[2]);
                        if ($n2 > $n1) {
                            $num = $num + $n2 - $n1 + 1;
                        } else {
                            return 0;
                        }
                    } else {
                        return 0;
                    }
                }
                return $num;
            }
        }
        return 0;
    }

    //删除订单
    public function actiondeleteOrder() {
        $business_model = business::model();

        $orderId = $_POST['orderId']; //订单号
        $business_info = $business_model->find(array('condition' => "orderId = '$orderId'"));

        $business_info->isdelete = 1;

        if ($business_info->save()) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    //修改文件名
    public function actioneditFilename() {
        $attachmentid = $_POST['attachmentid']; //文件ID
        $attachmentname = $_POST['attachmentname']; //文件名

        $attachment_model = attachment::model();
        $attachment_info = $attachment_model->find(array('condition' => "attachmentid = $attachmentid"));
        $attachment_info->attachmentname = $attachmentname;
        if ($attachment_info->save()) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

//删除文件
    public function actiondeleteFile() {
        $attachmentid = $_POST['attachmentid']; //文件ID
        $attachment_model = attachment::model();
        $attachment_info = $attachment_model->find(array('condition' => "attachmentid = $attachmentid"));
        $attachment_info->isdelete = 0;

        if ($attachment_info->save()) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    //线下支付处理
    public function actionxxzfProcess() {
        $user_model = user::model();

        $business_model = business::model();
        $subbusiness_model = subbusiness::model();

        $usernamess = Yii::app()->session['username'];
        $user_infoss = $user_model->find(array('condition' => "username = '$usernamess'"));

        $orderId = $_POST['orderId']; //订单号
        $business_info = $business_model->find(array('condition' => "orderId = $orderId"));

        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessid = $business_info->businessid"));

        foreach ($subbusiness_info as $subbus) {
            $subbus->payType = 0;
            $r = date_default_timezone_set('PRC');

            $subbus->payTime = date('Y-m-d H:i:s');

            $subbus->save();
        }

        if ($business_info->verificationCode != null) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $verificationCode = mt_rand(100000, 999999); //验证码
            $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一

            while ($verificationCodes) {
                $verificationCode = mt_rand(100000, 999999);
                $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一
            }

            $user = 'cqutprint'; //短信接口用户名 $user
            $pwd = '112233'; //短信接口密码 $pwd
            $mobiles = $user_infoss->phone; //说明：取用户输入的手号
            $contents = "亲爱的用户，您此次打印的验证码为" . $verificationCode . ",您选择的支付方式是线下支付，请您于终端机支付打印,谢谢！【颇闰科技】"; //说明：取用户输入的短信内容
            $chid = 0; //通道ID
//
//        date_default_timezone_set('PRC');
//        $sendtime = date('Y-m-d H:i:s'); //发送时间
            $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";

            $business_info->verificationCode = $verificationCode;
            file_get_contents($sendMessage);
            if ($business_info->save()) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        }
    }

    public function actionPaycomplete() {
        $this->renderPartial('Paycomplete');
    }

    public function actionsubStore_xszfz() {

        $businessid = Yii::app()->session['businessid'];
        if (isset($businessid)) {


            $business_model = business::model();
            $business_info = $business_model->find(array("condition" => "businessid=$businessid AND isdelete = 0"));

            $WIDout_trade_no = $business_info->orderId;
            $WIDsubject = "文件打印->" . $business_info->orderId;
            $WIDtotal_fee = $business_info->paidMoney - Yii::app()->session['subintegration'] / 100;
            $WIDbody = "文件打印->" . $business_info->orderId;
            $WIDshow_url = "http://www.cqutprint.com/index.php?r=myFile/myFileDetail&businessid=" . base64_encode($business_info->businessid);

            $business_info->paymoney = $business_info->paidMoney - Yii::app()->session['subintegration'] / 100;
            $business_info->save();

            require_once("lib/alipay.config.php");
            require_once("lib/alipay_submit.class.php");

            /*             * ************************请求参数************************* */

            //支付类型
            $payment_type = "1";
            //必填，不能修改
            //服务器异步通知页面路径
            $notify_url = "http://www.cqutprint.com/alipay/notify_url.php";
//            $notify_url = "http://www.cqutprint.com/index.php?r=myFile/notify_url";
//        $notify_url = "http://183.230.116.223/printsystem/printsystem/alipay/notify_url.php";
            //需http://格式的完整路径，不能加?id=123这类自定义参数
            //页面跳转同步通知页面路径
            $return_url = "http://www.cqutprint.com/alipay/return_url.php";
//            $return_url = "http://www.cqutprint.com/index.php?r=myFile/Payment";
            //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/
            //商户订单号
            $out_trade_no = $WIDout_trade_no;
            //商户网站订单系统中唯一订单号，必填
            //订单名称
            $subject = $WIDsubject;
            //必填
            //付款金额
            $total_fee = $WIDtotal_fee;
            //必填
            //订单描述

            $body = $WIDbody;
            //商品展示地址
            $show_url = $WIDshow_url;
            //需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html
            //防钓鱼时间戳
            $anti_phishing_key = "";
            //若要使用请调用类文件submit中的query_timestamp函数
            //客户端的IP地址
            $exter_invoke_ip = "";
            //非局域网的外网IP地址，如：221.0.0.1


            /*             * ********************************************************* */

//构造要请求的参数数组，无需改动
            $parameter = array(
                "service" => "create_direct_pay_by_user",
                "partner" => trim($alipay_config['partner']),
                "seller_email" => trim($alipay_config['seller_email']),
                "payment_type" => $payment_type,
                "notify_url" => $notify_url,
                "return_url" => $return_url,
                "out_trade_no" => $out_trade_no,
                "subject" => $subject,
                "total_fee" => $total_fee,
                "body" => $body,
                "show_url" => $show_url,
                "anti_phishing_key" => $anti_phishing_key,
                "exter_invoke_ip" => $exter_invoke_ip,
                "_input_charset" => trim(strtolower($alipay_config['input_charset']))
            );

//建立请求
            $alipaySubmit = new AlipaySubmit($alipay_config);
            $html_text = $alipaySubmit->buildRequestForm($parameter, "get", "页面跳转中>>>");
//        echo $html_text;
//
//
//        var_dump($html_text);
            unset(Yii::app()->session['businessid']);

            $this->renderPartial('alipay', array("html_text" => $html_text));
//        $this->redirect(array('alipay/alipay', 'out_trade_no' => $WIDout_trade_no, 'subject' => $WIDsubject, 'total_fee' => $WIDtotal_fee, 'body' => $WIDbody, 'show_url' => $WIDshow_url));
//        header("location: ./index.php?r = alipay/alipay&out_trade_no = $WIDout_trade_no&subject = $WIDsubject&total_fee = $WIDtotal_fee&body = $WIDbody&show_url = $WIDshow_url");
        } else {
            echo"请返回";
        }
    }

//线上支付
    public function actionsubStore_xszf() {
        $integration = $_POST['integration']; //需要减去的积分
//        $addintegration = $_POST['addintegration']; //增加的积分
        $businessid = $_POST['businessid']; //businessid

        Yii::app()->session['subintegration'] = $integration;
//        Yii::app()->session['addintegration'] = $addintegration;
        Yii::app()->session['businessid'] = $businessid;

        $json = '{"data":"success"}';
        echo $json;
    }

    //积分即可支付完

    public function actionsubStore() {
        $integration = $_POST['integration']; //需要减去的积分

        $user_model = user::model();
        $business_model = business::model();
        $subbusiness_model = subbusiness::model();
        $record_model = record::model();
        $integralDetails_model = new integralDetails();


        $usernamess = Yii::app()->session['username'];
        $user_info = $user_model->find(array('condition' => "username = '$usernamess'"));

        $orderId = $_POST['orderId']; //订单号
        $business_info = $business_model->find(array('condition' => "orderId = $orderId"));

        $record_info = $record_model->find(array('condition' => "userid = '$user_info->userid'"));

        $record_info->points -= $integration;
//        $user_info->integration -= $integration;
        $integralDetails_model->_userid = $user_info->userid;
        $integralDetails_model->addIntegral = 0;
        $integralDetails_model->reduceIntegral = $integration;
        date_default_timezone_set('PRC');

        $integralDetails_model->happentime = date('Y-m-d H:i:s');
        $integralDetails_model->happenInfo = "支付订单号" . $orderId . "扣积分";

        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessid = $business_info->businessid"));

        foreach ($subbusiness_info as $subbus) {
            $subbus->isPay = 1;

            $subbus->payType = 5;
            date_default_timezone_set('PRC');

            $subbus->payTime = date('Y-m-d H:i:s');

            $subbus->save();
        }

        if (!isset($business_info->verificationCode)) {//为空
            $verificationCode = mt_rand(100000, 999999); //验证码
            $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'"));

            while ($verificationCodes) {
                $verificationCode = mt_rand(100000, 999999);
                $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断验证码是否唯一
            }
            $user = 'cqutprint'; //短信接口用户名 $user
            $pwd = '112233'; //短信接口密码 $pwd
            $mobiles = $user_info->phone; //说明：取用户输入的手号
            $contents = "亲爱的用户，您此次打印的验证码为" . $verificationCode . ",您已支付成功，请您于终端机打印,谢谢！【颇闰科技】"; //说明：取用户输入的短信内容
            $chid = 0; //通道ID
//
//        date_default_timezone_set('PRC');
//        $sendtime = date('Y-m-d H:i:s'); //发送时间
            $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";

            $business_info->verificationCode = $verificationCode;
            $business_info->consumptionIntegral = $integration;
            file_get_contents($sendMessage);
        } else
            $verificationCode = $business_info->verificationCode;

        if ($user_info->save() && $business_info->save() && $record_info->save() && $integralDetails_model->save()) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

//下载文件
    public function actiondownload($attachmentid) {
        $attachment_model = attachment::model();
        $attachmentids = base64_decode($attachmentid);
        $attachment_info = $attachment_model->find(array('condition' => "attachmentid = '$attachmentids'"));
        $name = $attachment_info->attachmentfile;
        $truename = $attachment_info->attachmentname;
        $filename = "http://" . $attachment_info->ip . '/assets/userfile/' . $name;
        header('Content-Type:application/octet-stream'); //文件的类型
        Header("Accept-Ranges: bytes");
        header('Content-Disposition:attachment;filename = "' . $truename . '"'); //下载显示的名字
        ob_clean();
        flush();
        readfile($filename);
        exit();
    }

    public function actionuploadp() {

        $key = $_POST['key'];
        $key2 = $_POST['key2'];

        $targetFolder = './assets/userfile'; // Relative to the root
        $attachment_model = new attachment();
        $user_model = user::model();
        $username = Yii::app()->session['username'];
        $user_info = $user_model->find(array('condition' => "username= '$username'"));
        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            //$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
            date_default_timezone_set('PRC');

            $fileName = $user_info->userid . '-' . date('YmdHis') . mt_rand(100000, 999999);
            $attachment_after = substr(strrchr($_FILES['file']['name'], '.'), 1);  //文件的后缀名 docx
            $road = dirname(Yii::app()->basePath) . "\assets\userfile\\" . $fileName . "." . $attachment_after;
            $road = str_replace("\\", "/", $road);
            $targetPath = $targetFolder;
            $targetFile = rtrim($targetPath, '/') . '/' . $fileName . "." . $attachment_after;
            // Validate the file type
            $fileTypes = array('doc', 'docx', 'pdf'); // File extensions
            $fileParts = pathinfo($_FILES['file']['name']);
            if (in_array($fileParts['extension'], $fileTypes)) {
                move_uploaded_file($tempFile, $targetFile);
                $filenumber = $this->getPages(str_replace('\\', '/', $road) . "\n");
                if ($filenumber > 0) {
                    $attachment_model->attachmentname = $_FILES['file']['name']; //原名
                    $attachment_model->attachmentfile = $fileName . "." . $attachment_after; //新名
                    $attachment_model->_userid = $user_info->userid;
                    $attachment_model->filetype = $attachment_after;
                    date_default_timezone_set('PRC');
                    $attachment_model->uploadtime = date('Y-m-d H:i:s');
                    $attachment_model->filenumber = (int) $filenumber;
                    $attachment_model->isdelete = 1;
                    $attachment_model->_storeid = $user_info->_storeid;
                    $attachment_model->ip = gethostbyname($_SERVER['SERVER_NAME']);
                    $attachment_model->save();

//                    echo $key;
//                    echo $key2;
                }
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    function getPages($filePath) {
        $pages = 0;
        $host = "127.0.0.1";
        $port = 8090;
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array("sec" => 10, "usec" => 0));  // 发送超时2秒
        socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, array("sec" => 10, "usec" => 0));  // 接收超时4秒
        socket_connect($socket, $host, $port);    //  连接  
        $num = socket_write($socket, $filePath); // 数据传送 向服务器发送消息
        if ($num > 0) {
            $pages = socket_read($socket, 1024, PHP_NORMAL_READ);
        }
        socket_close($socket);
        return $pages;
    }

    public function actiondoalipay() {
        $this->renderPartial('doalipay');
    }

    public function actionFilePreview($filename) {
        $this->renderPartial('FilePreview', array("filename" => $filename));
    }

    //退款
    public function actionrefund() {
        $subbusinessIdd = $_POST['subbusinessIdd'];

        $subbusiness_model = subbusiness::model();
        $business_model = business::model();

        $subbusinessId = explode(',', $subbusinessIdd); //ID 1,2,3,4
        $status = 0;
//        for ($i = 0; $i < count($subbusinessId); $i++) {
        foreach ($subbusinessId as $l => $y) {
            $subbusiness_info = $subbusiness_model->find(array("condition" => "subbusinessId= $y"));
            $business_info = $business_model->find(array("condition" => "businessid= '$subbusiness_info->_businessId'"));
            $record_model = record::model();
            $refund_model = new refund();
            $refund_info = $refund_model->find(array("condition" => "subbusinessId= '$subbusiness_info->subbusinessId'"));

            if (count($refund_info) == 0) {
                //积分支付，退积分
                if ($subbusiness_info->payType == 5 && $subbusiness_info->isPay == 1 && $subbusiness_info->status != 1 && $subbusiness_info->isrefund == 0) {
                    $record_info = $record_model->find(array("condition" => "userid= $business_info->_userid"));
                    $record_info->points += $subbusiness_info->paidMoney * 100;

                    $integralDetails_model = new integralDetails();
                    $integralDetails_model->_userid = $business_info->_userid;
                    $integralDetails_model->addIntegral = $subbusiness_info->paidMoney * 100;
                    $integralDetails_model->reduceIntegral = 0;
                    date_default_timezone_set('PRC');
                    $integralDetails_model->happentime = date('Y-m-d H:i:s');
                    $integralDetails_model->happenInfo = "订单号" . $business_info->orderId . "退款";

                    $subbusiness_info->isrefund = 1;

                    $refund_model->subbusinessId = $subbusiness_info->subbusinessId;
                    $refund_model->_userId = $business_info->_userid;
                    $refund_model->_orderId = $business_info->orderId;
                    $refund_model->tborderId = $subbusiness_info->trade_no;
//                    $refund_model->money = $subbusiness_info->paidMoney;
                    $refund_model->money = 0;
                    $refund_model->payType = $subbusiness_info->payType;
                    $refund_model->consumptionIntegral = $subbusiness_info->paidMoney * 100;
                    $refund_model->sellerAccounter = $subbusiness_info->sellerAccounter;
                    $refund_model->statue = 1;
                    date_default_timezone_set('PRC');
                    $refund_model->applyTime = date('Y-m-d H:i:s');
                    $refund_model->refundTime = date('Y-m-d H:i:s');
                    $refund_model->refundType = 5;

                    $refund_model->save();
                    $record_info->save();
                    $integralDetails_model->save();
                    $subbusiness_info->save();
                    $status = 1;
                }
                //积分+支付宝支付  首先判断下本订单号是否存在并且判断下金额   看第二个需要退多少？
                else {

                    $refund_infoq = $refund_model->findAll(array("condition" => "_orderId= '$business_info->orderId'"));

                    if (count($refund_infoq) != 0) {
                        $paidMoney = 0;
                        $consumptionIntegral = 0;
                        foreach ($refund_infoq as $q => $w) {
                            $paidMoney += $w->money;
                            $consumptionIntegral +=$w->consumptionIntegral;
                        }
                        if ($subbusiness_info->paidMoney <= $business_info->paidMoney - $business_info->consumptionIntegral / 100 - $paidMoney) {
                            $refund_model->subbusinessId = $subbusiness_info->subbusinessId;
                            $refund_model->_userId = $business_info->_userid;
                            $refund_model->_orderId = $business_info->orderId;
                            $refund_model->tborderId = $subbusiness_info->trade_no;
                            $refund_model->money = $subbusiness_info->paidMoney;
                            $refund_model->payType = $subbusiness_info->payType;
                            $refund_model->consumptionIntegral = 0;
                            $refund_model->sellerAccounter = $subbusiness_info->sellerAccounter;
                            date_default_timezone_set('PRC');
                            $refund_model->applyTime = date('Y-m-d H:i:s');
                            $refund_model->refundType = 1;

                            $subbusiness_info->isrefund = 2;
                            $subbusiness_info->save();
                        } else if ($subbusiness_info->paidMoney > $business_info->paidMoney - $business_info->consumptionIntegral / 100 - $paidMoney) {
                            $Integral = $business_info->paidMoney - $business_info->consumptionIntegral / 100 - $paidMoney;
                            $Integrals = intval(round(floatval($Integral)));
                            //如果支付宝支付金额刚好支付完  则积分退款
                            if ($Integrals == 0) {
                                $record_info = $record_model->find(array("condition" => "userid= $business_info->_userid"));
                                $record_info->points = $record_info->points + $subbusiness_info->paidMoney * 100;

                                $integralDetails_model = new integralDetails();
                                $integralDetails_model->_userid = $business_info->_userid;
                                $integralDetails_model->addIntegral = $subbusiness_info->paidMoney * 100;
                                $integralDetails_model->reduceIntegral = 0;
                                date_default_timezone_set('PRC');
                                $integralDetails_model->happentime = date('Y-m-d H:i:s');
                                $integralDetails_model->happenInfo = "订单号" . $business_info->orderId . "退款";

                                $subbusiness_info->isrefund = 1;

                                $refund_model->subbusinessId = $subbusiness_info->subbusinessId;
                                $refund_model->_userId = $business_info->_userid;
                                $refund_model->_orderId = $business_info->orderId;
                                $refund_model->tborderId = $subbusiness_info->trade_no;
                                $refund_model->money = 0;
                                $refund_model->payType = $subbusiness_info->payType;
                                $refund_model->consumptionIntegral = $subbusiness_info->paidMoney * 100;
                                $refund_model->sellerAccounter = $subbusiness_info->sellerAccounter;
                                $refund_model->statue = 1;
                                date_default_timezone_set('PRC');
                                $refund_model->applyTime = date('Y-m-d H:i:s');
                                $refund_model->refundTime = date('Y-m-d H:i:s');
                                $refund_model->refundType = 5;

                                $refund_model->save();
                                $record_info->save();
                                $integralDetails_model->save();
                                $subbusiness_info->save();
                            }//若支付宝未退完，则积分加支付宝同时退
                            else if ($Integrals > 0) {
                                $refund_model->subbusinessId = $subbusiness_info->subbusinessId;
                                $refund_model->_userId = $business_info->_userid;
                                $refund_model->_orderId = $business_info->orderId;
                                $refund_model->tborderId = $subbusiness_info->trade_no;
                                $refund_model->money = $business_info->paidMoney - $business_info->consumptionIntegral / 100 - $paidMoney;
                                $refund_model->payType = $subbusiness_info->payType;
                                $Integral = ($subbusiness_info->paidMoney - $business_info->paidMoney + $business_info->consumptionIntegral / 100 + $paidMoney) * 100;
                                $Integrals = intval(round(floatval($Integral)));
                                $refund_model->consumptionIntegral = $Integrals;
                                $refund_model->sellerAccounter = $subbusiness_info->sellerAccounter;
                                date_default_timezone_set('PRC');
                                $refund_model->applyTime = date('Y-m-d H:i:s');
                                $refund_model->refundType = 6;

                                $subbusiness_info->isrefund = 2;
                                $subbusiness_info->save();
                            }
                        }
                    } else {
                        if ($subbusiness_info->paidMoney <= $business_info->paidMoney - $business_info->consumptionIntegral / 100) {
                            $refund_model->subbusinessId = $subbusiness_info->subbusinessId;
                            $refund_model->_userId = $business_info->_userid;
                            $refund_model->_orderId = $business_info->orderId;
                            $refund_model->tborderId = $subbusiness_info->trade_no;
                            $refund_model->money = $subbusiness_info->paidMoney;
                            $refund_model->payType = $subbusiness_info->payType;
                            $refund_model->consumptionIntegral = 0;
                            $refund_model->sellerAccounter = $subbusiness_info->sellerAccounter;
                            date_default_timezone_set('PRC');
                            $refund_model->applyTime = date('Y-m-d H:i:s');
                            $refund_model->refundType = 1;

                            $subbusiness_info->isrefund = 2;
                            $subbusiness_info->save();
                        } else if ($subbusiness_info->paidMoney > $business_info->paidMoney - $business_info->consumptionIntegral / 100) {
                            $refund_model->subbusinessId = $subbusiness_info->subbusinessId;
                            $refund_model->_userId = $business_info->_userid;
                            $refund_model->_orderId = $business_info->orderId;
                            $refund_model->tborderId = $subbusiness_info->trade_no;
                            $refund_model->money = $business_info->paidMoney - $business_info->consumptionIntegral / 100;
                            $refund_model->payType = $subbusiness_info->payType;
                            $Integral = ($subbusiness_info->paidMoney - $business_info->paidMoney + $business_info->consumptionIntegral / 100) * 100;
                            $Integrals = intval(round(floatval($Integral)));
                            $refund_model->consumptionIntegral = $Integrals;
                            $refund_model->sellerAccounter = $subbusiness_info->sellerAccounter;
                            date_default_timezone_set('PRC');
                            $refund_model->applyTime = date('Y-m-d H:i:s');
                            $refund_model->refundType = 6;

                            $subbusiness_info->isrefund = 2;
                            $subbusiness_info->save();
                        }
                    }
                    //退款方式 1 支付宝 3 现金投币支付  5 积分 6积分+支付宝  7转账
                    //支付方式  0线下支付 1线上支付宝支付 2一卡通支付 3现金投币支付 4终端扫码支付 5积分支付 6积分+支付宝支付
//                    $subbusiness_infos = $subbusiness_model->findAll(array("condition" => "_businessId=$business_info->businessid AND payType = 1"));
//                    $subbusiness_infox = $subbusiness_model->findAll(array("condition" => "_businessId=$business_info->businessid AND payType = 6"));
//
//                    if (count($subbusiness_infos) == 1 || $subbusiness_info->payType == 4) {
//                        $refund_model->refundType = 1;
//                    } else if (count($subbusiness_infos) > 1) {
//                        $refund_model->refundType = 7;
//                    } else if (count($subbusiness_infox) == 1) {
//                        $refund_model->refundType = 6;
//                    } else if (count($subbusiness_infox) > 1) {
//                        $i = $business_info->paidMoney - $business_info->consumptionIntegral / 100;
//                        if ($subbusiness_info->paidMoney <= $business_info->paidMoney - $business_info->consumptionIntegral / 100) {
//                            $refund_model->refundType = 7;
//                        } else if ($subbusiness_info->paidMoney > $business_info->paidMoney - $business_info->consumptionIntegral / 100) {
//                            $refund_model->refundType = 6;
//                        }
//                    }
                    if ($refund_model->save()) {
                        $status = 1;
                    }
                }
            }
        }
        if ($status == 0) {
            $json = '{"data":"false"}';
            echo $json;
        } else if ($status == 1) {
            $json = '{"data":"success"}';
            echo $json;
        }
    }

}
