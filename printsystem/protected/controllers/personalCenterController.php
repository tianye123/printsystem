<?php

class personalCenterController extends Controller {

    public function filterIsSessionWrong($filterChain) {
        if (isset(Yii::app()->session['username']) && (!empty(Yii::app()->session['username']))) {
            $filterChain->run();
        } else {
            $this->redirect(array('login/login'));
        }
    }

    public function filters() {
        return array('IsSessionWrong');
    }

    public function actionpersonalCenter() {
        $username = Yii::app()->session['username'];

        $user_model = user::model();
        $record_model = record::model();
        $user_info = $user_model->find(array('condition' => "username = '$username'"));
        $record_info = $record_model->find(array('condition' => "userid='$user_info->userid'"));

        $signtime = $record_info->signtime;
        $integration = (int)$record_info->points;

        //获取今天凌晨的时间戳
        $day = strtotime(date('Y-m-d', time()));
        //获取昨天凌晨的时间戳
        $pday = strtotime(date('Y-m-d', strtotime('-1 day')));
        //获取明天凌晨的时间戳
        $tday = strtotime(date('Y-m-d', strtotime('+1 day')));

        if (count($record_info) != 0) {
            if ($signtime < $tday && $signtime > $day) {//今天
                $status = "yes";
            } else {
                $status = "no";
            }
        } else {
            $status = "no";
        }
        $this->renderPartial('personalCenter', array('username' => $username, 'user_info' => $user_info, "status" => $status, "integration" => $integration));
    }
    public function actionchangePersonalInfo() {
        $user_model = user::model();

        $Username = $_POST['Username']; //用户名
        $Password = md5($_POST['Password']); //密码
        $Phone = $_POST['Phone']; //手机
        $Email = $_POST['Email']; //邮箱



        $user_infos = $user_model->find(array('condition' => "username= '$Username'")); //判断用户是否存在？

        if ($user_infos) {
            $user_infos->userpsw = $Password;
            $user_infos->phone = $Phone;
            $user_infos->email = $Email;

            if ($user_infos->save()) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    public function actionsignInfo() {
        $username = Yii::app()->session['username'];

        $user_model = user::model();
        $record_model = new record();
        $integralDetails_model = new integralDetails();
        $user_info = $user_model->find(array('condition' => "username = '$username'"));

        $record_info = $record_model->find(array('condition' => "userid='$user_info->userid'"));

//        $record_info = $record_model->findAll(array('condition' => "userid='$user_info->userid'", 'limit' => 1, 'order' => "signtime DESC"));
        //获取今天凌晨的时间戳
        $day = strtotime(date('Y-m-d', time()));
        //获取昨天凌晨的时间戳
        $pday = strtotime(date('Y-m-d', strtotime('-1 day')));
        //获取明天凌晨的时间戳
        $tday = strtotime(date('Y-m-d', strtotime('+1 day')));

        if (count($record_info) != 0) {

            $record_info->toldays = $record_info->toldays + 1;
            if ($record_info->signtime < $tday && $record_info->signtime > $day) {//今天
                $json = '{"data":"' . (float) $record_info->seridays . '"}';
                echo $json;
            } else if ($record_info->signtime < $day && $record_info->signtime > $pday) {//是昨天
                $record_info->seridays = $record_info->seridays + 1;
                if ($record_info->seridays == 1) {
                    $record_info->points = $record_info->points + 5;
                    $integralDetails_model->addIntegral = 5;
                } else if ($record_info->seridays == 2) {
                    $record_info->points = $record_info->points + 10;
                    $integralDetails_model->addIntegral = 10;
                } else if ($record_info->seridays == 3) {
                    $record_info->points = $record_info->points + 15;
                    $integralDetails_model->addIntegral = 15;
                } else if ($record_info->seridays == 4) {
                    $record_info->points = $record_info->points + 20;
                    $integralDetails_model->addIntegral = 20;
                } else if ($record_info->seridays == 5) {
                    $record_info->points = $record_info->points + 25;
                    $integralDetails_model->addIntegral = 25;
                } else if ($record_info->seridays >= 6) {
                    $record_info->points = $record_info->points + 30;
                    $integralDetails_model->addIntegral = 30;
                }
            } else if ($record_info->signtime < $pday) {//昨天以前  不是连续签到
                $record_info->points = $record_info->points + 5;
                $integralDetails_model->addIntegral = 5;
                $record_info->seridays = 1;
            } else if ($record_info->signtime < $tday && $record_info->signtime > $day) {//今天
                $record_info->seridays = $record_info->seridays;
            }

            date_default_timezone_set('PRC');
            $integralDetails_model->_userid = $user_info->userid;
            $integralDetails_model->reduceIntegral = 0;
            $integralDetails_model->happentime = date('Y-m-d H:i:s');
            $integralDetails_model->happenInfo = "签到送积分";

            $record_info->signtime = time();
        }
        if ($record_info->save() && $integralDetails_model->save()) {
            $json = '{"data":"' . (float) $record_info->seridays . '"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    //更改个人信息查看手机号是否还存在
    public function actioncheckPhone() {
        $Phone = $_POST['Phone']; //手机

        $username = Yii::app()->session['username'];
        $user_model = user::model();
        $user_info = $user_model->find(array('condition' => "phone= '$Phone'")); //判断手机是否注册？

        if (isset($user_info)) {
            if ($user_info->username != $username) {
                $json = '{"data":"phone_exist"}';
                echo $json;
            } else if ($user_info->username == $username) {
                $json = '{"data":"phone_no"}';
                echo $json;
            }
        } else {
            $json = '{"data":"phone_yes"}';
            echo $json;
        }
    }

    //修改已绑定手机号码
    public function actionchangePhone() {

        $username = Yii::app()->session['username'];

        $this->renderPartial('changePhone', array('username' => $username));
    }

    public function actionchangePhones() {
        $Phone = $_POST['Phone']; //手机
        $username = Yii::app()->session['username'];
        $user_model = user::model();
        $user_info = $user_model->find(array('condition' => "phone= '$Phone'")); //判断手机是否存在？

        if (isset($user_info)) {
            if ($user_info->username != $username) {
                $json = '{"data":"phone_exist"}';
                echo $json;
            } else if ($user_info->username == $username) {
                $json = '{"data":"phone_no"}';
                echo $json;
            }
        } else {
            unset(Yii::app()->session['phoneChangecheckma']);
            $phoneChangecheckma = mt_rand(100000, 999999);
            Yii::app()->session['phoneChangecheckma'] = $phoneChangecheckma;
            $user = 'cqutprint'; //短信接口用户名 $user
            $pwd = '112233'; //短信接口密码 $pwd
            $mobiles = $Phone; //说明：取用户输入的手号
            $contents = "亲爱的用户，您此次更改绑定手机的验证码为" . $phoneChangecheckma . "，验证码很重要，打死都不要告诉任何人哦，谢谢！【颇闰科技】"; //说明：取用户输入的短信内容
            $chid = 0; //通道ID
            $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
            file_get_contents($sendMessage);
            $json = '{"data":"success"}';
            echo $json;
        }
    }

    public function actionchangePersonalPhone() {
        $Phone = $_POST['Phone']; //手机
        $phonecode = $_POST['phonecode']; //验证码
        $user_model = user::model();
        $username = Yii::app()->session['username'];



        if ($phonecode == Yii::app()->session['phoneChangecheckma']) {

            $user_infos = $user_model->find(array('condition' => "username= '$username'")); //判断用户是否存在？

            if (isset($user_infos)) {
                $user_infos->phone = $Phone;

                if ($user_infos->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        } else {
            $json = '{"data":"phonecode_false"}';
            echo $json;
        }
    }

}
