<?php

class userController extends Controller {

    public function filterIsSessionWrong($filterChain) {
        if (isset(Yii::app()->session['username']) && (!empty(Yii::app()->session['username']))) {
            $filterChain->run();
        } else {
            $this->redirect(array('login/login'));
        }
    }

    public function filters() {
        return array('IsSessionWrong');
    }

    public function actionLogin() {
        $user_model = user::model();
        if (isset($_POST['user'])) {
            $userName = $_POST['user']['username'];
            $userPsw = $_POST['user']['userpsw'];
            $userReturn = $user_model->find(array('condition' => "username=$userName AND userpsw=$userPsw",));
            if ($userReturn) {
                echo "登陆成功！";
            } else
                echo "serror";
        }

        $this->renderPartial('login', array('user_model' => $user_model));
    }

    public function actionRegister() {
        $this->renderPartial('register');
    }

}
