<?php

/*
 * 终端向平台获取信息 接口
 */

class printorController extends Controller {

    public function actionSearchOrder($verificationCode = null) {//终端拿订单
        $business_model = business::model();
        $business_info = $business_model->find(array("condition" => "verificationCode= $verificationCode AND isdelete = 0"));
        if ($business_info) {
            $businessid = $business_info->businessid;
            $orderId = $business_info->orderId;
            $subbusiness_model = subbusiness::model();
            $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId= $businessid AND isrefund = 0")); //"_businessId = $businessid"
            $attachment_model = attachment::model();
            $str = '';
            foreach ($subbusiness_info as $K => $V) {
                $attachmentId = $V->_attachmentId;
                $attachment_info = $attachment_model->findByPk($attachmentId);
                $filePath = "http://" . $attachment_info->ip . '/assets/userfile/' . $attachment_info->attachmentfile; //下载地址
                $str .= "{'attachmentId':'$attachmentId','attachmentName':'$attachment_info->attachmentname','fileNumber':'$attachment_info->filenumber','filePath':'$filePath','isPay':'$V->isPay','payType':'$V->payType','payTime':'$V->payTime','paidMoney':'$V->paidMoney','printNumbers':'$V->printNumbers','printSet':'$V->printSet','successNumbers':'$V->successNumbers','errorPage':'$V->errorPage','printTime':'$V->printTime','trade_no':'$V->trade_no','status':'$V->status'},";
            }
            $str = substr($str, 0, -1);
            $returnMessage = "{'resultCode':200,'resultDescription': '获取订单成功(包含所有文件)','orderId':'$orderId','file': ["
                    . $str
                    . "]}";
            $returnMessage = str_replace("'", '"', $returnMessage);
            echo $returnMessage;
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"订单不存在或已失效！"}';
            echo $returnMessage;
        }
    }

    public function actionOperateOrder() {//线下订单处理
//        $_POST = '{"orderId":"2015080210491612302","filestatus":[{"attachmentid":"4","marchineId":"ABC","isPay":"1","payType":"1","payTime":"20150303123040","paidMoney":"12.20","printNumbers":"5","printSet":"1-9", "successNumbers":"3", "errorPage":"1", "printTime":"20150905120930", "status":"1"},{"attachmentid":"5","marchineId":"DEF","isPay":"1","payType":"1","payTime":"20150303123040","paidMoney":"12.1","printNumbers":"5","printSet":"1-9", "successNumbers":"3", "errorPage":"1", "printTime":"20150905120930", "status":"1","trade_no":"201415210212"}]}';
        if (!empty($_POST)) {
            if (is_array($_POST['json'])) {//是数组
                $arrayCode = $_POST['json'];
            } else {
                $arrayCode = CJSON::decode($_POST['json']);
            }
            $orderId = $arrayCode['orderId'];
            $data = $arrayCode['filestatus'];

            $business_model = business::model();
            $business_info = $business_model->find("orderId = $orderId");
            $businessId = $business_info->businessid;
            $subbusiness_model = subbusiness::model();
            $flag = 1;
            foreach ($data as $K => $V) {
                $attachmentid = $V['attachmentid'];
                $subbusiness_info = $subbusiness_model->find("_attachmentId = $attachmentid AND _businessId =$businessId");
                $subbusiness_info->marchineId = $V['marchineId'];
                $subbusiness_info->isPay = $V['isPay'];
                $subbusiness_info->payType = $V['payType'];
                $subbusiness_info->paidMoney = $V['paidMoney'];
                $subbusiness_info->printNumbers = $V['printNumbers'];
                $subbusiness_info->successNumbers = $V['successNumbers'];
                $subbusiness_info->errorPage = $V['errorPage'];
                date_default_timezone_set('PRC');
                $subbusiness_info->printTime = date('Y-m-d H:i:s');
                $subbusiness_info->status = $V['status'];
                $subbusiness_info->trade_no = $V['trade_no'];
                if (!$subbusiness_info->save()) {
                    $flag = 0;
                }
            }
            if ($flag == 1) {
                $returnMessage = '{"resultCode":200,"resultDescription":"信息保存成功！"}';
                echo $returnMessage;
            } else {
                $returnMessage = '{"resultCode":400,"resultDescription":"信息保存失败！"}';
                echo $returnMessage;
            }
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"未获取到信息！"}';
            echo $returnMessage;
        }
    }

    public function actionPrintState() {//拿终端状态
//        $_POST['json'] = '{"machineId":"A","paper_remain":"111","yuan_remain":"123","jiao_remain":"33","jiao_changer":"1","yuan_changer":"1","coin_acceptor":"1","note_acceptor":"1","card_acceptor":"1","printer_status":"1","printer_errorcode":"1","current_version":"1.0.0.1"}';
        if (!empty($_POST)) {
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            try {
                $machineId = $arrayPrintor['machineId']; //终端标识
                $paper_remain = $arrayPrintor['paper_remain']; //剩余纸张
                $yuan_remain = $arrayPrintor['yuan_remain']; //找零器剩余多少一元的纸币
                $jiao_remain = $arrayPrintor['jiao_remain']; //找零器剩余多少一角的纸币
                $jiao_changer = $arrayPrintor['jiao_changer']; //找零器角状态
                $yuan_changer = $arrayPrintor['yuan_changer']; //找零器元状态
                $coin_acceptor = $arrayPrintor['coin_acceptor']; //硬币投币器状态
                $note_acceptor = $arrayPrintor['note_acceptor']; //纸币投币器状态
                $card_acceptor = $arrayPrintor['card_acceptor']; //刷卡器状态
                $printer_status = $arrayPrintor['printer_status']; //打印机状态
                $printer_errorcode = $arrayPrintor['printer_errorcode']; //打印机出错码
                $current_version = $arrayPrintor['current_version']; //终端版本
            } catch (Exception $e) {
                $return = '{"resultCode":300,"resultDescription":"未知错误"}';
                echo $return;
            }
            $printor_model = printor::model();
            $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));
            if ($printor_info) {
                $printor_info->machineId = $machineId;
                $printor_info->paper_remain = $paper_remain;
                $printor_info->yuan_remain = $yuan_remain;
                $printor_info->jiao_remain = $jiao_remain;
                $printor_info->jiao_changer = $jiao_changer;
                $printor_info->yuan_changer = $yuan_changer;
                $printor_info->coin_acceptor = $coin_acceptor;
                $printor_info->note_acceptor = $note_acceptor;
                $printor_info->card_acceptor = $card_acceptor;
                $printor_info->printer_status = $printer_status;
                $printor_info->printer_errorcode = $printer_errorcode;
                $printor_info->version = $current_version;
                if ($printor_info->save()) {
                    $return = '{"resultCode":200,"resultDescription":"状态传输成功"}';
                    echo $return;
                } else {
                    $return = '{"resultCode":400,"resultDescription":"状态保存失败"}';
                    echo $return;
                }
            } else {
                $return = '{"resultCode":400,"resultDescription":"没找到该终端"}';
                echo $return;
            }


            /*             * *************如果有问题则发送短信给管理员***************************** */
            if ($paper_remain < 100 || $yuan_remain < 50 || $jiao_remain < 50 || $jiao_changer != 4 || $yuan_changer != 4 || $coin_acceptor != 1 || $note_acceptor != 1 || $printer_status != 0) {
                $user = 'cqutprint'; //短信接口用户名 $user
                $pwd = '112233'; //短信接口密码 $pwd
                $mobiles = "13983168130"; //说明：取用户输入的手机号
                $chid = 0; //通道ID
                if ($paper_remain < 100) {
                    $contents = "尊敬的管理员，" . $printor_info->printorName . "的纸张已不足100张，请及时添加！【颇闰科技】";
                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
                    //file_get_contents($sendMessage);
                    $message = array(
                        'description' => $contents
                    );
                    $this->actionBiaduPush($message);
                }
//                if ($yuan_remain < 50) {
//                    $contents = "尊敬的管理员，" . $printor_info->printorName . "的剩余元数量已不足50，请及时添加！【颇闰科技】";
//                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
//                    //file_get_contents($sendMessage);
//                    $message = array(
//                        'description' => $contents
//                    );
//                    $this->actionBiaduPush($message);
//                }
//                if ($jiao_remain < 50) {
//                    $contents = "尊敬的管理员，" . $printor_info->printorName . "的剩余角数量已不足50，请及时添加！【颇闰科技】";
//                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
//                    //file_get_contents($sendMessage);
//                    $message = array(
//                        'description' => $contents
//                    );
//                    $this->actionBiaduPush($message);
//                }
//                if ($jiao_changer != 4) {
//                    $contents = "尊敬的管理员，" . $printor_info->printorName . "的找零器角出现故障，请及时排除！【颇闰科技】";
//                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
//                    //file_get_contents($sendMessage);
//                    $message = array(
//                        'description' => $contents
//                    );
//                    $this->actionBiaduPush($message);
//                }
//                if ($yuan_changer != 4) {
//                    $contents = "尊敬的管理员，" . $printor_info->printorName . "的找零器元出现故障，请及时排除！【颇闰科技】";
//                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
//                    //file_get_contents($sendMessage);
//                    $message = array(
//                        'description' => $contents
//                    );
//                    $this->actionBiaduPush($message);
//                }
//                if ($coin_acceptor != 1) {
//                    $contents = "尊敬的管理员，" . $printor_info->printorName . "的硬币投币器出现故障，请及时排除！【颇闰科技】";
//                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
//                    //file_get_contents($sendMessage);
//                    $message = array(
//                        'description' => $contents
//                    );
//                    $this->actionBiaduPush($message);
//                }
//                if ($note_acceptor != 1) {
//                    $contents = "尊敬的管理员，" . $printor_info->printorName . "的纸币投币器出现故障，请及时排除！【颇闰科技】";
//                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
//                    //file_get_contents($sendMessage);
//                    $message = array(
//                        'description' => $contents
//                    );
//                    $this->actionBiaduPush($message);
//                }
                if ($printer_status != 0) {
                    $contents = "尊敬的管理员，" . $printor_info->printorName . "的打印机出现故障，请及时排除！【颇闰科技】";
                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
                    //file_get_contents($sendMessage);

                    $message = array(
                        'description' => $contents
                    );
                }
            }
            /*             * ****************************************** */
        } else {
            $return = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $return;
        }
    }

    public function actionBiaduPush($message) {
        require_once 'BaiduPush/sdk.php';

        // 创建SDK对象.
        $sdk = new PushSDK();

        // 消息控制选项。
        $opts = array(
            'msg_type' => 0,
        );
        $optss = array(
            'msg_type' => 1,
        );
// 发送
        $rs = $sdk->pushMsgToAll($message, $opts);
        $rs = $sdk->pushMsgToAll($message, $optss);
    }

    public function actionPostBusinessInfo() {//终端直接打印业务信息
        //$_POST['json'] = '{"machineId":"A","sessionId":"111","udiskId":"123","filename":"33","totalpages":"1","copies":"1","printset":"1","paytype":"1","paidmoney":"1","needpay":"1","exchange":"1","starttime":"20150907153040","endtime":"20150907153040","exit_phrase":"6","translation_status":"","print_status":"1","trade_no":"123123"}';
        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            $sessionId = $arrayPrintor['sessionId'];
            $trade_no = $arrayPrintor['trade_no'];
            $exit_phrase = $arrayPrintor['exit_phrase'];
            $totalpages = $arrayPrintor['totalpages']; //需要打印的页数
            $printedpages = $arrayPrintor['printedpages']; // 实际打印的页数
            $machineId = $arrayPrintor['machineId']; // 获取machineId
            $printor_model = printor::model();
            $printor_info = $printor_model->find("machineId = '$machineId'");

            $prbusiness_model = prbusiness::model();
            $prbusiness_info = $prbusiness_model->find("sessionId = $sessionId AND trade_no = '$trade_no'");
            $refund_modei = refund::model();
            $refund_infi = $refund_modei->find("_sessionId = $sessionId AND tborderId = '$trade_no'");
//判断是否需要存入退款
            if ($arrayPrintor['exit_phrase'] == 5 && $printedpages < $totalpages) {
                if ((count($prbusiness_info) == 0 || (count($prbusiness_info) != 0 && $prbusiness_info->exit_phrase <= 5)) && count($refund_infi) == 0) {
                    include_once("WxPayPubHelper/WxPayPubHelper.php");
                    $wechatpay_model = wechatpay::model();
                    $wechatpay_info = $wechatpay_model->find("transaction_id = '$trade_no'");

                    $refund_model = new refund();
                    $refund_model->_sessionId = $arrayPrintor['sessionId'];
                    $refund_model->tborderId = $arrayPrintor['trade_no'];
                    $refund_model->money = $arrayPrintor['paidmoney'];
                    date_default_timezone_set('PRC');
                    $refund_model->applyTime = date('Y-m-d H:i:s');
                    if (count($printor_info) != 0)
                        $refund_model->_storeid = $printor_info->_storeId;

//终端状态值 1、现金、；2、刷卡；3、支付宝 4微信支付
//数据库状态值 0线下支付 1线上支付宝支付 2一卡通支付 3现金投币支付 4终端扫码支付 5积分支付 6积分+支付宝支付 7终端微信
//退款方式 1 支付宝 2一卡通  3 现金  5 积分 6积分+支付宝 7微信
                    if ($arrayPrintor['paytype'] == 1) {
                        $refund_model->refundType = 3;
                        $refund_model->payType = 3;
                    } else if ($arrayPrintor['paytype'] == 2) {
                        $refund_model->refundType = 2;
                        $refund_model->payType = 2;
                    } else if ($arrayPrintor['paytype'] == 3) {
                        $refund_model->refundType = 1;
                        $refund_model->payType = 4;
                    } else if ($arrayPrintor['paytype'] == 4) {
                        $refund_model->refundType = 7;
                        $refund_model->payType = 7;

                        //微信退款可以自动退款
                        $out_trade_no = $wechatpay_info->out_trade_no;
                        ;
                        $refund_fee = $arrayPrintor['paidmoney'] * 100;
//商户退款单号，商户自定义，此处仅作举例
                        $out_refund_no = "$out_trade_no";
//总金额需与订单号out_trade_no对应，demo中的所有订单的总金额为1分
                        $total_fee = $wechatpay_info->total_fee;

//使用退款接口
                        $refund = new Refund_pub();
//设置必填参数
//appid已填,商户无需重复填写
//mch_id已填,商户无需重复填写
//noncestr已填,商户无需重复填写
//sign已填,商户无需重复填写
                        $refund->setParameter("out_trade_no", "$out_trade_no"); //商户订单号
                        $refund->setParameter("out_refund_no", "$out_refund_no"); //商户退款单号
                        $refund->setParameter("total_fee", "$total_fee"); //总金额
                        $refund->setParameter("refund_fee", "$refund_fee"); //退款金额
                        $refund->setParameter("op_user_id", WxPayConf_pub::MCHID); //操作员
//非必填参数，商户可根据实际情况选填
//$refund->setParameter("sub_mch_id","XXXX");//子商户号 
//$refund->setParameter("device_info","XXXX");//设备号 
//$refund->setParameter("transaction_id","XXXX");//微信订单号
//调用结果
                        $refundResult = $refund->getResult();
//商户根据实际情况设置相应的处理流程,此处仅作举例
                        if ($refundResult["return_code"] == "FAIL") {
                            $refund_model->statue = 2;
                            echo "通信出错：" . $refundResult['return_msg'] . "<br>";
                        } else {

                            if ($refundResult['result_code'] == "SUCCESS") {
                                $refund_model->statue = 1;
                                date_default_timezone_set('PRC');
                                $refund_model->refundTime = date('Y-m-d H:i:s');
                            } else {
                                $refund_model->statue = 3;
                            }
                        }
                    }
                    $refund_model->save();
                }
                if (count($prbusiness_info) == 0) {
                    $prbusiness_model = new prbusiness();

                    foreach ($arrayPrintor as $K => $V) {
                        $prbusiness_model->$K = $V;
                    }
                    $prbusiness_model->save();
                } else if (count($prbusiness_info) != 0) {
                    if ($prbusiness_info->exit_phrase < $exit_phrase) {
                        $prbusiness_info->exit_phrase = $exit_phrase;
                        $prbusiness_info->save();
                    }
                }
            }//判断是否需要存入prbusiness表和refund是否需要删除以前保存错误的退款 
            else if ($arrayPrintor['exit_phrase'] > 5 && $arrayPrintor['exit_phrase'] <= 8) {
                if (count($refund_infi) != 0) {
                    $refund_infi->delete();
                    $refund_infi->save();
                }
                if (count($prbusiness_info) != 0) {
                    $prbusiness_info->exit_phrase = $exit_phrase;
                    $prbusiness_info->save();
                } else {
                    $prbusiness_model = new prbusiness();

                    foreach ($arrayPrintor as $K => $V) {
                        $prbusiness_model->$K = $V;
                    }
                    if (count($printor_info) != 0)
                        $prbusiness_model->_storeid = $printor_info->_storeId;
                    $prbusiness_model->save();
                }
            } else {
                $prbusiness_model = new prbusiness();

                foreach ($arrayPrintor as $K => $V) {
                    $prbusiness_model->$K = $V;
                }
                $prbusiness_model->save();
            }
            $return = '{"resultCode":200,"resultDescription":"success"}';
            echo $return;
        } else {
            $return = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $return;
        }
    }

    public function actionPostCardTradeInfo() {//终端直接打印业务信息 暂停使用
        if (!empty($_POST)) {
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            $cardtrade_model = new cardtrade();
            foreach ($arrayPrintor as $K => $V) {
                $cardtrade_model->$K = $V;
            }
            if ($cardtrade_model->save()) {
                $return = '{"resultCode":200,"resultDescription":"刷卡信息传输成功"}';
                echo $return;
            } else {
                $return = '{"resultCode":400,"resultDescription":"刷卡信息传输失败"}';
                echo $return;
            }
        } else {
            $return = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $return;
        }
    }

    public function actionVersion($type = null) {//终端版本控制
        $printVersion_model = new printVersion();
        if ($type == null) {
            $printVersion_info = $printVersion_model->find(array('order' => 'versionId DESC'));
            $versionFilePash = Yii::app()->request->hostInfo . '/printVersion/' . $printVersion_info->versionFile;
            $str = "{'versionCode':'$printVersion_info->versionCode','versionFilePash':'$versionFilePash','updateTime':'$printVersion_info->updateTime'}";
            $returnMessage = str_replace("'", '"', $str);
            echo $returnMessage;
        }
    }

    public function actionPayInfo($out_trade_no = null) {//返回支付结果给终端
        if ($out_trade_no == null) {
            echo '{"resultCode":"400","resultMessage":"没有传递数据"}';
        } else {
            $printpaytemp_model = printpaytemp::model();
            $printpaytemp_info = $printpaytemp_model->find("out_trade_no='$out_trade_no'");

            if ($printpaytemp_info) {
                $trade_status = $printpaytemp_info->trade_status;
                $returnMessage = "{'resultCode':'200','resultMessage':'成功','trade_status':'$trade_status'}";
                echo str_replace("'", '"', $returnMessage);
            } else {
                echo '{"resultCode":"400","resultMessage":"信息不存在"}';
            }
        }
    }

    public function actionChangeprintor($machineId) {//是否开关机
        $printor_model = printor::model();
        $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));
        if ($printor_info) {
            $printor_info->last_time = time();
            if ($printor_info->save()) {
                $return = '{"resultCode":200,"resultDescription":"状态传输成功"}';
                echo $return;
            } else {
                $return = '{"resultCode":400,"resultDescription":"状态保存失败"}';
                echo $return;
            }
        } else {
            $return = '{"resultCode":400,"resultDescription":"没找到该终端"}';
            echo $return;
        }
    }

//获取广告播放列表
    public function actiongetAdsFileList($marchineId = null) {
        $printor_model = printor::model();
        $advertisement_model = advertisement::model();
        $adsdelivery_model = adsdelivery::model();
        $machineIds = addslashes($marchineId);
        $printor_info = $printor_model->find("machineId = '$machineIds'");
        $str = "";
        if (count($printor_info) != 0) {
            $adsdelivery_info = $adsdelivery_model->findAll("_groupID = $printor_info->_groupID");
            foreach ($adsdelivery_info as $K => $L) {
                $advertisement_info = $advertisement_model->find("advertisementID = $L->_advertisementID");
                if (count($advertisement_info) != 0) {
                    $str .= "{'name':'$advertisement_info->advertisementName','downloadUrl':'http://123.56.94.175/assets/advertisements/$advertisement_info->saveName','times':'$L->times','md5':'$advertisement_info->FileMD5','length':'$advertisement_info->advertisementLength'},";
                }
            }
            $str = substr($str, 0, -1);
            $returnMessage = "{'resultCode':'200','description': 'success','files': ["
                    . $str
                    . "]}";
            $returnMessage = str_replace("'", '"', $returnMessage);
            echo $returnMessage;
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"终端机不存在！"}';
            $returnMessage = str_replace("'", '"', $returnMessage);
            echo $returnMessage;
        }
    }

    //终端机判断是否联网
    public function actionnetworking() {
        echo "success";
    }

    //终端升级程序升级结果状态回传服务器
    public function actionReportUpdateResult() {
//        $_POST['json'] = '{"machineId":"CQUT-PRINTD1","resultCode":"200","clientVersion":"1.0.5.2"}';

        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            $machineId = $arrayPrintor['machineId'];
            $resultCode = $arrayPrintor['resultCode']; //升级结果状态码
            $clientVersion = $arrayPrintor['clientVersion']; //终端程序版本号(不管有无升级成功，都回传)

            $printor_model = printor::model();

            $printor_info = $printor_model->find("machineId = '$machineId'");

            if (count($printor_info) != 0) {
                $printor_info->version = $clientVersion;
                $printor_info->updatetime = date('Y-m-d H:i:s'); //更新时间
                $printor_info->updatestate = $resultCode;
                // 200 升级成功   201 无需升级  400 升级失败 其他未知错误 401 获取服务器版本失败
                //402 下载主程序文件失败 403 下载的主程序文件MD5校验不通过 404 解压失败 405 覆盖原程序失败
                if ($printor_info->save()) {
                    $returnMessage = '{"resultCode":200,"resultDescription":"保存成功！"}';
                    echo $returnMessage;
                }
            }
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"未获取到信息！"}';
            echo $returnMessage;
        }
    }

    // 终端获取需要的版本信息
    public function actionGetUpdateInfoById($machineId = null) {
        $printor_model = printor::model();
        $printor_info = $printor_model->find("machineId = '$machineId'");
        $str = "";
        if (count($printor_info) != 0) {
            $group_model = group::model();
            $group_info = $group_model->find("groupID = $printor_info->_groupID");

            $printversion_model = printVersion::model();
            $printversion_info = $printversion_model->find("versionId = $group_info->_versionId");
            $versionFilePash = Yii::app()->request->hostInfo . '/printVersion/' . $printversion_info->versionFile;
            $str .= "{'version':'$printversion_info->versionCode','url':'$versionFilePash','md5':'$printversion_info->FileMD5'},";
            $str = substr($str, 0, -1);
            $returnMessage = str_replace("'", '"', $str);
            echo $returnMessage;
        } else {
            $returnMessage = '{"resultCode":401,"resultDescription":"未找到该终端！"}';
            echo $returnMessage;
        }
    }

    //终端查询颇闰服务器后台获取当前交易是否成功
    public function actionGetTradeResult() {
        //$_POST['json'] = '{"payMethod":3,"payMethodStr":"ALIPAY","outTradeNo":"CQUT-PRINTD114520512181"}';

        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }

            $payMethod = $arrayPrintor["payMethod"]; //支付方式(3:支付宝；4:微信)
            $payMethodStr = $arrayPrintor["payMethodStr"]; //支付方式描述（ALIPAY/WEIXIN）
            $outTradeNo = $arrayPrintor["outTradeNo"]; //本次交易终端生成的交易号
            if ($payMethod == 3) {
                $printpaytemp_model = printpaytemp::model();
                $printpaytemp_info = $printpaytemp_model->find("out_trade_no = '$outTradeNo'");
                if (count($printpaytemp_info) != 0) {
                    $returnMessage = '{"resultCode":200,"resultDescription":"' . $printpaytemp_info->trade_status . '","tradeNo":"' . $printpaytemp_info->trade_no . '"}';
                    echo $returnMessage;
                } else {
                    $returnMessage = '{"resultCode":400,"resultDescription":"NOT_FOUND"}';
                    echo $returnMessage;
                }
            } else if ($payMethod == 4) {
                $wechatpay_model = wechatpay::model();
                $wechatpay_info = $wechatpay_model->find("out_trade_no = '$outTradeNo'");
                if (count($wechatpay_info) != 0) {
                    $returnMessage = '{"resultCode":200,"resultDescription":"' . $wechatpay_info->result_code . '","tradeNo":"' . $printpaytemp_info->transaction_id . '"}';
                    echo $returnMessage;
                } else {
                    $returnMessage = '{"resultCode":400,"resultDescription":"NOT_FOUND"}';
                    echo $returnMessage;
                }
            }
        } else {
            $returnMessage = '{"resultCode":401,"resultDescription":"未获取到信息！"}';
            echo $returnMessage;
        }
    }
    //终端广告系统回传播放器状态接口
    //向服务器回传当前终端播放器的状态，当前回传频率，每20秒回传一次。
    public function actionreportPlayerStatus(){
        
    }
}
