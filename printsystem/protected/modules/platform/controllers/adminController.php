<?php

include 'baseController.php';
class adminController extends baseController {
    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function actionIndex() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            //覆盖校园
            $store_model = store::model();
            $store_all = count($store_model->findAll());
            //会员量
            $user_model = user::model();
            $user_all = count($user_model->findAll()); //会员总数
            //在外总积分
            $totalPoints = 0.00;
            $record_model = record::model();
            $record_info = $record_model->findAll();
            foreach ($record_info as $k => $l) {
                $totalPoints += $l->points;
            }
            //终端数量
            $printor_model = printor::model();
            $printor_info = $printor_model->findAll();
            //新增院校
            $store_new = $store_model->findAll(array(
                'order' => 'storeid DESC',
                'limit' => 4
            ));

            $administrator_model = administrator::model();
            $administrator_info = $administrator_model->findAll(array(
                'order' => 'administratorid DESC',
                'limit' => 4
            ));

            $this->renderPartial('index', array('leftContent' => $leftContent, 'recommend' => $recommend, 'store_all' => $store_all, 'user_all' => $user_all, 'totalPoints' => $totalPoints, 'printor_info' => $printor_info, 'store_new' => $store_new, 'administrator_info' => $administrator_info));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    //注销
    public function actionLogout() {
        unset(Yii::app()->session['adminuser']);
        if (!isset(Yii::app()->session['adminuser'])) {
            $this->redirect('./index.php?r=platform');
        }
    }

    //下载APP
    public function actiondownloadApp() {
        $filename = './appVersion/cqutprintAdmin.apk';
        header('Content-Type:application/octet-stream'); //文件的类型
        Header("Accept-Ranges: bytes");
        header('Content-Disposition:attachment;filename = "cqutprintAdmin.apk"'); //下载显示的名字
        ob_clean();
        flush();
        readfile($filename);
        exit();
    }

    public function actionModify() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $issetadmin = Yii::app()->session['adminuser']; //用户名
        if (isset($issetadmin)) {
            $administrator_model = administrator::model();
            if (isset($_POST['password'])) {

                $adminInfo = $administrator_model->find("username = '$issetadmin'");
                $adminInfo->password = md5($_POST['password']);

                if ($adminInfo->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $this->render('modifyInfo', array('administrator_model' => $administrator_model, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        }
    }

}
