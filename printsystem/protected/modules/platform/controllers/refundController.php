<?php

include 'baseController.php';

class refundController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }
    /*
      权限管理
     */

    public function filters() {
        return array(
            'refund + refund', //退款管理
            'deleterefund + deleterefund',
            'prbdetails + prbdetails',
            'alreadyRefund + alreadyRefund',//已退款列表
            'orderDetail + orderDetail',
            'refund_zfb + refund_zfb',
        );
    }

    public function filterrefund($filterChain) {
        $this->checkAccess("退款列表", $filterChain);
    }
     public function filterdeleterefund($filterChain) {
        $this->checkAccess("删除退款", $filterChain);
    }
     public function filterprbdetails($filterChain) {
        $this->checkAccess("退款列表", $filterChain);
    }
     public function filteralreadyRefund($filterChain) {
        $this->checkAccess("已退款列表", $filterChain);
    }
     public function filterorderDetail($filterChain) {
        $this->checkAccess("退款列表", $filterChain);
    }
    public function filterrefund_zfb($filterChain) {
        $this->checkAccess("支付宝退款", $filterChain);
    }
    

    /*     * ************** 退款 start ************** */

    public function actionrefund() {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $refund_model = refund::model();
            $refund_info = $refund_model->findAll(array(
                'condition' => 'statue != 1',
                'order' => 'refundId DESC',
            ));
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagRefund = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除退款") {
                    $flagRefund = "true";
                    break;
                }
            }
            
            $this->renderPartial('refund', array('refund_info' => $refund_info,'flagRefund' => $flagRefund, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actiondeleterefund() {//删除退款记录
        $refundIdd = $_POST["refundIdd"];
        $refundId = explode(",", $refundIdd);

        $refund_model = refund::model();
        $statues = 0;

        foreach ($refundId as $k => $l) {
            $refund_info = $refund_model->findByPk($l);

            if (count($refund_info) != 0) {
                $refund_model->deleteByPk($l);
            }
        }
        $json = '{"data":"success"}';
        echo $json;
    }

    public function actionprbdetails($_sessionId, $trade_no) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $prbusiness_model = prbusiness::model();
            $prbusiness_info = $prbusiness_model->findAll("sessionId = '$_sessionId'AND trade_no='$trade_no'");
            $this->renderPartial('prbdetails', array('prbusiness_info' => $prbusiness_info, '_sessionId' => $_sessionId, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 退款end ************** */
    /*     * ************** 已退款列表start ************** */

    public function actionalreadyRefund() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $refund_model = refund::model();
            $refund_info = $refund_model->findAll(array(
                'condition' => 'statue = 1',
                'order' => 'refundId DESC',
            ));

            $this->renderPartial('alreadyRefund', array('refund_info' => $refund_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 已退款列表end ************** */
    /*     * ************** 订单详情 start************** */

//订单详情
    public function actionorderDetail($businessid) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $username = Yii::app()->session['adminuser'];

            $businessidd = base64_decode($businessid);

            $attachment_model = attachment::model();
            $record_model = record::model();
            $businssid_model = business::model();
            $subbusiness_model = subbusiness::model();

            $businessid_info = $businssid_model->find(array("condition" => "businessid=$businessidd"));

            $integration = (int) $record_model->find(array("condition" => "userid = '$businessid_info->_userid'"))->points;

            $attachmentArray = array();

            $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId=$businessidd"));
            if ($subbusiness_info) {
                foreach ($subbusiness_info as $l => $y) {
                    $attachmentId = $y->_attachmentId;
                    $attachinfo = $attachment_model->find(array('condition' => "attachmentid = $attachmentId"));
                    array_push($attachmentArray, array("subbusinessId" => $y->subbusinessId, "attachmentname" => $attachinfo->attachmentname, "printNumber" => $y->printNumbers, "paidMoney" => $y->paidMoney, "printSet" => $y->printSet, "payType" => $y->payType, "isPay" => $y->isPay, "status" => $y->status, "marchineId" => $y->marchineId, "isrefund" => $y->isrefund));
                }
            }
            $this->renderPartial('orderDetail', array("businessid_info" => $businessid_info, "attachmentArray" => $attachmentArray, "username" => $username, "integration" => $integration, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 订单列表 end************** */
    /*     * ************** 支付宝退款 start************** */

    public function actionrefund_zfb($refundIdd) {
//        $refundIdd = $_POST['refundIdd'];
        $refundIdd = explode(',', $refundIdd); //ID 1,2,3,4

        if (isset($refundIdd)) {
            $refund_model = refund::model();
            $detail_datas = "";
            require_once("refundlib/alipay.config.php");
            require_once("refundlib/alipay_submit.class.php");
            foreach ($refundIdd as $k => $l) {
                $refund_info = $refund_model->find("refundId = $l");

                $detail_datas .= $refund_info->tborderId . "^" . $refund_info->money . "^" . "协商退款#";
            }
            /*             * ************************请求参数************************* */

//服务器异步通知页面路径
            $notify_url = "http://www.cqutprint.com/refundalipay/notify_url.php";
//需http://格式的完整路径，商户网关地址不允许加?id=123这类自定义参数
//卖家支付宝帐户
            $seller_email = "cqutprint@aliyun.com";
//必填
//退款当天日期
            date_default_timezone_set('PRC');
            $refund_date = date('Y-m-d H:i:s');
//必填，格式：年[4位]-月[2位]-日[2位] 小时[2位 24小时制]:分[2位]:秒[2位]，如：2007-10-01 13:13:13
//批次号
            $verificationCode = mt_rand(1000000000, 9999999999); //验证码
            $batch_no = date('Ymd') . $verificationCode;
//必填，格式：当天日期[8位]+序列号[3至24位]，如：201008010000001
//退款笔数
            $batch_num = count($refundIdd);
//必填，参数detail_data的值中，“#”字符出现的数量加1，最大支持1000笔（即“#”字符出现的数量999个）
//退款详细数据
            $detail_data = substr($detail_datas, 0, strlen($detail_datas) - 1);
//必填，具体格式请参见接口技术文档


            /*             * ********************************************************* */

//构造要请求的参数数组，无需改动
            $parameter = array(
                "service" => "refund_fastpay_by_platform_pwd",
                "partner" => trim($alipay_config['partner']),
                "notify_url" => $notify_url,
                "seller_email" => $seller_email,
                "refund_date" => $refund_date,
                "batch_no" => $batch_no,
                "batch_num" => $batch_num,
                "detail_data" => $detail_data,
                "_input_charset" => trim(strtolower($alipay_config['input_charset']))
            );

//建立请求
            $alipaySubmit = new AlipaySubmit($alipay_config);
            $html_text = $alipaySubmit->buildRequestForm($parameter, "get", "确认");
//            echo $html_text;
            $this->renderPartial('alipay', array("html_text" => $html_text));
        } else
            echo '请返回';
    }

    /*     * ************** 支付宝退款end ************** */
}
