<?php

include 'baseController.php';

class printorController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'printor + printor',//终端信息
            'addprintor + addprintor',
            'deleteterninal + deleteterninal',
            'editprintor + editprintor',
            'printormonitor + printormonitor',//终端监控
            'group + group',//终端分组
             'addgroup + addgroup',
             'editgroup + editgroup',
             'deleteGroup + deleteGroup',
             'printversion + printversion',//终端版本
             'addversion + addversion',//终端监控
             'deleteversion + deleteversion',//终端监控
            
        );
    }

    public function filterprintor($filterChain) {
        $this->checkAccess("终端信息", $filterChain);
    }

    public function filteraddprintor($filterChain) {
        $this->checkAccess("添加终端", $filterChain);
    }

    public function filterdeleteterninal($filterChain) {
        $this->checkAccess("删除终端", $filterChain);
    }

    public function filtereditprintor($filterChain) {
        $this->checkAccess("编辑终端", $filterChain);
    }
     public function filterprintormonitor($filterChain) {
        $this->checkAccess("终端监控", $filterChain);
    }
    public function filtergroup($filterChain) {
        $this->checkAccess("终端分组", $filterChain);
    }
    public function filteraddgroup($filterChain) {
        $this->checkAccess("添加分组", $filterChain);
    }
    public function filtereditgroup($filterChain) {
        $this->checkAccess("编辑分组", $filterChain);
    }
    public function filterdeleteGroup($filterChain) {
        $this->checkAccess("删除分组", $filterChain);
    }
    public function filterprintversion($filterChain) {
        $this->checkAccess("终端版本", $filterChain);
    }
    public function filteraddversion($filterChain) {
        $this->checkAccess("添加版本", $filterChain);
    }
    public function filterdeleteversion($filterChain) {
        $this->checkAccess("删除版本", $filterChain);
    }

    /*     * ************** 终端信息 start************** */

    public function actionprintor() {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $printor_model = printor::model();
            $printor_info = $printor_model->findAll();
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
             $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagPrintor = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除终端") {
                    $flagPrintor = "true";
                }
            }


            $this->renderPartial('printor', array('leftContent' => $leftContent, 'recommend' => $recommend,'flagPrintor' => $flagPrintor, 'printor_info' => $printor_info));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 终端信息 end************** */
    /*     * ************** 删除终端 start************** */

    public function actiondeleteterninal() {//删除终端
        $printorId = $_POST['printorId'];
        $printor_model = printor::model();
        $printor_info = $printor_model->find(array('condition' => "printorId='$printorId'"));
        if ($printor_info) {
            $counts = $printor_model->deleteAll(array('condition' => "printorId='$printorId'"));
            if ($counts > 0) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        }
    }

    /*     * ************** 删除终端 end************** */
    /*     * ************** 添加终端 start ************** */

    public function actionaddprintor() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $printor_model = new printor();
            if (isset($_POST['machineId'])) {
                $machineId = $_POST['machineId'];
                $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));
                if (count($printor_info) == 0) {
                    $printor_model->machineId = $_POST['machineId'];
                    $printor_model->printorName = $_POST['printorname'];
                    $printor_model->address = $_POST['printoraddress'];
                    $printor_model->_storeId = $_POST['school'];
                    $printor_model->_groupID = $_POST['printorgroup'];

                    if ($printor_model->save()) {
                        $newId = $printor_model->attributes['printorId'];
                        $printor_info = $printor_model->find("printorId=$newId");
                        $tempFile = $_FILES['showpicture']['tmp_name'];
                        $attachment_after = substr(strrchr($_FILES['showpicture']['name'], '.'), 1);  //文件的后缀名 docx
                        $fileName = $newId . '.' . $attachment_after;
                        $targetPath = './images/terninalPictures';
                        $targetFile = rtrim($targetPath, '/') . '/' . $fileName;
                        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));
                        $printor_info->picture = $fileName;
                        if ($printor_info->save()) {
                            $this->redirect('./index.php?r=platform/printor/printor');
                        }
                    }
                } else {
                    echo "<script>alert('此终端已存在,请重新输入！');</script>";
                }
            } else {
                $group_info = group::model()->findAll();
                $store_info = store::model()->findAll();
                $this->renderPartial('addprintor', array('leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info, 'store_info' => $store_info));
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*
      学校与分组级联ajax数据
     */

    public function actionaddprintorAjax() {
        if (isset($_POST)) {
            $id = $_POST["value"];
            $group = group::model()->findAll("_storeid='$id'");
            $str = "";
            if (count($group) > 0) {
                $i = 0;
                foreach ($group as $value) {
                    $i++;
                    $groupId = $value->groupID;
                    $groupName = $value->groupName;
                    $str .= "{'id':'$i','groupid':'$groupId','groupname':'$groupName'},";
                }
                $str = substr($str, 0, -1);
                $json = "["
                        . $str
                        . "]";
                $json = str_replace("'", '"', $json);
            }
            echo $json;
        }
    }

    /*     * ************** 添加终端 end ************** */
    /*     * ************** 编辑终端 start ************** */

    public function actioneditprintor($printorId) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $printor_model = new printor();
            if (isset($_POST['machineId'])) {
                $machineId = $_POST['machineId'];
                $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));
                if (count($printor_info) != 0) {
                    $printor_info->machineId = $_POST['machineId'];
                    $printor_info->printorName = $_POST['printorname'];
                    $printor_info->address = $_POST['printoraddress'];
                    $printor_info->_storeId = $_POST['school'];
                    $printor_info->_groupID = $_POST['printorgroup'];

                    $newId = $_POST['machineId'];

                    if (isset($_FILES)) {
                        $tempFile = $_FILES['showpicture']['tmp_name'];
                        $attachment_after = substr(strrchr($_FILES['showpicture']['name'], '.'), 1);  //文件的后缀名 docx
                        $fileName = $newId . '.' . $attachment_after;
                        $targetPath = './images/terninalPictures';
                        $targetFile = rtrim($targetPath, '/') . '/' . $fileName;
                        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));
                        $printor_info->picture = $fileName;
                    }
                    if ($printor_info->save()) {
                        $this->redirect('./index.php?r=platform/printor/printor');
                    }
                }
            } else {
                $group_info = group::model()->findAll();
                $store_info = store::model()->findAll();
                $printor_info = $printor_model->find("printorId = '$printorId'");
                $this->renderPartial('editprintor', array('leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info, 'store_info' => $store_info, 'printor_info' => $printor_info));
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actioneditprintorAjax() {
        if (isset($_POST)) {
            $id = $_POST["value"];
            $group = group::model()->findAll("_storeid='$id'");
            $str = "";
            if (count($group) > 0) {
                $i = 0;
                foreach ($group as $value) {
                    $i++;
                    $groupId = $value->groupID;
                    $groupName = $value->groupName;
                    $str .= "{'id':'$i','groupid':'$groupId','groupname':'$groupName'},";
                }
                $str = substr($str, 0, -1);
                $json = "["
                        . $str
                        . "]";
                $json = str_replace("'", '"', $json);
            }
            echo $json;
        }
    }

    /*     * ************** 编辑终端 end ************** */
    /*     * ************** 终端监控 start ************** */

    public function actionprintormonitor() {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $printor_model = printor::model();
            $printor_info = $printor_model->findAll();


            $this->renderPartial('printormonitor', array('leftContent' => $leftContent, 'recommend' => $recommend, 'printor_info' => $printor_info));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 终端监控 end ************** */
    /*     * ************** 终端分组 start ************** */

    public function actiongroup() {//分组首页
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $group_model = group::model();
            $group_info = $group_model->findAll();
             //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagGroup = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除终端分组") {
                    $flagGroup = "true";
                }
            }
            $this->renderPartial('group', array('group_info' => $group_info, 'flagGroup' => $flagGroup,'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 终端监控 end ************** */
    /*     * ************** 删除分组 start ************** */

    public function actiondeleteGroup() {//删除分组功能
        if (isset(Yii::app()->session['adminuser'])) {
            $groupID = $_POST["groupID"];

            $group_model = group::model();
            $group_info = $group_model->find("groupID='$groupID'");

            if (count($group_info) != 0) {
                if ($group_info->delete()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"no"}';
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 删除分组 end ************** */
    /*     * ************** 增加分组 start ************** */

    public function actionaddgroup() {//增加分组功能
        if (isset(Yii::app()->session['adminuser'])) {
            if (isset($_POST["groupName"])) {
                $groupName = $_POST["groupName"];
                $groupDetail = $_POST["groupDetail"];
                $storeid = $_POST["schoolName"];
                $group_model = new group();
                $group_info = $group_model->find("groupName='$groupName'");
                if (count($group_info) != 0) {
                    $json = '{"data":"exist"}';
                    echo $json;
                } else {
                    $group_model->groupName = $groupName;
                    $group_model->groupDetail = $groupDetail;
                    $group_model->_storeid = $storeid;

                    if ($group_model->save()) {
                        $json = '{"data":"success"}';
                        echo $json;
                    } else {
                        $json = '{"data":"false"}';
                        echo $json;
                    }
                }
            } else {
                $leftContent = $this->getLeftContent();
                $recommend = $this->getrecommend();
                $store_model = store::model();
                $store_info = $store_model->findAll();
                $this->renderPartial('addgroup', array('leftContent' => $leftContent, 'recommend' => $recommend, 'store_info' => $store_info));
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 增加分组 end ************** */
    /*     * ************** 编辑分组 start ************** */

    public function actioneditgroup($groupID) {//编辑分组页面
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $group_model = group::model();
            $group_info = $group_model->find("groupID='$groupID'");
            $store_model = store::model();
            $store_info = $store_model->findAll();
            $this->renderPartial('editgroup', array('group_info' => $group_info, 'leftContent' => $leftContent, 'recommend' => $recommend, 'store_info' => $store_info));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actioneditgroups() {//编辑分组
        if (isset(Yii::app()->session['adminuser'])) {
            $groupID = $_POST["groupID"];
            $groupName = $_POST["groupName"];
            $groupDetail = $_POST["groupDetail"];
            $storeid = $_POST["schoolName"];

            $group_model = group::model();
            $group_info = $group_model->find("groupID='$groupID'");

            if (count($group_info) != 0) {
                $group_info->groupName = $groupName;
                $group_info->groupDetail = $groupDetail;
                $group_info->_storeid = $storeid;

                if ($group_info->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"no"}';
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 编辑分组 end ************** */
    /*     * ************** 终端版本 start ************** */

    public function actionprintversion() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
             $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagVersion = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除学校") {
                    $flagVersion = "true";
                }
            }

            $printVesion_model = printVersion::model();
            $printVesion_info = $printVesion_model->findAll();
            $this->renderPartial('printvesion', array('printVesion_info' => $printVesion_info,'flagVersion' => $flagVersion,  'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 终端版本 end ************** */
    /*     * ************** 删除终端版本 start ************** */

    public function actiondeleteversion() {
        if (isset(Yii::app()->session['adminuser'])) {
            $versionId = $_POST["versionId"];
            $printVesion_model = printVersion::model();
            $printVesion_info = $printVesion_model->findByPk($versionId);
            if ($printVesion_info->delete()) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 删除终端版本 end ************** */

    /*     * ************** 新增终端版本 start ************** */

    public function actionaddversion() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $group_info = group::model()->findAll();

            $store_model = new store();
            if (isset($_POST['versionname'])) {
                if ($_FILES["versionfile"]["error"] > 0) {
                    echo "Error: ";
                } else {
                    $tempFile = $_FILES["versionfile"]["tmp_name"];
                    $fileName = $_FILES["versionfile"]["name"];

                    $targetPath = './printVersion';
                    $targetFile = rtrim($targetPath, '/') . '/' . $fileName;


                    $printVesion_model = new printVersion();
                    $printVesion_model->versionCode = $_POST['vesionCode'];
                    $printVesion_model->versionFile = $fileName;
                    $printVesion_model->updateTime = date('Y-m-d H:i:s', time());
                    $printVesion_model->versiondescript = $_POST['versiondescript'];
                    $printVesion_model->FileMD5 = md5_file($tempFile);
                    move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));

                    if ($printVesion_model->save()) {
                        $this->redirect('./index.php?r=platform/printor/printversion');
                    }
                }
            } else {
                $this->renderPartial('addversion', array('leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info));
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 新增终端版本 end ************** */
}
