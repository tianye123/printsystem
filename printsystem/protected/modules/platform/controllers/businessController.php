<?php
include 'baseController.php';
class businessController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*     * ************** 订单列表 ************** */

    public function filters() {
        
        return array(
            'business + business',
            'orderDetail + orderDetail',
        );
    }
    public function filterbusiness($filterChain){
        $this->checkAccess("订单", $filterChain);
    }
    public function filterorderDetail($filterChain){
        $this->checkAccess("订单详情", $filterChain);
    }
//订单列表
    public function actionbusiness() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $business_model = business::model();
            $business_info = $business_model->findAll(array('condition' => "isdelete = 0", 'order' => 'businessid DESC'));
            $this->renderPartial('business', array('business_info' => $business_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actionbusinessAjax() {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=platform');
        }
        $business_model = business::model();
        $business_info = $business_model->findAll(array('condition' => "isdelete = 0", 'order' => 'businessid DESC'));
        $i = 0;
        foreach ($business_info as $v) {
            $i++;
            if ($v->_userid != "") {
                $user_models = user::model();
                $user_infox = $user_models->find(array('condition' => "userid=$v->_userid"));
                if (isset($user_infox)) {
                    $name = $user_infox->username;
                }
            }
            $businessId = $v->businessid;
            $orderId = $v->orderId;
            $placeOrdertime = $v->placeOrdertime;
            $paidMoney = $v->paidMoney;
            $consumptionIntegral = $v->consumptionIntegral;
            $data1[] = array(
                'businessId' => $businessId,
                'orderId' => $orderId,
                'name' => $name,
                'placeOrdertime' => $placeOrdertime,
                'paidMoney' => $paidMoney,
                'consumptionIntegral' => $consumptionIntegral,
            );
        }
        $data2 = array(
            'draw' => 0,
            'recordsTotal' => $i,
            'recordsFiltered' => $i,
            'aaData' => $data1,
        );
        echo $data = json_encode($data2);
    }

    /*     * ************** 订单列表 ************** */
    /*     * ************** 订单详情 ************** */

//订单详情
    public function actionorderDetail($businessid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $username = Yii::app()->session['adminuser'];

        $businessidd = $businessid;

        $attachment_model = attachment::model();
        $user_model = user::model();
        $record_model = record::model();
        $businssid_model = business::model();
        $subbusiness_model = subbusiness::model();
        $businessid_info = $businssid_model->find(array("condition" => "businessid=$businessidd"));


        $integration = (int) $record_model->find(array("condition" => "userid = '$businessid_info->_userid'"))->points;


        $attachmentArray = array();
        $printNumber = 0; //打印

        $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId=$businessidd"));
        if ($subbusiness_info) {
            foreach ($subbusiness_info as $l => $y) {
                $attachmentId = $y->_attachmentId;
                $attachinfo = $attachment_model->find(array('condition' => "attachmentid = $attachmentId"));
                array_push($attachmentArray, array("attachmentname" => $attachinfo->attachmentname, "printNumber" => $y->printNumbers, "paidMoney" => $y->paidMoney, "printSet" => $y->printSet, "payType" => $y->payType, "isPay" => $y->isPay, "status" => $y->status, "marchineId" => $y->marchineId, "isrefund" => $y->isrefund));
            }
        }
        $this->renderPartial('orderDetail', array("businessid_info" => $businessid_info, "attachmentArray" => $attachmentArray, "username" => $username, "integration" => $integration, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    /*     * ************** 订单列表 ************** */
}
