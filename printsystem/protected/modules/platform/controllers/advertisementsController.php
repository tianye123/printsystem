<?php

include 'baseController.php';

class advertisementsController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'companyInfo + companyInfo', //公司管理
            'addcompany + addcompany',
            'addCompanys + addCompanys',
            'editcompany + editcompany',
            'editCompanys + editCompanys',
            'deleteCompany + deleteCompany',
            'advertisementInfo + advertisementInfo', //广告管理
            'addadvertisement + addadvertisement',
            'editAds + editAds',
            'DeleteAds + DeleteAds',
            'saveads + saveads', 
        );
    }

    public function filtercompanyInfo($filterChain) {
        $this->checkAccess("公司管理", $filterChain);
    }

    public function filteraddcompany($filterChain) {
        $this->checkAccess("添加公司", $filterChain);
    }

    public function filteraddCompanys($filterChain) {
        $this->checkAccess("添加公司", $filterChain);
    }

    public function filtereditCompanys($filterChain) {
        $this->checkAccess("编辑公司", $filterChain);
    }
     public function filtereditcompany($filterChain) {
        $this->checkAccess("编辑公司", $filterChain);
    }
    public function filteradvertisementInfo($filterChain) {
        $this->checkAccess("广告管理", $filterChain);
    }
    public function filteraddadvertisement($filterChain) {
        $this->checkAccess("添加广告", $filterChain);
    }
     public function filtereditAds($filterChain) {
        $this->checkAccess("编辑广告", $filterChain);
    }
    public function filtereDeleteAds($filterChain) {
        $this->checkAccess("删除广告", $filterChain);
    }

    /*     * ************** 公司信息 start************** */

    public function actioncompanyInfo() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagCompany = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除公司") {
                    $flagCompany = "true";
                    break;
                }
            }
            $company_model = company::model();
            $company_info = $company_model->findAll(array('order' => "companyID DESC"));
            $this->renderPartial('companyInfo', array('company_info' => $company_info, 'flagCompany' => $flagCompany, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 公司信息 end************** */

    public function actionaddcompany() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $this->renderPartial('addcompany', array('leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actionaddCompanys() {
        $companyName = $_POST["name"];
        $companyPhone = $_POST["phone"];
        $companyAddress = $_POST["address"];
        $linkMan = $_POST["man"];


        if (isset(Yii::app()->session['adminuser'])) {
            $company_model = new company();
            $company_info = $company_model->find("companyName='$companyName'");
            if (count($company_info) != 0) {
                $json = '{"data":"exist"}';
                echo $json;
            } else {
                $company_model->companyName = $companyName;
                $company_model->companyPhone = $companyPhone;
                $company_model->companyAddress = $companyAddress;
                $company_model->linkMan = $linkMan;
                date_default_timezone_set('PRC');
                $company_model->addTime = date('Y-m-d H:i:s', time());

                if ($company_model->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actiondeleteCompany() {
        if (isset(Yii::app()->session['adminuser'])) {
            $companyID = $_POST["ID"];

            $company_model = new company();
            $company_info = $company_model->find("companyID='$companyID'");

            if (count($company_info) != 0) {
                if ($company_info->delete()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"no"}';
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actioneditcompany($companyID) {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $company_model = new company();
            $company_info = $company_model->find("companyID='$companyID'");

            $this->renderPartial('editCompany', array('company_info' => $company_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actioneditCompanys() {
        $companyID = $_POST["companyID"];
        $companyName = $_POST["name"];
        $companyPhone = $_POST["phone"];
        $companyAddress = $_POST["address"];
        $linkMan = $_POST["man"];

        if (isset(Yii::app()->session['adminuser'])) {
            $company_model = company::model();
            $company_info = $company_model->find("companyID='$companyID'");

            if (count($company_info) != 0) {
                $company_info->companyName = $companyName;
                $company_info->companyPhone = $companyPhone;
                $company_info->linkMan = $linkMan;
                $company_info->companyAddress = $companyAddress;


                if ($company_info->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"no"}';
                echo $json;
            }
        } else
            $this->redirect('./index.php?r=platform');
    }

    /*     * *********************** 公司管理  *************************** */

    /*     * *********************** 广告管理  *************************** */

    public function actionadvertisementInfo() {

        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();

        if (isset(Yii::app()->session['adminuser'])) {
            $advertisement_model = advertisement::model();
            $advertisement_info = $advertisement_model->findAll(array('order' => "advertisementID DESC"));
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagAds = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除广告") {
                    $flagAds = "true";
                }
            }

            $this->renderPartial('advertisementInfo', array('advertisement_info' => $advertisement_info,'flagAds' => $flagAds, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else
            $this->redirect('./index.php?r = platform');
    }

    public function actionaddadvertisement() {
        $group_model = group::model();
        $group_info = $group_model->findAll();

        $company_model = new company();
        $company_info = $company_model->findAll();

        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();

        if (isset(Yii::app()->session['adminuser'])) {
            $this->renderPartial('addadvertisement', array('group_info' => $group_info, 'company_info' => $company_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actionaddadvertisements() {
        if (isset(Yii::app()->session['adminuser'])) {

            $startdate = $_POST["startdate"];
            $advertisementLength = $_POST["advertisementLength"];
            $enddate = $_POST["enddate"];
            $starttime = $_POST["starttime"];
            $endtime = $_POST["endtime"];
            $group = $_POST["group"];
            $company = $_POST["company"];
            $times = $_POST["times"];

            $advertisement_model = new advertisement();

            $adsdelivery_model = new adsdelivery();

            if ($_FILES["file"]["error"] > 0) {
                echo '<script>$("#file_error").text("上传文件错误！");</script>';
            } else {
                $fileName = $_FILES["file"]["name"];
                $advertisement_info = $advertisement_model->find(array("condition" => "_companyID=$company AND advertisementName = '$fileName'"));
                if (count($advertisement_info) != 0) {
                    echo '<script>alert("文件已存在！");window.location.href="./index.php?r=platform/advertisements/addadvertisement&companyID=' . $company . '"</script>';
                } else {
                    $attachment_after = substr(strrchr($fileName, '.'), 1);  //文件的后缀名

                    $saveName = $company . '-' . date('YmdHis');

                    $targetPath = './assets/advertisements';
                    $targetFile = rtrim($targetPath, '/') . '/' . $saveName . "." . $attachment_after;


                    $url = dirname(Yii::app()->BasePath); //C:\xampp\htdocs\printsystem\printsystem
                    $advertisement_model->_companyID = $company;
                    $advertisement_model->advertisementName = $fileName;
                    $advertisement_model->advertisementUrl = $url . "\\assets\\advertisements\\";
                    $advertisement_model->advertisementFormat = $attachment_after;
                    $advertisement_model->saveName = $saveName . "." . $attachment_after;
                    $advertisement_model->advertisementLength = $advertisementLength;
                    $advertisement_model->FileMD5 = md5_file($_FILES["file"]["tmp_name"]);

                    date_default_timezone_set('PRC');
                    $advertisement_model->addTime = date('Y-m-d H:i:s', time());

                    move_uploaded_file($_FILES["file"]["tmp_name"], iconv("UTF-8", "gb2312", $targetFile));

//                $vtime = exec("ffmpeg -i " . $_FILES["file"]["name"] . " 2>&1 | grep Duration | cut -d -f 4 | sed s/,//"); //CuPlayer.com提示总长度 
                    //$duration = explode(":",$time); 
                    // $duration_in_seconds = $duration[0]*3600 + $duration[1]*60+ round($duration[2]);//CuPlayer.com提示转化为秒 
//                return array(vtime => $vtime,
//                    ctime => $ctime
//                );
                    if ($advertisement_model->save()) {
                        $adsdelivery_model->_advertisementID = $advertisement_model->advertisementID;
                        $adsdelivery_model->_groupID = $group;
                        $adsdelivery_model->starttime = $starttime;
                        $adsdelivery_model->endtime = $endtime;
                        $adsdelivery_model->startDate = $startdate;
                        $adsdelivery_model->endDate = $enddate;
                        $adsdelivery_model->times = $times;
                        date_default_timezone_set('PRC');
                        $adsdelivery_model->addTime = date('Y-m-d H:i:s', time());

                        if ($adsdelivery_model->save()) {
                            $this->redirect('./index.php?r=platform/advertisements/advertisementInfo');
                        }
                    }
                }
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*
     * 编辑广告
     */

    public function actioneditAds($advertisementID) {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $group_model = group::model();
            $group_info = $group_model->findAll();

            $company_model = new company();
            $company_info = $company_model->findAll();

            $advertisement_model = advertisement::model();
            $group_model = group::model();
            $delivery = adsdelivery::model();
            $group_info = $group_model->findAll();

            $advertisement_info = $advertisement_model->find("advertisementID=" . $advertisementID);
            $delivery_info = $delivery->find("_advertisementID=" . $advertisementID);
            $this->renderPartial("editAds", array('leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info, 'company_info' => $company_info, 'advertisement_info' => $advertisement_info, 'delivery_info' => $delivery_info));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*
     * 编辑广告并保存
     */

    public function actionsaveads() {
        $advertisementID = $_POST["advertisementID"];
        $startdate = $_POST["startdate"];
        $advertisementLength = $_POST["advertisementLength"];
        $enddate = $_POST["enddate"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $group = $_POST["group"];
        $company = $_POST["company"];
        $times = $_POST["times"];

        $advertisement_model = advertisement::model();
        $adsdelivery_model = adsdelivery::model();

        $advertisement_info = $advertisement_model->find("advertisementID=$advertisementID");
        $adsdelivery_info = $adsdelivery_model->find("_advertisementID=$advertisementID");

        if (count($advertisement_info) == 0) {
            echo '<script>alert("广告不存在！");"</script>';
        } else {
            $advertisement_model->_companyID = $company;
            $advertisement_model->advertisementLength = $advertisementLength;

            //选择了文件
            if (!empty($_FILES['file']['tmp_name'])) {
                $fileName = $_FILES["file"]["name"];
                $attachment_after = substr(strrchr($fileName, '.'), 1);  //文件的后缀名
                $saveName = $company . '-' . date('YmdHis');

                $targetPath = './assets/advertisements';
                $targetFile = rtrim($targetPath, '/') . '/' . $saveName . "." . $attachment_after;

                $url = dirname(Yii::app()->BasePath); //C:\xampp\htdocs\printsystem\printsystem
                $advertisement_info->advertisementUrl = $url . "\\assets\\advertisements\\";
                $advertisement_info->advertisementName = $fileName;
                $advertisement_info->advertisementFormat = $attachment_after;
                $advertisement_info->saveName = $saveName . "." . $attachment_after;
                $advertisement_info->FileMD5 = md5_file($_FILES['file']['tmp_name']);
                move_uploaded_file($_FILES["file"]["tmp_name"], iconv("UTF-8", "gb2312", $targetFile));
            }

            if ($advertisement_info->save()) {
                $adsdelivery_info->_groupID = $group;
                $adsdelivery_info->starttime = $starttime;
                $adsdelivery_info->endtime = $endtime;
                $adsdelivery_info->startDate = $startdate;
                $adsdelivery_info->endDate = $enddate;
                $adsdelivery_info->times = $times;

                if ($adsdelivery_info->save()) {
                    $this->redirect('./index.php?r=platform/advertisements/advertisementInfo');
                }
            }
        }
    }

    public function actionDeleteAds() {
        $advertisement_model = advertisement::model();
        if (isset($_POST['advertisementID'])) {
            $advertisementID = $_POST['advertisementID'];
        }
        if (count($advertisement_model->deleteAll("advertisementID=" . $advertisementID)) > 0) {
            $json = '{"data":"success"}';
        } else {
            $json = '{"data":"false"}';
        }
        echo $json;
    }

    /*     * *********************** 广告管理  *************************** */
}
