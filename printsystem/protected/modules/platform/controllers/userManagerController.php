<?php
include 'baseController.php';
class userManagerController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }
     /*
      权限管理
     */

    public function filters() {
        return array(
            'serInfo + serInfo', 
            'userstoreDetail + userstoreDetail',
            'searchFile + searchFile',
            'searchBusiness + searchBusiness',
            'editUsers + editUsers',
            'deleteUsers + deleteUsers',
        );
    }

    public function filterserInfo($filterChain) {
        $this->checkAccess("用户", $filterChain);
    }
    public function filteruserstoreDetail($filterChain) {
        $this->checkAccess("查看积分", $filterChain);
    }
    public function filtersearchFile($filterChain) {
        $this->checkAccess("查看文件", $filterChain);
    }
    public function filtersearchBusiness($filterChain) {
        $this->checkAccess("查看订单", $filterChain);
    }
     public function filtereditUsers($filterChain) {
        $this->checkAccess("编辑用户", $filterChain);
    }
    public function filterdeleteUsers($filterChain) {
        $this->checkAccess("删除用户", $filterChain);
    }


    public function actionuserInfo() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $user_model = user::model();
            $user_info = $user_model->findAll();
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagUser = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除用户") {
                    $flagUser = "true";
                    break;
                }
            }
            $this->renderPartial('userInfo', array('user_info' => $user_info,'flagUser' => $flagUser, "leftContent" => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actionInfoToServerSideAjax() {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=platform');
        }
        $user_model = user::model();
        $user_info = $user_model->findAll();
        $i = 0;
        foreach ($user_info as $v) {
            $i++;
            $userid = $v->userid;
            $username = $v->username;
            $phone = $v->phone;
            $email = $v->email;
            $registertime = $v->registertime;
            $record_models = record::model();
            $record_infox = $record_models->find(array('condition' => "userid=$v->userid"));
            if (isset($record_infox)) {
                $record_info = $record_infox->points . " 点";
            }
            $databuf[] = array(
                'userid' => $userid,
                'id' => $i,
                'username' => $username,
                'phone' => $phone,
                'email' => $email,
                'registertime' => $registertime,
                'record_info' => $record_info,
            );
        }
        $data1 = array(
            'draw' => 0,
            'recordsTotal' => $i,
            'recordsFiltered' => $i,
            'aaData' => $databuf,
        );
        echo json_encode($data1);
    }

    /*     * ************** 查找文件 ************** */

    public function actionsearchFile($fileName, $userName) {
        $attachment_model = attachment::model();
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset($fileName) && isset($userName)) {//如果存在userName $fileName
            if ($fileName == "" && $userName == "") {
                $attachment_info = $attachment_model->findAll(array('condition' => "isdelete = 1", 'order' => 'attachmentid DESC'));
                $this->redirect('./index.php?r=platform/userManager/files', array('attachment_info' => $attachment_info, "fileName" => $fileName, "userName" => $userName, "leftContent" => $leftContent, 'recommend' => $recommend));
            } else if ($fileName != "" && $userName == "") {
                $criteria = new CDbCriteria;
                $criteria->addCondition("isdelete = 1");
                $criteria->addSearchCondition('attachmentname', $fileName);
                $criteria->order = 'attachmentid DESC';
                $attachment_info = $attachment_model->findAll($criteria);
                $this->renderPartial('files', array('attachment_info' => $attachment_info, "fileName" => $fileName, "userName" => $userName, "leftContent" => $leftContent, 'recommend' => $recommend));
            } else if ($fileName == "" && $userName != "") {
                $user_model = user::model();
                $userArray = array();
                $userArrays = "";
                $criterias = new CDbCriteria;
                $criterias->addSearchCondition('username', $userName);
                $user_info = $user_model->findAll($criterias);
                if ($user_info) {
                    foreach ($user_info as $k => $l) {
                        $userArrays .= $l->userid . ",";
                        array_push($userArray, $l->userid);
                    }
                    $userArrays = substr($userArrays, 0, strlen($userArrays) - 1);
                } else {
                    array_push($userArray, "-1");
                }
                $criteria = new CDbCriteria;
                $criteria->addCondition("isdelete = 1");
                $criteria->compare('_userid', $userArray);
                $criteria->order = 'attachmentid DESC';
                $attachment_info = $attachment_model->findAll($criteria);

                $this->renderPartial('files', array('attachment_info' => $attachment_info, "fileName" => $fileName, "userName" => $userName, "leftContent" => $leftContent, 'recommend' => $recommend));
            } else {
                $user_model = user::model();
                $userArray = array();
                $userArrays = "";
                $criterias = new CDbCriteria;
                $criterias->addSearchCondition('username', $userName);
                $user_info = $user_model->findAll($criterias);
                if ($user_info) {
                    foreach ($user_info as $k => $l) {
                        $userArrays .= $l->userid . ",";
                        array_push($userArray, $l->userid);
                    }
                    $userArrays = substr($userArrays, 0, strlen($userArrays) - 1);
                } else {
                    array_push($userArray, "-1");
                }

                $criteria = new CDbCriteria;
                $criteria->addCondition("isdelete = 1");
                $criteria->addSearchCondition('attachmentname', $fileName);
                $criteria->compare('_userid', $userArray);
                $criteria->order = 'attachmentid DESC';
                $attachment_info = $attachment_model->findAll($criteria);
                $this->renderPartial('files', array('attachment_info' => $attachment_info, "page_list" => $page_list, "fileName" => $fileName, "userName" => $userName, "leftContent" => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $attachment_info = $attachment_model->findAll(array('order' => 'attachmentid DESC'));
            $this->redirect('./index.php?r=platform/userManager/files', array('attachment_info' => $attachment_info, "fileName" => $fileName, "userName" => $userName, "leftContent" => $leftContent, 'recommend' => $recommend));
        }
    }

    public function actionfiles() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $attachment_model = attachment::model();
        $attachment_info = $attachment_model->findAll(array('condition' => "isdelete = 1", 'order' => 'attachmentid DESC'));
        $this->renderPartial('files', array('attachment_info' => $attachment_info, "leftContent" => $leftContent, 'recommend' => $recommend));
    }

//查询订单列表
    public function actionsearchBusiness($orderId, $userName) {
        $business_model = business::model();
        $recommend = $this->getrecommend();
        $leftContent = $this->getLeftContent();
        if (isset($orderId)) {//如果存在$orderId $userName
            if ($orderId == "" && $userName == "") {
                $business_info = $business_model->findAll(array('condition' => "isdelete = 0", 'order' => 'businessid DESC'));
                $this->redirect('./index.php?r=platform/userManager/business', array('business_info' => $business_info, "page_list" => $page_list, 'orderId' => $orderId, 'userName' => $userName, 'leftContent' => $leftContent, 'recommend' => $recommend));
            } else if ($orderId != "" && $userName == "") {
                $criteria = new CDbCriteria;
                $criteria->addSearchCondition('orderId', $orderId);
                $criteria->addCondition("isdelete = 0");

                $business_info = $business_model->findAll($criteria);
                $this->renderPartial('business', array('business_info' => $business_info, "page_list" => $page_list, 'orderId' => $orderId, 'userName' => $userName, 'leftContent' => $leftContent, 'recommend' => $recommend));
            } else if ($orderId == "" && $userName != "") {
                $user_model = user::model();
                $userArray = array();
                $userArrays = "";
                $criterias = new CDbCriteria;
                $criterias->addSearchCondition('username', $userName);
                $user_info = $user_model->findAll($criterias);
                if (count($user_info) != 0) {
                    foreach ($user_info as $k => $l) {
                        $userArrays .= $l->userid . ",";
                        array_push($userArray, $l->userid);
                    }
                    $userArrays = substr($userArrays, 0, strlen($userArrays) - 1);

                    $criteria = new CDbCriteria;
                    $criteria->compare('_userid', $userArray);
                    $criteria->addCondition("isdelete = 0");

                    $business_info = $business_model->findAll($criteria);
                    $this->renderPartial('business', array('business_info' => $business_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
                } else {
                    $this->renderPartial('business', array('business_info' => array(), 'orderId' => $orderId, 'userName' => $userName, 'leftContent' => $leftContent, 'recommend' => $recommend));
                }
            } else {
                $user_model = user::model();
                $userArray = array();
                $userArrays = "";
                $criterias = new CDbCriteria;
                $criterias->addSearchCondition('username', $userName);
                $user_info = $user_model->findAll($criterias);
                foreach ($user_info as $k => $l) {
                    $userArrays .= $l->userid . ",";
                    array_push($userArray, $l->userid);
                }
                $userArrays = substr($userArrays, 0, strlen($userArrays) - 1);

                $criteria = new CDbCriteria;
                $criteria->compare('_userid', $userArray);
                $criteria->addSearchCondition('orderId', $orderId);
                $criteria->addCondition("isdelete = 0");

                $business_info = $business_model->findAll($criteria);
                $this->renderPartial('business', array('business_info' => $business_info, "page_list" => $page_list, 'orderId' => $orderId, 'userName' => $userName, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $business_info = $business_model->findAll(array('condition' => "isdelete = 0", 'order' => 'businessid DESC'));
            $this->redirect('./index.php?r=platform/userManager/business', array('business_info' => $business_info, 'orderId' => $orderId, 'userName' => $userName, 'leftContent' => $leftContent, 'recommend' => $recommend));
        }
    }

    //订单列表
    public function actionbusiness() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $business_model = business::model();
        $business_info = $business_model->findAll(array('condition' => "isdelete = 0", 'order' => 'businessid DESC'));
        $this->renderPartial('business', array('business_info' => $business_info, "page_list" => $page_list, 'orderId' => "", 'userName' => "", 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    //订单详情
    public function actionorderDetail($businessid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $username = Yii::app()->session['adminuser'];

        $businessidd = base64_decode($businessid);

        $attachment_model = attachment::model();
        $user_model = user::model();
        $record_model = record::model();
        $businssid_model = business::model();
        $subbusiness_model = subbusiness::model();
        $businessid_info = $businssid_model->find(array("condition" => "businessid=$businessidd"));


        $integration = (int) $record_model->find(array("condition" => "userid = '$businessid_info->_userid'"))->points;


        $attachmentArray = array();
        $printNumber = 0; //打印

        $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId=$businessidd"));
        if ($subbusiness_info) {
            foreach ($subbusiness_info as $l => $y) {
                $attachmentId = $y->_attachmentId;
                $attachinfo = $attachment_model->find(array('condition' => "attachmentid = $attachmentId"));
                array_push($attachmentArray, array("attachmentname" => $attachinfo->attachmentname, "printNumber" => $y->printNumbers, "paidMoney" => $y->paidMoney, "printSet" => $y->printSet, "payType" => $y->payType, "isPay" => $y->isPay, "status" => $y->status, "marchineId" => $y->marchineId, "isrefund" => $y->isrefund));
            }
        }
        $this->renderPartial('orderDetail', array("businessid_info" => $businessid_info, "attachmentArray" => $attachmentArray, "username" => $username, "integration" => $integration, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    /*     * ************** 订单列表 ************** */

    /*     * ************** 编辑用户 ************** */

    public function actioneditUsers($userid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset(Yii::app()->session['adminuser'])) {
            $user_model = user::model();
            if (isset($_POST['user'])) {
                $usernamee = $_POST['user']['username'];
                $userpsww = md5($_POST['user']['userpsw']);
                $phone = $_POST['user']['phone'];
                $email = $_POST['user']['email'];

                $user_info = $user_model->find(array('condition' => "username='$usernamee'"));
                if ($user_info) {
                    $user_info->userpsw = $userpsww;
                    $user_info->phone = $phone;
                    $user_info->email = $email;
                    if ($user_info->save()) {
                        echo "<script>alert('重置成功！');window.location.href='./index.php?r=platform/userManager/userInfo';</script>";
                    } else {
                        echo "<script>alert('重置失败！');</script>";
                    }
                }
            } else {
                $user_model = user::model();
                $user_info = $user_model->find(array('condition' => "userid='$userid'"));
                $this->renderPartial('editUsersInfo', array('user_info' => $user_info, 'leftContent' => $leftContent,'recommend' => $recommend));
            }
        } else
            $this->redirect('./index.php?r=platform');
    }

    /*     * ************** 编辑用户 ************** */

    //用户积分流水
    public function actionuserstoreDetail($userid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $integralDetails_models = integralDetails::model();
        $useridx = $userid;
        $integralDetails_infos = $integralDetails_models->findAll(array('condition' => "_userid = '$useridx'", 'order' => 'integralDetailsId DESC'));
        $this->renderPartial('userstoreDetail', array('integralDetails_infos' => $integralDetails_infos, "leftContent" => $leftContent, 'recommend' => $recommend));
    }

    public function actiondeleteUsers() {//删除用户
        $userid = $_POST['userid'];
        $user_model = user::model();
        $user_info = $user_model->find(array('condition' => "userid='$userid'"));
        if ($user_info) {
            $counts = $user_model->deleteAll(array('condition' => "userid='$userid'"));
            if ($counts > 0) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        }
    }

}
