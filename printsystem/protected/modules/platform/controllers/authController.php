<?php

include 'baseController.php';

class authController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function filters() {
        return array(
            'Role + Role',
            'deleteRole + deleteRole',
            'assignRole + assignRole',
            'addAuth + addAuth',
            'addRole + addRole',
            'editRole + editRole',
        );
    }
    public function filterRole($filterChain){
        $this->checkAccess("角色与权限", $filterChain);
    }
     public function filterassignRole($filterChain){
        $this->checkAccess("权限分配", $filterChain);
    }
     public function filteraddAuth($filterChain){
        $this->checkAccess("创建权限", $filterChain);
    }
     public function filteraddRole($filterChain){
        $this->checkAccess("创建角色", $filterChain);
    }
     public function filterdeleteRole($filterChain){
        $this->checkAccess("删除角色", $filterChain);
    }
     public function filtereditRole($filterChain){
        $this->checkAccess("编辑角色", $filterChain);
    }

    public function actionRole() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $role_mode = role :: model();
        $role_info = $role_mode->findAll();
        if ($role_info) {
            $crt = count($role_info);
            $per = 10;
            $page = new page($crt, $per);
            $page_list = $page->fpage(array(0, 3, 4, 5, 6, 7));
        } else {
            $page_list = "";
        }
         //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagRole = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除角色") {
                    $flagRole = "true";
                    break;
                }
            }

        $this->renderPartial('role', array('role_info' => $role_info, 'page_list' => $page_list,'flagRole' => $flagRole, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    /*
     * 删除角色
     */

    public function actiondeleteRole() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $role_mode = role :: model();
            $iteminfo_model = assignment::model();
            if ((count($role_mode->deleteAll("roleId=" . $id)) && count($iteminfo_model->deleteAll("_roleId=" . $id)) ) > 0) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        }
    }

    /*
     * 分配角色
     */

    public function actionassignRole() {
        //管理员
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $admin_model = administrator::model();
        $admin_info = $admin_model->findAll();
        //角色名称
        $role = role::model();
        $role_info = $role->findAll();
        if (count($admin_info) > 0) {
            $crt = count($admin_info);
            $per = 10;
            $page = new page($crt, $per);
            $page_list = $page->fpage(array(0, 3, 4, 5, 6, 7));
        } else {
            $page_list = "";
        }
        $this->renderPartial('assignRole', array('admin_info' => $admin_info, 'role_info' => $role_info, 'page_list' => $page_list, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    public function actionaddAuth() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $item_model = item::model();
        $item_info = $item_model->findAll("parentItem =0");
        $this->renderPartial('addAuth', array('item_info' => $item_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    public function actionsaveAuth() {
        $item_model = new item;
        $authitem_model = item::model();
        if (isset($_POST['parentItem1'])) {
            $auth = $_POST['parentItem1']; //新增权限
            $authchild = $_POST['childTtem1'];
            $itemName1 = $authitem_model->find("itemName = '$auth'");
            if (!$itemName1) {
                $item_model = new item;
                $item_model->itemName = $auth;
                $item_model->parentItem = 0;
                $item_model->save();
                if ($authchild != NULL) {
                    $itemInfo = $authitem_model->find("itemName = '$auth'");
                    $itemId = $itemInfo->itemId;
                    $itemchild_model = new item;
                    $itemchild_model->itemName = $authchild;
                    $itemchild_model->parentItem = $itemId;
                    $itemchild_model->save();
                    if ($itemchild_model->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
                }
            }
        }
        if (isset($_POST['childTtem'])) {  //已有权限添加子权限
            $parentauth = $_POST['parentItem'];
            $childauth = $_POST['childTtem'];
            $item_model1 = new item;
            $itemName = $authitem_model->find("itemName = '$parentauth'");
            $parentId = $itemName->itemId;
            if (count($itemName) > 0) {
                $item_model1->itemName = $childauth;
                $item_model1->parentItem = $parentId;
                $item_model1->save();
                if ($item_model1->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                echo "<script>alert('添加失败！');fwindow.location.href='./index.php?r=platform/auth/role';</script>";
            }
        }
    }

//
//    /*
//     * 为管理员分配角色
//     */
//
    public function actionassign() {
        if (isset($_POST['adminID']) && isset($_POST['roleID'])) {
            $administratorid = $_POST['adminID'];
            $_roleID = $_POST['roleID'];
            $admin_model = administrator::model();
            $admin_info = $admin_model->find("administratorid=" . $administratorid);
            $admin_info->_roleid = $_roleID;
            if ($admin_info->save()) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        }
    }

    /*
     * 创建角色
     */

    public function actionaddRole() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $role_model = role::model();
        $item_model = item::model();
        $authitem_info = $item_model->findAll("parentItem = 0");
        $this->renderPartial('addRole', array('authitem_info' => $authitem_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    /*
     * 
     */

    public function actionsaveRole() {
        $leftContent = $this->getLeftContent();
        $authrole_model2 = new role;
        $authitem_model = new item;
//        $authitemChild_model = auth_item_child::model();
        if (isset($_POST)) {
            $authrole = trim($_POST['role']);
            $authrole_model2->rolename = $authrole;
            $authrole_model2->description = $authrole;
            $authrole_model2->save();
            $authrole_model = role :: model();
            $authItem_model = item::model();
            $role_info = $authrole_model->find("rolename='$authrole'");
            if (isset($_POST['authparent'])) {
                foreach ($_POST['authparent'] as $value) {
                    $authassignment_model = new assignment;
                    $role_id = $role_info->roleId;
                    $authassignment_model->_roleId = $role_id;
                    $item_info = $authItem_model->find("itemName='$value'");
                    $authassignment_Pid = $item_info->itemId;
                    $authassignment_model->_itemId = $authassignment_Pid;
                    $authassignment_model->save();
                }
            }
            if (isset($_POST['authchild'])) {
                $flag = TRUE;
                foreach ($_POST['authchild'] as $value) {
                    $authassignment_model1 = new assignment;
                    $role_id = $role_info->roleId;
                    $authassignment_model1->_roleId = $role_id;
                    $item_info = $authItem_model->find("itemName='$value'");
                    $authassignment_Cid = $item_info->itemId;
                    $authassignment_model1->_itemId = $authassignment_Cid;
                    if ($authassignment_model1->save()) {
                        
                    } else {
                        $flag = FALSE;
                    }
                }
                if ($flag) {
                    echo "<script>window.location.href='./index.php?r=platform/auth/role';</script>";
                    // $this->redirect("./index.php?r=platform/auth/role");
                } else {
                    echo "<script>alert('创建失败！');window.location.href='./index.php?r=platform/auth/role';</script>";
                }
            }
        }
    }

    /*
     * 编辑角色
     */

    public function actioneditRole($roleid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $role_mode = role :: model();
        $authitem_model = item::model();
        $assignment = assignment::model();
        $authrole_info = $role_mode->find("roleId='$roleid'");
        $assignment_info = $assignment->findAll("_roleId='$roleid'");
        $assign_info = $authitem_model->findAll("parentItem=0");
        $auth_name = array();
        foreach ($assign_info as $value) {
            $id = $value->itemId;
            $name = $value->itemName;
            foreach ($assignment_info as $auth_info) {
                $auth_id = $auth_info->_itemId;
                $assig_id = $authitem_model->find("itemId='$auth_id'");
                $parent_id = $assig_id->parentItem;
                $parent_name = $assig_id->itemName;
                if ($parent_id == 0 && $parent_name == $name) {
                    $auth_name[$id][0] = $assig_id->itemName;
                } else if ($parent_id == $id) {
                    $auth_name[$id][$auth_id] = $assig_id->itemName;
                }
            }
        }
        $this->renderPartial('editRole', array('authrole_info' => $authrole_info, 'auth_name' => $auth_name, 'assign_info' => $assign_info, 'roleid' => $roleid, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    public function actioneditupdateRole() {
        $roleid = $_POST['roleid'];
        $authrole_model2 = new role;
        $authitem_model = new item;
        $authroleid = $roleid;
        $authassignment_model = assignment::model();
        $item_model = item::model();
        $count = $authassignment_model->deleteAll("_roleId = '$authroleid'");
        if (isset($_POST)) {
            $authrole_model = role :: model();
            if (isset($_POST['authparent'])) {
                $flag = TRUE;
                foreach ($_POST['authparent'] as $value) {
                    $authupdate_model = new assignment;
                    $parent_info = $item_model->find("itemName='$value'");
                    $id = $parent_info->itemId;
                    $authupdate_model->_roleId = $authroleid;
                    $authupdate_model->_itemId = $id;
                    $authupdate_model->save();
                    if ($authupdate_model->save()) {
                        
                    } else {
                        $flag = FALSE;
                    }
                }
                if ($flag) {
                    echo "<script>window.location.href='./index.php?r=platform/auth/role';</script>";
                    // $this->redirect("./index.php?r=platform/auth/role");
                } else {
                    echo "<script>alert('保存失败！');window.location.href='./index.php?r=platform/auth/role';</script>";
                }
            }if (isset($_POST['authchild'])) {
                $flag = TRUE;
                foreach ($_POST['authchild'] as $value) {
                    $authupdate_model1 = new assignment;
                    $parent_info = $item_model->find("itemName='$value'");
                    $id = $parent_info->itemId;
                    $authupdate_model1->_roleId = $authroleid;
                    $authupdate_model1->_itemId = $id;
                    $authupdate_model1->save();
                    if ($authupdate_model1->save()) {
                        
                    } else {
                        $flag = FALSE;
                    }
                }
                if ($flag) {
                    echo "<script>window.location.href='./index.php?r=platform/auth/role';</script>";
                    // $this->redirect("./index.php?r=platform/auth/role");
                } else {
                    echo "<script>alert('保存失败！');window.location.href='./index.php?r=platform/auth/role';</script>";
                }
            } else {
                echo "<script>alert('请选择数据！');window.location.href='./index.php?r=platform/auth/role';</script>";
            }
        }
    }

}
