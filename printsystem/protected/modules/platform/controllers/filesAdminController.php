<?php

include 'baseController.php';

class filesAdminController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function filters() {
        return array(
            'files + files',
            'download + download',
            'deleteFile + deleteFile',
        );
    }

    public function filterfiles($filterChain) {
        $this->checkAccess("文件", $filterChain);
    }

    public function filterdownload($filterChain) {

        $this->checkAccess("文件下载", $filterChain);
    }

    public function filterdeleteFile($filterChain) {
        //增加终端
        $this->checkAccess("文件删除", $filterChain);
    }

    public function actionfiles() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $attachment_model = attachment::model();
            $attachment_info = $attachment_model->findAll(array('condition' => "isdelete = 1", 'order' => 'attachmentid DESC'));
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagFiles = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "文件删除") {
                    $flagFiles = "true";
                    break;
                }
            }
            $this->renderPartial('files', array('attachment_info' => $attachment_info, "flagFiles" => $flagFiles, "leftContent" => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    public function actionfilesToserverSideAjax() {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=platform');
        }
        $attachment_model = attachment::model();
        $attachment_info = $attachment_model->findAll(array('condition' => "isdelete = 1", 'order' => 'attachmentid DESC'));
        $i = 0;
        foreach ($attachment_info as $v) {
            $i++;
            $user_models = user::model();
            $user_infox = $user_models->find(array('condition' => "userid=$v->_userid"));
            if (isset($user_infox)) {
                $name = $user_infox->username;
            }
            $attachmentId = $v->attachmentid;
            $attachment = $v->attachmentname;
            $filenumber = $v->filenumber . "页";
            $uploadtime = $v->uploadtime;
            $data1[] = array(
                'attachmentid' => $attachmentId,
                'id' => $i,
                'name' => $name,
                'attachment' => $attachment,
                'filenumber' => $filenumber,
                'uploadtime' => $uploadtime,
            );
        }
        $data2 = array(
            'draw' => 0,
            'recordsTotal' => $i,
            'recordsFiltered' => $i,
            'aaData' => $data1,
        );
        echo $data = json_encode($data2);
    }

    /*     * ************** 查找文件 ************** */

//下载文件
    public function actiondownload($attachmentid) {

        $attachment_model = attachment::model();
        $attachmentids = $attachmentid;
        $attachment_info = $attachment_model->find(array('condition' => "attachmentid = '$attachmentids'"));
        $name = $attachment_info->attachmentfile;
        $truename = $attachment_info->attachmentname;
        $filename = "http://" . $attachment_info->ip . '/assets/userfile/' . $name;
        header('Content-Type:application/octet-stream'); //文件的类型
        Header("Accept-Ranges: bytes");
        header('Content-Disposition:attachment;filename = "' . $truename . '"'); //下载显示的名字
        ob_clean();
        flush();
        readfile($filename);
        exit();
    }

//删除文件
    public function actiondeleteFile() {
        $attachmentid = $_POST['attachmentid']; //文件ID
        $attachment_model = attachment::model();
        $attachment_info = $attachment_model->find(array('condition' => "attachmentid = $attachmentid"));
        $attachment_info->isdelete = 0;

        if ($attachment_info->save()) {
            echo"success";
        } else {
            echo 'false';
        }
    }

    /*     * ************** 文件列表 ************** */
}
