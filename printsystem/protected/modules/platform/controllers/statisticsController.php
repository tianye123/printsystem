<?php
include 'baseController.php';
class statisticsController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

     /*
      权限管理
     */

    public function filters() {
        return array(
            'school + school', //公司管理
            'terninalStatistics + terninalStatistics',
           
        );
    }

    public function filterschool($filterChain) {
        $this->checkAccess("统计", $filterChain);
    }
    public function filterterninalStatistics($filterChain) {
        $this->checkAccess("统计", $filterChain);
    }
    /*     * ************** 统计选择学校 start ************** */

    public function actionschool($type) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset(Yii::app()->session['adminuser'])) {
            $store_model = store::model();
            $store_info = $store_model->findAll(array('order' => "storeid DESC"));

            $this->renderPartial('school', array('store_info' => $store_info, 'leftContent' => $leftContent, 'recommend' => $recommend, 'type' => $type));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 统计选择学校 end ************** */

    public function actionterninalStatistics($storeid, $type) {//根据type的不同，跳转到不同的界面去统计
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset(Yii::app()->session['adminuser'])) {

            if ($type == "order") {//订单统计
                $prbusiness_model = prbusiness::model();
                $business_model = business::model();
                $store_model = store::model();
                $store_info = $store_model->find("storeid = $storeid");

                $monthArray = array();
                $dayArray = array();

                //按月统计
                $totalCount_months = 0;
                $year = date("Y"); //2015-11    2015-10-26 10:32:09
                for ($i = 1; $i <= 12; $i++) {
                    if ($i != 12) {
                        $stime = $year . "-" . $i . "-1 00:00:00";
                        $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                    } else {
                        $stime = $year . "-" . $i . "-1 00:00:00";
                        $etime = $year . "-" . $i . "-31 23:59:59";
                    }
                    $totalCount_month = 0;
                    $business_info = $business_model->findAll(array('condition' => "placeOrdertime >='$stime' AND placeOrdertime <='$etime'"));
                    $totalCount_month += count($business_info);

                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !='' AND starttime >='$stime' AND starttime <='$etime'"));
                    $totalCount_month += count($prbusiness_info);
                    $totalCount_months += $totalCount_month;
                    array_push($monthArray, array('totalCount_month' => $totalCount_month));
                }

                //按日统计
                $day = date("Y-m"); //2015-11
                $fate = date("t"); //本月有多少天
                $month = date("m"); //11月
                $totalCount_dayss = 0;
                for ($i = 1; $i <= $fate; $i++) {
                    $stime = $day . "-" . $i . " 00:00:00";
                    $etime = $day . "-" . ($i + 1) . " 00:00:00";
                    $totalCount_days = 0;
                    $business_info = $business_model->findAll(array('condition' => "placeOrdertime >='$stime' AND placeOrdertime <='$etime'"));
                    $totalCount_days += count($business_info);

                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !=''AND starttime >='$stime' AND starttime <='$etime'"));
                    $totalCount_days += count($prbusiness_info);
                    $totalCount_dayss += $totalCount_days;
                    array_push($dayArray, array('totalCount_days' => $totalCount_days, 'day' => $i));
                }
                $this->renderPartial('statisticOrderone', array('monthArray' => $monthArray, 'totalCount_months' => $totalCount_months, 'totalCount_dayss' => $totalCount_dayss, 'totalCount_month' => $totalCount_month, 'schoolname' => $store_info->storename, "year" => $year, 'leftContent' => $leftContent, 'recommend' => $recommend, "dayArray" => $dayArray, "storeid" => $storeid));
            }
            if ($type == "income") {//收入统计
                $prbusiness_model = prbusiness::model();
                $subbusiness_model = subbusiness::model();
                $store_model = store::model();
                $refund_model = refund::model();
                $store_info = $store_model->find("storeid = $storeid");

                $monthArray = array();
                $dayArray = array();
                //按月统计
                $totalIncome_months = 0;
                $year = date("Y"); //2015-11    2015-10-26 10:32:09
                for ($i = 1; $i <= 12; $i++) {
                    if ($i != 12) {
                        $stime = $year . "-" . $i . "-1 00:00:00";
                        $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                    } else {
                        $stime = $year . "-" . $i . "-1 00:00:00";
                        $etime = $year . "-" . $i . "-31 23:59:59";
                    }
                    $totalIncome_month = 0;
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "payTime >='$stime' AND payTime <='$etime' AND isPay =1 AND isrefund!=1"));
                    foreach ($subbusiness_info as $sub) {
                        $totalIncome_month+=$sub->paidMoney;
                    }
                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !='' AND starttime >='$stime' AND starttime <='$etime'"));
                    foreach ($prbusiness_info as $pr) {

                        $refund_info = $refund_model->find("_sessionId=$pr->sessionId");
                        if (count($refund_info) == 0) {
                            $totalIncome_month+=$pr->paidmoney;
                        } else if (count($refund_info) != 0 && $refund_info->statue != 1) {
                            $totalIncome_month+=$pr->paidmoney;
                        }
                    }
                    $totalIncome_months += $totalIncome_month;
                    array_push($monthArray, array('totalIncome_month' => $totalIncome_month));
                }
                //按日统计
                $day = date("Y-m"); //2015-11
                $fate = date("t"); //本月有多少天
                $month = date("m"); //11月
                $totalIncome_dayss = 0;
                for ($i = 1; $i <= $fate; $i++) {
                    $stime = $day . "-" . $i . " 00:00:00";
                    $etime = $day . "-" . ($i + 1) . " 00:00:00";
                    $totalIncome_day = 0;
                    $subbusinessDay_info = $subbusiness_model->findAll(array('condition' => "payTime >='$stime' AND payTime <='$etime' AND isPay =1 AND isrefund!=1"));
                    foreach ($subbusinessDay_info as $subDay) {
                        $totalIncome_day +=$subDay->paidMoney;
                    }
                    $prbusinessDay_info = $prbusiness_model->findAll(array('condition' => "trade_no !=''AND starttime >='$stime' AND starttime <='$etime'"));
                    foreach ($prbusinessDay_info as $pr) {

                        $refund_info = $refund_model->find("_sessionId=$pr->sessionId");
                        if (count($refund_info) == 0) {
                            $totalIncome_day+=$pr->paidmoney;
                        } else if (count($refund_info) != 0 && $refund_info->statue != 1) {
                            $totalIncome_day+=$pr->paidmoney;
                        }
                    }
                    $totalIncome_dayss += $totalIncome_day;
                    array_push($dayArray, array('totalIncome_day' => $totalIncome_day, 'day' => $i));
                }
                $this->renderPartial('statisticIncomeone', array('monthArray' => $monthArray, 'totalIncome_month' => $totalIncome_month, 'totalIncome_day' => $totalIncome_day, 'totalIncome_dayss' => $totalIncome_dayss, 'totalIncome_months' => $totalIncome_months, 'schoolname' => $store_info->storename, "year" => $year, 'leftContent' => $leftContent, 'recommend' => $recommend, "dayArray" => $dayArray, "storeid" => $storeid));
            }
            if ($type == "refund") {//退款统计
                $refund_model = refund::model();
                $store_model = store::model();
                $store_info = $store_model->find("storeid = $storeid");

                $monthArray = array();
                $dayArray = array();

                //按月统计
                $totalrefund_months = 0;
                $year = date("Y"); //2015-11    2015-10-26 10:32:09
                for ($i = 1; $i <= 12; $i++) {
                    if ($i != 12) {
                        $stime = $year . "-" . $i . "-1 00:00:00";
                        $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                    } else {
                        $stime = $year . "-" . $i . "-1 00:00:00";
                        $etime = $year . "-" . $i . "-31 23:59:59";
                    }
                    $totalrefund_month = 0;
                    //钱
                    $refund_info = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 1"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_month += $l->money;
                    }
                    //积分
                    $refund_infox = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 5"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_month += $l->consumptionIntegral / 100;
                    }
                    $totalrefund_months += $totalrefund_month;
                    array_push($monthArray, array('totalrefund_month' => $totalrefund_month));
                }

                //按日统计
                $day = date("Y-m"); //2015-11
                $fate = date("t"); //本月有多少天
                $month = date("m"); //11月
                $totalrefund_dayss = 0;
                for ($i = 1; $i <= $fate; $i++) {
                    if ($i != $fate) {
                        $stime = $day . "-" . $i . " 00:00:00";
                        $etime = $day . "-" . ($i + 1) . " 00:00:00";
                    } else {
                        $stime = $day . "-" . $i . " 00:00:00";
                        $etime = $day . "-" . "$fate 23:59:59";
                    }
                    $totalrefund_days = 0;
                    //钱
                    $refund_info = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 1"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_days += $l->money;
                    }
                    //积分
                    $refund_infox = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 5"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_days += $l->consumptionIntegral / 100;
                    }

                    $totalrefund_dayss += $totalrefund_days;
                    array_push($dayArray, array('totalrefund_days' => $totalrefund_days, 'day' => $i));
                }
                $this->renderPartial('statisticRefundone', array('monthArray' => $monthArray, 'totalrefund_months' => $totalrefund_months, 'totalrefund_dayss' => $totalrefund_dayss, 'schoolname' => $store_info->storename, "year" => $year, 'leftContent' => $leftContent, 'recommend' => $recommend, "dayArray" => $dayArray, "storeid" => $storeid));
            }
            if ($type == "service") {//服务人数统计
                $administrator_model = administrator::model();
                $schoolname = store::model()->find("storeid=$storeid")->storename;
                $printor_model = printor::model();
                $printor_info = $printor_model->findAll(array('condition' => "_storeid=$storeid"));
                $subbusiness_model = subbusiness::model();
                $prbusiness_model = prbusiness::model();
                $totalserviceArray = array();
                $totalservices = 0; //所有终端服务的人数
                if ($printor_info) {
                    foreach ($printor_info as $l => $y) {
                        $totalservice = 0;
                        $machineId = $y->machineId;
                        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId'"));

                        $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != ''"));
                        $totalservice = count($subbusiness_info) + count($prbusiness_info);
                        $totalservices +=$totalservice;
                        array_push($totalserviceArray, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalservice' => $totalservice));
                    }
                    $this->renderPartial('statisticsservice', array('totalserviceArray' => $totalserviceArray, 'schoolname' => $schoolname, "storeid" => $storeid, 'leftContent' => $leftContent, 'recommend' => $recommend, "totalservices" => $totalservices));
                }
            }
            if ($type == "printpaper") {//打印纸张统计
                $administrator_model = administrator::model();
                $schoolname = store::model()->find("storeid=$storeid")->storename;
                $printor_model = printor::model();
                $printor_info = $printor_model->findAll(array('condition' => "_storeid=$storeid"));
                $subbusiness_model = subbusiness::model();
                $prbusiness_model = prbusiness::model();
                $schoolArray = array();
                $totalPages = 0; //所有终端的打印纸张
                if ($printor_info) {
                    foreach ($printor_info as $l => $y) {
                        $totalPage = 0;
                        $machineId = $y->machineId;
                        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId'"));
                        foreach ($subbusiness_info as $q => $w) {
                            $filePage = explode(',', $w->printSet);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPage +=(int) $filePa;
                                }
                            }
                        }
                        $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != ''"));
                        foreach ($prbusiness_info as $q => $w) {
                            $filePage = explode(',', $w->printset);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPage +=(int) $filePa;
                                }
                            }
                        }
                        $totalPages +=$totalPage;
                        array_push($schoolArray, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalPage' => $totalPage));
                    }
                    $this->renderPartial('statisticsPaper', array('schoolArray' => $schoolArray, 'schoolname' => $schoolname, "storeid" => $storeid, 'leftContent' => $leftContent, 'recommend' => $recommend, "totalPages" => $totalPages));
                }
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * *************************订单统计***************************** */

    //按时间段查询
    public function actionsearch_statisticOrderone() {

        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $time = $_POST["time"];
        $prbusiness_model = prbusiness::model();
        $business_model = business::model();
        $store_model = store::model();
        $store_info = $store_model->find("storeid = $storeid");

        $totalCounts = 0;
        //按月统计
        if ($time == "month") {
            $monthArray = array();
            $smonth = date("m", strtotime($starttime));
            $syear = date("Y", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $eyear = date("Y", strtotime($endtime));

            if ($smonth <= $emonth)
                $day = $emonth - $smonth;
            if ($smonth > $emonth)
                $day = 12 - $smonth + $emonth;
            if ($syear == $eyear) {
                for ($smonth; $smonth <= $emonth; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalCount_month = 0;
                    $business_info = $business_model->findAll(array('condition' => "placeOrdertime >='$stime' AND placeOrdertime <='$etime'"));
                    $totalCount_month += count($business_info);

                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !='' AND starttime >='$stime' AND starttime <='$etime'"));
                    $totalCount_month += count($prbusiness_info);
                    $totalCounts += $totalCount_month;
                    array_push($monthArray, array('totalCount' => $totalCount_month, 'num' => $smonth . "月"));
                }
            } else if ($syear < $eyear) {
                for ($smonth; $smonth <= 12; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalCount_month = 0;
                    $business_info = $business_model->findAll(array('condition' => "placeOrdertime >='$stime' AND placeOrdertime <='$etime'"));
                    $totalCount_month += count($business_info);

                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !='' AND starttime >='$stime' AND starttime <='$etime'"));
                    $totalCount_month += count($prbusiness_info);
                    $totalCounts += $totalCount_month;
                    array_push($monthArray, array('totalCount' => $totalCount_month, 'num' => $smonth . "月"));
                }
                $syear = $syear + 1;
                if ($syear == $eyear) {
                    for ($p = 1; $p <= $emonth; $p++) {
                        if ($p != 12) {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . ($p + 1) . "-1 00:00:00";
                        } else {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . $p . "-31 23:59:59";
                        }
                        $totalCount_month = 0;
                        $business_info = $business_model->findAll(array('condition' => "placeOrdertime >='$stime' AND placeOrdertime <='$etime'"));
                        $totalCount_month += count($business_info);

                        $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !='' AND starttime >='$stime' AND starttime <='$etime'"));
                        $totalCount_month += count($prbusiness_info);
                        $totalCounts += $totalCount_month;
                        array_push($monthArray, array('totalCount' => $totalCount_month, 'num' => $p . "月"));
                    }
                } else if ($syear < $eyear) {
                    $totalCounts = 0;
                    array_push($monthArray, array('totalCount' => 0, 'num' => ""));
                }
            }
            array_push($monthArray, $totalCounts);
            $json_string = json_encode($monthArray);
            echo $json_string;
        }
        //按天统计
        if ($time == "day") {
            $dayArray = array();
            $year = date("Y", strtotime($starttime));
            $smonth = date("m", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $sday = date("d", strtotime($starttime));
            $eday = date("d", strtotime($endtime));



            //按日统计
            //同一个月
            if ($smonth == $emonth) {
                for ($i = $sday; $i <= $eday; $i++) {
                    $stime = $year . "-" . $smonth . "-" . $i . " 00:00:00";
                    $etime = $year . "-" . $smonth . "-" . ($i + 1) . " 00:00:00";
                    $totalCount_days = 0;
                    $business_info = $business_model->findAll(array('condition' => "placeOrdertime >='$stime' AND placeOrdertime <='$etime'"));
                    $totalCount_days += count($business_info);

                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !=''AND starttime >='$stime' AND starttime <='$etime'"));
                    $totalCount_days += count($prbusiness_info);
                    $totalCounts += $totalCount_days;
                    array_push($dayArray, array('totalCount' => $totalCount_days, 'num' => (int) $i));
                }
            }//不是同一个月 
            else if ($smonth + 1 == $emonth) {
                $totalCounts += 0;
                array_push($dayArray, array('totalCount' => 0, 'num' => ""));
            }
            array_push($dayArray, $totalCounts);
            $json_string = json_encode($dayArray);
            echo $json_string;
        }
    }

    /*     * *************************订单统计***************************** */
    /*     * *************************收入统计***************************** */

    public function actionsearch_statisticIncomeone() {

        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $time = $_POST["time"];
        $prbusiness_model = prbusiness::model();
        $subbusiness_model = subbusiness::model();
        $store_model = store::model();
        $refund_model = refund::model();
        $store_info = $store_model->find("storeid = $storeid");

        $totalIncomes = 0;
        //按月统计
        if ($time == "month") {
            $monthArray = array();
            $smonth = date("m", strtotime($starttime));
            $syear = date("Y", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $eyear = date("Y", strtotime($endtime));

            if ($smonth <= $emonth)
                $day = $emonth - $smonth;
            if ($smonth > $emonth)
                $day = 12 - $smonth + $emonth;
            if ($syear == $eyear) {
                for ($smonth; $smonth <= $emonth; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalIncome_month = 0;
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "payTime >='$stime' AND payTime <='$etime' AND isPay =1 AND isrefund!=1"));
                    foreach ($subbusiness_info as $sub) {
                        $totalIncome_month+=$sub->paidMoney;
                    }
                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !='' AND starttime >='$stime' AND starttime <='$etime'"));
                    foreach ($prbusiness_info as $pr) {

                        $refund_info = $refund_model->find("_sessionId=$pr->sessionId");
                        if (count($refund_info) == 0) {
                            $totalIncome_month+=$pr->paidmoney;
                        } else if (count($refund_info) != 0 && $refund_info->statue != 1) {
                            $totalIncome_month+=$pr->paidmoney;
                        }
                    }
                    $totalIncomes += $totalIncome_month;
                    array_push($monthArray, array('totalIncome' => $totalIncome_month, 'num' => $smonth . "月"));
                }
            } else if ($syear < $eyear) {
                for ($smonth; $smonth <= 12; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalIncome_month = 0;
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "payTime >='$stime' AND payTime <='$etime' AND isPay =1 AND isrefund!=1"));
                    foreach ($subbusiness_info as $sub) {
                        $totalIncome_month+=$sub->paidMoney;
                    }
                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !='' AND starttime >='$stime' AND starttime <='$etime'"));
                    foreach ($prbusiness_info as $pr) {

                        $refund_info = $refund_model->find("_sessionId=$pr->sessionId");
                        if (count($refund_info) == 0) {
                            $totalIncome_month+=$pr->paidmoney;
                        } else if (count($refund_info) != 0 && $refund_info->statue != 1) {
                            $totalIncome_month+=$pr->paidmoney;
                        }
                    }
                    $totalIncomes += $totalIncome_month;
                    array_push($monthArray, array('totalIncome' => $totalIncome_month, 'num' => $smonth . "月"));
                }
                $syear = $syear + 1;
                if ($syear == $eyear) {
                    for ($p = 1; $p <= $emonth; $p++) {
                        if ($p != 12) {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . ($p + 1) . "-1 00:00:00";
                        } else {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . $p . "-31 23:59:59";
                        }
                        $totalIncome_month = 0;
                        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "payTime >='$stime' AND payTime <='$etime' AND isPay =1 AND isrefund!=1"));
                        foreach ($subbusiness_info as $sub) {
                            $totalIncome_month+=$sub->paidMoney;
                        }
                        $prbusiness_info = $prbusiness_model->findAll(array('condition' => "trade_no !='' AND starttime >='$stime' AND starttime <='$etime'"));
                        foreach ($prbusiness_info as $pr) {

                            $refund_info = $refund_model->find("_sessionId=$pr->sessionId");
                            if (count($refund_info) == 0) {
                                $totalIncome_month+=$pr->paidmoney;
                            } else if (count($refund_info) != 0 && $refund_info->statue != 1) {
                                $totalIncome_month+=$pr->paidmoney;
                            }
                        }
                        $totalIncomes += $totalIncome_month;
                        array_push($monthArray, array('totalIncome' => $totalIncome_month, 'num' => $p . "月"));
                    }
                } else if ($syear < $eyear) {
                    $totalIncomes = 0;
                    array_push($monthArray, array('totalIncome' => 0, 'num' => ""));
                }
            }
            array_push($monthArray, $totalIncomes);
            $json_string = json_encode($monthArray);
            echo $json_string;
        }
        //按天统计
        if ($time == "day") {
            $dayArray = array();
            $year = date("Y", strtotime($starttime));
            $smonth = date("m", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $sday = date("d", strtotime($starttime));
            $eday = date("d", strtotime($endtime));



            //按日统计
            //同一个月
            if ($smonth == $emonth) {
                for ($i = $sday; $i <= $eday; $i++) {
                    $stime = $year . "-" . $smonth . "-" . $i . " 00:00:00";
                    $etime = $year . "-" . $smonth . "-" . ($i + 1) . " 00:00:00";
                    $totalIncome_day = 0;
                    $subbusinessDay_info = $subbusiness_model->findAll(array('condition' => "payTime >='$stime' AND payTime <='$etime' AND isPay =1 AND isrefund!=1"));
                    foreach ($subbusinessDay_info as $subDay) {
                        $totalIncome_day +=$subDay->paidMoney;
                    }
                    $prbusinessDay_info = $prbusiness_model->findAll(array('condition' => "trade_no !=''AND starttime >='$stime' AND starttime <='$etime'"));
                    foreach ($prbusinessDay_info as $pr) {

                        $refund_info = $refund_model->find("_sessionId=$pr->sessionId");
                        if (count($refund_info) == 0) {
                            $totalIncome_day+=$pr->paidmoney;
                        } else if (count($refund_info) != 0 && $refund_info->statue != 1) {
                            $totalIncome_day+=$pr->paidmoney;
                        }
                    }
                    $totalIncomes += $totalIncome_day;
                    array_push($dayArray, array('totalIncome' => $totalIncome_day, 'num' => (int) $i));
                }
            }//不是同一个月 
            else if ($smonth + 1 == $emonth) {
                $totalIncomes += 0;
                array_push($dayArray, array('totalIncome' => 0, 'num' => ""));
            }
            array_push($dayArray, $totalIncomes);
            $json_string = json_encode($dayArray);
            echo $json_string;
        }
    }

    /*     * *************************收入统计***************************** */
    /*     * *************************退款统计***************************** */

    public function actionsearch_statisticRefundone() {

        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $time = $_POST["time"];
        $prbusiness_model = prbusiness::model();
        $subbusiness_model = subbusiness::model();
        $store_model = store::model();
        $refund_model = refund::model();
        $store_info = $store_model->find("storeid = $storeid");

        $totalRefunds = 0;
        //按月统计
        if ($time == "month") {
            $monthArray = array();
            $smonth = date("m", strtotime($starttime));
            $syear = date("Y", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $eyear = date("Y", strtotime($endtime));

            if ($smonth <= $emonth)
                $day = $emonth - $smonth;
            if ($smonth > $emonth)
                $day = 12 - $smonth + $emonth;
            if ($syear == $eyear) {
                for ($smonth; $smonth <= $emonth; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalrefund_month = 0;
                    //钱
                    $refund_info = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 1"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_month += $l->money;
                    }
                    //积分
                    $refund_infox = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 5"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_month += $l->consumptionIntegral / 100;
                    }

                    $totalRefunds += $totalrefund_month;
                    array_push($monthArray, array('totalrefund' => $totalrefund_month, 'num' => $smonth . "月"));
                }
            } else if ($syear < $eyear) {
                for ($smonth; $smonth <= 12; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalrefund_month = 0;
                    //钱
                    $refund_info = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 1"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_month += $l->money;
                    }
                    //积分
                    $refund_infox = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 5"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_month += $l->consumptionIntegral / 100;
                    }
                    $totalRefunds += $totalrefund_month;
                    array_push($monthArray, array('totalrefund' => $totalrefund_month, 'num' => $smonth . "月"));
                }
                $syear = $syear + 1;
                if ($syear == $eyear) {
                    for ($p = 1; $p <= $emonth; $p++) {
                        if ($p != 12) {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . ($p + 1) . "-1 00:00:00";
                        } else {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . $p . "-31 23:59:59";
                        }
                        $totalrefund_month = 0;
                        //钱
                        $refund_info = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 1"));
                        foreach ($refund_info as $k => $l) {
                            $totalrefund_month += $l->money;
                        }
                        //积分
                        $refund_infox = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 5"));
                        foreach ($refund_info as $k => $l) {
                            $totalrefund_month += $l->consumptionIntegral / 100;
                        }
                        $totalRefunds += $totalrefund_month;
                        array_push($monthArray, array('totalrefund' => $totalrefund_month, 'num' => $p . "月"));
                    }
                } else if ($syear < $eyear) {
                    $totalRefunds = 0;
                    array_push($monthArray, array('totalIncome' => 0, 'num' => ""));
                }
            }
            array_push($monthArray, $totalRefunds);
            $json_string = json_encode($monthArray);
            echo $json_string;
        }
        //按天统计
        if ($time == "day") {
            $dayArray = array();
            $year = date("Y", strtotime($starttime));
            $smonth = date("m", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $sday = date("d", strtotime($starttime));
            $eday = date("d", strtotime($endtime));



            //按日统计
            //同一个月
            if ($smonth == $emonth) {
                for ($i = $sday; $i <= $eday; $i++) {
                    $stime = $year . "-" . $smonth . "-" . $i . " 00:00:00";
                    $etime = $year . "-" . $smonth . "-" . ($i + 1) . " 00:00:00";
                    $totalrefund_day = 0;
                    //钱
                    $refund_info = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 1"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_day += $l->money;
                    }
                    //积分
                    $refund_infox = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 5"));
                    foreach ($refund_info as $k => $l) {
                        $totalrefund_day += $l->consumptionIntegral / 100;
                    }
                    $totalRefunds += $totalrefund_day;
                    array_push($dayArray, array('totalrefund' => $totalrefund_day, 'num' => (int) $i));
                }
            }//不是同一个月 
            else if ($smonth + 1 == $emonth) {
                $totalRefunds = 0;
                array_push($dayArray, array('totalrefund' => 0, 'num' => ""));
            }
            array_push($dayArray, $totalRefunds);
            $json_string = json_encode($dayArray);
            echo $json_string;
        }
    }

    /*     * *************************退款统计***************************** */
    /*     * *************************服务人数统计***************************** */

    //查询今天服务人数
    public function actionsearchTodayservice() {
        $leftContent = $this->getLeftContent();

        $storeid = $_POST["storeid"];
        $printor_model = printor::model();
        $printor_info = $printor_model->findAll(array('condition' => "_storeid=$storeid"));
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $totalserviceArray = array();

        $date = date('Y-m-d');
        $tomorrow = date("Y-m-d", strtotime("+1 day"));
        $totalservices = 0;
        if ($printor_info) {
            foreach ($printor_info as $l => $y) {
                $machineId = $y->machineId;
                $criteria = new CDbCriteria;
                $criteria->addCondition("status=1"); //查询条件，即where id =1  
                $criteria->addCondition("marchineId='$machineId'"); //查询条件，即where id =1  
                $criteria->addBetweenCondition('printTime', $date, $tomorrow); //between1 and 4   
                $subbusiness_info = $subbusiness_model->findAll($criteria);

                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase >= 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$date' AND starttime <='$tomorrow'"));
                $totalservice = count($subbusiness_info) + count($prbusiness_info);
                $totalservices += $totalservice;
                array_push($totalserviceArray, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalservice' => $totalservice));
            }
            array_push($totalserviceArray, $totalservices);
            $json_string = json_encode($totalserviceArray);
            echo $json_string;
        }
    }

    //查询本月服务人数
    public function actionsearchMonthservice() {

        $leftContent = $this->getLeftContent();
        $storeid = $_POST["storeid"];
        $printor_model = printor::model();
        $printor_info = $printor_model->findAll(array('condition' => "_storeid=$storeid"));
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $totalserviceArray = array();
        $totalservices = 0;
        $date = date('Y-m', strtotime("+1 month"));
        $lastmonth = date("Y-m");
        $date = $date . '-01';
        $lastmonth = $lastmonth . '-01';
        if ($printor_info) {
            foreach ($printor_info as $l => $y) {
                $machineId = $y->machineId;
                $criteria = new CDbCriteria;
                $criteria->addCondition("status=1"); //查询条件，即where id =1  
                $criteria->addCondition("marchineId='$machineId'"); //查询条件，即where id =1  
                $criteria->addBetweenCondition('printTime', $lastmonth, $date); //between1 and 4   
//                $sql = "select * from tbl_subbusiness where status =1 && marchineId = '$machineId' && printTime between '$date' AND '$tomorrow'";
                $subbusiness_info = $subbusiness_model->findAll($criteria);

                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase >= 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$lastmonth' AND starttime <='$date'"));
                $totalservice = count($subbusiness_info) + count($prbusiness_info);
                $totalservices += $totalservice;
                array_push($totalserviceArray, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalservice' => $totalservice));
            }
            array_push($totalserviceArray, $totalservices);
            $json_string = json_encode($totalserviceArray);
            echo $json_string;
        }
    }

    //按时间段查询服务人数
    public function actionsearchservice() {

        $leftContent = $this->getLeftContent();
        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $printor_model = printor::model();
        $printor_info = $printor_model->findAll(array('condition' => "_storeid=$storeid"));
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $totalserviceArray = array();
        $totalservices = 0;
        $date = $endtime;
        $lastmonth = $starttime;

        if ($printor_info) {
            foreach ($printor_info as $l => $y) {
                $machineId = $y->machineId;
                $criteria = new CDbCriteria;
                $criteria->addCondition("status=1"); //查询条件，即where id =1  
                $criteria->addCondition("marchineId='$machineId'"); //查询条件，即where id =1  
                $criteria->addBetweenCondition('printTime', $lastmonth, $date); //between1 and 4   
                $subbusiness_info = $subbusiness_model->findAll($criteria);

                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase >= 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$lastmonth' AND starttime <='$date'"));
                $totalservice = count($subbusiness_info) + count($prbusiness_info);
                $totalservices += $totalservice;
                array_push($totalserviceArray, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalservice' => $totalservice, "leftContent" => $leftContent));
            }
            array_push($totalserviceArray, $totalservices);
            $json_string = json_encode($totalserviceArray);
            echo $json_string;
        }
    }

    //单个终端查看服务人数
    public function actionstatisticservicesone($machineId) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();

        if (isset(Yii::app()->session['adminuser'])) {
            $monthArray = array();
            $dayArray = array();

            $subbusiness_model = subbusiness::model();
            $prbusiness_model = prbusiness::model();
            $printor_model = printor::model();
            $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));

            $totalmonthservices = 0;
            $totaldayservices = 0;
            //按月统计
            $year = date("Y"); //2015-11    2015-10-26 10:32:09
            for ($i = 1; $i <= 12; $i++) {
                if ($i != 12) {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                } else {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . $i . "-31 23:59:59";
                }
                $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));

                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));

                $totalmonthservice = count($subbusiness_info) + count($prbusiness_info);
                $totalmonthservices += $totalmonthservice;
                array_push($monthArray, array('totalmonthservice' => $totalmonthservice));
            }

            //按日统计
            $day = date("Y-m"); //2015-11
            $fate = date("t"); //本月有多少天
            $month = date("m"); //11月
            for ($i = 1; $i <= $fate; $i++) {
                $stime = $day . "-" . $i . " 00:00:00";
                $etime = $day . "-" . ($i + 1) . " 00:00:00";


                $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));

                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));

                $totaldayservice = count($subbusiness_info) + count($prbusiness_info);
                $totaldayservices += $totaldayservice;
                array_push($dayArray, array('totaldayservice' => $totaldayservice, 'day' => $i));
            }

            $this->renderPartial('statisticservicesone', array('monthArray' => $monthArray, 'schoolname' => $printor_info->printorName, "year" => $year, "dayArray" => $dayArray, "machineId" => $machineId, 'leftContent' => $leftContent, 'recommend' => $recommend, "totalmonthservices" => $totalmonthservices, "totaldayservices" => $totaldayservices));
        } else
            $this->redirect('./index.php?r=platform');
    }

    //单个终端查询
    public function actionsearch_serviceStatisticsone() {

        $machineId = $_POST["machineId"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $time = $_POST["time"];
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $printor_model = printor::model();
        $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));
        $totalservices = 0;
        //按月统计
        if ($time == "month") {
            $monthArray = array();
            $smonth = date("m", strtotime($starttime));
            $syear = date("Y", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $eyear = date("Y", strtotime($endtime));

            if ($smonth <= $emonth)
                $day = $emonth - $smonth;
            if ($smonth > $emonth)
                $day = 12 - $smonth + $emonth;

            if ($syear == $eyear) {
                for ($smonth; $smonth <= $emonth; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));

                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));

                    $totalmonthservice = count($subbusiness_info) + count($prbusiness_info);
                    $totalservices += $totalmonthservice;
                    array_push($monthArray, array('totalservices' => $totalmonthservice, 'num' => $smonth . "月"));
                }
            } else if ($syear < $eyear) {
                for ($smonth; $smonth <= 12; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));

                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));

                    $totalmonthservice = count($subbusiness_info) + count($prbusiness_info);

                    $totalservices += $totalmonthservice;
                    array_push($monthArray, array('totalservices' => $totalmonthservice, 'num' => $smonth . "月"));
                }
                $syear = $syear + 1;
                if ($syear == $eyear) {
                    for ($p = 1; $p <= $emonth; $p++) {
                        if ($p != 12) {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . ($p + 1) . "-1 00:00:00";
                        } else {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . $p . "-31 23:59:59";
                        }
                        $totalPage = 0;
                        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));

                        $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));

                        $totalmonthservice = count($subbusiness_info) + count($prbusiness_info);

                        $totalservices += $totalmonthservice;
                        array_push($monthArray, array('totalservices' => $totalmonthservice, 'num' => $p . "月"));
                    }
                } else if ($syear < $eyear) {
                    $totalservices = 0;
                    array_push($monthArray, array('totalservices' => 0, 'num' => ""));
                }
            }
            array_push($monthArray, $totalservices);
            $json_string = json_encode($monthArray);
            echo $json_string;
        }
        //按天统计
        if ($time == "day") {
            $dayArray = array();
            $year = date("Y", strtotime($starttime));
            $smonth = date("m", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $sday = date("d", strtotime($starttime));
            $eday = date("d", strtotime($endtime));

            //按日统计
            //同一个月
            if ($smonth == $emonth) {
                for ($i = $sday; $i <= $eday; $i++) {
                    $stime = $year . "-" . $smonth . "-" . $i . " 00:00:00";
                    $etime = $year . "-" . $smonth . "-" . ($i + 1) . " 00:00:00";

                    $criteria = new CDbCriteria;
                    $criteria->addCondition("status=1"); //查询条件，即where id =1  
                    $criteria->addCondition("marchineId='$machineId'"); //查询条件，即where id =1  
                    $criteria->addBetweenCondition('printTime', $stime, $etime); //between1 and 4   
                    $subbusiness_info = $subbusiness_model->findAll($criteria);

                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase >= 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));
                    $totalservice = count($subbusiness_info) + count($prbusiness_info);
                    $totalservices += $totalservice;
                    array_push($dayArray, array('totalservices' => $totalservice, 'num' => (int) $i));
                }
            }//不是同一个月 
            else if ($smonth + 1 == $emonth) {
                $totalservices = 0;
                array_push($dayArray, array('totalservices' => 0, 'num' => ""));
            }
            array_push($dayArray, $totalservices);
            $json_string = json_encode($dayArray);
            echo $json_string;
        }
    }

    /*     * *************************服务人数统计***************************** */
    /*     * *************************打印纸张统计***************************** */

    //查询今天用纸量
    public function actionsearchToday() {
        $leftContent = $this->getLeftContent();

        $storeid = $_POST["storeid"];
        $administrator_model = administrator::model();
        $schoolname = store::model()->find("storeid=$storeid")->storename;
        $printor_model = printor::model();
        $printor_info = $printor_model->findAll(array('condition' => "_storeid=$storeid"));
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $schoolArray = array();

        $date = date('Y-m-d');
        $tomorrow = date("Y-m-d", strtotime("+1 day"));
        $totalPages = 0;

        if ($printor_info) {
            foreach ($printor_info as $l => $y) {
                $totalPage = 0;
                $machineId = $y->machineId;
                $criteria = new CDbCriteria;
                $criteria->addCondition("status=1"); //查询条件，即where id =1  
                $criteria->addCondition("marchineId='$machineId'"); //查询条件，即where id =1  
                $criteria->addBetweenCondition('printTime', $date, $tomorrow); //between1 and 4   
//                $sql = "select * from tbl_subbusiness where status =1 && marchineId = '$machineId' && printTime between '$date' AND '$tomorrow'";
                $subbusiness_info = $subbusiness_model->findAll($criteria);
                foreach ($subbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printSet);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $totalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $totalPage +=(int) $filePa;
                        }
                    }
                }
                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase >= 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$date' AND starttime <='$tomorrow'"));
                foreach ($prbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printset);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $totalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $totalPage +=(int) $filePa;
                        }
                    }
                }
                $totalPages += $totalPage;
                array_push($schoolArray, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalPage' => $totalPage, "leftContent" => $leftContent));
            }
            array_push($schoolArray, $totalPages);
            $json_string = json_encode($schoolArray);
            echo $json_string;
        }
    }

//查询本月用纸量
    public function actionsearchMonth() {

        $leftContent = $this->getLeftContent();
        $storeid = $_POST["storeid"];
        $printor_model = printor::model();
        $printor_info = $printor_model->findAll(array('condition' => "_storeid=$storeid"));
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $schoolArray = array();

        $date = date('Y-m', strtotime("+1 month"));
        $lastmonth = date("Y-m");
        $date = $date . '-01';
        $lastmonth = $lastmonth . '-01';
        $totalPages = 0;

        if ($printor_info) {
            foreach ($printor_info as $l => $y) {
                $totalPage = 0;
                $machineId = $y->machineId;
                $criteria = new CDbCriteria;
                $criteria->addCondition("status=1"); //查询条件，即where id =1  
                $criteria->addCondition("marchineId='$machineId'"); //查询条件，即where id =1  
                $criteria->addBetweenCondition('printTime', $lastmonth, $date); //between1 and 4   
//                $sql = "select * from tbl_subbusiness where status =1 && marchineId = '$machineId' && printTime between '$date' AND '$tomorrow'";
                $subbusiness_info = $subbusiness_model->findAll($criteria);
                foreach ($subbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printSet);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $totalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $totalPage +=(int) $filePa;
                        }
                    }
                }
                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase >= 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$lastmonth' AND starttime <='$date'"));
                foreach ($prbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printset);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $totalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $totalPage +=(int) $filePa;
                        }
                    }
                }
                $totalPages += $totalPage;
                array_push($schoolArray, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalPage' => $totalPage));
            }
            array_push($schoolArray, $totalPages);
            $json_string = json_encode($schoolArray);
            echo $json_string;
        }
    }

//按时间段查询用纸量
    public function actionsearch() {

        $leftContent = $this->getLeftContent();
        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $printor_model = printor::model();
        $printor_info = $printor_model->findAll(array('condition' => "_storeid=$storeid"));
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $schoolArray = array();

        $date = $endtime;
        $lastmonth = $starttime;
        $totalPages = 0;

        if ($printor_info) {
            foreach ($printor_info as $l => $y) {
                $totalPage = 0;
                $machineId = $y->machineId;
                $criteria = new CDbCriteria;
                $criteria->addCondition("status=1"); //查询条件，即where id =1  
                $criteria->addCondition("marchineId='$machineId'"); //查询条件，即where id =1  
                $criteria->addBetweenCondition('printTime', $lastmonth, $date); //between1 and 4   
                $subbusiness_info = $subbusiness_model->findAll($criteria);
                foreach ($subbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printSet);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $totalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $totalPage +=(int) $filePa;
                        }
                    }
                }
                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase >= 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$lastmonth' AND starttime <='$date'"));
                foreach ($prbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printset);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $totalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $totalPage +=(int) $filePa;
                        }
                    }
                }
                $totalPages += $totalPage;
                array_push($schoolArray, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalPage' => $totalPage));
            }
            array_push($schoolArray, $totalPages);
            $json_string = json_encode($schoolArray);
            echo $json_string;
        }
    }

//单个终端查看详情
    public function actionstatisticPapersone($machineId) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();

        if (isset(Yii::app()->session['adminuser'])) {
            $monthArray = array();
            $dayArray = array();

            $subbusiness_model = subbusiness::model();
            $prbusiness_model = prbusiness::model();
            $printor_model = printor::model();
            $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));

            //按月统计
            $year = date("Y"); //2015-11    2015-10-26 10:32:09
            $monthtotalPages = 0;

            for ($i = 1; $i <= 12; $i++) {
                if ($i != 12) {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                } else {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . $i . "-31 23:59:59";
                }
                $totalPage = 0;

                $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));
                foreach ($subbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printSet);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $totalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $totalPage +=(int) $filePa;
                        }
                    }
                }
                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));
                foreach ($prbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printset);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $totalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $totalPage +=(int) $filePa;
                        }
                    }
                }
                $monthtotalPages += $totalPage;
                array_push($monthArray, array('totalPage' => $totalPage));
            }

            //按日统计
            $day = date("Y-m"); //2015-11
            $fate = date("t"); //本月有多少天
            $month = date("m"); //11月
            $daytotalPages = 0;

            for ($i = 1; $i <= $fate; $i++) {

                $stime = $day . "-" . $i . " 00:00:00";
                $etime = $day . "-" . ($i + 1) . " 00:00:00";

                $daytotalPage = 0;
                $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));
                foreach ($subbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printSet);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $daytotalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $daytotalPage +=(int) $filePa;
                        }
                    }
                }
                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));
                foreach ($prbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printset);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $daytotalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $daytotalPage +=(int) $filePa;
                        }
                    }
                }
                $daytotalPages += $daytotalPage;
                array_push($dayArray, array('totalPage' => $daytotalPage, 'day' => $i));
            }

            $this->renderPartial('statisticPapersone', array('monthArray' => $monthArray, 'schoolname' => $printor_info->printorName, "year" => $year, "dayArray" => $dayArray, "machineId" => $machineId, 'leftContent' => $leftContent, 'recommend' => $recommend, "monthtotalPages" => $monthtotalPages, "daytotalPages" => $daytotalPages));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    //按时间段查询
    public function actionsearch_terninalStatisticsone() {

        $machineId = $_POST["machineId"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $time = $_POST["time"];
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $printor_model = printor::model();
        $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));
        $totalPages = 0;
        //按月统计
        if ($time == "month") {
            $monthArray = array();
            $smonth = date("m", strtotime($starttime));
            $syear = date("Y", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $eyear = date("Y", strtotime($endtime));

            if ($smonth <= $emonth)
                $day = $emonth - $smonth;
            if ($smonth > $emonth)
                $day = 12 - $smonth + $emonth;
            if ($syear == $eyear) {
                for ($smonth; $smonth <= $emonth; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalPage = 0;
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));
                    foreach ($subbusiness_info as $q => $w) {
                        $filePage = explode(',', $w->printSet);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPage +=(int) $filePa;
                            }
                        }
                    }
                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));
                    foreach ($prbusiness_info as $q => $w) {
                        $filePage = explode(',', $w->printset);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPage +=(int) $filePa;
                            }
                        }
                    }
                    $totalPages += $totalPage;
                    array_push($monthArray, array('totalPage' => $totalPage, 'num' => $smonth . "月"));
                }
            } else if ($syear < $eyear) {
                for ($smonth; $smonth <= 12; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalPage = 0;
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));
                    foreach ($subbusiness_info as $q => $w) {
                        $filePage = explode(',', $w->printSet);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPage +=(int) $filePa;
                            }
                        }
                    }
                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));
                    foreach ($prbusiness_info as $q => $w) {
                        $filePage = explode(',', $w->printset);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPage +=(int) $filePa;
                            }
                        }
                    }
                    $totalPages += $totalPage;
                    array_push($monthArray, array('totalPage' => $totalPage, 'num' => $smonth . "月"));
                }
                $syear = $syear + 1;
                if ($syear == $eyear) {
                    for ($p = 1; $p <= $emonth; $p++) {
                        if ($p != 12) {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . ($p + 1) . "-1 00:00:00";
                        } else {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . $p . "-31 23:59:59";
                        }
                        $totalPage = 0;
                        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));
                        foreach ($subbusiness_info as $q => $w) {
                            $filePage = explode(',', $w->printSet);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPage +=(int) $filePa;
                                }
                            }
                        }
                        $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));
                        foreach ($prbusiness_info as $q => $w) {
                            $filePage = explode(',', $w->printset);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPage +=(int) $filePa;
                                }
                            }
                        }
                        $totalPages += $totalPage;
                        array_push($monthArray, array('totalPage' => $totalPage, 'num' => $p . "月"));
                    }
                } else if ($syear < $eyear) {
                    $totalPages = 0;
                    array_push($monthArray, array('totalPage' => 0, 'num' => ""));
                }
            }
            array_push($monthArray, $totalPages);
            $json_string = json_encode($monthArray);
            echo $json_string;
        }
        //按天统计
        if ($time == "day") {
            $dayArray = array();
            $year = date("Y", strtotime($starttime));
            $smonth = date("m", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $sday = date("d", strtotime($starttime));
            $eday = date("d", strtotime($endtime));



            //按日统计
            //同一个月
            if ($smonth == $emonth) {
                for ($i = $sday; $i <= $eday; $i++) {
                    $stime = $year . "-" . $smonth . "-" . $i . " 00:00:00";
                    $etime = $year . "-" . $smonth . "-" . ($i + 1) . " 00:00:00";
                    $daytotalPage = 0;
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "status = 1 AND marchineId = '$machineId' AND printTime >='$stime' AND printTime <='$etime'"));
                    foreach ($subbusiness_info as $q => $w) {
                        $filePage = explode(',', $w->printSet);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $daytotalPage += 1;
                            } else {
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $daytotalPage +=(int) $filePa;
                            }
                        }
                    }
                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "exit_phrase = 6 AND machineId = '$machineId' AND printset != '' AND starttime >='$stime' AND starttime <='$etime'"));
                    foreach ($prbusiness_info as $q => $w) {
                        $filePage = explode(',', $w->printset);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $daytotalPage += 1;
                            } else {
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $daytotalPage +=(int) $filePa;
                            }
                        }
                    }
                    $totalPages += $daytotalPage;
                    array_push($dayArray, array('totalPage' => $daytotalPage, 'num' => (int) $i));
                }
            }//不是同一个月 
            else if ($smonth + 1 == $emonth) {
                $totalPages += 0;
                array_push($dayArray, array('totalPage' => 0, 'num' => ""));
            }
            array_push($dayArray, $totalPages);
            $json_string = json_encode($dayArray);
            echo $json_string;
        }
    }

    /*     * *************************打印纸张统计***************************** */
}
