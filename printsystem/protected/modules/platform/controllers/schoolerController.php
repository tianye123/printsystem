<?php

include 'baseController.php';

class schoolerController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=platform/recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'school + school',
            'addschool + addschool',
            'deleteschool + deleteschool',
            'editInfo + editInfo',
        );
    }

    public function filterschool($filterChain) {
        $this->checkAccess("学校", $filterChain);
    }

    public function filteraddschool($filterChain) {
        $this->checkAccess("添加学校", $filterChain);
    }

    public function filterdeleteschool($filterChain) {
        $this->checkAccess("删除学校", $filterChain);
    }

    public function filtereditInfo($filterChain) {
        $this->checkAccess("编辑信息", $filterChain);
    }

    /*     * ************** 学校列表界面 start ************** */

    public function actionschool() {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagSchool = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除学校") {
                    $flagSchool = "true";
                }
            }
            $store_model = store::model();
            $store_info = $store_model->findAll(array('order' => "storeid DESC"));
            $this->renderPartial('school', array('store_info' => $store_info, 'flagSchool' => $flagSchool, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

    /*     * ************** 学校列表界面 end ************** */

    /*     * ************** 添加学校 start ************** */

    public function actionaddschool() {//平台添加院校同时设置其管理员账号        
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $store_model = new store();
            $administrator_model = new administrator();
            if (isset($_POST['schoolname'])) {
                $usernamee = $_POST['schoolusername'];
                $user = $administrator_model->find(array('condition' => "username='$usernamee'"));
                if (count($user) == 0) {
                    $store_model->storename = $_POST['schoolname'];
                    $store_model->storedescript = $_POST['schooldescript'];
                    $store_model->addtime = date('Y-m-d H:i:s', time());
                    $tempFile = $_FILES['schoollogo']['tmp_name'];

                    if ($store_model->save()) {
                        $newId = $store_model->attributes['storeid'];
                        $store_info = $store_model->find("storeid=$newId");
                        $attachment_after = substr(strrchr($_FILES['schoollogo']['name'], '.'), 1);  //文件的后缀名 docx

                        $fileName = $newId . '.' . $attachment_after;

                        $targetPath = './images/storelogo';
                        $targetFile = rtrim($targetPath, '/') . '/' . $fileName;

                        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));

//                        $uploadFile->saveAs('./images/storelogo/' . $fileName);
                        $store_info->storelogo = $fileName;
                        if ($store_info->save()) {
                            $administrator_model->username = $usernamee;
                            $administrator_model->_storeid = $newId;
                            $administrator_model->_roleid = 3; //院校管理员
                            $password = $_POST['schoolpassword'];
                            $administrator_model->password = md5($password);
                            $administrator_model->phone = $_POST['phone'];
                            $administrator_model->QQ = $_POST['QQ'];
                            $administrator_model->addtime = date('Y-m-d H:i:s', time());
                            if ($administrator_model->save()) {
                                $this->redirect('./index.php?r=platform/schooler/school');
                            }
                        }
                    }
                } else {
                    echo "<script>alert('用户名已存在,请重新输入！');</script>";
                }
            } else {
                $this->renderPartial('addschool', array('leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else
            $this->redirect('./index.php?r=platform');
    }

    /*     * ************** 添加学校 end ************** */

    /*     * ************** 删除院校 start************** */

    public function actiondeleteschool() {
        $storeid = $_POST["storeid"];
        $store_model = store::model();

        $store_info = $store_model->findByPk($storeid);
        $filename = './images/storelogo/' . $store_info->storelogo;

        $count = $store_model->deleteAll(array('condition' => "storeid = $storeid"));

        $administrator_model = administrator::model();
        $counts = $administrator_model->deleteAll(array('condition' => "_storeid = $storeid"));

        if (is_file($filename)) {
            if (unlink($filename) && $count > 0 && $counts > 0) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 删除院校 end************** */


    /*     * ************** 编辑院校信息 start************** */

    public function actioneditInfo($storeid) {

        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $administrator_model = administrator::model();
            $store_model = store::model();
            if (isset($_POST['schoolname'])) {
                $schoolId = $_POST["schoolId"];
                $store_info = $store_model->find("storeid = $schoolId");
                if (count($store_info) != 0) {
                    $store_info->storename = $_POST['schoolname'];
                    $store_info->storedescript = $_POST['schooldescript'];
                    if (isset($_FILES)) {
                        $tempFile = $_FILES['schoollogo']['tmp_name'];
                        $fileName = $schoolId . '.jpg';
                        $targetPath = './images/storelogo';
                        $targetFile = rtrim($targetPath, '/') . '/' . $fileName;
                        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));
                        $store_info->storelogo = $fileName;
                    }
                }
                $administrator_info = $administrator_model->find("_storeid = $schoolId");
                $username = $_POST['schoolusername'];
                $users = $administrator_model->find(array('condition' => "username='$username' AND _storeid !=$storeid"));

                if (count($users) != 0) {
                    echo "<script>alert('用户名已存在,请重新输入！');window.location.href='./index.php?r=platform/schooler/school&storeid='" . $schoolId . ";</script>";
                } else {
                    if (count($administrator_info) != 0) {
                        $administrator_info->username = $_POST['schoolusername'];
                        $password = $_POST['schoolpassword'];
                        $administrator_info->password = md5($password);
                        $administrator_info->phone = $_POST['phone'];
                        $administrator_info->QQ = $_POST['QQ'];
                    }
                }
                if ($store_info->save() && $administrator_info->save()) {
                    echo "<script>alert('保存成功！');window.location.href='./index.php?r=platform/schooler/school';</script>";
                } else {
                    echo "<script>alert('保存失败！');window.location.href='./index.php?r=platform/schooler/school';</script>";
                }
            } else {
                $administrator_info = administrator::model()->find("_storeid=$storeid");
                $store_info = $store_model->find("storeid = $storeid");
                $this->renderPartial('editInfo', array('administrator_info' => $administrator_info, 'store_info' => $store_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $this->redirect('./index.php?r=platform');
        }
    }

}
