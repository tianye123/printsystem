<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
                letter-spacing: 0;
            }
            #user-open{
                 background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            function deleteUsers(userid) {
                if ('<?php echo $flagUser; ?>' == "true") {
                if (confirm("确定删除？")) {
                    $.post("./index.php?r=platform/userManager/deleteUsers", {userid: userid}, function (datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "success")
                            window.location.href = "./index.php?r=platform/userManager/userInfo";
                    });
                } 
            } else if ('<?php echo $flagUser; ?>' == "false") {
                    window.location.href = './index.php?r=platform/nonPrivilege/index';
                }
            }
            $(function () {
                $('#alreadytable').dataTable({
                    "ServerSide": true,
                    "Processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "deferRender": true,
                    "ajax": {
                        "url": './index.php?r=platform/userManager/InfoToServerSideAjax',
                        "type": "POST"
                    },
//                    "ajax": './index.php?r=platform/userManager/InfoToServerSideAjax',
                    "stateSave": true,
                    "order": [[5, "desc"]],
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "数据加载中.......",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "搜索：",
                        "Processing": "正在加载..."
                    },
                    'columns': [
                        {"data": "userid", "visible": false, "orderble": false, "searchable": false},
                        {"data": "id", "orderble": true, "searchable": true, },
                        {"data": "username", "orderble": true, "searchable": true},
                        {"data": "phone", "orderble": true, "searchable": true},
                        {"data": "email", "orderble": true, "searchable": true, },
                        {"data": "registertime", "orderble": true, "searchable": true},
                        {"data": "record_info", "orderble": true, "searchable": true},
                        {"data": "userid",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);' " +
                                        "onclick='recordInfo(" + oData.userid + ")'><span class='label label-success' style='cursor:pointer'>查看</span></a>");
                            }
                        },
                        {"data": "username",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);' " +
                                        "onclick='fileInfo(\"" + oData.username + "\")'><span class='label label-success' style='cursor:pointer'>查看</span></a>");
                            }
                        },
                        {"data": "username",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);' " +
                                        "onclick='businessInfo(\"" + oData.username + "\")'><span class='label label-success' style='cursor:pointer'>查看</span></a>");
                            }
                        },
                        {"data": "userid",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);' " +
                                        "onclick='editInfo(" + oData.userid + ")'><span class='label label-success' style='cursor:pointer'>编辑</span></a>&nbsp")
                                        .append("<a href='javascript:void(0);' onclick='deleteUsers(" + oData.userid + ")'><span class='label label-success' style='cursor:pointer'>删除</span></a>");
                            }
                        },
                    ]
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
            });
            function recordInfo(userid)
            {
                window.location.href = "./index.php?r=platform/userManager/userstoreDetail&userid=" + userid;
            }
            function fileInfo(username)
            {
                window.location.href = "./index.php?r=platform/userManager/searchFile&userName=" + username + "&fileName";
            }
            function businessInfo(username)
            {
                window.location.href = "./index.php?r=platform/userManager/searchBusiness&userName=" + username + "&orderId";
            }
            function editInfo(userid)
            {
                window.location.href = "./index.php?r=platform/userManager/editUsers&userid=" + userid;
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>用户列表</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/userManager/userInfo">用户</a>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="alreadytable" width="100%">
                                <thead>
                                    <tr class="th">
                                        <th>用户序列</th>
                                        <th>序列</th>
                                        <th>用户名</th>
                                        <th>电话号码</th>
                                        <th>邮箱</th>
                                        <th>注册时间</th>
                                        <th>剩余积分</th>
                                        <th>积分流水</th>
                                        <TH>文件列表</TH>
                                        <TH>订单列表</TH>
                                        <TH>操作</TH>
                                    </TR>
                                </thead>
                            </table>
                        </DIV>
                    </DIV>     
                    <!-- FOOTER -->

                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    </div>
                    <!-- / END OF FOOTER -->
                </DIV>  
                <br>
            </DIV>

    </BODY>
</HTML>