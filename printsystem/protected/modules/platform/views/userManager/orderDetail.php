<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
           
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
               letter-spacing: 0;
            }
              #user-open{
                 background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
//            function deleteUsers(userid) {
//                if (confirm("确定删除？")) {
//                    $.post("./index.php?r=platform/userManager/deleteUsers", {userid: userid}, function(datainfo) {
//                        var data = eval("(" + datainfo + ")");
//                        if (data.data == "success")
//                            window.location.href = "./index.php?r=platform/userManager/userInfo";
//                    });
//                }
//            }
            $(function() {
                $('#alreadytable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
            });       
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <SECTION class="content-header">
                    <H1>订单详情</H1>
                    <OL class="breadcrumb">
                        <LI><A href="./index.php?r=platform"><I class="fa fa-dashboard"></I>首页</A></LI>
                        <LI class="active"><A href="./index.php?r=platform/userManager/business">订单列表</A></LI>
                        <LI class="active"><A onclick="history.go(0);" style="cursor: pointer;">订单详情</A></LI>

                    </OL>
                </SECTION> 

                 <div class="row">
                        <div class="col-lg-12">
                            <table class="table" id="alreadytable">
                            <TR >
                                <td style="text-align:left" colspan="8"><SPAN class="spans">订单查看</SPAN>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:red;">*以下显示订单最新状态</span>
                                    <div style="float:right;color: #15CFCD;padding: 3px"> 
                                        <!--<button type="button" class="btn btn-success" id="deleteOrder">删除</button>-->
                                    </DIV>
                                </td>
                            </TR>
                            <TR>
                                <td class="orderTop" colspan="8">
                                    <DIV style="float:left;margin-right: 20px">订单号：<?php echo $businessid_info->orderId ?></DIV>
                                    <DIV style="float:right;margin-right: 20px">验证码：<?php
                                        if (isset($businessid_info->verificationCode))
                                            echo "<sapn style = 'color:red'>" . $businessid_info->verificationCode . "</span>";
                                        else
                                            echo "无";
                                        ?></DIV>


                                    <HR style="margin-top:25px;margin-bottom: 7px;border: 1px dashed #EEE;">
                                    <DIV style="float:left">
                                        亲爱的用户，请在下单后尽快支付订单，支付完成后即可凭借手机验证码到终端打印，如果您有什么不满意，您可联系客服！
                                    </DIV>
                                </td>
                            </TR>
                            <TR>
                                <TD colspan="8">
                                    <div style="border: 1px dashed #BBBBBB;height:94px;color: #333;margin-left: auto;margin-right: auto;">
                                        <!--width: 125px-->
                                        <div style="margin-top: 40px;margin-left: 50px">

                                            <div style="float: left;margin-right: 35px;margin-top: -20px;text-align: center">
                                                <img src="./css/bt/choice.png">
                                                <div>选择打印文件</div>
                                            </div>
                                            <div style="float: left;margin-right: 35px;margin-top: -20px">
                                                <img src="./css/bt/jt.png">
                                            </div>
                                            <div style="float: left;margin-right: 35px;margin-top: -20px;text-align: center">
                                                <img src="./css/bt/order-gray.png">
                                                <div style="color:#FFCF75">确认订单</div>
                                            </div>
                                            <div style="float: left;margin-right: 35px;margin-top: -20px">
                                                <img src="./css/bt/jt.png">
                                            </div>
                                            <div style="float: left;margin-right: 35px;margin-top: -20px;text-align: center">
                                                <img src="./css/bt/pay.png">
                                                <div>选择支付</div>
                                            </div>
                                            <div style="float: left;margin-right: 35px;margin-top: -20px">
                                                <img src="./css/bt/jt.png">
                                            </div>
                                            <div style="float: left;margin-right: 35px;margin-top: -20px;text-align: center">
                                                <img src="./css/bt/print.png">
                                                <div>到终端打印</div>
                                            </div>





                                        </div>
                                    </div>
                                </TD>
                            </TR>
                            <TR>
                                <TD  class="orderTop" colspan="8">
                                    <DIV class="menuFont">订单详情</DIV>
                                </TD>
                            </TR>
                            <TR>
                                <TD colspan="4">
                                    处理时间
                                </TD>
                                <TD colspan="4">
                                    处理信息
                                </TD>    
                            </TR>
                            <TR>
                                <TD colspan="4">
                                    <?php echo $businessid_info->placeOrdertime ?>
                                </TD>
                                <TD colspan="4">
                                    您提交了订单，请完成支付和打印。
                                </TD>    
                            </TR>
                            <TR>
                                <TD  class="orderTop" colspan="8">
                                    <DIV class="menuFont">付款信息</DIV>
                                </TD>
                            </TR>
                            <TR>

                                <TD colspan="8">
                                    商家信息：重庆颇闰科技有限公司
                                </TD>   
                            </TR>
                            <TR>
                                <TD style="color:red" colspan="3">
                                    商品总金额：￥ <?php
                                    echo $businessid_info->paidMoney;
                                    ?> 元
                                </TD>
                                <TD style="color:red" colspan="3">
                                    应支付金额：￥ <?php
                                    echo $businessid_info->paidMoney;
                                    ?> 元
                                </TD>    
                                <TD style="color:red" colspan="2">
                                    <?php
                                    echo "消费积分：" . $businessid_info->consumptionIntegral . " 点";
                                    ?>
                                </TD>    
                            </TR>
                            <TR>
                                <TD  class="orderTop" colspan="8">
                                    <DIV class="menuFont">打印信息</DIV>
                                </TD>
                            </TR>
                            <TR>
                                <TD>文件名称</TD>
                                <TD>打印份数</TD>
                                <TD>打印页码</TD>
                                <TD>支付金额</TD>
                                <TD>支付状态</TD>
                                <TD>支付方式</TD>
                                <td>打印状态</td>
                                <td>打印终端</td>
                            </TR>

                            <?php
                            foreach ($attachmentArray as $K => $V) {
                                echo '<TR><TD>' . $V["attachmentname"] . '</TD>'
                                . '<TD><span style="color:red">' . $V["printNumber"] . '</span> 份</TD>'
                                . '<TD>第' . $V["printSet"] . '页</TD>'
                                . '<TD><span style="color:red">￥' . $V["paidMoney"] . '</span> 元</TD>';

                                if ($V["isrefund"] == 0) {
                                    if ($V["isPay"] == 0)
                                        echo '<td><span class ="ispay">未支付</span></td>';
                                    else if ($V["isPay"] == 1)
                                        echo '<td class ="ispay">已支付</td>';
                                    else
                                        echo '<td class ="ispay">未知的错误</td>';
                                }
                                else if ($V["isrefund"] == 1)
                                    echo '<td class ="ispay">已退款</td>';
                                else if ($V["isrefund"] == 2)
                                    echo '<td class ="ispay">退款中</td>';

                                if ($V["payType"] == "0")
                                    echo '<TD>线下支付</TD>';
                                if ($V["payType"] == "1")
                                    echo '<TD>支付宝</TD>';
                                else if ($V["payType"] == "2")
                                    echo '<TD>一卡通</TD>';
                                else if ($V["payType"] == "3")
                                    echo '<TD>投币</TD>';
                                else if ($V["payType"] == "4")
                                    echo '<TD>终端扫码</TD>';
                                else if ($V["payType"] == "5")
                                    echo '<TD>积分</TD>';
                                else if ($V["payType"] == "6")
                                    echo '<TD>积分+支付宝</TD>';
                                else if ($V["payType"] == "7")
                                    echo '<TD>微信</TD>';
                                else if ($V["payType"] == null)
                                    echo '<TD>无</TD>';

                                if ($V["status"] == "0")
                                    echo '<td>未打印</td>';
                                else if ($V["status"] == "1")
                                    echo '<td>已打印</td>';
                                else if ($V["status"] == "2")
                                    echo '<td>打印失败</td>';

                                $marchineid = $V["marchineId"];
                                if ($marchineid == NULL) {
                                    echo "<td>无</td>";
                                } else {
                                    $marchine_model = printor::model()->find(array('condition' => "machineId = '$marchineid'"));
                                    echo "<td>" . $marchine_model->printorName . "</td>";
                                }

                                echo'</TR>';
                            }
                            ?>
                            </TR>

                            <TR>
                                <TH style="text-align:right" colspan="8">
                                    <button type="button" class="btn btn-info" style="display:none" href="#payMeansmodal" id="rightPay">立即支付</button>
                                </TH>
                            </TR>
                        </TABLE>
                    </DIV>
                </DIV>
                <br>
            </DIV>
            <FOOTER class="main-footer"  style="text-align: center">
                <STRONG>Copyright © 2014-2015 <A href="http://www.cqutprint.com/">重庆颇闰科技</A>.</STRONG> All rights reserved.       
            </FOOTER>
    </BODY>
</HTML>