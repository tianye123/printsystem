<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            .th{
                padding-bottom: 10px;
                padding-top: 10px;
            }
            #file-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $('#alreadytable').dataTable({
                    "ServerSide": true,
                    "Processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "deferRender": true,
                    "ajax": './index.php?r=platform/filesAdmin/filesToserverSideAjax',
                    "StateSave": true,
                    "order": [[1, "asc"]],
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "数据加载中.......",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "搜索：",
                        "Processing": "正在加载...",
                    },
                    'columns': [
                        {"data": "attachmentid", "visible": false, "orderble": false, "searchable": false},
                        {"data": "id", "orderble": true, "searchable": true, "width": "50px"},
                        {"data": "name", "orderble": true, "searchable": true},
                        {"data": "attachment", "orderble": true, "searchable": true, "width": "250"},
                        {"data": "filenumber", "orderble": true, "searchable": true, },
                        {"data": "uploadtime", "orderble": true, "searchable": true},
                        {"data": "id",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);' " +
                                        "onclick='downloads(" + oData.attachmentid + ")'><span class='label label-success' style='cursor:pointer'>下载</span></a>&nbsp")
                                        .append("<a href='javascript:void(0);' onclick='deleteFile(" + oData.attachmentid + ")'><span class='label label-success' style='cursor:pointer'>删除</span></a>");
                            }
                        },
                    ],
                    "columnDefs": [
                        {"targets": [0], "data": "attachmentid", visible: false}
                    ],
                });

                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
            });
            function downloads(attachmentid)
            {
                window.location.href = "./index.php?r=platform/filesAdmin/download&attachmentid=" + attachmentid;
            }
            function deleteFile(attachmentid)
            {
                if ('<?php echo $flagFiles; ?>' == "true") {
                    if (confirm("确定删除这个文件吗？"))
                    {
                        $.post("./index.php?r=platform/filesAdmin/deleteFile", {attachmentid: attachmentid}, function (data) {
                            if (data.replace(/(^\s*)|(\s*$)/g, '') == "success")
                            {
                                alert("删除成功！");
                                window.location.href = './index.php?r=platform/filesAdmin/files';
                            } else
                            {
                                alert("删除失败！");
                            }
                        });
                    }
                } else if ('<?php echo $flagFiles; ?>' == "false") {
                    window.location.href = './index.php?r=platform/nonPrivilege/index';
                }
            }
        </script>
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>文件列表</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/filesAdmin/files">文件</a>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap">               
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="alreadytable" width="100%">
                                <thead>
                                    <tr class="th">
                                        <th>文件序列</th>
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>用户名</th>
                                        <th>文件</th>
                                        <th>文件页数</th>
                                        <th>上传时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                            </table>
                        </DIV>
                    </DIV>   
                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    </div>
                    <!-- / END OF FOOTER -->
                    <!-- FOOTER -->

                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    </div>
                    <!-- / END OF FOOTER -->
                </DIV>
                <br>
            </DIV>
        </DIV>
    </BODY>
</HTML>