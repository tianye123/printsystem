<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <?php echo $recommend; ?>
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-monitoring{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
              a{color:#9ea7b3!important;}
        </style>
        <script type="text/javascript">
            $(function() {
                $('#printortable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });
                   $("#terminal-open").css("display","block");
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>终端监控</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>终端
                        </li>
                         <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">终端监控</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table  id="printortable">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>终端ID</th>
                                        <th>终端名</th>
                                        <th>状态</th>
                                        <th>地址</th>
                                        <!--<th>展示图片</th>-->
                                        <th>剩余纸张</th>
                                        <th>所属学校</th>
                                        <th>所属组</th>
                                        <th>版本号</th>
                                        <!--                                                <th>剩余元</th>
                                                                                        <th>剩余角</th>
                                                                                        <th>找零器角</th>
                                                                                        <th>找零器元</th>
                                                                                        <th>硬币投币器</th>
                                                                                        <th>纸币投币器</th>
                                                                                        <th>刷卡器</th>-->
                                        <th>错误码</th>                                        
                                        <th>升级状态</th>
                                        <th>升级时间</th>
                                        <th>打印机</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($printor_info as $K => $V) { ?>
                                        <tr>
                                            <td style="padding-left: 13px;"><A><?php echo $K + 1; ?></A></td>
                                            <td><?php echo $V->machineId; ?></td>
                                            <td><?php echo $V->printorName; ?></td>
                                            <td>
                                                <?php
                                                $data = time();
                                                if ($data - $V->last_time > 120)
                                                    echo"关机";
                                                else
                                                    echo"开机";
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo $V->address;
                                                ?>
                                            </td>
    <!--                                            <td>
                                            <?php
                                            if ($V->picture == NULL)
                                                echo "无";
                                            else {
                                                echo '<a class="fancy "  href="./images/terninalPictures/' . $V->picture . '" data-fancybox-group="gallery">';
                                                echo '<img style = "width:20px;height:20px" class="thumbnails" src="./images/terninalPictures/' . $V->picture . '"/>';
                                                echo '</a>';
                                            }
                                            ?>
                                            </td>-->
                                            <td>
                                                <?php
                                                if ($V->paper_remain < 100)
                                                    echo '<span style="color:red">' . $V->paper_remain . '</span>';
                                                else
                                                    echo $V->paper_remain;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo store::model()->find("storeid=$V->_storeId")->storename;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($V->_groupID == "")
                                                    echo '暂无';
                                                else {
                                                    if ($V->_groupID == 0 || $V->_groupID == "") {
                                                        echo "暂无";
                                                    } else {
                                                        $groupName = group::model()->find("groupID = $V->_groupID")->groupName;
                                                        echo $groupName;
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo $V->version; ?>
                                            </td>

                                                                                                                                                        <!--                                                    <td><?php
                                            if ($V->yuan_remain < 50)
                                                echo '<SPAN style="color:red">' . $V->yuan_remain . '</SPAN>' . ' 张';
                                            else
                                                echo $V->yuan_remain . " 张";
                                            ?></td>
                                                                                                                                                                                                            <td><?php
                                            if ($V->jiao_remain < 50)
                                                echo '<SPAN style="color:red">' . $V->jiao_remain . '</SPAN>' . ' 张';
                                            else
                                                echo $V->jiao_remain . " 张";
                                            ?></td>
                                                                                                                                                                                                            <td><?php
                                            if ($V->jiao_changer == 0)
                                                echo'<SPAN class="label label-danger">未知状态或初始化状态</SPAN>';
                                            else if ($V->jiao_changer == 1)
                                                echo '<SPAN class="label label-danger">打开串口失败</SPAN>';
                                            else if ($V->jiao_changer == 2)
                                                echo '<SPAN class="label label-danger">自检失败</SPAN>';
                                            else if ($V->jiao_changer == 3)
                                                echo '<SPAN class="label label-danger">找零失败</SPAN>';
                                            else if ($V->jiao_changer == 4)
                                                echo '<SPAN class="label label-success">运行正常</SPAN>';
                                            ?></td>
                                                                                                                                                                                                            <td><?php
                                            if ($V->yuan_changer == 0)
                                                echo'<SPAN class="label label-danger">未知状态或初始化状态</SPAN>';
                                            else if ($V->yuan_changer == 1)
                                                echo '<SPAN class="label label-danger">打开串口失败</SPAN>';
                                            else if ($V->yuan_changer == 2)
                                                echo '<SPAN class="label label-danger">自检失败</SPAN>';
                                            else if ($V->yuan_changer == 3)
                                                echo '<SPAN class="label label-danger">找零失败</SPAN>';
                                            else if ($V->yuan_changer == 4)
                                                echo '<SPAN class="label label-success">运行正常</SPAN>';
                                            ?></td>
                                                                                                                                                                                                            <td><?php
                                            if ($V->coin_acceptor == 0)
                                                echo'<SPAN class="label label-danger">未知状态或初始化状态</SPAN>';
                                            else if ($V->coin_acceptor == 1)
                                                echo '<SPAN class="label label-success">运行正常</SPAN>';
                                            else if ($V->coin_acceptor == 2)
                                                echo '<SPAN class="label label-danger">打开失败</SPAN>';
                                            ?></td>
                                                                                                                                                                                                            <td><?php
                                            if ($V->note_acceptor == 0)
                                                echo'<SPAN class="label label-danger">未知状态或初始化状态</SPAN>';
                                            else if ($V->note_acceptor == 1)
                                                echo '<SPAN class="label label-success">运行正常</SPAN>';
                                            else if ($V->note_acceptor == 2)
                                                echo '<SPAN class="label label-danger">打开失败</SPAN>';
                                            ?></td>
                                                                                                                                                                                                            <td><?php
                                            if ($V->card_acceptor == 1)
                                                echo '<SPAN class="label label-success">运行正常</SPAN>';
                                            else {
                                                echo '<SPAN class="label label-danger">故障</SPAN>';
                                            }
                                            ?></td>-->

                                            <td>
                                                <?php
                                                echo $V->printer_errorcode;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($V->updatestate == 200)
                                                    echo "升级成功";
                                                else if ($V->updatestate == 201)
                                                    echo "无需升级";
                                                else if ($V->updatestate == 400)
                                                    echo "升级失败，其他未知错误";
                                                else if ($V->updatestate == 401)
                                                    echo "获取服务器版本失败";
                                                else if ($V->updatestate == 402)
                                                    echo "下载主程序文件失败";
                                                else if ($V->updatestate == 403)
                                                    echo "下载的主程序文件MD5校验不通过";
                                                else if ($V->updatestate == 404)
                                                    echo "解压失败";
                                                else if ($V->updatestate == 405)
                                                    echo "覆盖原程序失败";
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo $V->updatetime;
                                                ?>
                                            </td>

                                            <td><?php
                                                if ($V->printer_status == 0)
                                                    echo '<SPAN class="label label-success">运行正常</SPAN>';
                                                else if ($V->printer_status == 1)
                                                    echo '<SPAN class="label label-danger">自检失败</SPAN>';
                                                else if ($V->printer_status == 2)
                                                    echo '<SPAN class="label label-danger">打印出错</SPAN>';
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

