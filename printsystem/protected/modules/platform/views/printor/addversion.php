<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-version{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
                $("#save").click(function() {
                    if ($("#versionfile").val() == "" || $("#versionfile").val() == null)
                    {
                        reback();
                        $("#version-file").text("请上传版本文件！");
                        return false;
                    }
                    else if ($("#versionname").val() == "" || $("#versionname").val() == null)
                    {
                        reback();
                        $("#version-name").text("请输入版本名称！");
                        return false;
                    } else if ($("#vesionCode").val() == "" || $("#vesionCode").val() == null)
                    {
                        reback();
                        $("#version-num").text("请输入版本号！");
                        return false;
                    } else if ($("#vesionGroup").val() ==0)
                    {
                        reback();
                        $("#version-group").text("请选择所属组别！");
                        return false;
                    }
                    else
                    {
                        $("#version-file,#version-name,#version-num,#version-group").text("");
                        if (confirm("确认保存？"))
                            addversionform.submit();
                    }
                });
                $("#terminal-open").css("display", "block");
            });
            function reback() {
                $("#version-file,#version-name,#version-num,#version-group").text("*");

                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>新增版本</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/printor/printversion">终端版本</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">新增版本</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form role="form" id="addversionform" method="post" enctype="multipart/form-data"  class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="versionfile">版本文件:</label>    </div>
                                            <div class="col-sm-3"> <input type="file" id="versionfile" name="versionfile" style="outline:none;margin-top: 7px;">    </div>
                                            <div class="col-sm-3 star" style="margin-top: 13px;" id="version-file">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="versionname">版本名称:</label>    </div>
                                            <div class="col-sm-3"><input type="text" placeholder="" id="versionname" name ="versionname" class="form-control" placeholder="请输入版本名称">    </div>
                                            <div class="col-sm-3 star" id="version-name">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="versiondescript">版本简介:</label>    </div>
                                            <div class="col-sm-3"> <textarea rows="9" style="width:100%;color:#8FBDE6;text-indent:1em" id="versiondescript" name="versiondescript"></textarea>    </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="vesionCode">版本号:</label>    </div>
                                            <div class="col-sm-3"> <input type="email" placeholder="" id="vesionCode" name="vesionCode" class="form-control" placeholder="请输入版本号">    </div>
                                            <div class="col-sm-3 star" id="version-num">*</div>
                                        </div>  
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="group">所属分组:</label>    </div>
                                            <div class="col-sm-3"> <select id="vesionGroup" name="group" class="form-control">
                                                    <option value="0">******请选择******</option>
                                                    <?php
                                                    foreach ($group_info as $k => $l)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $l->groupID; ?>"><?php echo $l->groupName; ?></option>
<?php } ?>
                                                </select>    </div>
                                            <div class="col-sm-3 star" id="version-group">*</div>
                                        </div>
                                        <div class="form-group" style="padding-top: 20px;">
                                            <div class="col-sm-5"></div>

                                            <div class="col-sm-2"><button class="btn btn-success" type="button" id="addgroup" style="width: 95%;">增加分组</button></div>
                                            <div class="col-sm-1"> <button class="btn btn-info" type="button" id="save"  style="width:100%;">保存</button></div>

                                            <div class="col-sm-3 star"><span id="info"></span></div>
                                        </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

