<!DOCTYPE html>
<html>
    <head>
        <META content="IE=11.0000" http-equiv="X-UA-Compatible">
        <META charset="UTF-8">    
        <META name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> 
        <TITLE>重庆颇闰科技-后台管理系统</TITLE>   
        <!-- Le styles -->
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            #breadcrumb {
                width: 96.5%;
                margin-left:auto;
                margin-right: auto;
            }
            #index-open{
                 background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
        <!-- END OF RIGHT SLIDER CONTENT-->
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
            });
        </script>
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>重庆颇闰科技-后台管理平台<SMALL>Version 1.0</SMALL></H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                    </ul>
                </div>
                <!-- END OF BREADCRUMB -->
                <!--  DEVICE MANAGER -->
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="profit" id="profitClose">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="maki-commerical-building"></i>&#160;&#160;覆盖校园</span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span class="pull-left"><i class=" clock-position"></i>
                                    </span><?php echo $store_all; ?><b>/个</b>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="revenue" id="revenueClose">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="fontawesome-group"></i>&#160;&#160;会员量</span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span class="pull-left"><i class="gauge-position"></i>
                                    </span><?php echo $user_all; ?><b>/人</b>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="order" id="orderClose">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="entypo-skype-circled"></i>&#160;&#160;在外总积分</span>
                                    </h3>
                                </div>
                                <div class="value">
                                    </span><?php echo $totalPoints; ?><b>/分</b>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class=" member" id="memberClose">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="maki-beer"></i>
                                            &#160;&#160;终端总数量
                                        </span>
                                    </h3>
                                </div>
                                <div class="value">
                                    </span><?php echo count($printor_info); ?><b>/台</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-8">
                            <div id="siteClose" class="nest">
                                <div class="title-alt">
                                    <h6>
                                        <span class="icon icon-user"></span>&nbsp;新院校管理员
                                    </h6>
                                </div>
                                <div id="site" class="body-nest" style="min-height:296px;">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>                                        
                                                <?php foreach ($administrator_info as $k => $l) { ?>
                                                    <tr>
                                                        <td class="armada-devider">
                                                            <div class="armada">
                                                                <span style="background:#65C3DF">
                                                                    <span class="maki-commerical-building"></span>                                                                     
                                                                    <?php
                                                                    if ($l->_roleid == 1)
                                                                        echo '超级管理员';
                                                                    else
                                                                        echo store::model()->find("storeid = $l->_storeid")->storename;
                                                                    ?>
                                                                </span>
                                                                <p>
                                                                    <span class="fa fa-clock-o"></span>&nbsp;执责时间<i>
                                                                        <?php
                                                                        if ($l->addtime == "")
                                                                            echo "暂无";
                                                                        else
                                                                            echo $l->addtime;
                                                                        ?>
                                                                    </i>
                                                                </p>
                                                            </div>
                                                        </td>
                                                        <td class="driver-devider">
                                                            <?php
                                                            if ($l->_roleid == 1)
                                                                echo '<img class="armada-pic img-circle" alt="" src="./images/storelogo/logo.png">';
                                                            else
                                                                echo '<img class="armada-pic img-circle" alt="" src="./images/storelogo/' . store::model()->find("storeid = $l->_storeid")->storelogo . '">';
                                                            ?>
                                                            <h3><?php echo $l->username; ?></h3>
                                                            <br>
                                                            <p>
                                                                <?php
                                                                if ($l->_roleid == 1)
                                                                    echo '超级管理员';
                                                                else if ($l->_roleid == 3)
                                                                    echo '院校管理员';
                                                                ?>
                                                            </p>
                                                        </td>
                                                        <td class="progress-devider">
                                                            <span class="label">联系电话：
                                                                <?php
                                                                if ($l->phone == "")
                                                                    echo "暂无";
                                                                else
                                                                    echo $l->phone;
                                                                ?></span><br><br>
                                                            <span class="label">QQ：<?php
                                                                if ($l->QQ == "")
                                                                    echo "暂无";
                                                                else
                                                                    echo $l->QQ;
                                                                ?></span>
                                                            <!--<span class="label pull-right">Muchen</span>-->
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div id="RealTimeClose" class="nest">
                                <div class="title-alt">
                                    <h6>
                                        <span class="fontawesome-resize-horizontal"></span>&nbsp;新增院校
                                    </h6>
                                </div>
                                <div id="RealTime" style="min-height:296px;padding-top:20px;" class="body-nest">
                                    <ul class="direction">
                                        <?php foreach ($store_new as $K => $V) { ?>
                                            <li>
                                                <span class="direction-icon"><img src="./images/storelogo/<?php echo $V->storelogo; ?>" style="width:55px;"></span>
                                                <h3>
                                                    <span><?php echo $V->storename; ?></span>
                                                </h3>
                                                <p>投放终端：
                                                    <?php
                                                    echo count(printor::model()->findAll("_storeid = $V->storeid"));
                                                    ?></p>
                                                <p><i>加入时间</i>：&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php echo $V->addtime; ?></p>
                                            </li>
                                            <li class="divider"></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">

                                <div class="chart-dash">
                                    <div style="width:100%;height:200px;">
                                        <div class="temperature" STYLE="margin-top: -10PX;color:white;">
                                            <b>用户增长</b>
                                            <span>U</span>
                                            <span><b>G</b>
                                            </span>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="speed">
                                            <h2>平均日增量</h2>
                                            <h1>74
                                                <span>人/ 天</span>
                                            </h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="traffic-rates">
                                            <h4>上个月</h4>
                                            <h1>134
                                                <span>人</span>
                                            </h1>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="traffic-rates">
                                            <h4>本月至今</h4>
                                            <h1>234
                                                <span>人</span>
                                            </h1>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="weather-wrap">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="temperature"><b>收入统计</b>&nbsp;<small>重庆理工大学</small>
                                            <span>S</span>
                                            <span><b>I</b>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="weather-text">
                                            <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br><br> <br><br> <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="weather-dash">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <div class="daily-weather">
                                                <h2>星期一</h2>
                                                <h3>485
                                                    <span>￥</span>
                                                </h3>

                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="daily-weather">
                                                <h2>星期二</h2>
                                                <h3>281
                                                    <span>￥</span>
                                                </h3>

                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="daily-weather">
                                                <h2>星期三</h2>
                                                <h3>183
                                                    <span>￥</span>
                                                </h3>

                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="daily-weather">
                                                <h2>星期四</h2>
                                                <h3>380
                                                    <span>￥</span>
                                                </h3>

                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="daily-weather">
                                                <h2>星期五</h2>
                                                <h3>279
                                                    <span>￥</span>
                                                </h3>

                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="daily-weather">
                                                <h2>星期六</h2>
                                                <h3>382
                                                    <span>￥</span>
                                                </h3>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /END OF CONTENT -->



                    <!-- FOOTER -->
                    <div class="footer-space"></div>
                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.
                        </div>
                    </div>
                    <!-- / END OF FOOTER -->
                </div>
            </div>
        </div>
        <!--  END OF PAPER WRAP -->
    </body>
</html>

