<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>

        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }

            //checkbox 
            .checkboxFour {
                width: 20px;
                height: 20px;
                border: 1px #E5E5E5 solid;
                border-radius: 100%;
                position: relative;
            }
            .checkboxFour label {
                width: 14px;
                height: 14px;
                border-radius: 100px;

                -webkit-transition: all .5s ease;
                -moz-transition: all .5s ease;
                -o-transition: all .5s ease;
                -ms-transition: all .5s ease;
                transition: all .5s ease;
                cursor: pointer;
                position: static;
                top: 2px;
                left: 2px;
                z-index: 1;
                background-color:#FFFFFF;
                border: 1px #65C91B solid;
            }
            input[type=checkbox] {
                visibility: hidden;
            }
            #statistics-open{
                display: block;
            }

        </style>
        <script type="text/javascript">
            $(function() {
                $('#schooltable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });

                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });

            });

        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>统计</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">统计</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="schooltable">
                                <thead>
                                    <tr class="th">
                                        <th  style="padding-left: 10px;">序列</th>
                                        <th>院校名称</th>
                                        <th>加入时间</th>
                                        <th>操作</th></tr></thead>
                                <tbody>
                                    <?php foreach ($store_info as $K => $V) { ?>
                                        <tr>
                                            <td  style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->storename; ?></td>

                                            <td> <div class="sparkbar" data-color="#00a65a" data-height="20">
                                                    <?php echo $V->addtime; ?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php if ($type == "order") { ?>
                                                    <a href="./index.php?r=platform/statistics/terninalStatistics&storeid=<?php echo $V->storeid; ?>&type=<?php echo $type; ?>">
                                                        <span class="label label-success">订单统计</span>
                                                    </a>
                                                <?php } else if ($type == "income") { ?>
                                                    <a href="./index.php?r=platform/statistics/terninalStatistics&storeid=<?php echo $V->storeid; ?>&type=<?php echo $type; ?>">
                                                        <span class="label label-success">收入统计</span>
                                                    </a>
                                                <?php } else if ($type == "refund") { ?>
                                                    <a href="./index.php?r=platform/statistics/terninalStatistics&storeid=<?php echo $V->storeid; ?>&type=<?php echo $type; ?>">
                                                        <span class="label label-success">退款统计</span>
                                                    </a>
                                                <?php } else if ($type == "service") { ?>
                                                    <a href="./index.php?r=platform/statistics/terninalStatistics&storeid=<?php echo $V->storeid; ?>&type=<?php echo $type; ?>">
                                                        <span class="label label-success">服务人数统计</span>
                                                    </a>
                                                <?php } else if ($type == "printpaper") { ?>
                                                    <a href="./index.php?r=platform/statistics/terninalStatistics&storeid=<?php echo $V->storeid; ?>&type=<?php echo $type; ?>">
                                                        <span class="label label-success">打印纸张统计</span>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

