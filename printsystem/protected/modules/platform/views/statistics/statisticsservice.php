<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <script src="./css/bootstrap/highcharts.js" type="text/javascript"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            #statistics-open{
                display: block;
            }
            #statistics-people{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
                $('#servicetable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                //查询今天
                $("#searchToday").click(function() {
                    $.post("./index.php?r=platform/statistics/searchTodayservice", {storeid:<?php echo $storeid; ?>}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        var seridataArr = [];
//                        
                        for (var i = 0; i < data.length - 1; i++)
                        {
                            var dd = {};
                            dd.type = 'column';
                            dd.name = data[i]["printorName"];
                            dd.data = [data[i]["totalservice"]];
                            seridataArr.push(dd);
                        }
                        $('#columnLinePieChart').highcharts({
                            chart: {
                            },
                            title: {
                                text: '终端服务人数组合图(共' + data[data.length - 1] + '人）'
                            },
                            xAxis: {
                                categories: [
                                    '服务人数'
                                ]
                            },
                            tooltip: {
                                formatter: function() {
                                    var s;
                                    if (this.point.name) {
                                        s = '' +
                                                this.point.name + ': ' + this.y + ' 人';
                                    } else {
                                        s = '' +
                                                this.x + ': ' + this.y;
                                    }
                                    return s;
                                }
                            },
                            labels: {
                                items: [{
                                        html: '服务人数',
                                        style: {
                                            left: '40px',
                                            top: '0px',
                                            color: 'black'
                                        }
                                    }]
                            },
                            series: seridataArr
                        });
                    });
                });
                //查询本月
                $("#searchMonth").click(function() {
                    $.post("./index.php?r=platform/statistics/searchMonthservice", {storeid:<?php echo $storeid; ?>}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        var seridataArr = [];
//                        
                        for (var i = 0; i < data.length - 1; i++)
                        {
                            var dd = {};
                            dd.type = 'column';
                            dd.name = data[i]["printorName"];
                            dd.data = [data[i]["totalservice"]];
                            seridataArr.push(dd);
                        }
                        $('#columnLinePieChart').highcharts({
                            chart: {
                            },
                            title: {
                                text: '终端服务人数组合图(共' + data[data.length - 1] + '人）'
                            },
                            xAxis: {
                                categories: [
                                    '服务人数'
                                ]
                            },
                            tooltip: {
                                formatter: function() {
                                    var s;
                                    if (this.point.name) {
                                        s = '' +
                                                this.point.name + ': ' + this.y + ' 人';
                                    } else {
                                        s = '' +
                                                this.x + ': ' + this.y;
                                    }
                                    return s;
                                }
                            },
                            labels: {
                                items: [{
                                        html: '服务人数',
                                        style: {
                                            left: '40px',
                                            top: '0px',
                                            color: 'black'
                                        }
                                    }]
                            },
                            series: seridataArr
                        });
                    });
                });
                //查询时间段
                $("#searchs").click(function() {
                    var starttime = $("#starttime").val();
                    var endtime = $("#endtime").val();
                    if (starttime == "") {
                        alert("请输入开始时间");
                        return false;
                    }
                    if (endtime == "") {
                        alert("请输入结束时间");
                        return false;
                    }

                    $.post("./index.php?r=platform/statistics/searchservice", {storeid:<?php echo $storeid; ?>, starttime: starttime, endtime: endtime}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        var seridataArr = [];
//                        
                        for (var i = 0; i < data.length - 1; i++)
                        {
                            var dd = {};
                            dd.type = 'column';
                            dd.name = data[i]["printorName"];
                            dd.data = [data[i]["totalservice"]];
                            seridataArr.push(dd);
                        }
                        $('#columnLinePieChart').highcharts({
                            chart: {
                            },
                            title: {
                                text: '终端服务人数组合图(共' + data[data.length - 1] + '人）'
                            },
                            xAxis: {
                                categories: [
                                    '服务人数'
                                ]
                            },
                            tooltip: {
                                formatter: function() {
                                    var s;
                                    if (this.point.name) {
                                        s = '' +
                                                this.point.name + ': ' + this.y + ' 张';
                                    } else {
                                        s = '' +
                                                this.x + ': ' + this.y;
                                    }
                                    return s;
                                }
                            },
                            labels: {
                                items: [{
                                        html: '服务人数',
                                        style: {
                                            left: '40px',
                                            top: '0px',
                                            color: 'black'
                                        }
                                    }]
                            },
                            series: seridataArr
                        });
                    });
                });
                //查询全部
                $("#searchAll").click(function() {
                    $('#columnLinePieChart').highcharts({
                        chart: {
                        },
                        title: {
                            text: '终端服务人数组合图（共<?php echo $totalservices; ?>人）'
                        },
                        xAxis: {
                            categories: [
                                '服务人数'
//                            , '橘子', '梨子', '香蕉', '草莓'
                            ]
                        },
                        tooltip: {
                            formatter: function() {
                                var s;
                                if (this.point.name) {
                                    s = '' +
                                            this.point.name + ': ' + this.y + ' 张';
                                } else {
                                    s = '' +
                                            this.x + ': ' + this.y;
                                }
                                return s;
                            }
                        },
                        labels: {
                            items: [{
                                    html: '服务人数',
                                    style: {
                                        left: '40px',
                                        top: '0px',
                                        color: 'black'
                                    }
                                }]
                        },
                        series: [
<?php
foreach ($totalserviceArray as $K => $V)
{
    ?>
                                {
                                    type: 'column',
                                    name: '<?php echo $V["printorName"]; ?>',
                                    data: [<?php echo $V["totalservice"]; ?>]
                                },
<?php } ?>
                        ]
                    });
                });
//                初始化
                            $('#columnLinePieChart').highcharts({
                    chart: {
                    },
                            title: {
                            text: '终端服务人数组合图（共<?php echo $totalservices; ?>人）'
                            },
                            xAxis: {
                            categories: ['服务人数']
                            },
                            tooltip: {
                            formatter: function() {
                            var s;
                                    if (this.point.name) {
                            s = '' +
                                    this.point.name + ': ' + this.y + ' 张';
                            } else {
                            s = '' +
                                    this.x + ': ' + this.y;
                            }
                            return s;
                            }
                            },
                            labels: {
                            items: [{
                            html: '服务人数',
                                    style: {
                                    left: '40px',
                                            top: '0px',
                                            color: 'black'
                                    }
                            }]
                            },
                            series: [
<?php
foreach ($totalserviceArray as $K => $V)
{
    ?>
                             {
                              type: 'column',
                                            name: '<?php echo $V["printorName"]; ?>',
                                                            data: [<?php echo $V["totalservice"]; ?>]
                                                    },
                    <?php } ?>
                            ]
                    });
            });</script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
<?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3><?php echo $schoolname; ?> 服务人数统计 <span style="color:red;font-size: 15px">合计：<?php echo $totalservices; ?> 人</span></H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                         <li>统计
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/statistics/school&type=service">学校列表</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">服务人数统计</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row" style="margin-top: 10px">          
                                <div class="col-md-12">   
                                    <div class="box box-info">
                                        <table id="servicetable">
                                            <thead>
                                                <tr>
                                                    <th>序列</th>
                                                    <th>终端ID</th>
                                                    <th>终端名</th>
                                                    <th>地址</th>
                                                    <th>共服务人数</th>
                                                    <th>操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($totalserviceArray as $K => $V)
                                                {
                                                    ?>
                                                    <tr>
                                                        <td><a><?php echo $K + 1; ?></a></td>
                                                        <td><?php echo $V["machineId"]; ?></td>
                                                        <td><?php echo $V["printorName"]; ?></td>

                                                        <td>
    <?php
    echo $V["address"];
    ?>
                                                        </td>
                                                        <td>
    <?php
    echo $V["totalservice"];
    ?>
                                                        </td>
                                                        <td>
                                                            <a href="./index.php?r=platform/statistics/statisticservicesone&machineId=<?php echo $V['machineId']; ?>">
                                                                <span class="label label-success">查看详情</span>
                                                            </a>
                                                        </td>
                                                    </tr>
<?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row" style="margin-top: 10px">          
                                <div class="col-md-12"style="text-align: center;">   
                                    开始时间：<input id="starttime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                    结束时间：<input id="endtime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                    <input id="searchs" type="button" class="btn btn-success" value="查询">
                                    <input id="searchToday" type="button" class="btn btn-info" value="查看今天">
                                    <input id="searchMonth" type="button" class="btn btn-info" value="查看本月">
                                </div>                                
                            </div>
                            <div class="row" style="margin-top: 10px">          
                                <div class="col-md-12">      
                                    <div id="columnLinePieChart" style=" height: 600px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

