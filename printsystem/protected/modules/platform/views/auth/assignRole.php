<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
             #jurisdiction-open{
                display: block;
            }
            #permission-assignment{
                 background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#schooltable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
            });
            function deleteschool(storeid, storename) {
                if (confirm("确认删除 " + storename + " 院校?"))
                {
                    $.post("./index.php?r=platform/schooler/deleteschool", {storeid: storeid}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "success")
                        {
                            alert("删除成功！");
                            window.location.href = "./index.php?r=platform/admin/school";
                        }
                        else
                            alert("删除失败！");
                    });
                }
            }
              $(function () {
                $("#logout").click(function () {
                    if (confirm("确定注销？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
                $("#role .label.label-success").click(function () {
                    var adminID = $(this).parent("td").find("input").val();
                    var roleID = $(this).parents("tr").find('.action select').val();
                    $.post("./index.php?r=platform/auth/assign", {adminID: adminID, roleID: roleID}, function (data) {
                        if (data.data == "success")
                        {
                            alert("保存成功！");
                            window.location.href = "./index.php?r=platform/auth/assignRole";
                        } else if (data.data == "false")
                        {
                            alert("保存失败！");
                            window.location.href = "./index.php?r=platform/auth/assignRole";
                        }
                    }, 'json');

                });
                auto();


            });

            function auto() {
                var left = ($("td.action").width() - $("select.form-control").outerWidth()) / 2;
                $("select.form-control").css('margin-left', left - 10);
            }

        </script>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>权限分配</H3>
                    </div>
                   <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                         <li>权限</li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/auth/role">权限分配</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <TABLE class="no-margin" id="role">
                                <THEAD>
                                    <TR class="th">
                                        <TH class="num" style="padding-left: 10px;">序号</TH>
                                        <TH class="name">管理员名称</TH>
                                        <TH class="action" style="padding-left:50px;">选择角色</TH>
                                         <TH class="action" style="padding-left:50px;">选择院校</TH>
                                        <TH class="action" style="padding-left:12px;">操作</TH>
                                    </TR></THEAD>
                                <TBODY>
                                    <?php foreach ($admin_info as $K => $V) { ?>
                                        <TR>
                                            <TD class="num col-sm-2" style="padding-left: 13px;" ><?php echo $K + 1; ?></TD>
                                            <TD class="name col-sm-2">
                                                <?php echo $V->username; ?>
                                            </TD>
                                            <TD class="action">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select style="border:none;padding:7px 30px;outline:none;">
                                                            <option value="0">--请选择--</option>
                                                            <?php foreach ($role_info as $k => $l) { ?>
                                                                <option value="<?php echo $l->roleId; ?>" <?php echo ($l->roleId == $V->_roleid ? 'selected="selected"' : ''); ?>><?php
                                                                    echo $l->rolename;
                                                                    ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>   
                                                </div>
                                                <div class="form-group">
                                            </TD>
                                            <TD class="action">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <select style="border:none;padding:7px 30px;outline:none;">
                                                            <option value="0">--请选择--</option>
                                                            <?php foreach ($role_info as $k => $l) { ?>
                                                                <option value="<?php echo $l->roleId; ?>" <?php echo ($l->roleId == $V->_roleid ? 'selected="selected"' : ''); ?>><?php
                                                                    echo $l->rolename;
                                                                    ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>   
                                                </div>
                                                <div class="form-group">
                                            </TD>
                                            <TD class="action col-sm-2">
                                                <SPAN  class="label label-success" style="cursor: pointer">保存</SPAN>
                                                <input type="hidden" onclick = "save(<?php echo $V->administratorid; ?>)" value="<?php echo $V->administratorid;?>">

                                            </TD>
                                        </TR>
                                    <?php } ?>

                                </TBODY>
                            </TABLE>

                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

