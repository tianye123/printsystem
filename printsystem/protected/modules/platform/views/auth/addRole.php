<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 40px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #e6e6e6  solid;
                outline:none;
            }

            .content-wrap input[type="text"]{
                background-color: white;
                color:#00B9FF;
                border-radius: 3px;
                margin-left: 10px;
                border:1px #e6e6e6  solid;
                padding: 5px 10px;
                margin-top: 10px;
            }
             .content-wrap input[type="checkbox"]{
               width: 16px;
               height:16px;
            }
            #jurisdiction-open{
                display: block;
            }
            #roles-permissions{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定取消？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
                $("#save").click(function() {
                    if ($("#roleid").val() == "" || $("#roleid").val() == null)
                    {
                        alert("请输入角色名称！");
                        return false;
                    } else
                    {
                        if (confirm("确认添加？")) {
                            $(this).parent("form").submit();
//                            $.post("./index.php?r=platform/auth/addRole",{authparent:authparent[],authchild[]:authchild[]},function(data){
//                            if (data.code == 200) {
//                                alert("创建成功！");
//                            window.location.href = "./index.php?r=platform/auth/role";
//                            } else {
//                               alert("创建失败！");
//                            }
//                            },'json');
                        }
                    }
                });
                $(".authchild").click(function() {
                    if ($(this).parent().find("input[type='checkbox'].authchild:checked").length > 0) {
                        $(this).parent().prev("td").find(".auth").attr("checked", "checked");
                    } else {
                        $(this).parent().prev("td").find(".auth").removeAttr("checked");
                    }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>创建角色</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>权限</li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/auth/role">角色与权限</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/auth/addRole">创建角色</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">

                        <div class="col-lg-12">
                            角色名称: <INPUT type="text" name="role" class="text-blue " id="roleid"></div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <FORM method="post" name="roleform" action="./index.php?r=platform/auth/saveRole">
                                <TABLE class="no-margin">
                                    <TBODY>
                                        <?php
                                        $item_model = item::model();
                                        foreach ($authitem_info as $auth_info)
                                        {
                                            $auth_name = $auth_info->itemName;
                                            $id = $auth_info->itemId;
                                            $item_child = $item_model->findAll("parentItem = '$id'");
                                            ?>
                                            <TR style="padding-top:20px;padding-bottom:20px">
                                                <TD style="text-align:center;font-weight:bold;"><INPUT style="float:left;margin-left: 90px;" name="authparent[]" class="auth" type="checkbox" value="<?php echo $auth_name; ?>"><div style="float:left;margin-left: 8px;margin-top:2px;"><?php echo $auth_name . "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"; ?></div></TD>
                                                <TD style="text-align:justify;">
                                                    <?php
                                                    foreach ($item_child as $child_info)
                                                    {
                                                        $itemchild = $child_info->itemName;
                                                        ?>
                                                    <INPUT style="float:left;" name="authchild[]" class="auth" type="checkbox" value="<?php echo $itemchild; ?>"><div style="float:left;margin-left:5px;margin-top:2px;width: 120px;"><?php echo $itemchild . "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"; ?></div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </TD>
                                        </TR>
                                    </TBODY>
                                </TABLE>
                                <div style="margin-top:30px;">
                                    <INPUT name="save" id="save" type="submit" value="保存" class="btn btn-primary" style="outline:none;width: 170px;">
                                    <INPUT name="logout" id="logout" type="submit" value="取消" class="btn btn-primary" style="outline:none;width:110px;margin-left: 12px;">
                                </div>
                            </FORM>  

                        </div>

                        <!--  / DEVICE MaNaGER -->
                        <!-- FOOTER -->

                        <div id="footer">
                            <div class="devider-footer-left"></div>
                            <div class="time">
                                <p id="spanDate">
                                <p id="clock">
                            </div>
                            <div class="copyright">Copyright © 2014-2015
                                <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                        </div>
                        <!-- / END OF FOOTER -->
                    </div>
                </div>
                <!--  END OF PaPER WRaP -->
                </body>

                </html>

