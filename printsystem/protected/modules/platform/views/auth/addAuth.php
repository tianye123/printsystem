<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;

                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            input[type="submit"]{
                padding:0px;

            }
            #jurisdiction-open{
                display: block;
            }
            #roles-permissions{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $('#schooltable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function () {
                    if (confirm("确定注销？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });

                $("#childsave").click(function () {
                    if ($("#authidchild-name").val() == "" || $("#authid1").val() == null)
                    {
                        $("#auth-name-error1").text("请输入子权限名称！");
                        return false;
                    } else
                    {
                        if (confirm("确认添加？")) {
                            var parentItem = $(this).parents("form").find('#childselect').val();
                            var childTtem = $(this).parents("form").find('#authidchild-name').val();
                            $.post("./index.php?r=platform/auth/saveAuth", {parentItem: parentItem, childTtem: childTtem}, function (datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "success")
                                {
                                    alert("保存成功！");
                                    window.location.href = "./index.php?r=platform/auth/assignRole";
                                } else if (data.data == "false")
                                {
                                    alert("保存失败！");
                                    window.location.href = "./index.php?r=platform/auth/assignRole";
                                }
                            }, 'json');
                        }
                    }
                });
                $("#save").click(function () {
                    if ($("#authid").val() == "")
                    {

                        $("#auth-error2").text("请输入权限名称！");
                        return false;
                    } else
                    {
                        if (confirm("确认添加？")) {
                            var parentItem = $(this).parents("form").find('#authid').val();
                            var childTtem = $(this).parents("form").find('#authid1').val();
                            $.post("./index.php?r=platform/auth/saveAuth", {parentItem1: parentItem, childTtem1: childTtem}, function (datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "success")
                                {
                                    alert("保存成功！");
                                    window.location.href = "./index.php?r=platform/auth/assignRole";
                                } else if (data.data == "false")
                                {
                                    alert("保存失败！");
                                    window.location.href = "./index.php?r=platform/auth/assignRole";
                                }
                            }, 'json');
                        }
                    }
                });
            });
            function add1() {
                var input1 = document.createElement('input');
                input1.setAttribute('type', 'text');
                input1.setAttribute('name', 'organizers[]');
                input1.setAttribute('class', 'git');

                var btn1 = document.getElementById("addid");
                btn1.insertBefore(input1, null);
            }
            function auto() {
                var left = ($("td.action").width() - $("select.form-control").outerWidth()) / 2;
                $("select.form-control").css('margin-left', left - 10);
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>创建权限</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>权限</li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/auth/role">角色与权限</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/auth/addAuth">创建权限</a>
                        </li>
                    </ul>
                </DIV>

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div style="border-bottom:1px #f5f5f5 dashed;padding-bottom: 10px;margin-bottom: 40px;"><h4>新增子权限</h4></div>
                                <div class="name">
                                    <form role="form" id="addversionform" method="post" enctype="multipart/form-data"  class="form-horizontal">
                                        <!--                                        <div class="form-group school">
                                                                                    <div class="col-sm-5 control-label"><label>院校名称:</label>    </div>
                                                                                    <div class="col-sm-3"> <select name="parentauth" class="form-control"><option  value="0">--请选择院校--</option>
                                        <?php
                                        foreach ($item_info as $k => $l) {
                                            ?>
                                                                                                    <option name="parentauth" value="<?php echo $l->itemName; ?>" selected="selected"><?php
                                        echo $l->itemName;
                                            ?></option>
                                        <?php } ?>
                                                                                        </select>      </div>
                                                                                    <div class="col-sm-3 star" id="school-name-error1">*</div>
                                                                                </div>-->
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label>选择权限:</label>    </div>
                                            <div class="col-sm-3"> 
                                                <select name="parentauth" id="childselect" class="form-control"><option  value="0">--请选择权限--</option>
                                                    <?php
                                                    foreach ($item_info as $k => $l) {
                                                        ?>
                                                        <option name="parentauth"id="childoption" value="<?php echo $l->itemName; ?>" selected="selected"><?php
                                                        echo $l->itemName;
                                                        ?></option>
                                                        <?php } ?>
                                                </select>   
                                            </div>
                                            <div class="col-sm-3 star" id="auth-error1">*</div>
                                        </div>
                                        <div class="form-group namechild">
                                            <div class="col-sm-5 control-label"><label>子权限名称:</label>    </div>
                                            <div class="col-sm-3"><input  type="text" name="childauth" class="text-blue form-control" id="authidchild-name">    </div>
                                            <div class="col-sm-3 star"  id="auth-name-error1">*</div>
                                        </div>
                                        <div class="form-group" style="padding-top: 20px;">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-2"><INPUT name="childsave" id="childsave" type="submit" class="btn btn-success"  value="保存" style="width:80%;"></div>
                                            <div class="col-sm-1"><INPUT name="logout" id="logout" type="submit" class="btn btn-success" value="取消" style="width: 100%;"></div>
                                            <div class="col-sm-3 star"></div>
                                        </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div style="border-bottom:1px #f5f5f5 dashed;padding-bottom: 10px;margin-bottom: 40px;"><h4>添加新权限</h4></div>
                                <form role="form" id="addversionform1" method="post" enctype="multipart/form-data"  class="form-horizontal">
                                    <div class="form-group name">
                                        <div class="col-sm-5 control-label"> <label>院校名称:</label>    </div>
                                        <div class="col-sm-3"> <select name="parentauth" class="form-control"><option  value="0">--请选择院校--</option>
<?php
foreach ($item_info as $k => $l) {
    ?>
                                                    <option name="parentauth" value="<?php echo $l->itemName; ?>" selected="selected"><?php
                                                    echo $l->itemName;
                                                    ?></option>
                                                    <?php } ?>
                                            </select>      </div>
                                        <div class="col-sm-3 star" id="school-name-error2">*</div>
                                    </div>
                                    <div class="form-group name">
                                        <div class="col-sm-5 control-label"> <label>权限名称:</label>    </div>
                                        <div class="col-sm-3"> <input type="text" name="auth" class="text-blue form-control" id="authid">    </div>
                                        <div class="col-sm-3 star"  id="auth-error2">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"><label>子权限名称:</label>    </div>
                                        <div class="col-sm-3"><input  type="text" name="authchild" class="text-blue form-control" id="authid1" >    </div>
                                    </div>

                                    <div class="form-group" style="padding-top: 20px;">
                                        <div class="col-sm-5"></div>
                                        <div class="col-sm-2"> <INPUT name="save" id="save" type="submit" class="btn btn-success" value="保存" style="width: 80%;"></div>
                                        <div class="col-sm-1"><INPUT name="logout" id="logout" type="submit" class="btn btn-success" value="取消" style="width:100%;"></div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>

                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>

        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>


