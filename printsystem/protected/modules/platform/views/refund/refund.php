<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }

            //checkbox 
            .checkboxFour {
                width: 20px;
                height: 20px;
                border: 1px #E5E5E5 solid;
                border-radius: 100%;
                position: relative;
            }
            .checkboxFour label {
                width: 14px;
                height: 14px;
                border-radius: 100px;

                -webkit-transition: all .5s ease;
                -moz-transition: all .5s ease;
                -o-transition: all .5s ease;
                -ms-transition: all .5s ease;
                transition: all .5s ease;
                cursor: pointer;
                position: static;
                top: 2px;
                left: 2px;
                z-index: 1;
                background-color:#FFFFFF;
                border: 1px #65C91B solid;
            }
            input[type=checkbox] {
                visibility: hidden;
            }
            #refund-open{
                display: block;
            }
            #refund-list{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $('#refundtable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });

                //支付宝退款按钮
                $("#refund").click(function () {
                    var num = 0;
                    var refundIdd = "";
                    for (var i = 0; i < $("#refundtable").find(":checkbox").length; i++)
                    {
                        if ($("#refundtable").find(":checkbox").eq(i).is(':checked'))
                        {
                            refundIdd += $("#refundtable").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    refundIdd = refundIdd.substring(0, refundIdd.length - 1);
                    if (num == 0)
                    {
                        alert("请您选择需要退款的选项！");
                        return false;
                    }
                    window.open("./index.php?r=platform/refund/refund_zfb&refundIdd=" + refundIdd, "_blank");
                });
                //删除退款记录
                $("#deleterefund").click(function () {
                    var num = 0;
                    var refundIdd = "";
                    for (var i = 0; i < $("#refundtable").find(":checkbox").length; i++)
                    {
                        if ($("#refundtable").find(":checkbox").eq(i).is(':checked'))
                        {
                            refundIdd += $("#refundtable").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    refundIdd = refundIdd.substring(0, refundIdd.length - 1);
                    if ('<?php echo $flagRefund; ?>' == "true") {
                        if (num == 0)
                        {
                            alert("请您选择需要退款的选项！");
                            return false;
                        }
                        if (confirm("确定删除？"))
                        {
                            $.post("./index.php?r=platform/refund/deleterefund", {refundIdd: refundIdd}, function (datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "success")
                                {
                                    alert("删除成功！");
                                    window.location.href = "./index.php?r=platform/refund/refund";
                                } else
                                    alert("删除失败！");
                            });
                        }
                    } else if ('<?php echo $flagRefund; ?>' == "false") {
                        window.location.href = './index.php?r=platform/nonPrivilege/index';
                    }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>

        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">

                <!-- CONTENT -->


                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>退款列表</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>退款
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/refund/refund">退款列表</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="refundtable">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;"><A href="#" id="select_all">全选</A></th>
                                        <th>sessionId</th>
                                        <th>用户</th>
                                        <th>订单号</th>
                                        <th>退款金额</th>
                                        <th>退款积分</th>
                                        <th>支付方式</th>
                                        <th>订单交易号</th>
                                        <th>用户支付账号</th>
                                        <th>打印终端</th>
                                        <th>申请退款时间</th>
                                        <th>退款方式</th>
                                        <th>状态</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($refund_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td>
                                                <div class="checkboxFour">
                                                    <input type='checkbox'  value= '<?php echo $V->refundId; ?>'>
                                                    <label for="checkboxFourInput"></label>
                                                </div>
                                            </td>
                                            <td><a href="./index.php?r=platform/refund/prbdetails&_sessionId=<?php echo $V->_sessionId; ?>&trade_no=<?php echo $V->tborderId; ?>"><?php echo $V->_sessionId; ?></a></td>
                                            <td><?php
                                                if ($V->_userId != "") {
                                                    $user_mode = user::model()->find(array("condition" => "userid= '$V->_userId'"));
                                                    echo $user_mode->username;
                                                } else {
                                                    echo "无";
                                                }
                                                ?></td>
                                            <td><?php if ($V->_orderId != "") {
                                                    ?>
                                                    <a href="./index.php?r=platform/refund/orderDetail&businessid=<?php
                                                    $busines = business::model()->find("orderId = '$V->_orderId'");
                                                    echo base64_encode($busines->businessid);
                                                    ?>">
                                                           <?php
                                                       } else {
                                                           ?>
                                                        <a href="#">
                                                        <?php } ?>
                                                        <?php echo $V->_orderId; ?></a></td>
                                            <td><?php echo $V->money . "元"; ?></td>
                                            <td><?php echo $V->consumptionIntegral; ?></td>
                                            <td><?php
                                                if ($V->payType == 1)
                                                    echo "WEB支付宝";
                                                else if ($V->payType == 2)
                                                    echo "刷卡";
                                                else if ($V->payType == 4)
                                                    echo "终端支付宝";
                                                else if ($V->payType == 5)
                                                    echo "积分";
                                                else if ($V->payType == 6)
                                                    echo "积分+支付宝";
                                                else if ($V->payType == 7)
                                                    echo "终端微信";
                                                ?>
                                            </td>
                                            <td><?php echo $V->tborderId; ?></td>
                                            <td><?php echo $V->sellerAccounter; ?></td>
                                            <td><?php
                                                if ($V->_sessionId == "") {
                                                    $marchine_info = subbusiness::model()->find("subbusinessId = '$V->subbusinessId'");
                                                    $marchine_name = printor::model()->find("machineId = '$marchine_info->marchineId'");
                                                    if (count($marchine_name) != 0) {
                                                        echo $marchine_name->printorName;
                                                    } else {
                                                        echo "无";
                                                    }
                                                } else {
                                                    $marchine_info = prbusiness::model()->find("sessionId = '$V->_sessionId'");
                                                    $marchine_name = printor::model()->find("machineId = '$marchine_info->machineId'");
                                                    if (count($marchine_name) != 0) {
                                                        echo $marchine_name->printorName;
                                                    } else {
                                                        echo "无";
                                                    }
                                                }
                                                ?></td>
                                            <td><?php echo $V->applyTime; ?></td>
                                            <td><?php
                                                if ($V->refundType == 1)
                                                    echo '支付宝';
                                                else if ($V->refundType == 2)
                                                    echo '一卡通';
                                                else if ($V->refundType == 3)
                                                    echo '现金';
                                                else if ($V->refundType == 5)
                                                    echo '积分';
                                                else if ($V->refundType == 6)
                                                    echo '积分+支付宝';
                                                else if ($V->refundType == 7)
                                                    echo '微信';
                                                ?></td>
                                            <td><?php
                                                if ($V->statue == 0)
                                                    echo '未退款';
                                                else if ($V->statue == 1)
                                                    echo '已退款';
                                                else if ($V->statue == 2)
                                                    echo '退款失败';
                                                ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-12 btnlist">
                            <button type="button" class="btn btn-info" id="refund">支付宝退款</button>
                            <button type="button" class="btn btn-success" id="deleterefund">删除退款</button>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

