<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
             #refund-open{
                display: block;
            }
            #refund-list{
                 background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#alreadytable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=platform/admin/Logout";
                    }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>已退款列表</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/refund/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>退款
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">已退款列表</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table  id="alreadytable">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>sessionId</th>
                                        <th>用户</th>
                                        <th>订单号</th>
                                        <th>支付金额</th>
                                        <th>支付消耗积分</th>
                                        <th>支付方式</th>
                                        <th>订单交易号</th>
                                        <th>用户支付账号</th>
                                        <th>申请退款时间</th>
                                        <th>实际退款时间</th>
                                        <th>退款方式</th>
                                        <th>状态</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($refund_info as $K => $V) { ?>
                                        <tr>
                                            <td style="padding-left: 13px;">
                                                <?php
                                                echo $K + 1;
                                                ?>
                                            </td>
                                            <td><a href="./index.php?r=platform/refund/prbdetails&_sessionId=<?php echo $V->_sessionId; ?>&trade_no=<?php echo $V->tborderId; ?>"><?php echo $V->_sessionId; ?></a></td>
                                            <td><?php
                                                if ($V->_userId != "") {
                                                    $user_mode = user::model()->find(array("condition" => "userid= '$V->_userId'"));
                                                    echo $user_mode->username;
                                                } else
                                                    echo "无";
                                                ?></td>
                                            <td><?php if ($V->_orderId != "") { ?>
                                                    <a href="./index.php?r=platform/refund/orderDetail&businessid=<?php
                                                    $busines = business::model()->find("orderId = '$V->_orderId'");
                                                    echo base64_encode($busines->businessid);
                                                    ?>">
                                                       <?php } else {
                                                           ?>
                                                        <a href="#">
                                                        <?php } ?>
                                                        <?php echo $V->_orderId; ?></a></td>
                                            <td><?php echo $V->money; ?></td>
                                            <td><?php echo $V->consumptionIntegral; ?></td>
                                            <td><?php
                                                if ($V->payType == 1)
                                                    echo "WEB支付宝";
                                                else if ($V->payType == 2)
                                                    echo "刷卡";
                                                else if ($V->payType == 4)
                                                    echo "终端支付宝";
                                                else if ($V->payType == 5)
                                                    echo "积分";
                                                else if ($V->payType == 6)
                                                    echo "积分+支付宝";
                                                else if ($V->payType == 7)
                                                    echo "终端微信";
                                                ?>
                                            </td>
                                            <td><?php echo $V->tborderId; ?></td>
                                            <td><?php echo $V->sellerAccounter; ?></td>
                                            <td><?php echo $V->applyTime; ?></td>
                                            <td><?php echo $V->refundTime; ?></td>
                                            <td><?php
                                                if ($V->refundType == 1)
                                                    echo '支付宝';
                                                else if ($V->refundType == 2)
                                                    echo '一卡通';
                                                else if ($V->refundType == 3)
                                                    echo '现金';
                                                else if ($V->refundType == 5)
                                                    echo '积分';
                                                else if ($V->refundType == 6)
                                                    echo '积分+支付宝';
                                                else if ($V->refundType == 7)
                                                    echo '微信';
                                                ?></td>
                                            <td><?php
                                                if ($V->statue == 0)
                                                    echo '未退款';
                                                else if ($V->statue == 1)
                                                    echo '已退款';
                                                ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

