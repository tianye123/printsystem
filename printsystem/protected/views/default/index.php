<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>重庆颇闰网络打印平台</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/bt/normalize.css">
        <link rel="stylesheet" href="./css/bt/font-awesome.css">
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <link rel="stylesheet" href="./css/bt/templatemo-style.css">
        <script src="./css/bt/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="./css/bt/jquery.spin.min.js" ></script>
        <script type="text/javascript" src="./css/bt/jquery.spin.js" ></script>  

        <script>
            //上传文件等待
            var opts = {
                lines: 13, // 花瓣数目
                length: 10, // 花瓣长度
                width: 5, // 花瓣宽度
                radius: 15, // 花瓣距中心半径
                corners: 1, // 花瓣圆滑度 (0-1)
                rotate: 0, // 花瓣旋转角度
                direction: 1, // 花瓣旋转方向 1: 顺时针, -1: 逆时针
                color: '#CCCCCC', // 花瓣颜色
                speed: 1, // 花瓣旋转速度
                trail: 60, // 花瓣旋转时的拖影(百分比)
                shadow: true, // 花瓣是否显示阴影
                hwaccel: false, //spinner 是否启用硬件加速及高速旋转            
                className: 'spinner', // spinner css 样式名称
                zIndex: 2e9, // spinner的z轴 (默认是2000000000)
                top: 'auto', // spinner 相对父容器Top定位 单位 px
                left: 'auto'// spinner 相对父容器Left定位 单位 px
            };
            var spinner = new Spinner(opts);
            /*
             * 上传文件等待
             */
            function loading()
            {
                $("#firstDiv").text("");
                var target = $("#firstDiv").get(0);
                spinner.spin(target);
                document.getElementById("bg").style.display = "block";
                document.getElementById("firstDiv").style.display = "block";
                fileform.submit();
            }
            function notDel(attachmentid)
            {
                if (confirm("确定要删除吗？"))
                {
                    $.post("./index.php?r=default/notDel", {attachmentid: attachmentid}, function(data) {
                        if (data == "false")
                        {
                            alert(data);
                        }
                        else
                        {
                            alert("删除成功！");
                            window.location.href = "./index.php?r=default/index";
                        }
                    });
                }
            }

            /*
             * 注册用户名判断             
             */
            function userNameChenge()
            {
                var userName = eval(document.getElementById('username')).value;
                if (userName == "")
                {
                    document.getElementById('reareas').innerHTML = "亲，您还没有输入用户名";
                    document.getElementById('username').focus();
                }
                else if (userName.length < 5)
                {
                    document.getElementById('reareas').innerHTML = "亲，请输入5位以上的用户名";
                    document.getElementById('username').focus();
                }
                else if (userName.length > 11)
                {
                    document.getElementById('reareas').innerHTML = "亲，请输入11位以下的用户名";
                    document.getElementById('username').focus();
                }
                else
                {
                    document.getElementById('reareas').innerHTML = "";
                }
            }


            /*
             * 密码输入判断
             */
            function userpswchenge() {
                var userpsw = eval(document.getElementById('userpsw')).value;
                if (userpsw == "")
                {
                    document.getElementById('reareas').innerHTML = "亲，您还没有输入密码";
                    document.getElementById('userpsw').focus();
                }
                else if (userpsw.length < 6)
                {
                    document.getElementById('reareas').innerHTML = "亲，请输入6位以上的密码";
                    document.getElementById('userpsw').focus();
                }
                else if (userpsw.length > 12)
                {
                    document.getElementById('reareas').innerHTML = "亲，请输入12位以下的密码";
                    document.getElementById('userpsw').focus();
                }
                else
                {
                    document.getElementById('reareas').innerHTML = "";
                }
            }
            /*
             * 确认密码判断
             */
            function cpswchange() {
                var userpsw = eval(document.getElementById('userpsw')).value;
                var userpsw2 = document.getElementById('userpsw2').value;
                if (userpsw != userpsw2)
                {
                    document.getElementById('reareas').innerHTML = "亲，请核对一下密码";
                    document.getElementById('userpsw2').focus();
                }
                else
                {
                    document.getElementById('reareas').innerHTML = "";
                }
            }
            /*
             * 电话号码判断
             */
            function phonechange() {
                var phone = eval(document.getElementById('phone')).value;
                if (phone == "")
                {
                    document.getElementById('reareas').innerHTML = "亲，您还没有输入电话号码";
                    document.getElementById('phone').focus();
                }
                else if (!/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/i.test(phone))
                {
                    document.getElementById('reareas').innerHTML = "亲，请核对一下您的电话号码";
                    document.getElementById('phone').focus();
                }
                else
                {
                    document.getElementById('reareas').innerHTML = "";
                }
            }
            /*
             * 注册和更新
             */
            function registers() {
                var username = eval(document.getElementById('username')).value;
                var userpsw = eval(document.getElementById('userpsw')).value;
                var userpsw2 = document.getElementById('userpsw2').value;
                var phone = eval(document.getElementById('phone')).value;
                var storeName = eval(document.getElementById('storename')).value;
                if (username == "")
                {
                    document.getElementById('reareas').innerHTML = "亲，您还没有输入用户名";
                    document.getElementById('username').focus();
                }
                else if (username.length < 5)
                {
                    document.getElementById('reareas').innerHTML = "亲，请输入5位以上的用户名";
                    document.getElementById('username').focus();
                }
                else if (username.length > 11)
                {
                    document.getElementById('reareas').innerHTML = "亲，请输入11位以下的用户名";
                    document.getElementById('username').focus();
                }
                else if (userpsw == "")
                {
                    document.getElementById('reareas').innerHTML = "亲，您还没有输入密码";
                    document.getElementById('userpsw').focus();
                }
                else if (userpsw.length < 6)
                {
                    document.getElementById('reareas').innerHTML = "亲，请输入6位以上的密码";
                    document.getElementById('userpsw').focus();
                }
                else if (userpsw.length > 12)
                {
                    document.getElementById('reareas').innerHTML = "亲，请输入12位以下的密码";
                    document.getElementById('userpsw').focus();
                }
                else if (userpsw2 == "")
                {
                    document.getElementById('reareas').innerHTML = "亲，请核对一下密码";
                    document.getElementById('userpsw2').focus();
                }
                else if (userpsw != userpsw2)
                {
                    document.getElementById('reareas').innerHTML = "亲，请核对一下密码";
                    document.getElementById('userpsw2').focus();
                }
                else if (phone == "")
                {
                    document.getElementById('reareas').innerHTML = "亲，您还没有输入电话号码";
                    document.getElementById('phone').focus();
                }
                else if (!/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/i.test(phone))
                {
                    document.getElementById('reareas').innerHTML = "亲，请核对一下您的电话号码";
                    document.getElementById('phone').focus();
                }
                else if (storeName == 0)
                {
                    document.getElementById('reareas').innerHTML = "亲，请选择您所在的学校";
                }
                else
                {
                    if ($("#register").val() == "注册")
                    {
                        $.post("./index.php?r=default/loginnamecheck", {username: username, userpsw: userpsw}, function(data) {
                            if (data == "false")
                            {
                                document.getElementById('reareas').innerHTML = "亲，此用户名太受欢迎了，被别人抢走了，换一个更帅气的吧！";
                                document.getElementById('username').focus();
                            }
                            else
                            {
                                activeForm.submit();
                                eval(document.getElementById("username")).value = "";
                                eval(document.getElementById("userpsw")).value = "";
                                document.getElementById("userpsw2").value = "";
                                eval(document.getElementById("phone")).value = "";
                                eval(document.getElementById("storename")).value = 0;
                            }
                        });
                    }
                    else//更新信息
                    {
                        activeForm.submit();
                        eval(document.getElementById("userpsw")).value = "";
                        document.getElementById("userpsw2").value = "";
                    }
                }
            }

            /*
             * 登录
             */

            function login()
            {
                var loginname = document.getElementById("loginname").value;
                var loginpsw = document.getElementById("loginpsw").value;
                var verifyCode = document.getElementById("verifyCode").value;
                if (loginname == "")
                {
                    document.getElementById('areas').innerHTML = "亲，用户名忘记了哦！";
                    return false;
                }
                if (loginpsw == "")
                {
                    document.getElementById('areas').innerHTML = "亲，密码忘记了哦！";
                    return false;
                }
                if (verifyCode == "")
                {
                    document.getElementById('areas').innerHTML = "亲，验证码忘记了哦！";
                    return false;
                }

                //登录验证
                $.post("./index.php?r=default/log", {loginname: loginname, loginpsw: loginpsw, verifyCode: verifyCode}, function(data) {
                    if (data == "success")
                    {
                        eval(document.getElementById("loginname")).value = "";
                        eval(document.getElementById("loginpsw")).value = "";
                        window.location.href = "./index.php?r=default/index";
                    }
                    else
                    {
                        document.getElementById('verifyCode').focus();
                        document.getElementById("areas").innerHTML = data;
                    }
                });
            }
            /*
             * Enter键登录
             */
            function keyLogin(event) {
                var event = window.event || arguments.callee.caller.arguments[0];
                if (event.keyCode == 13)  //回车键的键值为13
                    login();
            }
            $(function() {
                //先判断是否登录
                $.post("./index.php?r=default/sessioncheck", function(data) {
                    if (data == "success")
                    {
                        //获取登录者的信息
                        $.post("./index.php?r=default/loginfo", function(data) {
                            if (data == "false")
                            {
                                alert("由于登录已超过指定时间，请重新登录！");
                                window.location.href = "./index.php?r=default/index";
                            }
                            else
                            {
                                var info = eval("(" + data + ")");
                                $(".profile-thumb>img").attr("src", "./images/storelogo/" + info.storelogo);
                                $(".profile-content> ul > li:eq(0)").text("用户名:" + info.username);
                                $(".profile-content> ul > li:eq(1)").text("所属院校:" + info.school);
                                $(".profile-content> ul > li:eq(2)").text("联系手机:" + info.phone);
                                //最新未打印文件
                                for (var q = 0; q < info[0].length; q++)
                                {
                                    var attachmentid = info[0][q]["attachmentid"];
                                    var s = info[0][q]["userattachment"];
                                    $("#notPrint").append("<li><div class='row'><div class='col-md-12' style='overflow: hidden;white-space: nowrap;text-overflow: ellipsis' title='" + s + "'>" + s + "</div></li > ");
                                }
                                //最新上传文件
                                for (var q = 0; q < info[1].length; q++)
                                {
                                    var attachmentid = info[1][q]["attachmentid"];
                                    var s = info[1][q]["userattachment"];
                                    $("#uploadFile").append("<li><div class='row'><div class='col-md-12' style='overflow: hidden;white-space: nowrap;text-overflow: ellipsis' title='" + s + "'>" + s + "</div></li > ");
                                }
                                for (var q = 0; q < info[2].length; q++)
                                {
                                    var attachmentid = info[2][q]["attachmentid"];
                                    var s = info[2][q]["userattachment"];
                                    $("#yesPrint").append("<li><div class='row'><div class='col-md-12' style='overflow: hidden;white-space: nowrap;text-overflow: ellipsis' title='" + s + "'>" + s + "</div></li > ");
                                }


//                                for (var q = 0; q < info[0].length; q++)
//                                {
//                                    var attachmentid = info[0][q]["attachmentid"];
//                                    var s = info[0][q]["userattachment"];
//                                    var whetherPay = "";
//                                    if (info[0][q]["ispay"] == 0)//未支付
//                                    {
//                                        whetherPay = "未支付";
//                                        $("#notprint").append("<li><div class='row'><div class='col-md-6' style='overflow: hidden;white-space: nowrap;text-overflow: ellipsis' title='" + s + "'><input type='checkbox' value='" + attachmentid + "'> " + s + "</div><div class='col-md-6' style='float: right;'>" + info[0][q]["loadtime"].substr(5) + "&nbsp;" + whetherPay + "</div></div></li > ");
//                                    }
//                                    if (info[0][q]["ispay"] == 1)
//                                    {
//                                        whetherPay = "已支付";
//                                        $("#notprint").append("<li><div class='row'><div class='col-md-6' style='overflow: hidden;white-space: nowrap;text-overflow: ellipsis' title='" + s + "'><input type='checkbox' disabled='disabled' value='" + attachmentid + "'> " + s + "</div><div class='col-md-6' style='float: right;'>" + info[0][q]["loadtime"].substr(5) + "&nbsp;" + whetherPay + "</div></div></li > ");
//                                    }
//
//                                    //$("#notprint").append("<li><div class='row'><div class='col-md-8' style='width: 180px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis' title='" + s + "'><input type='checkbox' name='" + filenumber + "' value='" + attachmentid + "'> " + s + "</div><div class='col-md-6' style='float: right;'>" + info[0][q]["loadtime"].substr(5) + "    未支付</div></div></li > ");
//
//                                }

                            }
                        });
                        $('.homepage').hide(); //登录模块隐藏
                        $("#menu-container .content").slideUp('slow');
                        $("#menu-container .about-section").slideDown('slow');
                        $(".logo-top-margin").animate({marginTop: '0'}, "slow");
                        $(".logo-top-margin").animate({marginLeft: '0'}, "slow");
                        $('.show-1').slideUp('fast'); //登录
                        $('.show-3').slideUp('fast'); //注册
                        $('.col-md-3').css('margin-top', '107px');
                        $("#bottoms").css('margin-top', '2.9%');
                        $('.show-2').show(); //个人中心
                    }
                    else {
                        //进来先不显示个人中心
                        $('.show-2').hide();
                        $("#Login").click(function() {
                            login();
                        });
                    }
                });
                //注销
                $("#logout").click(function() {
                    if (confirm("确定要注销吗？"))
                    {
                        window.location.href = "./index.php?r=default/logout"
                    }
                });
                //反馈确认
                $("#feedBackSubmit").click(function() {
                    if ($("#feedback").val().replace(/(^\s*)|(\s*$)/g, "").length == 0)
                    {
                        alert("亲，请告诉我们你想说的话吧。");
                        return false;
                    }
                    feedbackform.submit();
                });
                //快捷支付
                $("#pay").click(function() {

                    var num = 0;
                    var attachmentids = "";
                    for (var i = 0; i < $("#notprint").find(":checkbox").length; i++)
                    {
                        if ($("#notprint").find(":checkbox").eq(i).is(':checked'))
                        {
                            attachmentids += $("#notprint").find(":checkbox").eq(i).attr('value') + "+";
                            num++;
                        }
                    }
                    attachmentids = attachmentids.substring(0, attachmentids.length - 1);
                    if (num == 0)
                    {
                        alert("请选择需要支付的文件。");
                        return false;
                    }
                    $("#attachmentids").val(attachmentids);
                    payform.submit();
                });
                //更多未打印支付
                $("#paynot").click(function() {
                    //$("#notprint").find(":checkbox").eq(0).attr('value')  4
                    //$("#notprint").find(":checkbox").eq(0).is(':checked') ture/false

                    var num = 0;
                    var attachmentidd = "";
                    var printnumbers = "";
                    for (var i = 0; i < $("#paynottable").find(":checkbox").length; i++)
                    {
                        if ($("#paynottable").find(":checkbox").eq(i).is(':checked'))
                        {
                            printnumbers += $("#paynottable").find(".numbers").eq(i).val() + ","; //打印份数
                            attachmentidd += $("#paynottable").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    attachmentidd = attachmentidd.substring(0, attachmentidd.length - 1);
                    printnumbers = printnumbers.substring(0, printnumbers.length - 1);
                    if (num == 0)
                    {
                        alert("请选择需要支付的文件。");
                        return false;
                    }

                    $("#attachmentidd").val(attachmentidd);
                    $("#printnumbers").val(printnumbers);
                    payNotform.submit();
                });
                //查看更多未打印文档
                $("#notMore").click(function() {
                    $("#personcenter").slideUp('slow'); //隐藏
                    $("#more").slideDown('slow'); //出现
                });
                //查看更多已打印文档
                $("#yesMore").click(function() {
                    $("#moreFile").slideUp('slow');
                    $("#personcenter").slideUp('slow');
                    $("#more").slideUp('slow');
                    $("#yesaMore").slideDown('slow');
                });
                //查看更多上传文件
                $("#newFile").click(function() {
                    $("#yesaMore").slideUp('slow');
                    $("#personcenter").slideUp('slow');
                    $("#more").slideUp('slow');
                    $("#moreFile").slideDown('slow');
                });
                //未打印文档返回
                $("#notback").click(function() {
                    $("#moreFile").slideUp('slow');
                    $("#personcenter").slideDown('slow');
                    $("#more").slideUp('slow');
                    $("#yesaMore").slideUp('slow');
                });
                //已打印文档返回
                $("#yesback").click(function() {
                    $("#moreFile").slideUp('slow');
                    $("#personcenter").slideDown('slow');
                    $("#more").slideUp('slow');
                    $("#yesaMore").slideUp('slow');
                });
                //更多上传文件返回
                $("#Fileback").click(function() {
                    $("#moreFile").slideUp('slow');
                    $("#personcenter").slideDown('slow');
                    $("#more").slideUp('slow');
                    $("#yesaMore").slideUp('slow');
                });
                //修改个人信息
                $("#update").click(function() {
                    $('.show-3').show(); //注册
                    //把注册模块显示出来
                    $("#menu-container .content").slideUp('slow');
                    $("#menu-container .gallery-section").slideDown('slow');
                    $(".logo-top-margin").animate({marginTop: '0'}, "slow");
                    $(".logo-top-margin").animate({marginLeft: '0'}, "slow");
                    //修改注册按钮文字为修改信息
                    $(".main-menu").each(function(i, dom) {
                        var me = $(this);
                        me.find("li").eq(2).html("<a class='show-3 projectbutton' href='#'><i class='fa fa-exchange'></i>修改信息</a>");
                    });
                    $("#menu-3").find(".widget-title").eq(0).html("修改个人信息");
                    $("#register").val("修改");
                    //$("#userpsw2").hide();
                    $("#username").attr("readonly", "readonly");
                    $("#username").css("background", "#DBDBDB");
                    //.$("#username").attr("readonly", "readonly");
                    $("#userpsw").attr("placeholder", "新密码");
                    $("#userpsw2").attr("placeholder", "确认新密码");
                    //获取需要修改者的信息
                    $.post("./index.php?r=default/logininfo", function(data) {
                        if (data == "false")
                        {
                            alert("由于登录已超过指定时间，请重新登录！");
                            window.location.href = "./index.php?r=default/index";
                        }
                        else
                        {
                            var info = eval("(" + data + ")");
                            $("#username").val(info.username);
                            $("#storename").val(info.school);
                            $("#phone").val(info.phone);
                        }
                    });
                });
            })
        </script>
        <script>
   $(window).bind("load", function() {
  var bottomsHeight = 0,
      bottomsTop = 0,
 $bottoms = $("#bottoms");
 positionbottoms();
//定义positionFooter function
function positionbottoms() {
    //取到div#footer高度
  bottomsHeight = $bottoms.height();
      //div#footer离屏幕顶部的距离
   bottomsTop = ($(window).scrollTop()+$(window).height()-bottomsHeight)+"px";
	 //如果页面内容高度小于屏幕高度，div#footer将绝对定位到屏幕底部，否则div#footer保留它的正常静态定位
	 if ( ($(document.body).height()+bottomsHeight) < $(window).height()) {
		 $bottoms.css({
		    position: "absolute"
                            });
                 $bottoms.css({top: bottomsTop});
	} else {
		 $bottoms.css({  
                    position: "static" 
                            });
	       }
        }
					$(window).scroll(positionbottoms).resize(positionbottoms);
				 });


        </script>
    </head>
    <body onkeydown="keyLogin()" style="font-family:Microsoft YaHei;">
        <div class="site-bg"></div>
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <p class="phone-info"><img src="./css/bt/andriod.jpg" width="5%" height="5%"> <a href="http://www.cqutprint.com/mobile/andriod.apk">Andriod版下载</a></p>

                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="social-icons">
                            <ul>
                                <li><a href="" class="fa fa-facebook"></a></li>
                                <li><a href="" class="fa fa-twitter"></a></li>
                                <li><a href="" class="fa fa-linkedin"></a></li>
                                <li><a href="" class="fa fa-dribbble"></a></li>
                                <li><a href="" class="fa fa-rss"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="visible-xs visible-sm responsive-menu">
            <a href="" class="toggle-menu">
                <i class="fa fa-bars"></i> 菜单
            </a>
            <div class="show-menu">
                <ul class="main-menu">
                    <li>
                        <a class="cs active homebutton" href="#"><i class="fa fa-home"></i>登录</a>
                    </li>
                    <li>
                        <a class="show-2 aboutbutton " href="#"><i class="fa fa-user"></i>个人中心</a>
                    </li>
                    <li>
                        <a class="show-3 projectbutton" href="#"><i class="fa fa-camera"></i>注册</a>
                    </li>
                    <li>
                        <a class="show-5 contactbutton" href="#"><i class="fa fa-envelope"></i>联系我们</a>
                    </li>
                    <!--                    <li>
                                            <a class="show-6 loginbutton" href=""><i class="fa fa-power-off"></i>注销</a>
                                        </li>-->
                </ul>
            </div>
        </div>
        <div class="container" id="page-content">
            <div class="row">
                <div class="col-md-9 col-sm-12 content-holder" >
                    <div id="menu-container">
                        <div class="logo-holder logo-top-margin" style="margin-top:0px;">
                            <a class="site-brand"><img src="./css/bt/logo.png" alt=""></a>
                        </div>
                        <div id="menu-1" class="homepage home-section text-center">
                            <div class="welcome-text">
                                <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>
                                <p>轻松打印，从这里开始。我们一直致力于为您提供更快更好的服务！</p>
                                <form class="subscribe-form">
                                    <div class="row">
                                        <fieldset class="col-md-offset-2 col-md-6" >
                                            <input name="loginname" id ="loginname" type="text"  class="email" placeholder="用户名..">                                            
                                        </fieldset>
                                        <div id="areas" class="col-md-4 button-holder" style="color: red;font-size: 17px;margin-top: 7px"></div>
                                        <fieldset class="col-md-offset-2 col-md-6" style="margin-top: 15px;" >
                                            <input name="loginpsw" id ="loginpsw"  type="password" class="email" placeholder="密码.."> 
                                        </fieldset>
                                        <fieldset class="col-md-offset-2 col-md-6" style="margin-top: 15px;">
                                            <input style="width:59%;margin-left: 10%" name="verifyCode" id ="verifyCode" type="text" class="col-md-offset-2 email col-md-4" placeholder="验证码..">
                                            <?php $this->widget('CCaptcha', array('showRefreshButton' => false, 'clickableImage' => true, 'imageOptions' => array('alt' => '点击换图', 'title' => '点击换图', 'class' => 'col-md-3', 'style' => 'cursor:pointer;'))); ?>
                                        </fieldset>
                                        <fieldset class="col-md-6 col-md-offset-2 button-holder" style="margin-top: 15px;text-align: center;">
                                            <input name="Login" type="button" class="button default" style="border-radius:4px;width: 55%;background-color: #00BAFF" id="Login" value="登录">
                                        </fieldset>

                                    </div>
                                    <p class="subscribe-text" style="margin-top: 52px">Please Write your userinfo for login print!</p>
                                </form>  

                            </div>
                        </div>

                        <div id="menu-2" class="content about-section">
                            <div id="personcenter">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7">
                                        <div class="box-content profile">
                                            <h3 class="widget-title">用户信息</h3>
                                            <div class="profile-thumb" id="storelogo" style="width: 40%;height: 40%;">                                            
                                                <img src="" alt="">
                                            </div>
                                            <div class="profile-content">
                                                <ul>
                                                    <li>用户名:</li>
                                                    <li>所属院校:</li>
                                                    <li>联系电话:</li>
                                                </ul>
                                                <br>
                                                <a href="#" id="update" class="button">修改个人信息</a>
                                                &nbsp;&nbsp;&nbsp;
                                                <a href="#" id="logout" class="button">注销</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5">
                                        <div class="box-content" id="">
                                            <h3 class="widget-title">上传文件</h3>
                                            <div class="project-item">
                                                <?php $form = $this->beginWidget('CActiveForm', array('htmlOptions' => array('enctype' => 'multipart/form-data', 'target' => 'filel', 'id' => 'fileform'), 'action' => array('default/uploadfile')));
                                                ?>
                                                <div id="bg" style ="display: none;  position: absolute;  top: 0%;  left: 0%;  width: 100%;  height: 100%;  background-color: black;  z-index:1001;  -moz-opacity: 0.7;  opacity:.70;  filter: alpha(opacity=70);"></div>

                                                <div id="firstDiv"  style ="position:absolute;margin-left: 135px;margin-top:80px;">
                                                </div>
                                                <img src="./css/bt/7.jpg" alt="">
                                                <div class="project-hover">
                                                    <?php echo $form->FileField($attachment_model, 'attachmentfile');
                                                    ?>
                                                    <input type="button" onclick="loading()" class="button" value="上传文件">
                                                </div> 
                                                <?php $form = $this->endWidget(); ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="box-content">
                                            <p style="font-size: 15px"><strong>最新上传的文件</strong><a href="#" id="newFile" style="font-size: 13px;margin-left: 52px">更多>></a></p>                                        
                                            <p>
                                            <ul id="uploadFile">

                                            </ul>
                                            <p style="color: red">温馨提示：点击更多进入支付！</p>
                                            <div class='row'>
                                                <?php $form = $this->beginWidget('CActiveForm', array('clientOptions' => array(), 'id' => 'payform', 'action' => array('pay/alipay/index'))); ?>
                                                <!--价格-->
<!--                                                <input style="display:none;" class="name" id="attachmentids" name="attachmentids"/>
                                                <input type="button" id="pay" style="float:" value="进入支付"  class="btn btn-default" >-->

                                                <?php $form = $this->endWidget(); ?>
                                            </div>
                                            </p>

                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="box-content">
                                            <p style="font-size: 15px"><strong>最新订单</strong><a href="#" id="notMore" style="font-size: 13px;margin-left: 52px">更多>></a></p>                                        
                                            <p>
                                            <ul id="neworder">
                                            </ul>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="moreFile" style="display:none">
                                <div class="box-body">
                                    <div class="table-responsive">

                                        <table class="table no-margin" id="paynottable">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>序号</th>
                                                    <th>文件名</th>
                                                    <th>页数</th>
                                                    <th>上传时间</th>
                                                    <th>打印份数</th>
                                                    <th>操作</th>
                                                </tr></thead>
                                            <tbody>
                                            <input type="button" class="btn btn-default" id="Fileback"  value="返回">
                                            <?php foreach ($moreFile as $K => $V) {
                                                ?>
                                                <tr>
                                                    <td><?php
                                                        $attachmentid = $V["attachmentid"];
                                                        echo "<input type='checkbox'  value= '$attachmentid'>";
                                                        ?></td>
                                                    <td><?php echo $K + 1; ?></td>
                                                    <td><?php echo $V["attachmentname"]; ?></td>
                                                    <td><?php echo $V["filenumber"]; ?>页</td>
                                                    <td><?php echo $V["loadtime"]; ?></td>
                                                    <td><input type='number' class="numbers"  min='1' step='1' value='1'  style='ime-mode:disabled;width:50px;color:black' onKeyUp='this.value = this.value.replace(/[^\.\d]/g, \'\');this.value = this.value.replace(\'.\', \'\');'></td>
                                                    <td><a href="#" onclick = "notDel(<?php echo $V["attachmentid"];
                                                        ?>)"><span class="label label-success">删除</span></a>
                                                </tr>
                                            <?php } ?>
                                            </tbody>                                            
                                        </table>
                                        <div>
                                            <?php $form = $this->beginWidget('CActiveForm', array('clientOptions' => array(), 'id' => 'payNotform', 'action' => array('pay/alipay/index'))); ?>
                                            <!--价格-->                                            
                                            <input  hidden="true" class="name" id="attachmentidd" name="attachmentidd"/>
                                            <!--份数-->
                                            <input  hidden="true" class="name" id="printnumbers" name="printnumbers"/>
                                            <input type="button" id="paynot" style="float: right" value="选择支付"  class="btn btn-default" >
                                            <?php $form = $this->endWidget(); ?>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div id="more" style="display:none">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <input type="button" class="btn btn-default" id="notback"  value="返回">
                                        <table class="table no-margin">
                                            <thead>
                                                <tr>                                                    
                                                    <th>序号</th>                                                
                                                    <th>订单号</th>
                                                    <th>支付方式</th>
                                                    <th>订单总价</th>
                                                    <th>订单状态</th>
                                                    <th>下单时间</th>                                                    
                                                    <th>操作</th></tr></thead>
                                            <tbody>
                                                <?php foreach ($orderInfo as $K => $V) { ?>
                                                    <tr>                                                        
                                                        <td><?php echo $K + 1; ?></td>                                                        
                                                        <td><?php echo $V["orderId"]; ?></td>
                                                        <td><?php
                                                            if ($V["payType"] == 1)
                                                                echo "线上支付";
                                                            if ($V["payType"] == 2)
                                                                echo '线下支付';
                                                            ?></td>
                                                        <td><?php echo $V["paidMoney"]; ?>元</td>
                                                        <?php
                                                        if ($V["ispay"] == 0)
                                                            echo '<td style="color:red">待支付</td>';
                                                        if ($V["ispay"] == 1)
                                                            echo '<td>已支付</td>';
                                                        ?>
                                                        <td><?php echo $V["placeOrdertime"]; ?></td>                                                        
                                                        <td><a href="#"><span class="label label-success">订单详情</span></a>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
                            </div>
                            <div id="yesaMore" style="display:none">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <input type="button" class="btn btn-default" id="yesback"  value="返回">
                                        <table class="table no-margin">
                                            <thead>
                                                <tr>
                                                    <th>序号</th>
                                                    <th>文件名</th>
                                                    <th>页数</th>
                                                    <th>打印份数</th>
                                                    <th>支付金额</th>
                                                    <th>打印时间</th>
                                                    <!--<th>操作</th>-->
                                                </tr></thead>
                                            <tbody>
                                                <?php foreach ($yesPrint as $K => $V) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $K + 1; ?></td>
                                                        <td><?php echo $V["attachmentname"]; ?></td>
                                                        <td><?php echo $V["fileNumber"]; ?></td>
                                                        <td><?php echo $V["printNumbers"]; ?></td>
                                                        <td> <?php echo $V["paidMoney"]; ?>
                                                        </td>
                                                        <td> <div class="sparkbar" data-color="#00a65a" data-height="20">
                                                                <?php echo $V["printTime"]; ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>


                                            </tbody>
                                        </table></div>
                                </DIV> 
                            </div>
                        </div>
                        <iframe style="display:none" name="test"></iframe>
                        <iframe style="display:none" name="filel" id ="filels"></iframe>
                        <div id="menu-3" class="content gallery-section">
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    <div class="box-content">
                                        <h3 class="widget-title">注册，开始打印！</h3>
                                        <?php $form = $this->beginWidget('CActiveForm', array('htmlOptions' => array('class' => 'contact-form', 'target' => 'test', 'id' => 'activeForm')));
                                        ?>
                                        <fieldset>
                                            <?php
                                            echo $form->textField($user_model, 'username', array('type' => "text", 'id' => "username", 'onchange' => 'userNameChenge()', 'style' => 'ime-mode: disabled;', 'class' => 'name', 'placeholder' => '用户名..'));
                                            ?>
                                        </fieldset> 
                                        <fieldset>
                                            <?php
                                            echo $form->passwordField($user_model, 'userpsw', array('type' => "text", 'id' => "userpsw", 'onchange' => 'userpswchenge()', 'placeholder' => '密码..'))
                                            ?>
                                        </fieldset> 
                                        <fieldset>
                                            <input  class="name" onchange="cpswchange()" name='userpsw2' type="password" id ="userpsw2" placeholder="确认密码..">
                                        </fieldset>                              
                                        <fieldset>
                                            <?php
                                            echo $form->textField($user_model, 'phone', array('type' => "text", 'onchange' => 'phonechange()', 'id' => "phone", 'placeholder' => '电话号码..'))
                                            ?>
                                        </fieldset>
                                        <fieldset style="margin-bottom: 20px">
                                            <?php
                                            echo $form->dropDownList($store_model, 'storename', $store_list, array('type' => "text", 'class' => "name", 'id' => "storename"));
                                            ?>
                                        </fieldset>
                                        <fieldset>                                            
                                            <input type="button" id="register" onclick="registers()" class="button col-md-4" value="注册">
                                            <div id="reareas" class="col-md-8 button-holder rea" style="color: red;font-size: 17px;margin-top: 10px"></div>
                                        </fieldset>
                                        <?php $form = $this->endWidget(); ?>
                                    </div>                                    
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="box-content">
                                        <h3 class="widget-title">温馨提示</h3>
                                        <p>如果您在打印过程中遇到问题，请第一时间告知我们。我们将尽最大努力，让你体验到自助打印的乐趣!</p>
                                        <!--                                        <div class="about-social">
                                                                                    <ul>
                                                                                        <li><a href="#" class="fa fa-facebook"></a></li>
                                                                                        <li><a href="#" class="fa fa-twitter"></a></li>
                                                                                        <li><a href="#" class="fa fa-linkedin"></a></li>
                                                                                        <li><a href="#" class="fa fa-dribbble"></a></li>
                                                                                    </ul>
                                                                                </div>-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="menu-4" class="content contact-section">
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    <div class="box-content">
                                        <h3 class="widget-title">给我们的一封信</h3>
                                        <?php $form = $this->beginWidget('CActiveForm', array('htmlOptions' => array('class' => 'contact-form', 'target' => 'test', 'id' => 'feedbackform'), 'action' => array('default/feedBack')));
                                        ?>
                                        <fieldset>
                                            <?php
                                            echo $form->textarea($usermessage_model, 'content', array('type' => "text", 'style' => 'width: 500px;height: 152px;max-width: 500px;min-width: 500px;max-height: 265px;min-height: 42px;', 'id' => "feedback", 'placeholder' => '亲，你对本系统有任何意见和期望都可以告诉我们。'))
                                            ?>
                                        </fieldset>
                                        <fieldset style="text-align: center">                                            
                                            <input type="button" class="button" id="feedBackSubmit" value="发送">                                            
                                        </fieldset>
                                        <?php $form = $this->endWidget(); ?>
                                        <!--                                        <form class="contact-form">
                                                                                    <fieldset>
                                                                                        <input type="text" class="name" id="name" placeholder="Name...">
                                                                                    </fieldset> 
                                                                                    <fieldset>
                                                                                        <input type="email" class="email" id="email" placeholder="Email...">
                                                                                    </fieldset> 
                                                                                    <fieldset>
                                                                                        <input type="text" class="subject" id="subject" placeholder="Subject...">
                                                                                    </fieldset>
                                                                                    <fieldset>
                                                                                        <textarea name="message" id="message" cols="30" rows="4" placeholder="Message.."></textarea>
                                                                                    </fieldset>
                                                                                    <fieldset>
                                                                                        <input type="submit" class="button" id="button" value="Send Message">
                                                                                    </fieldset>
                                                                                </form>-->
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="box-content">
                                        <h3 class="widget-title">温馨提示</h3>
                                        <p>如果您在打印过程中遇到问题，请第一时间告知我们。我们将尽最大努力，让你体验到自助打印的乐趣!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu-5" class="content more-section">

                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <nav id="nav" class="main-navigation hidden-xs hidden-sm">
                        <ul class="main-menu">
                            <li>
                                <a class="show-1 active homebutton" href="#"><i class="fa fa-home"></i>登录</a>
                            </li>
                            <li>
                                <a class="show-2 aboutbutton" href="#"><i class="fa fa-user"></i>个人中心</a>
                            </li>
                            <li>
                                <a class="show-3 projectbutton" href="#"><i class="fa fa-camera"></i>注册</a>
                            </li>
                            <li>
                                <a class="show-5 contactbutton" href="#"><i class="fa fa-envelope"></i>联系我们</a>
                            </li>
                            <!--                            <li>
                                                            <a class="show-6 logoutbutton" href=""><i class="fa fa-power-off"></i>注销</a>
                                                        </li>-->
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="site-footer" id="bottoms">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>Copyright © 2015 重庆颇闰科技</p>
                    </div>
                </div>
            </div>
        </div>
        <script src="./css/bt/plugins.js"></script>
        <script src="./css/bt/main.js"></script>
    </body></html>
