<html>
    <head>
        <META content="IE=11.0000" http-equiv="X-UA-Compatible">
        <META charset="UTF-8">    
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <title>支付完成</title>
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready(function() {
                $("#back").click(function() {
                    window.location.href = "./index.php?r=orderInfo/orderInfo";
                });
                time();
            });
            var wait = 5;
            function time() {
                if (wait == 0) {
                    window.location.href = "./index.php?r=orderInfo/orderInfo";
                    wait = 5;
                } else {
                    $(".second").text(wait + " 秒钟后，页面会自动返回......");
                    wait--;
                    setTimeout(function() {
                        time();
                    }, 1000)
                }
            }
        </script>
        <style>
            body{
                background-color: #F4F4F4;
                text-align: center;
                font-family:Microsoft YaHei;
            }
         
        </style>
    </head>
    <body>
        <div><img src="./css/bt/payComplete.png">恭喜你，你的支付已经完成！</div>
        <br>
        <div class="second">5 秒钟后，页面会自动返回......</div>
        <br>
        <input type="button" id="back" value="返回我的订单">
    </body>
</html>