<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <link href="./css/leftNavigation/css/style.css" rel="stylesheet" type="text/css">
        <LINK href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="./css/bt/footer.css">
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <script type="text/javascript" src="./css/outWindows/js/jquery.leanModal.min.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            #orderDetail tr td{
                text-align: left;
                font-size: 15px;
                padding: 12px;
            }
            #orderDetail tr th{
                font-size: 15px;
                padding: 12px;
            }
            #orderDetail tr span{
                color: red;
                font-size: 15px;  
                font-weight: bold;
            }
            #orderDetail tr:hover {
                background: #FCFCFC;
            }
            .orderTop
            {
                background-color:#FFFCEB;
            }
            .menuFont
            {
                font-size: 17px;
            }
            body{
                background-color: #E5E5E5;
                font-family:Microsoft YaHei;
            }
            .checkboxFour {
                width: 20px;
                height: 20px;
                border: 1px #E5E5E5 solid;
                border-radius: 100%;
                position: relative;
            }
            .checkboxFour label {
                display: block;
                width: 14px;
                height: 14px;
                border-radius: 100px;

                -webkit-transition: all .5s ease;
                -moz-transition: all .5s ease;
                -o-transition: all .5s ease;
                -ms-transition: all .5s ease;
                transition: all .5s ease;
                cursor: pointer;
                position: absolute;
                top: 2px;
                left: 2px;
                z-index: 1;
                background-color:#FFFFFF;
            }
            .container {
                padding-right: 0px; 
                padding-left: 0px; 
            }
        </style>
        <script language="javascript">

            function killerrors() {
                return true;
            }

            window.onerror = killerrors;</script>  
        <script>
            $(document).ready(function() {
                if (window.screen.availHeight >= 800)//登录 高 340px
                {
                    $('#rightPay').leanModal({top: 450, overlay: 0.45, closeButton: ".hidemodal"}); //立即支付弹窗
                }
                else {
                    $('#rightPay').leanModal({top: 90, overlay: 0.45, closeButton: ".hidemodal"}); //立即支付弹窗
                }
                $('#surePay').leanModal({top: 0, overlay: 0.45, closeButton: ".hidemodal"}); //立即支付弹窗

                $("nav").each(function(index, element) {
                    $(this).find("div").css("background-color", "#FFFFFF");
                });
                $("nav").find("div").eq(1).css("background-color", "#00B9FF");
                $("nav ul li").mouseover(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $(this).find("div").css("background-color", "#00B9FF");
                });
                $("nav").mouseout(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $("nav").find("div").eq(1).css("background-color", "#00B9FF");
                });
                for (var i = 0; i < $(".ispay").length; i++)
                {
                    var value_ispay = $(".ispay").eq(i).text();
                    var value_isprint = $(".isprint").eq(i).text();
                    if (value_ispay == "未支付")
                    {
                        $(".list").css("display", "none");
                        $(".isprint").eq(i).parent().find(".checkboxss").css("display", "none");
                    }
                    else if (value_ispay == "已支付" && value_isprint != "已打印")
                    {
                        $(".isprint").eq(i).parent().find(".checkboxss").css("display", "");
                    }
                    else if (value_ispay == "已支付" && value_isprint == "已打印")
                    {
                        $(".isprint").eq(i).parent().find(".checkboxss").css("display", "none");
                    }
                }
                if (<?php echo $sta; ?> == 0)//属于已打印列表
                {
                    $("#rightPay").css("display", "none");
                    $("#refund").css("display", "none");
                }
                if (<?php echo $sta; ?> == 1)//属于已支付列表
                {
                    $("#rightPay").css("display", "none");
                    $("#refund").css("display", "inline");
                }
                if (<?php echo $sta; ?> == 2)//属于未支付列表
                {
                    $("#rightPay").css("display", "inline");
                    $("#refund").css("display", "none");
                }

//                $(".ispay").each(function() {
//                    var value = $(this).text();
////                    if (value == "未支付")
////                    {
////                        $("#rightPay").css("display", "inline");
////                        $("#refund").css("display", "none");
////                        $(".checkboxss").css("display", "none");
////                        $(".list").css("display", "none");
////                    }
//                    else if (value == "已支付" && $(this).parent().find(".isprint").text() != "已打印")
//                    {
//                        $("#refund").css("display", "inline");
//                        $(".checkboxss").css("display", "");
//                        $(".list").css("display", "");
//                    }
//                    else if (value == "已支付" && $(this).parent().find(".isprint").text() == "已打印")
//                    {
//                        $("#refund").css("display", "none");
//                        $(".checkboxss").css("display", "none");
////                        $(".list").css("display", "none");
//                    }
//                });
                //注销
                $('#loginOut').click(function() {
                    if (confirm("确定注销？"))
                    {
                        $.post("./index.php?r=login/loginOut", function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                                window.location.href = "./index.php?r=login/login";
                        });
                    }
                });
                $("#login_center").click(function() {
                    window.location.href = './index.php?r=personalCenter/personalCenter';
                });
                $(".checkboxFour").click(function() {
                    if ($(this).find(":checkbox").prop("checked")) {//取消选择
                        $(this).find(":checkbox").removeAttr("checked");
                        $(this).find("label").css("background-color", "#FFFFFF");
                    }
                    else {//选择
                        $(this).find(":checkbox").prop("checked", true);
                        $(this).find("label").css("background-color", "#65C91B");
                    }
                });



                //删除订单
                $('#deleteOrder').click(function() {
                    if (confirm("确定删除 <?php echo $businessid_info->orderId ?> 此订单？"))
                    {
                        var orderId = "<?php echo $businessid_info->orderId ?>";
                        $.post("./index.php?r=myFile/deleteOrder", {orderId: orderId}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                window.location.href = "./index.php?r=orderInfo/orderInfo";
                            }
                            else
                            {
                                alert("未知的错误，请刷新重试！");
                            }
                        });
                    }
                });
                //立即支付
                $("#surePay").click(function() {
                    $("#payMeansmodal").css({"display": "none"});
                    //线上支付
                    var integration = $(".stores").val(); //减去的积分
                    if ($("input[name='zffs']:checked").val() == "xszf")
                    {
                        if ($("#slideTwo").is(":checked") && (integration / 100) == <?php echo $businessid_info->paidMoney; ?>)//选择积分支付完
                        {
                            $.post("./index.php?r=myFile/subStore", {orderId: "<?php echo $businessid_info->orderId ?>", integration: integration}, function(datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "false")
                                {
                                    alert("支付失败,请刷新重试！");
                                }
                                else if (data.data == "success")
                                {
                                    window.location.href = "./index.php?r=orderInfo/orderInfo";
                                }
                            });
                        }
                        else//积分支付不完
                        {
                            if ($("#slideTwo").is(":checked")) {//如果选择了积分一起支付，则需要扣除积分
                                $.post("./index.php?r=myFile/subStore_xszf", {integration: integration, businessid:<?php echo $businessid_info->businessid; ?>}, function(datainfo) {
                                    var data = eval("(" + datainfo + ")");
                                    if (data.data == "success")
                                        window.location.href = "./index.php?r=myFile/subStore_xszfz";
                                });
                            }
                            else
                            {
//                                var addintegration = <?php echo $businessid_info->paidMoney; ?> * 0.1;addintegration: addintegration,
                                $.post("./index.php?r=myFile/subStore_xszf", {integration: 0, businessid:<?php echo $businessid_info->businessid; ?>}, function(datainfo) {
                                    var data = eval("(" + datainfo + ")");
                                    if (data.data == "success")
                                        window.location.href = "./index.php?r=myFile/subStore_xszfz";
                                });
                            }
                        }
                    }
                    //线下支付
                    else
                    {
                        $.post("./index.php?r=myFile/xxzfProcess", {orderId: "<?php echo $businessid_info->orderId ?>"}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                                window.location.href = "./index.php?r=myFile/myFileDetail&businessid=<?php echo base64_encode($businessid_info->businessid) ?>";
                            else
                                alert("未知的错误，请刷新重试！");
                        });
                    }
                });
                //退款按钮
                $("#refund").click(function() {
                    var subbusinessIdd = "";
                    var num = 0;
                    for (var i = 0; i < $("#orderDetail").find(":checkbox").length; i++)
                    {
                        if ($("#orderDetail").find(":checkbox").eq(i).is(':checked'))
                        {
                            subbusinessIdd += $("#orderDetail").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    if (num == 0)
                    {
                        alert("请您选择需要退款的文件！");
                        return false;
                    }
                    subbusinessIdd = subbusinessIdd.substring(0, subbusinessIdd.length - 1);
                    $.post("./index.php?r=myFile/refund", {subbusinessIdd: subbusinessIdd}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "success")
                        {
                            alert("您的请求已提交成功，将在2个工作日内退还到您账户！");
                            window.location.href = "./index.php?r=orderInfo/orderInfo";
                        }
                        else if (data.data == "false")
                        {
                            alert("部分退款提交失败，如果已经提交，请勿重复提交，谢谢您的使用！");
                            window.location.href = "./index.php?r=orderInfo/orderInfo";
                        }
                    });
                });


                $(".slideTwo2").hide();
                $("#radioFourInputs").click(function() {
                    $(".slideTwo1").show();
                    $(".slideTwo2").hide();
                });
                $("#radioFourInputx").click(function() {
                    $(".slideTwo1").hide();
                    $(".slideTwo2").show();
                });
                $("#slideTwo").click(function() {
                    var useStore = $(".stores").val(); //获取已填写的积分
                    if ($("#slideTwo").is(":checked"))//如果选中的话
                    {
                        if (!isNaN(useStore))//如果是数字
                        {
                            if (useStore ><?php echo $integration; ?>)
                            {
                                $(".stores").css("border", "1px red solid");
                                $(".subtractingStore").text("-￥0");
                                $(".realPayment").text("￥" + <?php echo $businessid_info->paidMoney; ?>);
                                $(".gainScore").text(<?php echo ($businessid_info->paidMoney) * 10; ?> + "点");
                            }
                            else
                            {
                                if ((useStore / 100) > <?php echo $businessid_info->paidMoney; ?>)//积分
                                {
                                    $(".stores").css("border", "1px #ABADB3 solid");
                                    $(".stores").val((<?php echo $businessid_info->paidMoney; ?> * 100).toFixed(2));
                                    $(".subtractingStore").text("-￥" + <?php echo $businessid_info->paidMoney; ?>);
                                    $(".realPayment").text("￥0");
                                    $(".gainScore").text("0点");
                                }
                                else {
                                    $(".subtractingStore").text("-￥" + (useStore / 100).toFixed(2)); //消耗积分少多少钱
                                    $(".realPayment").text("￥" + (<?php echo $businessid_info->paidMoney; ?> - useStore / 100).toFixed(2)); //实付款
                                    $(".gainScore").text((((<?php echo $businessid_info->paidMoney; ?> - useStore / 100)) * 10).toFixed(2) + "点"); //获得的积分
                                }
                            }
                        }
                        else//如果不是数字
                        {
                            $(".stores").css("border", "1px red solid");
                        }
                    }
                    else//如果未选中
                    {
                        $(".subtractingStore").text("-￥0");
                        $(".realPayment").text("￥" + <?php echo $businessid_info->paidMoney; ?>);
                        $(".gainScore").text(<?php echo ($businessid_info->paidMoney) * 10; ?> + "点");
                    }
                });
            });
            function storeChanges() {
                var useStore = $(".stores").val();
                if (useStore == "")
                {
                    $(".stores").val(0);
                }
            }
            function storeChange() {
                var useStore = $(".stores").val();
                if (!isNaN(useStore))//是数字
                {
                    if (useStore >= 0) {
                        if (useStore ><?php echo $integration; ?>)
                        {
                            if ($("#slideTwo").is(":checked"))
                            {
                                if ((<?php echo $integration / 100; ?>) < <?php echo $businessid_info->paidMoney; ?>)//如果积分不够支付
                                {
                                    $(".stores").val(<?php echo $integration; ?>);
                                    var useStores = $(".stores").val();
                                    $(".subtractingStore").text("-￥" + useStores / 100); //消耗积分少多少钱
                                    $(".realPayment").text("￥" + (<?php echo $businessid_info->paidMoney; ?> - useStores / 100).toFixed(2)); //实付款
                                    $(".gainScore").text(((<?php echo $businessid_info->paidMoney; ?> - (useStores / 100)) * 10).toFixed(2) + "点"); //获得的积分
                                }
                                else//如果积分够支付
                                {
                                    $(".stores").val((<?php echo $businessid_info->paidMoney; ?> * 100).toFixed(2));
                                    $(".subtractingStore").text("-￥" + <?php echo $businessid_info->paidMoney; ?>);
                                    $(".realPayment").text("￥0");
                                    $(".gainScore").text("0点");
                                }
                            }
                            else
                            {
                                $(".stores").val(<?php echo $integration; ?>);
                                $(".subtractingStore").text("-￥" + <?php echo $integration / 100; ?>);
                                $(".realPayment").text("￥" + <?php echo $businessid_info->paidMoney; ?>);
                                $(".gainScore").text(<?php echo $businessid_info->paidMoney * 10; ?> + "点");
                            }
                        }
                        else if ((useStore / 100) > <?php echo $businessid_info->paidMoney; ?>)
                        {
                            if ($("#slideTwo").is(":checked"))
                            {
                                $(".stores").val((<?php echo $businessid_info->paidMoney; ?> * 100).toFixed(2));
                                $(".subtractingStore").text("-￥" + <?php echo $businessid_info->paidMoney; ?>);
                                $(".realPayment").text("￥0");
                                $(".gainScore").text("0点");
                            }
                        }
                        else
                        {
                            $(".stores").css("border", "1px #ABADB3 solid");
                            if ($("#slideTwo").is(":checked"))
                            {
                                $(".subtractingStore").text("-￥" + (useStore / 100).toFixed(2));
                                $(".realPayment").text("￥" + (<?php echo $businessid_info->paidMoney; ?> - useStore / 100).toFixed(2));
                                $(".gainScore").text(((<?php echo $businessid_info->paidMoney; ?> - (useStore / 100)) * 10).toFixed(2) + "点");
                            }
                        }
                    }
                    else
                    {
                        $(".stores").val(0);
                    }
                }
                else
                {
                    $(".stores").css("border", "1px red solid");
                }

            }
        </script>
        <script>
            window.onload = function() {
                document.getElementById("left").style.height = document.getElementById("orderDetail").offsetHeight + "px";
            };
        </script>
    </head>
    <body>
        <div class="containers">
            <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png);">
                <div class="row">
                    <div class="col-md-12" style="background-color:rgba(255,255,255,.2);text-align: center;padding-top: 5px;padding-bottom: 5px">
                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-4">
                                <a href="#" id="login_center"><div  style="width:35px;height:35px;border-radius:50px;float: left;background-color:rgba(255,255,255,.2);"><img style="margin-top:5px;margin-bottom: 5px;"  src="./css/bt/username.png"></div></a>
                                <div id="topLeft" style="text-align: left;font-size:18px;color: white;padding-top: 5px;margin-left: 45px;">
                                    <?php echo $username; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div  style="color: white;margin-left:238px;margin-top: 4px;font-size:18px;cursor:pointer" id="loginOut" >注销</div>  
                            </div>
                        </div>                    
                    </div>
                </div>
                <br />                
                <div class="row">
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2" style="text-align: center;">
                        <a><img src="./css/bt/logo.png" alt=""></a>
                    </div>
                    <div class="col-md-5">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6" style="text-align: center;color: #FFF;font-size: 18px">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <p style="letter-spacing:3px; line-height:150%">
                                    公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>    
                <br>
            </div>
            <div class="container" style="margin-top: 20px;">
                <div class="row">
                    <div class="col-md-2">
                        <nav id="left">
                            <ul>
                                <li class="movies"><div></div><span class="movies-icon"  style="float:left"></span><a href="./index.php?r=myFile/myFile">我的文件</a></li><hr>
                                <li class="store"><div></div><span class="store-icon"></span><a href="./index.php?r=orderInfo/orderInfo">我的订单</a></li><hr>
                                <li class="music"><div></div><span class="music-icon"  style="float:left"></span><a href="./index.php?r=terninalState/terninalState">终端位置</a></li><hr>
                                <li class="books"><div></div><span class="books-icon"  style="float:left"></span><a href="./index.php?r=contactUs/ContactUs">联系我们</a></li><hr>
                                <li class="magazines"><div></div><span class="magazines-icon"  style="float:left"></span><a href="./index.php?r=personalCenter/personalCenter">个人中心</a></li><hr>
                            </ul>
                        </nav>
                        <div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
                        </div>
                    </div>
                    <div class="col-md-10">
                        <table class="table" id="orderDetail" style="background-color:#FFF;color: #333;border-left: 2px #E5E5E5 solid">
                            <tr>
                                <td style="text-align:left" colspan="9"><span style="color:black;font-size: 20px">订单查看</span>&nbsp;&nbsp;&nbsp;&nbsp;<span>*以下显示订单最新状态</span>
                                    <div style="float:right;color: #15CFCD;padding: 3px"> 
                                        <button type="button" class="btn btn-success" id="deleteOrder">删除</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="orderTop" colspan="9">
                                    <div style="float:left;margin-right: 20px">订单号：<?php echo $businessid_info->orderId ?></div>
                                    <div style="float:right;margin-right: 20px">验证码：<?php
                                        if (isset($businessid_info->verificationCode))
                                            echo "<sapn style = 'color:red'>" . $businessid_info->verificationCode . "</span>";
                                        else
                                            echo "无";
                                        ?></div>


                                    <hr style="margin-top:25px;margin-bottom: 7px;border: 1px dashed #EEE;">
                                    <div style="float:left">
                                        亲爱的用户，请在下单后尽快支付订单，支付完成后即可凭借手机验证码到终端打印，如果您有什么不满意，您可联系客服！
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">
                                    <div class="row" style="color: #2DC0FD;text-align: center;padding: 20px; ">
                                        <div class="col-md-2">
                                            <img src="./css/bt/choice.png">
                                            <div>选择文件</div>      
                                        </div>
                                        <div class="col-md-1"><img src="./css/bt/jt.png"></div>
                                        <div class="col-md-2">
                                            <img src="./css/bt/order-gray.png">
                                            <div style="color:#FFCF75">确认订单</div>
                                        </div>
                                        <div class="col-md-1"><img src="./css/bt/jt.png"></div>
                                        <div class="col-md-2">
                                            <img src="./css/bt/pay.png">
                                            <div>选择支付</div>
                                        </div>
                                        <div class="col-md-1"><img src="./css/bt/jt.png"></div>
                                        <div class="col-md-2">
                                            <img src="./css/bt/print.png">
                                            <div>终端打印</div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td  class="orderTop" colspan="9">
                                    <div class="menuFont">订单详情</div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    处理时间
                                </td>
                                <td colspan="5">
                                    处理信息
                                </td>    
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <?php echo $businessid_info->placeOrdertime ?>
                                </td>
                                <td colspan="5">
                                    您提交了订单，请完成支付和打印。
                                </td>    
                            </tr>
                            <tr>
                                <td  class="orderTop" colspan="9">
                                    <div class="menuFont">付款信息</div>
                                </td>
                            </tr>
                            <tr>

                                <td colspan="9">
                                    商家信息：重庆颇闰科技有限公司
                                </td>   
                            </tr>
                            <tr>
                                <td style="color:red" colspan="3">
                                    商品总金额：￥ <?php
                                    echo $businessid_info->paidMoney;
                                    ?> 元
                                </td>
                                <td style="color:red" colspan="3">
                                    应支付金额：￥ <?php
                                    echo $businessid_info->paidMoney;
                                    ?> 元
                                </td>    
                                <td style="color:red" colspan="3">
                                    <?php
                                    echo "消费积分：" . $businessid_info->consumptionIntegral . " 点";
                                    ?>
                                </td>    
                            </tr>
                            <tr>
                                <td  class="orderTop" colspan="9">
                                    <div class="menuFont">打印信息</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="list">序列</td>
                                <td>文件名称</td>
                                <td>打印份数</td>
                                <td>打印页码</td>
                                <td>支付金额</td>
                                <td>支付状态</td>
                                <td>支付方式</td>
                                <td>打印状态</td>
                                <td>打印终端</td>
                            </tr>

                            <?php foreach ($attachmentArray as $K => $V) { ?>
                                <?php
                                $subbusinessId = $V["subbusinessId"];
                                echo '<tr>';
                                if ($V["status"] != "1") {
                                    echo '<td class="checkboxss"><div class="checkboxFour">';
                                    echo "<input type='checkbox' style='visibility: hidden;'  value= '$subbusinessId'>";
                                    echo '<label for="checkboxFourInput"></label></td>';
                                } else {
                                    echo '<td>';
                                    echo $K + 1;
                                    echo '</td>';
                                }
                                $attachmentname = $V["attachmentname"];
                                echo '<td><div style=" width:200px; cursor: pointer;word-break:keep-all;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;margin-left: 15px;" title=' . $attachmentname . '>' . $V["attachmentname"] . '</div></td>'
                                . '<td><span>' . $V["printNumber"] . '</span> 份</td>'
                                . '<td>第' . $V["printSet"] . '页</td>'
                                . '<td><span>￥' . $V["paidMoney"] . '</span> 元</td>';

                                if ($V["isrefund"] == 0) {
                                    if ($V["isPay"] == 0)
                                        echo '<td><span class ="ispay">未支付</span></td>';
                                    else if ($V["isPay"] == 1)
                                        echo '<td class ="ispay">已支付</td>';
                                    else
                                        echo '<td class ="ispay">未知的错误</td>';
                                }
                                else if ($V["isrefund"] == 1)
                                    echo '<td class ="ispay">已退款</td>';
                                else if ($V["isrefund"] == 2)
                                    echo '<td class ="ispay">退款中</td>';

                                if ($V["payType"] == "0")
                                    echo '<td>线下支付</td>';
                                if ($V["payType"] == "1")
                                    echo '<td>支付宝</td>';
                                else if ($V["payType"] == "2")
                                    echo '<td>一卡通</td>';
                                else if ($V["payType"] == "3")
                                    echo '<td>投币</td>';
                                else if ($V["payType"] == "4")
                                    echo '<td>终端扫码</td>';
                                else if ($V["payType"] == "5")
                                    echo '<td>积分</td>';
                                else if ($V["payType"] == "6")
                                    echo '<td>积分+支付宝</td>';
                                else if ($V["payType"] == "7")
                                    echo '<td>微信</td>';
                                else if ($V["payType"] == null)
                                    echo '<td>无</td>';

                                if ($V["status"] == "0")
                                    echo '<td class="isprint">未打印</td>';
                                else if ($V["status"] == "1")
                                    echo '<td class="isprint">已打印</td>';
                                else if ($V["status"] == "2")
                                    echo '<td class="isprint">打印失败</td>';

                                $marchineid = $V["marchineId"];
                                if ($marchineid == NULL) {
                                    echo "<td>无</td>";
                                } else {
                                    $marchine_model = printor::model()->find(array('condition' => "machineId = '$marchineid'"));
                                    echo "<td>" . $marchine_model->printorName . "</td>";
                                }

                                echo'</tr>';
                            }
                            ?>
                            </tr>

                            <tr>
                                <TH style="text-align:right" colspan="9">
                                    <button type="button" class="btn btn-info"  href="#payMeansmodal" id="rightPay">立即支付</button>
                                    <button type="button" class="btn btn-info"  id="refund">选择退款</button>
                                </TH>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>



            <!--选择支付弹出窗-->
            <div id="payMeansmodal">
                <h1 style="font-size: 1.0em">选择支付方式</h1> <a class="hidemodal" href="#"></a>
                <div id="payMeansform" style="margin-top: 30px;font-size: 0.7em;color: #333" name="payMeansform" method="post" action="index.html">
                    <hr style="margin-top:-19px;margin-bottom: 7px;border: 1px solid #DBDBDB;">
                    <div class="radioFour" style="float:left">
                        <input type="radio" value="xszf" name="zffs" checked="checked" id="radioFourInputs" />
                        <label for="radioFourInputs"></label>
                    </div><div style="float:left">&nbsp;<img src="./css/bt/xszf.png" style="width:30px">&nbsp;支付宝支付</div>
                    <br><br>
                    <div class="radioFour"  style="float:left">
                        <input type="radio" value="xxzf" name="zffs" id="radioFourInputx" />
                        <label for="radioFourInputx"></label>
                    </div><div style="float:left">&nbsp;<img src="./css/bt/xxzf.png"style="width:30px" >&nbsp;线下终端支付（投币或者一卡通）</div>
                    <br><br>
                    <div  class="slideTwo1">
                        <div style="float:left">  
                            <input type="checkbox" value="None" id="slideTwo" name="check"/>
                            <label for="slideTwo"></label>
                        </div><div style="float:left">使用积分：<INPUT type="text" class="stores" value="<?php echo $integration; ?>" onkeyup="storeChange()" onblur="storeChanges()" style="width:50px">点<div style="float:right;color: red"><STRONG class="subtractingStore">-￥<?php echo $integration / 100; ?></STRONG></div><div style="color: #B4B4B4;float: right">(可用 <?php echo $integration; ?> 点)</div></div>
                        <br><br>
                        <div style="float:left;color: #B4B4B4;margin-left: 142px">  
                            实付款：</div><div style="float:left;color: red;font-size: 25px" class="realPayment">￥<?php echo $businessid_info->paidMoney; ?></div>

                        <br>
                        <div style="float:left;color: #B4B4B4;margin-left: 142px">  
                            可获得积分：<div style="float:right;color: #333;" class="gainScore"><?php echo $businessid_info->paidMoney * 10; ?>点</div>
                        </div>
                        <br>
                    </div>
                    <div  class="slideTwo2" style="color: #B4B4B4;text-align: center">
                        *请您去线下完成支付
                        <br><br>
                    </div>
                    <button type="button" class="btn btn-primary" href="#loadmodal" style="float:right" id="surePay">确定</button>
                </div>
            </div>

            <!--等待弹出窗-->
            <div id="loadmodal" style="background-image: url(./css/bt/load_2.gif);">
            </div>
            <div style="width: 100%;height: 20px;"></div>
            <div class="container" id="footer" style="text-align:center;background-color: #333;width: 100%;color:white;font-size: 15px;height:50px;">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 15px;">
                        <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.       
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>