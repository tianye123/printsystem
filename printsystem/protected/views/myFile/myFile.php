<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <link rel="stylesheet" href="./css/bt/footer.css">
        <link href="./css/leftNavigation/css/style.css" rel="stylesheet" type="text/css">
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <script type="text/javascript" src="./css/upload_end/jquery.js"></script>
        <script type="text/javascript" src="./css/upload_end/jquery.loadscript.js"></script>
        <script type="text/javascript" src="./css/upload_end/jquery.uploadAdapter.js"></script>
        <script type="text/javascript" src="./css/outWindows/js/jquery.leanModal.min.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>

            .numbers{
                width:100px;
            }
            #fileList tr td{
                text-align: center;
                font-size: 15px;
                padding: 12px;
                vertical-align: inherit;
            }
            #fileList tr th{
                font-size: 15px;
                padding: 12px;
                vertical-align: inherit;
            }
            #fileList tr span{
                color: #333;
                font-size: 19px;  
                font-weight: bold;
            }

            #fileList tr:hover {
                background: #FCFCFC;

            }

            .checkboxFour {
                width: 20px;
                height: 20px;
                border: 1px #E5E5E5 solid;
                border-radius: 100%;
                position: relative;
            }
            .filepages{
                width:100px;
            }
            .checkboxFour label {
                display: block;
                width: 14px;
                height: 14px;
                border-radius: 100px;
               -webkit-border-radius: 100px;
	       -moz-border-radius: 100px;
                -o-border-radius: 100px;
                -ms-border-radius: 100px;
                -webkit-transition: all .5s ease;
                -moz-transition: all .5s ease;
                -o-transition: all .5s ease;
                -ms-transition: all .5s ease;
                transition: all .5s ease;
                cursor: pointer;
                position: absolute;
                top: 2px;
                left: 2px;
                z-index: 1;
                background-color:#FFFFFF;
            }
            input[type=checkbox] {
                visibility: hidden;
            }

             body{
                background-color: #E5E5E5;
                font-family:Microsoft YaHei;
                overflow-y:scroll;
                overflow-x: hidden;
            }
            #uplodefiles{
               padding-top:8px;
               margin-left:-4px;
               float:right;
               font-size:12px;
               color:white;
               padding-right: 11px;
            }
            #filesupload{
                  float:right;
                  border-radius: 6px;
                  -webkit-border-radius:6px;
	          -moz-border-radius:6px;
                  -o-border-radius:6px;
                  -ms-border-radius:6px;
                  background-color:#5cb85c;
                  cursor: pointer;
            }
            #filesupload:hover{
                background-color:#00a400
            }
           #uploadFilemodal h1{
                text-align: left;
            }
            #uploadSure{
                background-color: #00B9FF;
                color: white;
                font-size:15px;
                letter-spacing: 2px;
            }
            #uploadSure:hover{
                background-color:#428bca;
               
            }
            #uploadform .flatbtn-blu {
                padding:0;
                font-weight: normal;
            }
            #left{
                height: 811px;
                border-right:2px #E5E5E5 solid;
               -webkit-border-right:2px #E5E5E5 solid;
	       -moz-border-right:2px #E5E5E5 solid;
                -o-border-right:2px #E5E5E5 solid;
                -ms-border-right:2px #E5E5E5 solid;
            }
            .container {
               padding-right: 0px; 
               padding-left: 0px; 
            }
            nav {
              box-shadow: 0 0px 0px rgba(0, 0, 0, 0.1); 
            }
            #left ul hr{
                width: 100%;
            }
        </style>
        <script language="javascript">

            function killerrors() {
                return true;
            }

            window.onerror = killerrors;</script>  
        <script>
<?php $timestamp = time(); ?>
            $(document).ready(function() {

                $('#upload').uploadAdapter({
                    auto: true,
                    buttonImage: "./images/myfile/uploadfile.png",
                    buttonText: '',
                    fileObjName: 'file',
                    fileTypeExts: '*.doc;*.docx;*.pdf',
                    multi: true,
                    formData: {key: 123456, key2: 'vvvv'},
                    fileSizeLimit: 59999,
                    showUploadedSize: false,
                    showUploadedPercent: true, //是否实时显示上传的百分比，如20%
                    removeTimeout: 9999999,
                    uploader: './index.php?r=myFile/uploadp'
                });
               

                $('.uploads').leanModal({overlay: 0.45, closeButton: ".hidemodal"}); //上传文件弹窗
                $("#lean_overlay").click(function() {
                    window.location.href = "./index.php?r=myFile/myFile";
                });
                $('.hidemodal').click(function() {
                    window.location.href = "./index.php?r=myFile/myFile";
                });
                $('#uploadSure').click(function() {
                    window.location.href = "./index.php?r=myFile/myFile";
                });
        
              
                $("nav").each(function(index, element) {
                    $(this).find("div").css("background-color", "#FFFFFF");
                });
                $("nav").find("div").eq(0).css("background-color", "#00B9FF");
                $("nav ul li").mouseover(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $(this).find("div").css("background-color", "#00B9FF");
                });
                $("nav").mouseout(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $("nav").find("div").eq(0).css("background-color", "#00B9FF");
                });
                $("#fileList tr").mouseover(function() {
                    $(this).find(".operation").css("visibility", "visible");
                });
                $("#fileList tr").mouseout(function() {
                    $(this).find(".operation").css("visibility", "hidden");
                });
                $("#fileList tr td .checkboxFour").click(function() {
                    if ($(this).find(":checkbox").prop("checked")) {//取消选择
                        $(this).find(":checkbox").removeAttr("checked");
                        $(this).find("label").css("background-color", "#FFFFFF");
                        var num = 0;
                        for (var i = 0; i < $("#fileList").find(":checkbox").length; i++)
                        {
                            if ($("#fileList").find(":checkbox").eq(i).is(':checked'))
                            {
                                num++;
                            }
                        }
                        if (num == 0)
                        {
                            $("#fileList tr .numbers").text("");
                            $("#fileList tr .pages").text("");
                        }
                        $(this).parent().parent().find("td").find(".numberss").text("");
                        $(this).parent().parent().find("td").find(".pagess").text("");
                    }
                    else {//选择
                        $(this).find(":checkbox").prop("checked", true);
                        $(this).find("label").css("background-color", "#65C91B");
                        if ($("#fileList tr .numbers").text() == "")
                        {
                            $("#fileList tr .numbers").append("打印份数");
                            $("#fileList tr .pages").append("打印页码");
                        }
                        var totalpages = $(this).parent().parent().find(".totalpages").text();
                        totalpages = totalpages.replace("页", "");
                        $(this).parent().parent().find("td").find(".numberss").append("<input type='number' class='numbersss'  min='1' step='1' value='1'  style='ime-mode:disabled;width:50px;color:black'  onKeyUp='this.value = this.value.replace(/\D/g, '');this.value = this.value.replace(\'.\', \'\');'>");
                        $(this).parent().parent().find("td").find(".pagess").append('<input type="text" style="width:70px" class="pagesss" value="1-' + totalpages + '" name = "number" onkeyup = "checkChar(this);" onblur = "checkNumber(' + totalpages + ',this.value);" / > ');

                    }
                });
                //注销
                $('#loginOut').click(function() {
                    if (confirm("确定注销？"))
                    {
                        $.post("./index.php?r=login/loginOut", function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                                window.location.href = "./index.php?r=login/login";
                        });
                    }
                });
                $("#login_center").click(function() {
                    window.location.href = './index.php?r=personalCenter/personalCenter';
                });
                $("#choicePay").click(function() {
                    var num = 0;
                    var attachmentidd = "";
                    var printnumbers = "";
                    var printPages = "";
                    var status = 1;
                    for (var i = 0; i < $("#fileList").find(":checkbox").length; i++)
                    {
                        if ($("#fileList").find(":checkbox").eq(i).is(':checked'))
                        {
                            attachmentidd += $("#fileList").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    for (var i = 0; i < $("#fileList").find(".numbersss").length; i++)
                    {
                        printnumbers += $("#fileList").find(".numbersss").eq(i).val() + ","; //打印份数
                        var totalpages = $("#fileList").find(".numbersss").eq(i).parent().parent().prev().text().replace(/\s/g, "").replace("页", "");
                        if (!checkNumber(totalpages, $("#fileList").find(".pagesss").eq(i).val().replace(/\s/g, "")))
                        {
                            $("#fileList").find(".pagesss").eq(i).css("border", "1px red solid");
                            return false;
                        }
                        if ($("#fileList").find(".pagesss").eq(i).val().replace(/\s/g, "") == "")
                        {
                            $("#fileList").find(".pagesss").eq(i).css("border", "1px red solid");
                            return false;
                        }
                        if ($("#fileList").find(".numbersss").eq(i).val() <= 0)
                        {
                            return false;
                        }
                        printPages += $("#fileList").find(".pagesss").eq(i).val() + "*";
                    }

                    attachmentidd = attachmentidd.substring(0, attachmentidd.length - 1);
                    printnumbers = printnumbers.substring(0, printnumbers.length - 1);
                    printPages = printPages.substring(0, printPages.length - 1);

                    if (num == 0)
                    {
                        alert("请选择需要打印的文件.");
                        return false;
                    }

                    $("#attachmentidd").val(attachmentidd);
                    $("#printnumbers").val(printnumbers);
                    $("#printPages").val(printPages);

                    orderForm.submit();
                });   
            });
            function deleteFile(attachmentid)
            {

                if (confirm("确定删除 " + $('.attachmentname' + attachmentid).val() + " 这个文件吗？"))
                {
                    
                    $.post("./index.php?r=myFile/deleteFile", {attachmentid: attachmentid}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
 
                        if (data.data == "success")
                        {
                            alert("删除成功！");
                            $('.attachmentname' + attachmentid).parent().parent().parent().css("display", "none");
                            window.location.reload();
                        }
                        else
                        {
                            alert("删除失败！");
                        }
                    });
                }
            }

            function edit(attachment)
            {
                $("." + attachment).css("border", "1px #333 solid");
                $("." + attachment).select();
                $("." + attachment).removeAttr("readonly");
                $("." + attachment).attr("class", attachment + " edit");
                $("." + attachment).attr("onblur", "edit_end(\"" + attachment + "\")");
            }

            function edit_end(attachment) {
                $("." + attachment).css("border", "0px");
                $("." + attachment).attr("readonly", "readonly");
                $("." + attachment).removeAttr("onblur");
                var attachmentid = attachment.replace("attachmentname", "");
                var attachmentname = $("." + attachment).val();
                $.post("./index.php?r=myFile/editFilename", {attachmentid: attachmentid, attachmentname: attachmentname}, function(datainfo) {
                    var data = eval("(" + datainfo + ")");
                    if (data.data == "success")
                        alert("修改成功！");
                    else
                    {
                        alert("修改失败！");
                    }
                });
            }
            function checkChar(obj) {
                var value = obj.value;
                value = value.replace(/^[^1-9]/, '');
                value = value.replace(/[^0-9,，\-—]+/, '');
                value = value.replace('，', ',');
                value = value.replace('—|_', '-');
                value = value.replace(/([,\-])\D+/, '$1');
                obj.value = value;
            }
            function checkNumber(filenum, printset) {//页码  和  打印设置
                var exp = /^[1-9][0-9]*([,\-][1-9][0-9]*)*$/;
                var exp2 = /\-\d+\-/;
                var pages = 0;
                if (exp.test(printset)) {
                    if (!exp2.test(printset)) {
                        var arr = printset.split(",");
                        var matches = null;
                        for (var i = 0; i < arr.length; i++) {
                            text = arr[i];
                            if (/^\d+$/.test(text) && parseInt(text) <= filenum) {
                                pages++;
                            } else if ((matches = text.match(/^(\d+)\-(\d+)$/)) != null) {
                                var n1 = parseInt(matches[1]);
                                var n2 = parseInt(matches[2]);
                                if (n1 <= filenum && n2 <= filenum) {
                                    pages = pages + Math.abs(n1 - n2) + 1;
                                } else {
                                    pages = 0;
                                    break;
                                }

                            } else {
                                pages = 0;
                                break;
                            }
                        }
                    }
                }
                return pages;
            }
            function downloads(attachmentid)
            {
                window.location.href = "./index.php?r=myFile/download&attachmentid=" + attachmentid;
            }

        </script>

     
    </head>
    <body>
        <div class="containers">
            <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png);">
                <div class="row">
                    <div class="col-md-12" style="background-color:rgba(255,255,255,.2);text-align: center;padding-top: 5px;padding-bottom: 5px">
                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-4">
                                <a href="#" id="login_center"><div  style="width:35px;height:35px;border-radius:50px;float: left;background-color:rgba(255,255,255,.2);"><img style="margin-top:5px;margin-bottom: 5px;"  src="./css/bt/username.png"></div></a>
                                <div id="topLeft" style="text-align: left;font-size:18px;color: white;padding-top: 5px;margin-left: 45px;">
                                    <?php echo $username; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div  style="color: white;margin-left: 238px;margin-top: 4px;font-size:18px;cursor:pointer" id="loginOut" >注销</div>  
                            </div>
                        </div>                    
                    </div>
                </div>
                <br />                
                <div class="row">
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2" style="text-align: center;">
                        <a><img src="./css/bt/logo.png" alt=""></a>
                    </div>
                    <div class="col-md-5">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6" style="text-align: center;color: #FFF;font-size: 18px">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <p style="letter-spacing:3px; line-height:150%">
                                    公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>    
                <br>
            </div>
        
           <div class="container" style="margin-top: 20px;background-color: white;height: 811px;margin-bottom: 50px;">
                <div class="row">
                    <div class="col-md-2">
                           <nav id="left">
                            <ul >
                                <li class="movies"><div></div><span class="movies-icon"  style="float:left"></span><a href="./index.php?r=myFile/myFile">我的文件</a></li><hr>
                                <li class="store"><div></div><span class="store-icon"></span><a href="./index.php?r=orderInfo/orderInfo">我的订单</a></li><hr>
                                <li class="music"><div></div><span class="music-icon"  style="float:left"></span><a href="./index.php?r=terninalState/terninalState">终端位置</a></li><hr>
                                <li class="books"><div></div><span class="books-icon"  style="float:left"></span><a href="./index.php?r=contactUs/ContactUs">联系我们</a></li><hr>
                                <li class="magazines"><div></div><span class="magazines-icon"  style="float:left"></span><a href="./index.php?r=personalCenter/personalCenter">个人中心</a></li><hr>
                            </ul>
                        </nav>
                        <div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
                        </div>
                    </div>
                    <div class="col-md-10" >
                        <table class="table" id="fileList" style="background-color: #FFF;color: #333;margin-top: -1px;">
                            <tr>
                                <td colspan="7" style="text-align:left"><span style="letter-spacing: 1px;">我的文件</span>&nbsp;&nbsp;&nbsp;&nbsp;<span style="letter-spacing: 1px;color:#9d9d9d;font-size: 15px">*平台支持word各版本和PDF文档打印</span>
                                    <div  id="filesupload" class="uploads" href="#uploadFilemodal"  >
                                        <div  style="text-decoration:none;">&nbsp;
                                            <img style="margin-left:-13px;padding-top:7px;padding-left: 12px;" src="./css/bt/upfile3_3.png"/><p id="uplodefiles">&nbsp;上传文件&nbsp;</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <th style="text-align: left;width: 200px;">文件名称</th>
                                <th class="filepages">文件页数</th>
                                <th class="numbers"></th>
                                <th class="pages"></th>
                                <th>上传时间</th>
                                <th>操作</th>
                            </tr>
                            <?php
                            foreach ($FileList as $K => $V) {
                                ?>
                                <tr>
                                    <td style="width: 20px;">
                                        <div class="checkboxFour" style="margin-left: 13px;">
                                            <?php
                                            $attachmentid = $V["attachmentid"];
                                            echo "<input type='checkbox'  value= '$attachmentid'>";
                                            ?>
                                            <label for="checkboxFourInput"></label>
                                        </div>
                                    </td>

                                    <td>
                                         <div style="text-align: left;width: 280px;margin-left: -15px;">
<!--                                            <a href="./index.php?r=myFile/FilePreviews&filename=<?php echo $V["attachmentfile"]; ?>" target="_blank" style="text-decoration: none;cursor: pointer;"> -->
                                                <?php
                                                if ($V["filetype"] == "doc" || $V["filetype"] == "docx") {
                                                    echo "<img src='./css/bt/DOC.png'>";
                                                } else if ($V["filetype"] == "pdf") {
                                                    echo "<img src='./css/bt/PDF.png'>";
                                                }
                                                ?>
                                                <input readonly="readonly" class="attachmentname<?php echo $V["attachmentid"]; ?>" type="text" value="<?php echo $V["attachmentname"]; ?>" title="<?php echo $V["attachmentname"]; ?>" style="border:0px;background-color:rgba(0, 0, 0, 0);width: 200px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;cursor: pointer;">
                                            <!--</a>-->
                                        </div>
                                    </td>
                                    <td style="width: 100px;" class="totalpages"><div style="text-align: left;"><?php echo $V["filenumber"]; ?>页</div></td>
                                    <td style="width: 50px">
                                        <div class="numberss" style="text-align: left;" ></div>
                                    </td>
                                    <td style="width: 100px">
                                        <div class="pagess" style="text-align: left;" ></div>
                                    </td>
                                    <td><div style="text-align: left;"><?php echo $V["loadtime"]; ?></div></td>
                                    <td>
                                        <div class="operation" style="visibility:hidden">
                                            <!--<a href="./assets/userfile/<?php echo $V["attachmentfile"]; ?>"><img src="./css/bt/download.png" width="30px" height="30px" title="下载"></a>-->
                                            <a style="cursor:pointer" onclick="downloads('<?php echo base64_encode($V["attachmentid"]); ?>')"><img src="./css/bt/download.png" width="30px" height="30px" title="下载"></a>
                                            <!--<a  style="cursor:pointer" id="share" href="#sharesmodal"><img src="./css/bt/share.png" width="30px" height="30px" title="分享" ></a>-->
                                                                                    <!--<a style="cursor:pointer"><img src="./css/bt/share.png" width="30px" height="30px" title="分享"></a>-->
                                            <a style="cursor:pointer" onclick="edit('attachmentname<?php echo $V["attachmentid"]; ?>')"><img src="./css/bt/edit.png" width="30px" height="30px" title="编辑"></a>
                                            <a style="cursor:pointer" onclick="deleteFile('<?php echo $V["attachmentid"]; ?>')"><img src="./css/bt/delete.png" width="30px" height="30px" title="删除"></a>
                                        </div>
                                    </td>                            
                                </tr>
                            <?php } ?>
                            <?php
                            if ($page_list != "") {
                                echo '<tr><td colspan="7"><div style="text-align:center;color: black;background-color:#f9f9f9;padding:12px;margin:-12px;"><strong>';
                                echo $page_list;
                                echo'</strong></div></td></tr>';
                            } else {
                                echo '<tr><td colspan="7"  ><br><br>无<br><br></td></tr>';
                            }
                            ?>
                            <tr>
                                <th colspan="7" style="text-align:right">
                                    <?php $form = $this->beginWidget('CActiveForm', array('clientOptions' => array(), 'id' => 'orderForm', 'action' => array('myFile/placeOrder'))); ?>
                                    <!--价格-->                                            
                                    <input  hidden="true" class="name" id="attachmentidd" name="attachmentidd"/>
                                    <!--份数-->
                                    <input  hidden="true" class="name" id="printnumbers" name="printnumbers"/>
                                    <!--页码-->
                                    <input  hidden="true" class="name" id="printPages" name="printPages"/>

                                    <button type="button" class="btn btn-success" id="choicePay">选择打印</button>
                                    <?php $form = $this->endWidget(); ?>


                                </th>
                            </tr>
                        </table>                      
                    </div>
                       
                </div>
            </div>
    
          

            <!--上传文件弹出框-->
            <div id="uploadFilemodal">
                <div style="font-size: 1.1em;margin-bottom:30px;">上传文件 </div><a class="hidemodal" href="#"></a>          
                <form id="uploadform" style="margin-top: -10px" name="uploadform" method="post" action="index.html">
                    <table >
                        <tr>
                            <td style="padding-left:5px;padding-bottom: 10px;">                
                               <div id="upload" style="float: left;margin-left: -20px;"></div>
                               <div  id="uploadSure"  class="flatbtn-blu"  style="margin-left: 695px;margin-top: 10px;width: 70px;height: 30px;" ><div style="margin-top:5px;">确定</div></div>
                            </td>
                        </tr>
                    </table>
                </form>
              
            </div>
            <div style="width: 100%;height: 20px;"></div>
            <div class="container " id="footer" style="text-align:center;background-color:#333;width: 100%;color:white;font-size: 15px;height:50px;">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 15px;">
                        <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.       
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




