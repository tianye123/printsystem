<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <link rel="stylesheet" href="./css/bt/footer.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script language="javascript">

            function killerrors() {
                return true;
            }
            window.onerror = killerrors;

        </script>  
        <style>
            body{
                background-color: #E5E5E5;
                font-family:Microsoft YaHei;
            }
            #Contacttable tr td{
                font-size: 15px;
                padding: 16px;
            }

            #Contacttable tr span{
                color: #333;
                font-size: 19px;  
                font-weight: bold;
            }
        </style>
    </head>
    <body>
         <div class="containers">
        <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png)">
            <br>
            <div class="row">
                <div class="col-md-5">
                </div>
                <div class="col-md-2" style="text-align: center;">
                    <a><img src="./css/bt/logo.png" alt=""></a>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6" style="text-align: center;color: #FFF;font-size: 18px">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="letter-spacing:3px; line-height:150%">
                                公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>  
            <br>
        </div>
        <br /> 
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-9">
                    <table class="table" id="Contacttable" style="background-color: #FFF;text-align: center">
                        <tr>
                            <td colspan="6" style="text-align:left"><SPAN>邮箱验证</SPAN><div style="float:right;color: #4D4D4D;">&nbsp;<a href="./index.php?r=login/login">返回首页</a></div></td>
                        </tr>
                        <tr>
                            <td>
                                验证码已发送至您的邮箱，请打开和此账户绑定的邮箱重置密码！
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

            <div style="width: 100%;height: 20px;"></div>
         <div class="container" id="footer" style="text-align:center;background-color: #333;width: 100%;color:white;font-size: 15px;height:50px;">
            <div class="row">
                <div class="col-md-12" style="margin-top: 15px;">
                    <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.       
                </div>
            </div>
        </div>
           </div>
    </body>
</html>
