<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">    
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <link href="./css/3Dmenu/css/posterTvGrid.css" rel="stylesheet" type="text/css" />
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="./css/bt/footer.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src = "./css/bt/jquery-1.10.2.min.js" type = "text/javascript"></script>
        <script type="text/javascript" src="./css/3Dmenu/js/posterTvGrid.js"></script>
        <script type="text/javascript" src="./css/outWindows/js/jquery.leanModal.min.js"></script>
        <style>
            body{
                background-color: #F0F0F0;

            }
            #registermodal label{
                margin-top: 8px;
                text-align: right;

            }
            #registermodal .star{
                margin-top: 11px;margin-left: 1px;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                var menu = new posterTvGrid(
                        'menu',
                        {className: "posterTvGrid"},
                [{"img": ".\/css\/bt\/myFile.png", "title": "chinaz", "url": "./index.php?r=myFile/myFile"},
                    {"img": ".\/css\/bt\/myOrder.png", "title": "chinaz", "url": "./index.php?r=orderInfo/orderInfo"},
                    {"img": ".\/css\/bt\/terninalState.png", "title": "chinaz", "url": "./index.php?r=terninalState/terninalState"},
                    {"img": ".\/css\/bt\/contactUs.png", "title": "chinaz", "url": "./index.php?r=contactUs/contactUs"},
                    {"img": ".\/css\/bt\/personalCenter.png", "title": "chinaz", "url": "./index.php?r=personalCenter/personalCenter"}]);

                if ("<?php echo $username; ?>" != "" && "<?php echo $username; ?>" != null)
                {
                    $("#topLeft").text("<?php echo $username ?>");
                    $("#login_center").attr("href", "./index.php?r=personalCenter/personalCenter");
                    $("#img1").leanModal({top: 110, overlay: 0.45, closeButton: ".hidemodal"});
                    $("#loginOut").css("visibility", "");
                    $("#lean_overlay").css({"display": "none", opacity: 0});
//                    $("#actionmodal").css({"display": "none"});
                }
                else
                {
                    for (var i = 1; i < 7; i++)
                    {
                        $("#img" + i).attr("href", "#loginmodal");
                        $("#img" + i).leanModal({top: 110, overlay: 0.45, closeButton: ".hidemodal"}); //登录弹窗
                    }

                    $("#beginPrint_btn").attr("href", "#loginmodal");
                    $("#beginPrint_btn").leanModal({top: 110, overlay: 0.45, closeButton: ".hidemodal"}); //登录弹窗

                    var overlay = $("<div id='lean_overlay'></div>");
                }
                if ("<?php echo $cookie_username; ?>" != "" && "<?php echo $cookie_username; ?>" != null)
                {
                    $("#loginUsername").val("<?php echo $cookie_username; ?>");
                }
                if ("<?php echo $cookie_password; ?>" != "" && "<?php echo $cookie_password; ?>" != null)
                {
                    $("#loginPassword").val("<?php echo base64_decode($cookie_password); ?>");
                }


                if (window.screen.availHeight >= 800)//登录 高 340px
                {
                    $('#beLogin').leanModal({top: 100, overlay: 0.45, closeButton: ".hidemodal"}); //登录弹窗
                    $('#register_Login').leanModal({top: 100, overlay: 0.45, closeButton: ".hidemodal"}); //点此登录  弹窗
                    $('#beRegister').leanModal({top: 40, overlay: 0.45, closeButton: ".hidemodal"}); //注册弹窗
                    $('#Login_register').leanModal({top: 40, overlay: 0.45, closeButton: ".hidemodal"}); //点此注册  弹窗
                }
                else
                {
                    $('#beLogin').leanModal({top: 60, overlay: 0.45, closeButton: ".hidemodal"}); //登录弹窗
                    $('#register_Login').leanModal({top: 60, overlay: 0.45, closeButton: ".hidemodal"}); //点此登录  弹窗
                    $('#beRegister').leanModal({top: 10, overlay: 0.45, closeButton: ".hidemodal"}); //注册弹窗
                    $('#Login_register').leanModal({top: 10, overlay: 0.45, closeButton: ".hidemodal"}); //点此注册  弹窗
                }

                //注册跳登录，把登录窗口隐藏
                $('#Login_register').click(function() {
                    $("#loginmodal").css({"display": "none"});
                });
                //注册跳登录，把注册窗口隐藏
                $('#register_Login').click(function() {
                    $("#registermodal").css({"display": "none"});
                });
                //注销
                $('#loginOut').click(function() {
                    if (confirm("确定注销？"))
                    {
                        $.post("./index.php?r=login/loginOut", function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                window.location.href = "./index.php?r=login/login";
                            }
                        });
                    }
                });
                //登录错误信息
                function trueLoginInfo() {
                    $("#trueLoginInfo").css("background-image", "url(./images/login/error.png)")
                    $("#trueLoginInfo").css("background-color", "red");
                    $("#trueLoginInfo").css("color", "white");
                }


                //恢复错误信息
                function retrueLoginInfo() {
                    $("#trueLoginInfo").css("background-image", "url(./images/login/info.png)")
                    $("#trueLoginInfo").css("background-color", "#FFEBC0");
                    $("#trueLoginInfo").css("color", "#99747B");
                    $("#trueLoginInfo").text("公共场所不建议记住密码，以防帐号丢失");
                    $("#loginUsername").css("background-image", "url(./images/login/username-gray.png)");
                    $("#loginUsername").css("border", "2px #F3F6FA solid");
                    $("#loginPassword").css("background-image", "url(./images/login/userpsw-gray.png)");
                    $("#loginPassword").css("border", "2px #F3F6FA solid");
                }
                //登录用户名改变
                $("#loginUsername").change(function() {
                    retrueLoginInfo()
                });
                //登录密码改变
                $("#loginPassword").change(function() {
                    retrueLoginInfo()
                });
                //登录按钮
                $("#loginbtn").click(function() {
                    var loginUsername = $("#loginUsername").val();
                    var loginPassword = $("#loginPassword").val();
                    var remeber = $("#RememberMe").is(':checked');
                    if (loginUsername == "")
                    {
                        trueLoginInfo();
                        $("#loginUsername").css("background-image", "url(./images/login/username-red.png)");
                        $("#loginUsername").css("border", "2px #FE0000 solid");
                        $("#trueLoginInfo").text("请您输入用户名!");
                        return false;
                    }
                    else if (loginPassword == "")
                    {
                        trueLoginInfo();
                        $("#loginPassword").css("background-image", "url(./images/login/userpsw-red.png)");
                        $("#loginPassword").css("border", "2px #FE0000 solid");
                        $("#trueLoginInfo").text("请您输入密码!");
                        return false;
                    }
                    else
                    {
                        $.post("./index.php?r=Login/userLogin", {loginUsername: loginUsername, loginPassword: loginPassword, remeber: remeber}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                $("#loginUsername").val("");
                                $("#loginPassword").val("");
                                window.location.href = "./index.php?r=login/login";
                            }
                            else
                            {
                                trueLoginInfo();
                                $("#trueLoginInfo").text("请您输入正确的密码!");
                                $("#loginPassword").val("");
                                $("#loginPassword").css("background-image", "url(./images/login/userpsw-red.png)");
                                $("#loginPassword").css("border", "2px #FE0000 solid");
                            }
                        });
                    }

                });
                //登录错误信息
                function trueregisterInfo() {
                    $("#trueregisterInfo").css("background-image", "url(./images/login/error.png)")
                    $("#trueregisterInfo").css("background-color", "red");
                    $("#trueregisterInfo").css("color", "white");
                }
                //恢复错误信息
                function retrueregisterInfo() {
                    $("#trueregisterInfo").css("background-image", "url(./images/login/info.png)")
                    $("#trueregisterInfo").css("background-color", "#FFEBC0");
                    $("#trueregisterInfo").css("color", "#99747B");
                    $("#trueregisterInfo").text("温馨提示：*为必填");
                    $("#registerUsername").css("border", "2px #F3F6FA solid");
                    $("#registerPassword").css("border", "2px #F3F6FA solid");
                    $("#surePassword").css("border", "2px #F3F6FA solid");
                    $("#registerPhone").css("border", "2px #F3F6FA solid");
                    $("#registerEmail").css("border", "2px #F3F6FA solid");
                }

                $("#registerUsername").change(function() {
                    retrueregisterInfo();
                });
                $("#registerPassword").change(function() {
                    retrueregisterInfo();
                });
                $("#surePassword").change(function() {
                    retrueregisterInfo();
                });
                $("#registerPhone").change(function() {
                    retrueregisterInfo();
                });
                $("#registerEmail").change(function() {
                    retrueregisterInfo();
                });
                //注册
                $("#registerbtn").click(function() {
                    var registerUsername = $("#registerUsername").val();
                    var registerPassword = $("#registerPassword").val();
                    var surePassword = $("#surePassword").val();
                    var registerPhone = $("#registerPhone").val();
                    var registerEmail = $("#registerEmail").val();
                    var phonecode = $("#phonecode").val();
                    var school = $("#school").val();
                    var protocol = $("#protocol").is(":checked");
                    if (registerUsername == "")
                    {
                        trueregisterInfo();
                        $("#registerUsername").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("请您输入用户名!");
                        return false;
                    }
                    if (registerUsername.length < 5)
                    {
                        trueregisterInfo();
                        $("#registerUsername").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("用户名位数不能少于五位!");
                        return false;
                    }
                    if (registerUsername.length > 12)
                    {
                        trueregisterInfo();
                        $("#registerUsername").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("用户名位数不能多于十二位!");
                        return false;
                    }
                    if (registerPassword == "")
                    {
                        trueregisterInfo();
                        $("#registerPassword").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("请您输入密码!");
                        return false;
                    }
                    if (registerPassword.length < 6)
                    {
                        trueregisterInfo();
                        $("#registerPassword").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("密码位数不能少于六位!");
                        return false;
                    }
                    if (registerPassword.length > 20)
                    {
                        trueregisterInfo();
                        $("#registerPassword").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("密码位数不能多于二十位!");
                        return false;
                    }
                    if (surePassword == "")
                    {
                        trueregisterInfo();
                        $("#surePassword").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("请您输入确认密码!");
                        return false;
                    }
                    if (registerPassword != surePassword)
                    {
                        trueregisterInfo();
                        $("#surePassword").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("两次密码不一样");
                        return false;
                    }
                    if (school == 0)
                    {
                        trueregisterInfo();
                        $("#trueregisterInfo").text("请选择学校!");
                        return false;
                    }
                    if (registerPhone == "")
                    {
                        trueregisterInfo();
                        $("#registerPhone").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("请您输入手机号码!");
                        return false;
                    }
                    if (!/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/i.test(registerPhone))
                    {
                        trueregisterInfo();
                        $("#registerPhone").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("手机号码格式不对!");
                        return false;
                    }
                    if (phonecode == "")
                    {
                        trueregisterInfo();
                        $("#phonecode").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("请您输入验证码!");
                        return false;
                    }
                    if (protocol == false)
                    {
                        trueregisterInfo();
                        $("#trueregisterInfo").text("请先阅读用户注册协议!");
                        return false;
                    }
                    if (registerEmail != "")
                    {
                        var filter = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
                        if (filter.test(registerEmail))
                        {
                            $.post("./index.php?r=Login/userRegister", {registerUsername: registerUsername, registerPassword: registerPassword, registerPhone: registerPhone, registerEmail: registerEmail, phonecode: phonecode, school: school}, function(datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "success")
                                {
                                    $("#registerUsername").val("");
                                    $("#registerPassword").val("");
                                    $("#surePassword").val("");
                                    $("#registerPhone").val("");
                                    $("#registerEmail").val("");
                                    $("#phonecode").val("");
                                    window.location.href = "./index.php?r=login/login";
                                }
                                else if (data.data == "false-exist")
                                {
                                    trueregisterInfo();
                                    $("#registerUsername").css("border", "2px #FE0000 solid");
                                    $("#trueregisterInfo").text("用户名已存在，请您更换!");
                                }
                                else if (data.data == "phonecode_false")
                                {
                                    trueregisterInfo();
                                    $("#phonecode").css("border", "2px #FE0000 solid");
                                    $("#trueregisterInfo").text("手机验证码错误，请核对后重试!");
                                }
                                else
                                {
                                    trueregisterInfo();
                                    $("#trueregisterInfo").text("发生未知的错误，请刷新后重试!");
                                }
                            });
                        }
                        else {
                            trueregisterInfo();
                            $("#registerEmail").css("border", "2px #FE0000 solid");
                            $("#trueregisterInfo").text("邮箱格式不对!");
                            return false;
                        }
                    }
                    else {
                        $.post("./index.php?r=Login/userRegister", {registerUsername: registerUsername, registerPassword: registerPassword, registerPhone: registerPhone, registerEmail: registerEmail, phonecode: phonecode, school: school}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                $("#registerUsername").val("");
                                $("#registerPassword").val("");
                                $("#surePassword").val("");
                                $("#registerPhone").val("");
                                $("#registerEmail").val("");
                                $("#school").val(0);
                                $("#phonecode").val("");
                                window.location.href = "./index.php?r=login/login";
                            }
                            else if (data.data == "false-exist")
                            {
                                trueregisterInfo();
                                $("#registerUsername").css("border", "2px #FE0000 solid");
                                $("#trueregisterInfo").text("用户名已存在，请您更换!");
                            }
                            else if (data.data == "phonecode_false")
                            {
                                trueregisterInfo();
                                $("#phonecode").css("border", "2px #FE0000 solid");
                                $("#trueregisterInfo").text("手机验证码错误，请核对后重试!");
                            }
                            else
                            {
                                trueregisterInfo();
                                $("#trueregisterInfo").text("发生未知的错误，请刷新后重试!");
                            }
                        });
                    }
                });
                $("#btn_code").click(function() {
                    var registerPhone = $("#registerPhone").val();
                    if (registerPhone == "")
                    {
                        trueregisterInfo();
                        $("#registerPhone").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("请您输入手机号码!");
                        return false;
                    }
                    if (!/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/i.test(registerPhone))
                    {
                        trueregisterInfo();
                        $("#registerPhone").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("手机号码格式不对!");
                        return false;
                    }
                    else
                    {
                        $.post("./index.php?r=Login/Checkphone", {registerPhone: registerPhone}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                trueregisterInfo();
                                $("#registerPhone").css("border", "2px #FE0000 solid");
                                $("#trueregisterInfo").text("手机号码已经被注册过了，请您换一个!");
                            }
                            else
                            {
                                time();
                                $.post("./index.php?r=Login/userRegisterphone", {registerPhone: registerPhone}, function(datainfo) {
                                    var data = eval("(" + datainfo + ")");
                                    if (data.data == "success")
                                    {
                                        $("#trueregisterInfo").text("验证码已发送至您的手机，请输入验证码!");
                                    }
                                    else
                                    {
                                        trueregisterInfo();
                                        $("#trueregisterInfo").text("发生了不可预知的错误!");
                                    }
                                });
                            }
                        });
                    }
                });
                $("#beginPrint_btn").click(function() {
                    if ("<?php echo $username; ?>" != "" && "<?php echo $username; ?>" != null)
                    {
                        window.location.href = "./index.php?r=myFile/myFile";
                    }
                });

            });
            // Enter键登录 
            function keyLogin(event) {
                var event = window.event || arguments.callee.caller.arguments[0];
                if (event.keyCode == 13)  //回车键的键值为13
                {
                    if ($("#loginmodal").css("display") == "block")
                    {
                        $("#loginbtn").click();
                    }
                    else if ($("#registermodal").css("display") == "block")
                    {
                        $("#registerbtn").click();
                    }
                }
            }
            var wait = 60;
            function time() {
                if (wait == 0) {
                    $("#btn_code").attr("disabled", false);
                    $("#btn_code").attr("value", "获取验证码");
                    wait = 60;
                } else {
                    $("#btn_code").attr("value", wait + "秒后再次获取");
                    $("#btn_code").attr("disabled", true);
                    wait--;
                    setTimeout(function() {
                        time();
                    }, 1000)
                }
            }
            function killerrors() {
                return true;
            }
            window.onerror = killerrors;
        </script>
    </head>
    <body onkeydown="keyLogin()" >
        <div class="containers">
            <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png)">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="background-color:rgba(255,255,255,.2);text-align: center;padding-top: 5px;padding-bottom: 5px">
                        <div class="row" style="font-family:Microsoft YaHei;">
                            <div class="col-md-2 col-sm-2 col-xs-2">
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <a href="#" id="login_center">
                                    <div style="width:35px;height:35px;border-radius:50px;float: left;background-color:rgba(255,255,255,.2);">
                                        <img style="margin-top:5px;margin-bottom: 5px;"  src="./css/bt/username.png">
                                    </div>
                                </a>
                                <div id="topLeft" style="text-align: left;font-size:18px;color: white;padding-top: 5px;padding-left: 45px;">
                                    <div  style="color: white;float:left;margin-top: 0;cursor:pointer" id="beLogin" href="#loginmodal">登 录</div>                                    
                                    <p style="color: white;float:left;margin-left: 8px;margin-top: 0">|</p>
                                    <div  style="color: white;float:left;margin-left: 8px;margin-top: 0;cursor:pointer" id ="beRegister" href="#registermodal">注 册</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-4">
                                <div  style="color: white;margin-left: 300px;margin-top: 4px;font-size:18px;visibility: hidden;cursor:pointer" id="loginOut" >注销</div>                                    
                            </div>
                        </div>                    
                    </div>
                </div>
                <br />                
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-5">
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2" style="text-align: center;">
                        <a><img src="./css/bt/logo.png" alt=""></a>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-5">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6" style="text-align: center;color: #FFF;font-size: 18px">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="font-family:Microsoft YaHei;">
                                <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <p style="letter-spacing:3px; line-height:150%;font-family:Microsoft YaHei;">
                                    公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                                </p>
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                    </div>
                </div>                    
            </div>
            <br />  
            <div class="container" style="font-family:Microsoft YaHei;">
                <div class="row" style="color: #2DC0FD;border: 1px dashed #BBBBBB;text-align: center;padding: 2%; ">
                    <div class="col-md-3 col-sm-3 col-xs-3" style="margin-top:15px">怎么使用自主打印？</div>
                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <img src="./css/bt/choice.png">
                        <div>选择文件</div>      
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1"><img src="./css/bt/jt.png"></div>
                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <img src="./css/bt/order.png">
                        <div>确认订单</div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1"><img src="./css/bt/jt.png"></div>
                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <img src="./css/bt/pay.png">
                        <div>选择支付</div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1"><img src="./css/bt/jt.png"></div>
                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <img src="./css/bt/print.png">
                        <div>终端打印</div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <input type="button" style="margin-top:13px" name="beginPrint_btn" id="beginPrint_btn" class="btn btn-danger" value="开启打印之旅">
                    </div>
                </div>
            </div>
            <!--        <div style="float:right;margin-top: 20px;margin-right: 20px;color:black;border:1px #E0E0E0 solid;padding: 5px;">
                        <img src="./images/login/ewm.png" style="width:100px;height: 100px">
                        <div><a href="http://www.cqutprint.com/appVersion/cqutprint.apk"  style="color:#00D9FF">手机客户端下载</a></div>
                    </div>-->
            <div style="border:1px #E0E0E0 solid;padding: 5px;position: fixed;right: 10px;z-index: 1000;top:45%;font-family:Microsoft YaHei;" >
                <img src="./images/login/ewm.png" style="width:100px;height: 100px">
                <div><a href="http://www.cqutprint.com/appVersion/cqutprint.apk"  style="color:#00D9FF;font-size:13px;">android版客户端</a></div>
            </div>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div id="menu"></div>
                    </div>
                </div>
            </div>

            <div id="loginmodal">
                <h1 style="font-size: 1.5em;font-family:Microsoft YaHei;">用户登录</h1>  <a class="hidemodal" href="#"></a>
                <form id="loginform" style="margin-top: 10px;" name="loginform" method="post" action="index.html">

                    <div id="trueLoginInfo" style="font-family:Microsoft YaHei;background-image: url(./images/login/info.png);background-repeat:no-repeat;padding-left: 25px;background-color: #FFEBC0;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;margin-bottom: 15px;color:#787471;">
                        公共场所不建议记住密码，以防帐号丢失
                    </div>
                    <div style="background-color:white;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px">
                        <input type="text" style="font-family:Microsoft YaHei;float:left;background-image: url(./images/login/username-gray.png);background-repeat:no-repeat;background-color: white;width:300px;margin-top: 1px;border: 2px #F3F6FA solid;height: 38px;line-height: 38px;padding-left: 35px;font-size: 18px;" name="loginUsername" id="loginUsername" class="txtfield loginUsername" tabindex="1" placeholder="请输入用户名或手机号">
                    </div>
                    <div style="background-color:white;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;">
                        <input type="password" style="font-family:Microsoft YaHei;float:left;background-image: url(./images/login/userpsw-gray.png);background-repeat:no-repeat;background-color: white;width:300px;margin-top: 1px;border: 2px #F3F6FA solid;height: 38px;line-height: 38px;padding-left: 35px;font-size: 18px;padding-top: 0px" name="loginPassword" id="loginPassword" class="txtfield loginPassword" tabindex="2" placeholder="请输入密码">
                    </div>
                    <div style="border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;">
                        <DL>
                            <dd style="color:#333333">
                                <input id="RememberMe" name="RememberMe" type="checkbox" value="" checked="checked" style="font-family:Microsoft YaHei;position:relative;top:2px;">
                                记住密码
                                <a href="./index.php?r=login/Findpsw" id="forgetPsw" style="font-family:Microsoft YaHei;float: right">忘记密码>></a>
                            </dd>
                        </DL>
                    </div>  
                    <div class="center"><input style="font-family:Microsoft YaHei;" type="button" name="loginbtn" id="loginbtn" class="flatbtn-blu" value="登 录" tabindex="3"></div>
                    <div style="margin-top: 5px;">
                        <a style="font-family:Microsoft YaHei;margin-left: 179px;" href="#registermodal" id="Login_register">
                            还没有帐号？点此注册>>
                        </a>
                    </div>
                </form>
            </div>

            <!--注册弹出框-->
            <div id="registermodal">
                <h1 style="font-size: 1.5em;font-family:Microsoft YaHei;">用户注册</h1> <a class="hidemodal" href="#"></a>
                <form id="registerform" style="margin-top: 30px" name="registerform" method="post" action="index.html">
                    <div id="trueregisterInfo" style="font-family:Microsoft YaHei;background-image: url(./images/login/info.png);background-repeat:no-repeat;padding-left: 25px;background-color: #FFEBC0;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;margin-bottom: 15px;color:#787471">
                        <p style="float: left;font-family:Microsoft YaHei;">温馨提示：</p><p style="font-family:Microsoft YaHei;margin-top: 3px;float: left;">*</p>&nbsp;为必填
                    </div>
                    <div style="background-color: white;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;height: 36px">
                        <label for="username" style="background-color: white;float: left;width: 73px">用户名:&nbsp;</label>

                        <input type="text" style="float:left;background-color: white;width:220px;margin-top: 1px;border: 2px #F3F6FA solid;height: 38px;line-height: 38px;font-size: 18px;ime-mode: disabled;" name="registerUsername" id="registerUsername" class="txtfield registerUsername" tabindex="1" placeholder="请输入用户名">
                        <div class="star" style="color:red;float: left">*</div>
                    </div>
                    <div style="background-color: white;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;">
                        <label for="password" style="background-color: white;float: left;width: 73px">密 码:&nbsp;</label>

                        <input type="password" style="float:left;background-color: white;width:220px;margin-top: 1px;border: 2px #F3F6FA solid;height: 38px;line-height: 38px;ont-size: 18px;padding-top: 0px" name="registerPassword" id="registerPassword" class="txtfield" tabindex="2" placeholder="请输入密码">
                        <div class="star" style="color:red;float: left">*</div>
                    </div> 
                    <div style="background-color: white;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;">
                        <label for="surePassword" style="background-color: white;float: left;width: 73px">确认密码:&nbsp;</label>

                        <input type="password" style="float:left;background-color: white;width:220px;margin-top: 1px;border: 2px #F3F6FA solid;height: 38px;line-height: 38px;ont-size: 18px;padding-top: 0px" name="surePassword" id="surePassword" class="txtfield" tabindex="3" placeholder="请确认密码">
                        <div class="star" style="color:red;float: left">*</div>
                    </div> 
                    <div style="background-color: white;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;">
                        <label for="email" style="background-color: white;float: left;width: 73px">邮 箱:&nbsp;</label>

                        <input type="email" style="float:left;background-color: white;width:220px;margin-top: 1px;border: 2px #F3F6FA solid;height: 38px;line-height: 38px;ont-size: 18px;" name="registerEmail" id="registerEmail" class="txtfield" tabindex="4" placeholder="请输入邮箱">
                    </div> 
                    <div style="background-color: white;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;">
                        <label style="background-color: white;float: left;width: 73px">所属学校:&nbsp;</label>
                        <select style="float:left;background-color: white;width:220px;margin-top: 1px;border: 2px #F3F6FA solid;height: 38px;line-height: 38px;ont-size: 18px;padding-top: 0px" name="school" id="school" class="txtfield" tabindex="5">
                            <option value="0">请选择</option>
                            <?php foreach ($store_info as $k => $l) { ?>
                                <option value="<?php echo $l->storeid; ?>"><?php echo $l->storename; ?></option>
                            <?php } ?>
                        </select>
                        <div class="star" style="color:red;float: left">*</div>
                    </div> 
                    <div style="background-color: white;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;">
                        <label for="phone" style="background-color: white;float: left;width: 73px">手机号码:&nbsp;</label>

                        <input type="phone" style="float:left;background-color: white;width:220px;margin-top: 1px;border: 2px #F3F6FA solid;height: 38px;line-height: 38px;ont-size: 18px;" name="registerPhone" id="registerPhone" class="txtfield" tabindex="6" placeholder="请输入手机号码">
                        <div class="star" style="color:red;float: left">*</div>
                    </div> 
                    <div style="background-color:  white;border:0px #B7D1EA solid;margin-left: 30px;margin-right: 30px;">
                        <label for="code" style="background-color:  white;float: left;width: 73px">验证码:&nbsp;</label>
                        <input type="text" style="float:left;background-color: white;width:88px;margin-top: 1px;border: 2px #F3F6FA solid;height: 38px;line-height: 38px;ont-size: 18px;" name="phonecode" id="phonecode" class="txtfield" tabindex="7" placeholder="验证码">
                        <div class="star" style="color:red;float: left">*</div>
                        <input type="button" style="font-family:Microsoft YaHei;float:left;margin-left: 6px;margin-top: 3px;" class="btn btn-info" value="获取验证码" name="btn_code" id="btn_code" tabindex="8">
                        <div style="float:left;"><input type="checkbox" checked="checked" id="protocol" style="  margin-top: 9px; width: 14px;height: 14px;margin-left: 17px;margin-right: 140px;" tabindex="9"></div>
                        <div style="font-family:Microsoft YaHei;float:left;font-size: 12px;margin-left: -160px;width:220px" ><label style="font-family:Microsoft YaHei;">我已阅读并同意<a href="./index.php?r=login/protocol" target="_blank" style="font-family:Microsoft YaHei;color:#0e509e;text-decoration: none; ">《用户注册协议》</a></label> 
                        </div>
                    </div>    

                    <!--hidemodal  隐藏弹出框-->
                    <div class="center"><input type="button" name="registerbtn" id="registerbtn" class="flatbtn-blu" value="注 册" tabindex="10"></div>
                    <div style="margin-top: 10px;">
                        <a style="margin-left: 193px" href="#loginmodal" id="register_Login">
                            已有帐号？点此登录>>
                        </a>
                        <br><br>
                    </div>
                </form>
            </div>
            <!--首页活动弹出框-->
            <!--         <div class="container"  >
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" id="actionmodal" href="#registermodal" >
                               <img src="./images/login/online.png"/>
                            </div>
                        </div>
                    </div>
            -->

            <div style="width: 100%;height: 20px;"></div>
            <div class="container" id="footer" style="font-family:Microsoft YaHei;text-align:center;background-color: #333;width: 100%;color:white;font-size: 15px;height:50px;">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 15px;">
                        <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>