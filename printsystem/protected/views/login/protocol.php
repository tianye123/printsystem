<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">    
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
           <style>
            h5{margin-top: 15px;margin-bottom: 15px;font-size: 13px;color:#888;}
            .col-md-3{
                width: 12%;
            }
            .col-sm-3{
                width: 12%;
            }
            .col-xs-3{
                width:12%;
            }
            p{
              line-height:22px;  
            } 
        </style>  
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="background-color:#e8f4ef;font-family:宋体;">
        <div class="container" style="margin-top: 25px;margin-bottom: 25px;margin-left: auto;margin-right: auto;letter-spacing: 1px"> 
            <div class="row"> 
                <div class="col-md-3 col-sm-3 col-xs-3"></div>
                <div class="col-md-9 col-sm-9 col-xs-9" style="background-color:#fff;border-radius: 5px;">
                    <div style="border: 1px solid #bbdff0;margin-top: 15px;margin-bottom: 15px;">
                        <div style="margin-left: 35px;margin-right: 35px;margin-top: 5px;margin-bottom: 5px;">
                    <span style="font-size: 12px;color:#888888;margin: 40px">
                        <p style="text-align: center;font-weight: bold;color: #2a85bc;font-size: 16px;margin-top: 10px;margin-bottom: 50px;"><strong>颇闰服务协议</strong></p>
                        <p>&nbsp;&nbsp;&nbsp;欢迎您成为颇闰自助打印的注册用户！</p>
                        <p>&nbsp;&nbsp;&nbsp;为使您能够安全、便捷的使用本公司提供的自助打印服务，在平等、自愿、公平的基础上，由您与云打印官网运营方重庆颇闰科技有限公司订立协议，共同信守。</p>
                        <h5><strong>&nbsp;&nbsp;1.协议声明</STRONG></h5>
                        <p>1.1&nbsp;在您申请注册成为本网站的用户前，请您仔细阅读、充分理解并接受本协议的全部内容。一旦您选择“我已阅读并同意《用户注册协议》”即表示您自愿接受并遵守本协议，正式成为本网用户。</p>
                        <p>1.2&nbsp;若您不同意本协议，或对本协议的条款存在任何疑问，您可立即停止网站注册程序。</p>
                        <h5><strong>&nbsp;&nbsp;2.服务声明</STRONG></h5>
                        <p>2.1&nbsp;本公司通过互联网依法为用户提供互联网打印等服务，用户可自愿使用提供的打印平台和帐号。</p>
                        <p>2.2&nbsp;本公司提供的打印服务符合法律法规的规定及政府主管部门发布的相关标准；用户也应遵守法律法规的规定使用我方提供的服务。</p>
                        <p>2.3&nbsp;除非事先获得您的明确授权或相应的法律程序及相关政府部门要求提供，否则涉及您姓名、电子邮箱、账号和电话号码等个人信息的，将予以严格保密。</p>
                        <p>2.4&nbsp;本公司保留在您违反国家、地方法律法规规定或违反本服务条款的情况下中止或终止为您提供服务的权利。</p>
                        <h5><strong>&nbsp;&nbsp;3.用户信息</STRONG></h5>
                        <p>3.1&nbsp;注册资格，用户须具有法定的相应权利能力和行为能力的自然人、法人或其他组织，能够独立承担法律责任。您完成注册程序使用本平台服务时，即视为您确认自己具备主体资格，能够独立承担法律责任。若因您不具备主体资格，而导致的一切后果，由您及您的监护人自行承担。</p>
                        <p>3.2&nbsp;您注册成功后，即成为本网的用户，将持有用户名和密码等账户信息，您可以根据本站规定改变您的密码。</p>
                        <p>3.3&nbsp;用户应自行诚信向本站提供注册资料，用户同意其提供的注册资料真实、准确、完整、合法有效，用户注册资料如有变动的，应及时更新其注册资料。</p>
                        <p>3.4&nbsp;账号及密码如有遗忘，可在官网服务平台进行找回密码或联系服务客服进行重置。</p>
                        <h5><strong>&nbsp;&nbsp;4.费用交纳</STRONG></h5>
                        <p>4.1&nbsp;本公司提供的自助打印服务,需在官网或是手机终端上注册方可使用；新用户注册，立送500积分。</p>
                        <p>4.2&nbsp;用户首次签到可得5个积分，后连续签到可每日多领取5个积分；如有断日，则需从首次签到开始。</p>
                        <p>4.3&nbsp;新旧用户可凭积分数抵除部分打印金额。换算：1个积分=1分钱 </p>
                        <p>4.4&nbsp;用户使用本网站提供的自助打印服务应支付相关费用，按照双方约定，采用先付后使用的结算方式收取相关费用。</p>
                        <p>4.5&nbsp;支持支付宝和线下终端支付（投币或者一卡通）等支付方式，用户可自行选择。</p>
                        <p>4.6&nbsp;新旧用户若在网上选择支付宝支付可获得所支付金额换算所得积分的10%。如:支付1块钱=100个积分，送10个积分</p>
                        <p>4.7&nbsp;本公司申明，打印2毛钱一页。绝不额外收费。</p>
                        <p>4.8&nbsp;费用的使用解释权归本公司所有。</p>
                        <br><br>
                        <div class="row">
                            <div class="col-md-10">
                            </div>
                            <div class="col-md-2" style="text-align: center">
                                重庆颇闰科技
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                            </div>
                            <div class="col-md-2" style="text-align: center">
                                二零一五年九月
                                 <br> <br>
                            </div>
                              
                        </div>
                    </span>
                        </div>
                            </div>
                </div>
            </div>
        </div>
    </body>
</html>