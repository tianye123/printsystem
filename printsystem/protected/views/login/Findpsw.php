<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <link rel="stylesheet" href="./css/bt/footer.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script language="javascript">

            function killerrors() {
                return true;
            }
            window.onerror = killerrors;
            $(document).ready(function() {
                $("#FindPswbtn").click(function() {
                    var FindUsername = $("#FindUsername").val();
                    if (FindUsername == "")
                    {
                        $("#FindUsername").css("border", "2px red solid");
                        $("#username_error").text("请输入用户名！");
                        return false;
                    }
                    $("#FindUsername").change(function() {
                        $("#FindUsername").css("border", "2px #F3F6FA solid");
                        $("#username_error").text("");
                    });
                    jyyzForm.submit();
                });
            });
        </script>  
        <style>
            body{
                background-color: #E5E5E5;
            }
            #Contacttable tr td{
                font-size: 15px;
                padding: 16px;
            }

            #Contacttable tr span{
                color: #333;
                font-size: 19px;  
                font-weight: bold;
            }
        </style>
    </head>
    <body style="font-family:Microsoft YaHei;">
         <div class="containers">
        <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png)">
            <br>
            <div class="row">
                <div class="col-md-5">
                </div>
                <div class="col-md-2" style="text-align: center;">
                    <a><img src="./css/bt/logo.png" alt=""></a>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6" style="text-align: center;color: #FFF;font-size: 18px">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="letter-spacing:3px; line-height:150%">
                                公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>  
            <br>
        </div>
        <br /> 
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-9">
                    <form method="post" class="form-horizontal" role="form" id="jyyzForm" target="test" action="./index.php?r=login/checkpsw">

                        <table class="table" id="Contacttable" style="background-color: #FFF;text-align: center">
                            <tr>
                                <td colspan="6" style="text-align:left"><span>找回密码</span><div style="float:right;color: #4D4D4D;">&nbsp;<A href="./index.php?r=login/login">返回首页</A></div></td>
                            </tr>
                            <tr>
                                <td>
                                    请输入您需要找回登录密码的用户名
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group" style="">
                                        <label  class="col-sm-2 control-label">用户名:</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="FindUsername" id="FindUsername" placeholder="请输入用户名">
                                        </div>
                                        <div class="col-sm-2" style="color:red" id="username_error"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label  class="col-sm-2 control-label">验证码:</label>
                                        <div class="col-sm-4">
                                            <script async type="text/javascript" src="http://api.geetest.com/get.php?gt=562c1e19b5080bcd4893989beaf9db9b"></script>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="button" name="FindPswbtn" id="FindPswbtn" class="btn btn-info btn-lg btn-block" value="提交">
                                </td>
                            <tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>





        <iframe style="display:none" name="test"></iframe>
            <div style="width: 100%;height: 20px;"></div>
         <div class="container" id="footer" style="text-align:center;background-color: #333;width: 100%;color:white;font-size: 15px;height:50px;">
            <div class="row">
                <div class="col-md-12" style="margin-top: 15px;">
                    <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.       
                </div>
            </div>
        </div>
           </div>
    </body>
</html>
