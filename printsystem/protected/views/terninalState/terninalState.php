<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link href="./css/PictureDisplay/css/bootstrap.css" rel="stylesheet">
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <link href="./css/leftNavigation/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="./css/bt/footer.css">
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <script src = "./css/bt/bootstrap.min.js" type = "text/javascript"></script>
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            body{
                background-color: #E5E5E5;
                font-family:Microsoft YaHei;
                overflow-y:scroll;
                overflow-x: hidden;
            }
            #Contacttable tr td{
                text-align: left;
                font-size: 15px;
                padding: 12px;
            }
            #Contacttable tr td a{
                text-decoration:none;
            }
            #Contacttable tr th{
                font-size: 15px;
                padding: 12px;
            }
            #Contacttable tr span{
                color: red;
                font-size: 15px;  
                font-weight: bold;
            }
            #Contacttable .txtfield{
                width:220px;
                margin-top: 1px;
                border: 0px;
                height: 38px;
                line-height: 38px;
                font-size: 18px;
                box-shadow:none;
            }
            #Contacttable .star{
                color:red;
                visibility: hidden;
            }
            #editbtn{
                visibility: hidden;
            }
            #Contacttable .psw{
                display:  none;
            }
            #trueregisterInfo {
                visibility: hidden;
            }
            .nonedis{
                margin-left: 15px; 
            }
            .thumbnails{
                border: 1px #FFF solid;
            }
             #left{
                height: 600px;
              border-right:2px #E5E5E5 solid;
               -webkit-border-right:2px #E5E5E5 solid;
	       -moz-border-right:2px #E5E5E5 solid;
                -o-border-right:2px #E5E5E5 solid;
                -ms-border-right:2px #E5E5E5 solid;
            }
            .container {
               padding-right: 0px; 
               padding-left: 0px; 
            }
            nav {
              box-shadow: 0 0px 0px rgba(0, 0, 0, 0.1); 
            }
        </style>
        <script type="text/javascript">
            function killerrors() {
                return true;
            }
            window.onerror = killerrors;

        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $("nav").each(function(index, element) {
                    $(this).find("div").css("background-color", "#FFFFFF");
                });
                $("nav").find("div").eq(2).css("background-color", "#00B9FF");
                $("nav ul li").mouseover(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $(this).find("div").css("background-color", "#00B9FF");
                });
                $("nav").mouseout(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $("nav").find("div").eq(2).css("background-color", "#00B9FF");
                });
                $(".ispay").each(function() {
                    var value = $(this).text();
                    if (value == "未支付")
                    {
                        $("#rightPay").css("display", "inline");
                    }
                });
                //注销
                $('#loginOut').click(function() {
                    if (confirm("确定注销？"))
                    {
                        $.post("./index.php?r=login/loginOut", function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                                window.location.href = "./index.php?r=login/login";
                        });
                    }
                });
                $("#login_center").click(function() {
                    window.location.href = './index.php?r=personalCenter/personalCenter';
                });
            });
                 
        </script>
      
    </head>
    <body >
        <div class="containers">
            <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png);">
                <div class="row">
                    <div class="col-md-12" style="background-color:rgba(255,255,255,.2);text-align: center;padding-top: 5px;padding-bottom: 5px">
                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-4">
                                <a href="#" id="login_center"><div  style="width:35px;height:35px;border-radius:50px;float: left;background-color:rgba(255,255,255,.2);"><img style="margin-top:5px;margin-bottom: 5px;"  src="./css/bt/username.png"></div></a>
                                <div id="topLeft" style="text-align: left;font-size:18px;color: white;padding-top: 5px;padding-left: 45px;">
                                    <?php echo $username; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div  style="color: white;margin-left: 238px;margin-top: 4px;font-size:18px;cursor:pointer" id="loginOut" >注销</div>  
                            </div>
                        </div>                    
                    </div>
                </div>
                <br />                
                <div class="row">
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2" style="text-align: center;">
                        <a><img src="./css/bt/logo.png" alt=""></a>
                    </div>
                    <div class="col-md-5">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6" style="text-align: center;color: #FFF;font-size: 18px">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <p style="letter-spacing:3px; line-height:150%">
                                    公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>    
                <br>
            </div>
            <div class="container" style="margin-top: 20px;background-color: white;height:700px;margin-bottom: 50px;">
                <div class="row">
                    <div class="col-md-2">
                        <nav id="left" style="height:700px;">
                            <ul >
                                <li class="movies"><div></div><span class="movies-icon"  style="float:left"></span><a href="./index.php?r=myFile/myFile">我的文件</a></li><hr>
                                <li class="store"><div></div><span class="store-icon"></span><a href="./index.php?r=orderInfo/orderInfo">我的订单</a></li><hr>
                                <li class="music"><div></div><span class="music-icon"  style="float:left"></span><a href="./index.php?r=terninalState/terninalState">终端位置</a></li><hr>
                                <li class="books"><div></div><span class="books-icon"  style="float:left"></span><a href="./index.php?r=contactUs/ContactUs">联系我们</a></li><hr>
                                <li class="magazines"><div></div><span class="magazines-icon"  style="float:left"></span><a href="./index.php?r=personalCenter/personalCenter">个人中心</a></li><hr>
                            </ul>
                        </nav>
                        <div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
                        </div>
                    </div>
                    <div class="col-md-10">
                        <table class="table table-hover" id="Contacttable" style="background-color: #FFF;color: #333;margin-top: -1px;">
                            <tr>
                                <td colspan="6" style="text-align:left"><span style="color:black;font-size: 20px;letter-spacing: 1px;">终端位置</span>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#9d9d9d;letter-spacing: 1px;">*为您展示终端机位置及图片</span>
                                </td>
                            </tr>
                            <tr>
                                <th>序列</th>
                                <th>终端名称</th>
                                <th>终端地址</th>
                                <th>展示图片</th>
                            </tr>
                            <?php
                            foreach ($print_info as $k => $l) {
                                echo "<tr><td>";
                                echo $k + 1;
                                echo "</td>";
                                echo "<td>" . $l->printorName . "</td>";
                                echo "<td>" . $l->address . "</td>";
                                echo "<td>";
//                            echo "<a href='./index.php?r=terninalState/terninalPicture&machineId=" . base64_encode($l->machineId) . "' target='_blank'>查看图片</a>";
                                if ($l->picture == NULL)
                                    echo "无";
                                else {
                                    echo '<a class="fancy "  href="./images/terninalPictures/' . $l->picture . '" data-fancybox-group="gallery">';
                                    echo '<img style = "width:30px;height:30px" class="thumbnails" src="./images/terninalPictures/' . $l->picture . '"/>';
                                    echo '</a>';
                                }
//                                    echo "<img src=./images/terninalPictures/$l->picture style = 'width:30px;height:30px'>";
                                echo "</td>";
                                echo "</tr>";
                            }
                            if ($print_list != "") {
                                echo '<tr class="YetprintLists">';
                                echo '<td colspan="6"><div style="text-align:center;color: black;background-color:#f9f9f9;padding:12px;margin:-12px; border-bottom:1px #E5E5E5 solid;"><strong>';
                                echo $print_list;
                                echo'</strong></div></td></tr>';
                            } else {
                                echo '<tr class="YetprintLists"><TD colspan="6">无</TD>';
                            }
                            ?>
                         
                        </table>
                    </div>
                </div>
            </div>
            <div style="width: 100%;height: 20px;"></div>
            <div class="container" id="footer" style="text-align:center;background-color: #333;width: 100%;color:white;font-size: 15px;height:50px;">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 15px;">
                        <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.       
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>