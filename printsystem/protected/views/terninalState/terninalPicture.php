<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link href="./css/PictureDisplay/css/bootstrap.css" rel="stylesheet">
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <script src = "./css/bt/bootstrap.min.js" type = "text/javascript"></script>
        <link rel="stylesheet" href="./css/bt/footer.css">
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <script type="text/javascript">
            $(document).ready(function() {
                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });
            });
        </script>
        <script>
            function killerrors() {
                return true;
            }
            window.onerror = killerrors;
            $(function() {
                //注销
                $('#loginOut').click(function() {
                    if (confirm("确定注销？"))
                    {
                        $.post("./index.php?r=login/loginOut", function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                                window.location.href = "./index.php?r=login/login";
                        });
                    }
                });
                $("#login_center").click(function() {
                    window.location.href = './index.php?r=personalCenter/personalCenter';
                });
            })
        </script>
        <style>
           body{
                background-color: #E5E5E5;
                font-family:Microsoft YaHei;
            }
            .nonedis{
               margin-left: 15px; 
            }
            .thumbnails{
               border: 1px #FFF solid;
            }
        </style>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="containers">
        <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png);">
            <div class="row">
                <div class="col-md-12" style="background-color:rgba(255,255,255,.2);text-align: center;padding-top: 5px;padding-bottom: 5px">
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-4">
                            <a href="#" id="login_center"><div  style="width:35px;height:35px;border-radius:50px;float: left;background-color:rgba(255,255,255,.2);"><img style="margin-top:5px;margin-bottom: 5px;"  src="./css/bt/username.png"></div></a>
                            <div id="topLeft" style="text-align: left;font-size:18px;color: white;padding-top: 5px;">
                                <?php echo $username; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div  style="color: white;margin-left: 238px;margin-top: 4px;font-size:18px;cursor:pointer" id="loginOut" >注销</div>  
                        </div>
                    </div>                    
                </div>
            </div>
            <br />                
            <div class="row">
                <div class="col-md-5">
                </div>
                <div class="col-md-2" style="text-align: center;">
                    <a><img src="./css/bt/logo.png" alt=""></a>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6" style="text-align: center;color: #FFF;font-size: 18px">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="letter-spacing:3px; line-height:150%">
                                公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>    
            <br>
        </div>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-3" style="font-size:20px;font-weight: bold">
                    <?php
                    echo $print_name . "&nbsp;展示图片";
                    ?>
                </div>
            </div>
            <br>
            <div class="row">

                <?php
                if (count($picture_array) != 0) {
                    foreach ($picture_array as $k => $l) {
                        echo '<div class="col-xs-12 col-sm-4 col-md-3 fancybox img-responsive">';
                        echo '<a class="fancy "  href="./images/terninalPictures/' . $l . '" data-fancybox-group="gallery">';
                        echo '<img class="thumbnails" src="./images/terninalPictures/' . $l . '"/>';
                        echo '</a>';
                        echo '</div>';
                    }
                } else {
                    echo'<div class="nonedis">';
                    echo"无";
                    echo'</div>';
                }
                ?>
            </div>

        </div>

            <div style="width: 100%;height: 20px;"></div>
        <div class="container" id="footer" style="text-align:center;background-color: #333;width: 100%;color:white;font-size: 15px;height:50px;">
            <div class="row">
                <div class="col-md-12" style="margin-top: 15px;">
                    <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.       
                </div>
            </div>
        </div>
        </div>
    </body>
</html>