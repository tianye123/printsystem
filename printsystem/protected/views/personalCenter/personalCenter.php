<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <link href="./css/leftNavigation/css/style.css" rel="stylesheet" type="text/css">
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="./css/bt/footer.css">
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
             body{
                background-color: #E5E5E5;
                font-family:Microsoft YaHei;
                overflow-y:scroll;
                overflow-x: hidden;
            }
             .form-group {
               margin-bottom: 0;
            }
            #registerform label {
                display: block;
                font-size: 15px;
                /* font-weight: bold; */
                color: #7c8291;
                margin-bottom: 3px;
                text-align: right;
            }
            #Contacttable tr td{
                text-align: left;
                font-size: 15px;
                padding: 12px;
            }
            #Contacttable tr th{
                font-size: 15px;
                padding: 12px;
            }
            #Contacttable tr span{
                color: red;
                font-size: 15px;  
                font-weight: bold;
            }
           
            #Contacttable .txtfield {
                width: 220px;
                margin-top: 1px;
                border: 0px;
                height: 38px;
                line-height: 38px;
                font-size: 18px;
                box-shadow: none;
            }
            #Contacttable .star{
                color:red;
                visibility: hidden;
            }
            #editbtn{
                visibility: hidden;
            }
            #Contacttable .psw{
                display:  none;
            }
            #trueregisterInfo {
                visibility: hidden;
            }
            #editbtn{
                margin-left: 50px;
            }
            .star{
                margin-left: 8px;margin-top: 10px;   
            }
            .btn-warning[disabled]{
               border-color:#E0DEDC;
            }
                 #left{
                height: 600px;
               border-right:2px #E5E5E5 solid;
               -webkit-border-right:2px #E5E5E5 solid;
	       -moz-border-right:2px #E5E5E5 solid;
                -o-border-right:2px #E5E5E5 solid;
                -ms-border-right:2px #E5E5E5 solid;
            }
            .container {
               padding-right: 0px; 
               padding-left: 0px; 
            }
            nav {
              box-shadow: 0 0px 0px rgba(0, 0, 0, 0.1); 
            }
        </style>
        <script>
            function killerrors() {
                return true;
            }
            window.onerror = killerrors;
            $(document).ready(function() {
                $("nav").each(function(index, element) {
                    $(this).find("div").css("background-color", "#FFFFFF");
                });
                $("nav").find("div").eq(4).css("background-color", "#00B9FF");
                $("nav ul li").mouseover(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $(this).find("div").css("background-color", "#00B9FF");
                });
                $("nav").mouseout(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $("nav").find("div").eq(4).css("background-color", "#00B9FF");
                });
                if ("<?php echo $status; ?>" == "yes")
                {
                 $("#signIn").text("已签到");
                 $("#signIn").css("background-color", "#E5E5E5");
                 $("#signIn").css("color", "#000000");
                 $("#signIn").attr("disabled", "disabled");
                }
               
                //注销
                $('#loginOut').click(function() {
                    if (confirm("确定注销？"))
                    {
                        $.post("./index.php?r=login/loginOut", function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                window.location.href = "./index.php?r=login/login";
                            }
                        });
                    }
                });
                if ($("#Email").val() == "") {
                    $("#Email").val("无");
                }
                //点击编辑按钮
                $("#editpersonInfo").click(function() {

                    if ($("#editpersonInfo").text() == "编辑")
                    {      if ($("#Email").val() == "无") {
                             $("#Email").val("");
                             } 
                        $("#Contacttable .txtfield").css("box-shadow", "0px 1px 2px rgba(0, 0, 0, 0.25) inset, 0px 1px rgba(255, 255, 255, 0.4)");
                        $("#Contacttable .txtfield").css("border", "1px #F3F6FA solid");
                        $("#Contacttable .star").css("visibility", "visible");
                        $("#attention").text("*请认真填写个人信息，以免出错");
                        $("#Username").attr("readonly", "readonly");
                        $("#Username").css("background-color", "#E5E5E5");
                        $("#Password").attr("placeholder", "请输入密码");
                        $("#surePassword").attr("placeholder", "请确认密码");
                        $("#Phone").attr("placeholder", "请输入手机号码");
                        $("#Phone").attr("readonly", "readonly");
                        $("#Phone").css("background-color", "#E5E5E5");
                        $("#Email").attr("placeholder", "请输入邮箱");
                        $("#editbtn").css("visibility", "visible");
                        $("#Contacttable .psw").css("display", "block");
                        $("#trueregisterInfo").css("visibility", "visible");
                        $("#br").css("display", "block");
                        $(".integration").css("display", "none");
                          $(".form-group").css("margin-bottom", "10px");
                       document.getElementById("left").style.height = document.getElementById("Contacttable").offsetHeight+"px";
                        $("#editpersonInfo").text("取消");
                    }
                    else if ($("#editpersonInfo").text() == "取消")
                    {
                        if ($("#Email").val() == "") {
                            $("#Email").val("无");
                        }
                        $("#attention").text("*请仔细查看个人信息，如需纠正，可修改");
                        $("#Username").css("background-color", "#FFF");
                        $("#Username").attr("readonly", "readonly");
                        $("#Phone").css("background-color", "#FFF");
                        $("#Phone").attr("readonly", "readonly");
                        $("#Contacttable .txtfield").css("box-shadow", "none");
                        $("#Contacttable .txtfield").css("border", "0px");
                        $("#Contacttable .star").css("visibility", "hidden");
                        $("#editbtn").css("visibility", "hidden");
                        $("#Contacttable .psw").css("display", "none");
                        $("#trueregisterInfo").css("visibility", "hidden");
                        $("#Email").attr("placeholder", "");
                        $("#Email").attr("readonly", "readonly");
                        $("#Email").css("background-color", "#FFF");
                        $(".integration").css("display", "block");
                        $("#integration").attr("readonly", "readonly");
                        $("#integration").css("background-color", "#FFF");
                        $("#br").css("display", "none");
                        $(".form-group").css("margin-bottom", "0");
 
                        document.getElementById("left").style.height = document.getElementById("Contacttable").offsetHeight+"px";
                        $("#editpersonInfo").text("编辑");
                        //                        $(".main-footer").css("margin-top", "143px");
                    }

                });
                //点击更换手机号码
                $("#changePhone").click(function() {
                    window.location.href = "./index.php?r=personalCenter/changePhone";
                });
                function trueregisterInfo() {
                    $("#trueregisterInfo").css("color", "red");
                }
                //恢复错误信息
                function retrueregisterInfo() {
                    $("#trueregisterInfo").css("color", "#787471");
                    $("#trueregisterInfo").text("温馨提示：*为必填");
                    $("#Username").css("border", "1px #F3F6FA solid");
                    $("#Password").css("border", "1px #F3F6FA solid");
                    $("#surePassword").css("border", "2px #F3F6FA solid");
                    $("#Phone").css("border", "2px #F3F6FA solid");
                    $("#Email").css("border", "2px #F3F6FA solid");
                }


                $("#Password").change(function() {
                    retrueregisterInfo();
                });
                $("#surePassword").change(function() {
                    retrueregisterInfo();
                });
                $("#Email").change(function() {
                    retrueregisterInfo();
                });
                $("#editbtn").click(function() {
                    var Username = $("#Username").val();
                    var Password = $("#Password").val();
                    var surePassword = $("#surePassword").val();
                    var Phone = $("#Phone").val();
                    var Email = $("#Email").val();
                    if (Password == "")
                    {
                        trueregisterInfo();
                        $("#Password").css("border", "2px red solid");
                        $("#trueregisterInfo").text("请输入密码!");
                        return false;
                    }
                    if (Password.length < 6)
                    {
                        trueregisterInfo();
                        $("#Password").css("border", "2px red solid");
                        $("#trueregisterInfo").text("密码位数不能少于六位!");
                        return false;
                    }
                    if (Password.length > 20)
                    {
                        trueregisterInfo();
                        $("#Password").css("border", "2px red solid");
                        $("#trueregisterInfo").text("密码位数不能多于二十位!");
                        return false;
                    }
                    if (surePassword == "")
                    {
                        trueregisterInfo();
                        $("#surePassword").css("border", "2px red solid");
                        $("#trueregisterInfo").text("请输入确认密码!");
                        return false;
                    }
                    if (Password != surePassword)
                    {
                        trueregisterInfo();
                        $("#surePassword").css("border", "2px red solid");
                        $("#trueregisterInfo").text("两次密码不一样!");
                        return false;
                    }
                    if (Phone == "")
                    {
                        trueregisterInfo();
                        $("#Phone").css("border", "2px red solid");
                        $("#trueregisterInfo").text("请您输入手机号码!");
                        return false;
                    }
                    if (Email.trim() != "")
                    {
                        var filter = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
                        if (filter.test(Email))
                        {
                            if (confirm("确认修改？"))
                            {
                                $.post("./index.php?r=personalCenter/changePersonalInfo", {Username: Username, Password: Password, Phone: Phone, Email: Email}, function(datainfo) {
                                    var data = eval("(" + datainfo + ")");
                                    if (data.data == "success")
                                    {
                                        window.location.href = "./index.php?r=personalCenter/personalCenter";
                                    }
                                    else
                                    {
                                        trueregisterInfo();
                                        $("#trueregisterInfo").text("发生未知的错误，请刷新后重试!");
                                    }
                                });
                            }
                        }
                        else {
                            trueregisterInfo();
                            $("#Email").css("border", "2px #FE0000 solid");
                            $("#trueregisterInfo").text("邮箱格式不对!");
                            return false;
                        }
                    }
                    else
                    {
                        if (confirm("确认修改？"))
                        {
                            $.post("./index.php?r=personalCenter/changePersonalInfo", {Username: Username, Password: Password, Phone: Phone, Email: Email}, function(datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "success")
                                {
                                    window.location.href = "./index.php?r=personalCenter/personalCenter";
                                }
                                else
                                {
                                    trueregisterInfo();
                                    $("#trueregisterInfo").text("发生未知的错误，请刷新后重试!");
                                }
                            });
                        }
                    }
                });
                $("#login_center").click(function() {
                    window.location.href = './index.php?r=personalCenter/personalCenter';
                });
                //签到
                $("#signIn").click(function() {
                    $.post("./index.php?r=personalCenter/signInfo", function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "1")
                        {
                            alert("连续签到" + data.data + "天，增加5个积分！");
                            var integration = <?php echo $integration + 5; ?>;
                            $("#integration").val(integration + "点");
                            $("#integration").val(integration + "点");
                            $("#signIn").text("已签到");
                            $("#signIn").css("background-color", "#E5E5E5");
                            $("#signIn").css("color", "#000000");
                            $("#signIn").attr("disabled", "disabled");
                        }
                        else if (data.data == "2")
                        {
                            alert("连续签到" + data.data + "天，增加10个积分！");
                            var integration = <?php echo $integration + 10; ?>;
                            $("#integration").val(integration + "点");
                            $("#signIn").text("已签到");
                            $("#signIn").css("background-color", "#E5E5E5");
                            $("#signIn").css("color", "#000000");
                            $("#signIn").attr("disabled", "disabled");
                        }
                        else if (data.data == "3")
                        {
                            alert("连续签到" + data.data + "天，增加15个积分！");
                            var integration = <?php echo $integration + 15; ?>;
                            $("#integration").val(integration + "点");
                            $("#signIn").text("已签到");
                            $("#signIn").css("background-color", "#E5E5E5");
                            $("#signIn").css("color", "#000000");
                            $("#signIn").attr("disabled", "disabled");
                        }
                        else if (data.data == "4")
                        {
                            alert("连续签到" + data.data + "天，增加20个积分！");
                            var integration = <?php echo $integration + 20; ?>;
                            $("#integration").val(integration.toFixed(2) + "点");
                            $("#signIn").text("已签到");
                            $("#signIn").css("background-color", "#E5E5E5");
                            $("#signIn").css("color", "#000000");
                            $("#signIn").attr("disabled", "disabled");
                        }
                        else if (data.data == "5")
                        {
                            alert("连续签到" + data.data + "天，增加25个积分！");
                            var integration = <?php echo $integration + 25; ?>;
                            $("#integration").val(integration.toFixed(2) + "点");
                            $("#signIn").text("已签到");
                            $("#signIn").css("background-color", "#E5E5E5");
                            $("#signIn").css("color", "#000000");
                            $("#signIn").attr("disabled", "disabled");
                        }
                        else if (data.data >= 6)
                        {
                            alert("连续签到" + data.data + "天，增加30个积分！");
                            var integration = <?php echo $integration + 30; ?>;
                            $("#integration").val(integration.toFixed(2) + "点");
                            $("#signIn").text("已签到");
                            $("#signIn").css("background-color", "#E5E5E5");
                            $("#signIn").css("color", "#000000");
                            $("#signIn").attr("disabled", "disabled");
                        }
                        else
                        {
                            alert("未知的错误导致签到失败！");
                        }
                    });
                });
            });
              
        </script>
    </head>
    <body>
        <div class="containers">
        <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png);">
            <div class="row">
                <div class="col-md-12" style="background-color:rgba(255,255,255,.2);text-align: center;padding-top: 5px;padding-bottom: 5px">
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-4">
                            <a href="#" id="login_center"><div  style="width:35px;height:35px;border-radius:50px;float: left;background-color:rgba(255,255,255,.2);"><img style="margin-top:5px;margin-bottom: 5px;"  src="./css/bt/username.png"></div></a>
                            <div id="topLeft" style="text-align: left;font-size:18px;color: white;padding-top: 5px;padding-left: 45px;">
                                <?php echo $username; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div  style="color:white;margin-left: 238px;margin-top: 4px;font-size:18px;cursor:pointer" id="loginOut" >注销</div>  
                        </div>
                    </div>                    
                </div>
            </div>
            <br />                
            <div class="row">
                <div class="col-md-5">
                </div>
                <div class="col-md-2" style="text-align: center;">
                    <a><img src="./css/bt/logo.png" alt=""></a>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6" style="text-align: center;color: #FFF;font-size: 18px">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="letter-spacing:3px; line-height:150%">
                                公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>    
            <br>
        </div>
        <div class="container" style="margin-top: 20px;background-color: white;height: 600px;margin-bottom: 50px;">
            <div class="row">
                <div class="col-md-2">
                    <nav id="left">
                        <ul>
                            <li class="movies"><div></div><span class="movies-icon"  style="float:left"></span><a href="./index.php?r=myFile/myFile">我的文件</a></li><hr>
                            <li class="store"><div></div><span class="store-icon"></span><a href="./index.php?r=orderInfo/orderInfo">我的订单</a></li><hr>
                            <li class="music"><div></div><span class="music-icon"  style="float:left"></span><a href="./index.php?r=terninalState/terninalState">终端位置</a></li><hr>
                            <li class="books"><div></div><span class="books-icon"  style="float:left"></span><a href="./index.php?r=contactUs/ContactUs">联系我们</a></li><hr>
                            <li class="magazines"><div></div><span class="magazines-icon"  style="float:left"></span><a href="./index.php?r=personalCenter/personalCenter">个人中心</a></li><hr>
                        </ul>
                    </nav>
                    <div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
                </div>
                </div>
                <div class="col-md-10">
                    <table class="table" id="Contacttable" style="background-color: #FFF;color: #333;margin-top: -1px;height: 600px;">
                        <tr>
                            <td style="text-align:left;height: 55px;"><span style="color:black;font-size: 20px;letter-spacing: 1px;">个人信息</span>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#9d9d9d;letter-spacing: 1px;" id="attention" >*请仔细查看个人信息，如需纠正，可修改</span><div style="float:right;padding: 3px">
                                    <button type="button" class="btn btn-dafault" id="editpersonInfo">编辑</button>
                                      <button type="button" class="btn btn-warning" id="signIn">签到</button>&nbsp;&nbsp;
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <form id="registerform" style="margin-left:12%;" class="form-horizontal" role="form" name="registerform" method="post" action="index.html">
                                    <div class="row">
                                        <div class="col-md-6" id="trueregisterInfo" style="text-align:center;">
                                            温馨提示：*为必填
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="Username" class="col-sm-2 control-label">用户名:</label>
                                            <div class="col-sm-3">
                                                <input type="text" value="<?php echo $username; ?>"  name="Username" id="Username" class="form-control txtfield" tabindex="1">
                                            </div>
                                            <div class="col-sm-1 star">*</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group psw">
                                            <label for="password" class="col-sm-2 control-label">密 码:</label>
                                            <div class="col-sm-3">
                                                <input type="password"  name="Password" id="Password" class="form-control txtfield" tabindex="2">
                                            </div>
                                            <div class="col-sm-1 star">*</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group psw">
                                            <label for="surePassword" class="col-sm-2 control-label">确认密码:</label>
                                            <div class="col-sm-3">
                                                <input type="password" name="surePassword" id="surePassword" class="form-control txtfield" tabindex="3">
                                            </div>
                                            <div class="col-sm-1 star">*</div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <label for="phone" class="col-sm-2 control-label">手机号码:</label>
                                            <div class="col-sm-3">
                                                <input type="phone" value="<?php echo $user_info->phone; ?>"  name="Phone" id="Phone" class="form-control txtfield" tabindex="4">
                                            </div>
                                            <div class="col-sm-1 star">*</div>
                                            <a style="cursor:pointer;float: left;margin-top:9px;      " id="changePhone"> 更换>></a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="phone" class="col-sm-2 control-label">邮 箱:</label>
                                            <div class="col-sm-3">
                                                <input type="email" value="<?php echo $user_info->email ?>"  name="Email" id="Email" class="form-control txtfield " tabindex="6" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group integration">
                                            <label for="integration" class="col-sm-2 control-label">积 分:</label>
                                            <div class="col-sm-3">
                                                <input type="text" value="<?php echo $integration; ?>点"  name="integration" id="integration" class="form-control txtfield" tabindex="7">
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <input type="button" name="editbtn" id="editbtn" class="flatbtn-blu" value="保 存" tabindex="8">
 
                                    </div>
                                    <div id="br" style="display: none;"><br><br><br>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        
            <div style="width: 100%;height: 20px;"></div>
        <div class="container" id="footer" style="text-align:center;background-color: #333;width: 100%;color:white;font-size: 15px;height:50px;">
            <div class="row">
                <div class="col-md-12" style="margin-top: 15px;">
                    <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.       
                </div>
            </div>
        </div>
        </div>
    </body>
</html>