<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <link href="./css/leftNavigation/css/style.css" rel="stylesheet" type="text/css">
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="./css/bt/footer.css">
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <STYLE>
             body{
                background-color: #E5E5E5;
                font-family:Microsoft YaHei;
                overflow-y:scroll;
                overflow-x: hidden;
            }
            #Contacttable tr td{
                text-align: left;
                font-size: 15px;
                padding: 12px;
            }
            #Contacttable tr th{
                font-size: 15px;
                padding: 12px;
            }
            #Contacttable tr span{
                color: red;
                font-size: 15px;  
                font-weight: bold;
            }
            #Contacttable .txtfield{
                width:220px;
                margin-top: 1px;
                border: 0px;
                height: 38px;
                line-height: 38px;
                font-size: 18px;
            }
            label {
                margin-top: 8px;
            }
                 #left{
                height: 600px;
              border-right:2px #E5E5E5 solid;
               -webkit-border-right:2px #E5E5E5 solid;
	       -moz-border-right:2px #E5E5E5 solid;
                -o-border-right:2px #E5E5E5 solid;
                -ms-border-right:2px #E5E5E5 solid;
            }
            .container {
               padding-right: 0px; 
               padding-left: 0px; 
            }
            nav {
              box-shadow: 0 0px 0px rgba(0, 0, 0, 0.1); 
            }
        </style>
        <script>
            function killerrors() {
                return true;
            }
            window.onerror = killerrors;
            $(document).ready(function() {
                $("nav").each(function(index, element) {
                    $(this).find("div").css("background-color", "#FFFFFF");
                });
                $("nav").find("div").eq(4).css("background-color", "#00B9FF");
                $("nav ul li").mouseover(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $(this).find("div").css("background-color", "#00B9FF");
                });
                $("nav").mouseout(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $("nav").find("div").eq(4).css("background-color", "#00B9FF");
                });

                //注销
                $('#loginOut').click(function() {
                    if (confirm("确定注销？"))
                    {
                        $.post("./index.php?r=login/loginOut", function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                                window.location.href = "./index.php?r=login/login";
                        });
                    }
                });

                function trueregisterInfo() {
                    $("#trueregisterInfo").css("color", "red");
                }
                //恢复错误信息
                function retrueregisterInfo() {
                    $("#trueregisterInfo").css("color", "#787471");
                    $("#trueregisterInfo").text("");
                    $("#Phone").css("border", "2px #F3F6FA solid");
                }

                $("#Phone").change(function() {
                    retrueregisterInfo();
                    var Phone = $("#Phone").val();
                    if (Phone == "")
                    {
                        trueregisterInfo();
                        $("#Phone").css("border", "2px red solid");
                        $("#trueregisterInfo").text("请您输入手机号码!");
                        return false;
                    }

                    if (!/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/i.test(Phone))
                    {
                        trueregisterInfo();
                        $("#Phone").css("border", "2px red solid");
                        $("#trueregisterInfo").text("手机号码格式不对!");
                        return false;
                    }
                    $.post("./index.php?r=personalCenter/checkPhone", {Phone: Phone}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "phone_exist")
                        {
                            trueregisterInfo();
                            $("#Phone").css("border", "2px red solid");
                            $("#trueregisterInfo").text("手机号码已存在!");
                        }
                        else if (data.data == "phone_no")
                        {
                            trueregisterInfo();
                            $("#Phone").css("border", "2px red solid");
                            $("#trueregisterInfo").text("请更改您的手机号码!");
                        }
                    });
                });



                $("#editbtn").click(function() {
                    var Phone = $("#Phone").val();
                    var phonecode = $("#phonecode").val();


                    if (Phone == "")
                    {
                        trueregisterInfo();
                        $("#Phone").css("border", "2px red solid");
                        $("#trueregisterInfo").text("请您输入手机号码!");
                        return false;
                    }

                    if (!/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/i.test(Phone))
                    {
                        trueregisterInfo();
                        $("#Phone").css("border", "2px red solid");
                        $("#trueregisterInfo").text("手机号码格式不对!");
                        return false;
                    }
                    if (phonecode == "")
                    {
                        trueregisterInfo();
                        $("#phonecode").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("请您输入验证码!");
                        return false;
                    }
                    $.post("./index.php?r=personalCenter/checkPhone", {Phone: Phone}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "phone_exist")
                        {
                            trueregisterInfo();
                            $("#Phone").css("border", "2px red solid");
                            $("#trueregisterInfo").text("手机号码已存在!");
                        }
                        else if (data.data == "phone_no")
                        {
                            trueregisterInfo();
                            $("#Phone").css("border", "2px red solid");
                            $("#trueregisterInfo").text("请更改您的手机号码!");
                        }
                        else
                        {
                            if (confirm("确认重新绑定手机号码 " + Phone + " ？"))
                            {
                                $.post("./index.php?r=personalCenter/changePersonalPhone", {Phone: Phone, phonecode: phonecode}, function(datainfo) {
                                    var data = eval("(" + datainfo + ")");
                                    if (data.data == "success")
                                    {
                                        window.location.href = "./index.php?r=personalCenter/personalCenter";
                                    }
                                    else if (data.data == "phonecode_false")
                                    {
                                        trueregisterInfo();
                                        $("#trueregisterInfo").text("验证码错误!");
                                    }
                                    else
                                    {
                                        trueregisterInfo();
                                        $("#trueregisterInfo").text("发生未知的错误，请刷新后重试!");
                                    }
                                });
                            }
                        }
                    });

                });
                $("#login_center").click(function() {
                    window.location.href = './index.php?r=personalCenter/personalCenter';
                });

                $("#btn_code").click(function() {
                    var Phone = $("#Phone").val();
                    if (Phone == "")
                    {
                        trueregisterInfo();
                        $("#Phone").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("请您输入手机号码!");
                        return false;
                    }
                    if (!/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/i.test(Phone))
                    {
                        trueregisterInfo();
                        $("#Phone").css("border", "2px #FE0000 solid");
                        $("#trueregisterInfo").text("手机号码格式不对!");
                        return false;
                    }
                    else
                    {
                        $.post("./index.php?r=personalCenter/checkPhone", {Phone: Phone}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "phone_exist")
                            {
                                trueregisterInfo();
                                $("#Phone").css("border", "2px #FE0000 solid");
                                $("#trueregisterInfo").text("手机号码已存在!");
                            }
                            else if (data.data == "phone_no")
                            {
                                trueregisterInfo();
                                $("#Phone").css("border", "2px #FE0000 solid");
                                $("#trueregisterInfo").text("请更改您的手机号码!");
                            }
                            else
                            {
                                time();
                                $.post("./index.php?r=personalCenter/changePhones", {Phone: Phone}, function(datainfos) {
                                    var datas = eval("(" + datainfos + ")");
                                    if (datas.data == "phone_exise")
                                    {
                                        trueregisterInfo();
                                        $("#Phone").css("border", "2px #FE0000 solid");
                                        $("#trueregisterInfo").text("手机号码已存在!");
                                    }
                                    else if (datas.data == "phone_no")
                                    {
                                        trueregisterInfo();
                                        $("#Phone").css("border", "2px red solid");
                                        $("#trueregisterInfo").text("请更改您的手机号码!");
                                    }
                                    else if (datas.data == "success")
                                    {
                                        $("#trueregisterInfo").text("验证码已发送至您的手机，请输入验证码!");
                                    }
                                    else
                                    {
                                        trueregisterInfo();
                                        $("#trueregisterInfo").text("发生未知的错误，请刷新后重试!");
                                    }
                                })
                            }
                        });
                    }
                })

                var wait = 60;
                function time() {
                    if (wait == 0) {
                        $("#btn_code").attr("disabled", false);
                        $("#btn_code").attr("value", "获取验证码");
                        wait = 60;
                    } else {
                        $("#btn_code").attr("value", wait + "秒后再次获取");
                        $("#btn_code").attr("disabled", true);
                        wait--;
                        setTimeout(function() {
                            time();
                        }, 1000)
                    }
                }

            });
            
        </script>
    </head>
    <body>
        <div class="containers">
        <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png);">
            <div class="row">
                <div class="col-md-12" style="background-color:rgba(255,255,255,.2);text-align: center;padding-top: 5px;padding-bottom: 5px">
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-4">
                            <a href="#" id="login_center"><div  style="width:35px;height:35px;border-radius:50px;float: left;background-color:rgba(255,255,255,.2);"><img style="margin-top:5px;margin-bottom: 5px;"  src="./css/bt/username.png"></div></a>
                            <div id="topLeft" style="text-align: left;font-size:18px;color: white;padding-left: 45px;padding-top: 5px;">
                                <?php echo $username; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div  style="color: white;margin-left: 8px;margin-top: 4px;font-size:18px;cursor:pointer" id="loginOut" >注销</div>  
                        </div>
                    </div>                    
                </div>
            </div>
            <br />                
            <div class="row">
                <div class="col-md-5">
                </div>
                <div class="col-md-2" style="text-align: center;">
                    <a><img src="./css/bt/logo.png" alt=""></a>
                </div>
                <div class="col-md-5">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6" style="text-align: center;color: #FFF;font-size: 18px">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="letter-spacing:3px; line-height:150%">
                                公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>    
            <br>
        </div>
        <br>
        <div class="container" style="margin-top: 20px;background-color: white;height: 600px;margin-bottom: 50px;">
            <div class="row">
                <div class="col-md-2">
                    <nav id="left">
                        <ul>
                            <li class="movies"><div></div><span class="movies-icon"  style="float:left"></span><a href="./index.php?r=myFile/myFile">我的文件</a></li><hr>
                            <li class="store"><div></div><span class="store-icon"></span><a href="./index.php?r=orderInfo/orderInfo">我的订单</a></li><hr>
                            <li class="music"><div></div><span class="music-icon"  style="float:left"></span><a href="./index.php?r=terninalState/terninalState">终端位置</a></li><hr>
                            <li class="books"><div></div><span class="books-icon"  style="float:left"></span><a href="./index.php?r=contactUs/ContactUs">联系我们</a></li><hr>
                            <li class="magazines"><div></div><span class="magazines-icon"  style="float:left"></span><a href="./index.php?r=personalCenter/personalCenter">个人中心</a></li><hr>
                        </ul>
                    </nav>
                    <div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
                    </div>
                </div>
                <div class="col-md-10">
                    <table class="table" id="Contacttable" style="background-color: #FFF;color: #333;height: 600px;">
                        <tr>
                            <td colspan="6" style="text-align:left;height: 55px;">
                                <span style="color:black;font-size: 20px">个人信息</span>
                                <span style="color:#9d9d9d;margin-left: 15px;">*请认真填写个人信息，以免出错</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row" style="margin-left:10%">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <div id="trueregisterInfo" style="color:#787471;">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-2 control-label text-right">手机号码:</label>
                                        <div class="col-sm-3">
                                            <input type="text" value=""  name="Phone" id="Phone" class="form-control txtfield" placeholder="请输入新手机号码">        
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="code" class="col-sm-2 control-label text-right">验 证 码:</label>
                                        <div class="col-sm-3">
                                            <input type="text" name="phonecode" id="phonecode" class="form-control txtfield" placeholder="验证码">
                                            <input type="button" class="btn btn-info" value="获取验证码" name="btn_code" id="btn_code">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-11">
                                        <input type="button" name="editbtn" id="editbtn" class="flatbtn-blu" value="保 存">
                                    </div>
                                </div>
                                <br><br>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

            <div style="width: 100%;height: 20px;"></div>
         <div class="container" id="footer" style="text-align:center;background-color: #333;width: 100%;color:white;font-size: 15px;height:50px;">
            <div class="row">
                <div class="col-md-12" style="margin-top: 15px;">
                    <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.       
                </div>
            </div>
        </div>
        </div>
    </body>
</html>