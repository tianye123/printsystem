<!DOCTYPE HTML>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>重庆颇闰科技云打印平台</title>
        <link href="./css/bt/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="./css/bt/bootstrap.min.css">
        <link href="./css/leftNavigation/css/style.css" rel="stylesheet" type="text/css">
        <script src = "./css/bt/jQuery-1.10.2.min.js" type = "text/javascript"></script>
        <script src = "./css/bt/bootstrap.min.js" type = "text/javascript"></script>
        <link rel="stylesheet" href="./css/bt/footer.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            body{
                background-color: #E5E5E5;
                font-family:Microsoft YaHei;
                overflow-y:scroll;
                overflow-x: hidden;
            }
            #orderList tr td{
                text-align: center;
                font-size: 15px;
                padding: 12px;
            }
            #orderList tr th{
                text-align: center;
                font-size: 15px;
                padding: 12px;
            }
            #orderList tr span{
                color: #333;
                font-size: 19px;  
                font-weight: bold;
            }
            .black:hover *{
                background: #FCFCFC;
                color:#000;
            }
            .NotpaidList:hover *{
                background: #FCFCFC;
                color:#000;
            }
            .YetpaidList:hover *{
                background: #FCFCFC;
                color:#000;
            }
            .YetpaidList:hover *{
                background: #FCFCFC;
                color:#000;
            }
            #orderList tr td a{
                text-decoration:none;
            }
            #orderstyle li{ 
                background-color:white; 
                float:left; 
                list-style:none;
                width: 13%;
                border:1px #E5E5E5 solid;
                border-bottom:0px; 
                 margin-left: 15px;
            }
            #orderstyle ul { 
                margin-left: -28px;
            }
            #orderstyle li a{
                text-decoration:none; color:#00B9FF;
            }
            #orderstyle li a:hover{
                background:#00B9FF;
                font-weight:bolder;
                color:white;
            }
            #orderstyle .current{
                background:#00B9FF;
                color:white;
            }
            #orderstyle .current a{
                color:white;
                font-weight:bold;
            }
            .ViewOrde:hover{
                color:#F39C11;
            }
                 #left{
                height: 690px;
               border-right:2px #E5E5E5 solid;
               -webkit-border-right:2px #E5E5E5 solid;
	       -moz-border-right:2px #E5E5E5 solid;
                -o-border-right:2px #E5E5E5 solid;
                -ms-border-right:2px #E5E5E5 solid;
            }
            .container {
               padding-right: 0px; 
               padding-left: 0px; 
            }
            nav {
              box-shadow: 0 0px 0px rgba(0, 0, 0, 0.1); 
            }
        </style>
       
        <script type="text/javascript">
            $(document).ready(function() {
                var current_menu = $("#orderstyle li[class*='current']");
                var cur_index_menu = $("#orderstyle li").index(current_menu);
                var out_index_menu = 0;
                var timer_menu = null;
                $("ul li").click(function() {
                    $(this).addClass("current").siblings().removeClass("current");
                    current_menu = $("#orderstyle li[class*='current']");
                    cur_index_menu = $("#orderstyle li").index(current_menu);
                });
                $("#orderstyle li").hover(
                        function() {
                            if (typeof (timer_menu) !== "undefined") {
                                clearTimeout(timer_menu);
                            }
                            current_menu.removeClass('current');
                            $(this).addClass('current');
                        },
                        function() {
                            $(this).removeClass('current');
                            timer_menu = setTimeout(function() {
                                current_menu.addClass('current');
                            }, 500);
                            out_index_menu = $("#orderstyle li").index($(this));
                        });
                $("#orderstyle ul").mouseleave(function() {
                    if (cur_index_menu === out_index_menu) {
                        clearTimeout(timer_menu);
                        current_menu.addClass('current');
                    }
                });

            });

        </script>

        <script type="text/javascript">

            function killerrors() {
                return true;
            }
            window.onerror = killerrors;</script>  
        <script>
            $(document).ready(function() {
                $("nav").each(function(index, element) {
                    $(this).find("div").css("background-color", "#FFFFFF");
                });
                $("nav").find("div").eq(1).css("background-color", "#00B9FF");
                $("nav ul li").mouseover(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $(this).find("div").css("background-color", "#00B9FF");
                });
                $("nav").mouseout(function() {
                    $("nav").each(function(index, element) {
                        $(this).find("div").css("background-color", "#FFFFFF");
                    });
                    $("nav").find("div").eq(1).css("background-color", "#00B9FF");
                });
                $("#login_center").click(function() {
                    window.location.href = './index.php?r=personalCenter/personalCenter';
                });

                //订单列表 菜单处理  NotpaidList
                $(".YetpaidList").hide();
                $(".YetpaidLists").hide();
                $(".YetprintList").hide();
                $(".YetprintLists").hide();

                $(".nav").find("li").eq(0).click(function() {
                    $(".nav").find(".active").removeClass("active");
                    $(this).addClass("active");
                    $(".NotpaidList").show();
                    $(".NotpaidLists").show();
                    $(".YetpaidList").hide();
                    $(".YetpaidLists").hide();
                    $(".YetprintList").hide();
                    $(".YetprintLists").hide();
                });
                $(".nav").find("li").eq(1).click(function() {
                    $(".nav").find(".active").removeClass("active");
                    $(this).addClass("active");
                    $(".NotpaidList").hide();
                    $(".NotpaidLists").hide();
                    $(".YetpaidList").show();
                    $(".YetpaidLists").show();
                    $(".YetprintList").hide();
                    $(".YetprintLists").hide();
                });
                $(".nav").find("li").eq(2).click(function() {
                    $(".nav").find(".active").removeClass("active");
                    $(this).addClass("active");
                    $(".NotpaidList").hide();
                    $(".NotpaidLists").hide();
                    $(".YetpaidList").hide();
                    $(".YetpaidLists").hide();
                    $(".YetprintList").show();
                    $(".YetprintLists").show();
                });
                //注销
                $('#loginOut').click(function() {
                    if (confirm("确定注销？"))
                    {
                        $.post("./index.php?r=login/loginOut", function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                window.location.href = "./index.php?r=login/login";
                            }
                        });
                    }
                });
            });
        </script>
<!--        <script type="text/javascript">
$(function() {
   //判断控制页面初始时左右的高度一致	
   var hl = $("#left").outerHeight(); //获取左侧left层的高度 
   var hr = $("#orderList").outerHeight(); //获取右侧right层的高度  
   var mh = Math.max(hl,hr); //比较hl与hr的高度，并将最大值赋给变量mh
   $("#left").height(mh); //将left层高度设为最大高度mh  
   $("#orderList").height(mh); //将right层高度设为最大高度
   
   //标签切换效果
   $("#orderstyle ul li").click(function(){
     $(this).addClass("current").siblings().removeClass("current");
     var tabId = $(this).attr("id");
     $("#" + tabId + "_" + "con").show().siblings("div").hide(); 
     //执行标签效果时判断控制左右高度一致
     $("#left").height("");
     $("#orderList").height("");
     var hl = $(".left").outerHeight(); //获取左侧left层的高度 
     var hr = $("#orderList").outerHeight(); //获取右侧right层的高度  
     var mh = Math.max(hl,hr); //比较hl与hr的高度，并将最大值赋给变量mh
     $("#left").height(mh); //将left层高度设为最大高度mh  
     $("#orderList").height(mh); //将right层高度设为最大高度
   });
  
});
</script>-->
    </head>
    <body>
        <div class="containers">
            <div class="container" style="width: 100%;background-image: url(./css/bt/b_login.png);">
                <div class="row">
                    <div class="col-md-12" style="background-color:rgba(255,255,255,.2);text-align: center;padding-top: 5px;padding-bottom: 5px">
                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-4">
                                <a href="#" id="login_center"><div  style="width:35px;height:35px;border-radius:50px;float: left;background-color:rgba(255,255,255,.2);"><img style="margin-top:5px;margin-bottom: 5px;"  src="./css/bt/username.png"></div></a>
                                <div id="topLeft" style="text-align: left;font-size:18px;color: white;padding-top: 5px;padding-left: 45px;">
                                    <?php echo $username; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div  style="color: white;margin-left: 238px;margin-top: 8px;font-size:18px;cursor:pointer" id="loginOut" >注销</div>  
                            </div>
                        </div>                    
                    </div>
                </div>
                <br />                
                <div class="row">
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2" style="text-align: center;">
                        <a><img src="./css/bt/logo.png" alt=""></a>
                    </div>
                    <div class="col-md-5">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6" style="text-align: center;color: #FFF;font-size: 18px">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Hello, Welcome to <strong>颇闰自助打印</strong></h2>     
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <p style="letter-spacing:3px; line-height:150%">
                                    公司致力于随时打印、随处打印、打印自由、取件自由的全新打印理念，让您可以感受到轻松打印的用户体验，我们会一直致力于为您提供更快更好的服务。
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>    
                <br>
            </div>
            <div class="container" style="margin-top: 20px;background-color: white;height: 690px;margin-bottom: 50px;">
                <div class="row">
                    <div class="col-md-2">
                           <nav id="left">
                            <ul >
                                <li class="movies"><div></div><span class="movies-icon"  style="float:left"></span><a href="./index.php?r=myFile/myFile">我的文件</a></li><hr>
                                <li class="store"><div></div><span class="store-icon"></span><a href="./index.php?r=orderInfo/orderInfo">我的订单</a></li><hr>
                                <li class="music"><div></div><span class="music-icon"  style="float:left"></span><a href="./index.php?r=terninalState/terninalState">终端位置</a></li><hr>
                                <li class="books"><div></div><span class="books-icon"  style="float:left"></span><a href="./index.php?r=contactUs/ContactUs">联系我们</a></li><hr>
                                <li class="magazines"><div></div><span class="magazines-icon"  style="float:left"></span><a href="./index.php?r=personalCenter/personalCenter">个人中心</a></li><hr>
                            </ul>
                        </nav>
                        <div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
                        </div>
                    </div>

                     <div class="col-md-10">
                        <table class="table" id="orderList" style="background-color: #FFF;color: #333;margin-top: -1px;">
                            <tr class="black">
                                <td colspan="4" style="text-align:left"><span style=" letter-spacing: 1px;">我的订单</span>&nbsp;&nbsp;&nbsp;&nbsp;<span style="letter-spacing: 1px;color:#9d9d9d;font-size: 15px">*查看自己的订单</span></td>
                            </tr>
                            <tr id="orderstyle" style="padding-bottom:0px;">
                                <td colspan="4" style="padding-bottom:0px;">
                                    <ul class="nav nav-justified" id="orderlist">
                                        <li id="title01" class="current"><a style=" cursor:pointer">未支付订单</a></li>
                                        <li id="title02"><a style=" cursor:pointer">已支付订单</a></li>
                                        <li id="title03"><a style=" cursor:pointer">已打印订单</a></li>
                                    </ul>
                                </td>
                            </tr>
                             <tr class="black">
                                <th>订单号</th>
                                <th>总价</th>
                                <th>下单时间</th>
                                <th><div style='margin-right: 45px '>操作</div></th>
                            </tr>  
                            <?php
                            foreach ($NotpaidList as $K => $V)
                            {
                                echo'<tr class="NotpaidList">';
                                echo '<td>' . $V->orderId . '</td>';
                                echo '<td>' . $V->paidMoney . '</td>';
                                echo '<td style="width: 200px;">' . $V->placeOrdertime . '</td>';
                                echo '<td><a class="ViewOrde" style="margin-right: 45px" href="./index.php?r=myFile/myFileDetail&businessid=' . base64_encode($V->businessid) . '">查看订单</td>';
                                echo '</tr>';
                            }
                            if ($Notpaid_list != "")
                            {
                                echo '<tr class = "NotpaidLists">';
                                echo '<td colspan="7"><div style="text-align:center;color: black;background-color:#f9f9f9;padding:12px;margin:-12px; border-bottom:1px #E5E5E5 solid;"><strong>';
                                echo $Notpaid_list;
                                echo'</strong></div></td></tr>';
                            }
                            else
                            {
                                echo '<tr class = "NotpaidLists"><TD colspan="7"><br><br>无<br><br></TD>';
                            }
                            ?>
                            <?php
                            foreach ($YetpaidList as $K => $V)
                            {
                                echo'<tr class="YetpaidList">';
                                echo '<td>' . $V->orderId . '</td>';
                                echo '<td>' . $V->paidMoney . '</td>';
                                echo '<td style="width: 200px;">' . $V->placeOrdertime . '</td>';
                                echo '<td><a class="ViewOrde" style="margin-right: 45px" href="./index.php?r=myFile/myFileDetail&businessid=' . base64_encode($V->businessid) . '">查看订单</td>';
                                echo '</tr>';
                            }
                            if ($Yetpaid_list != "")
                            {
                                echo '<tr class = "YetpaidLists">';
                                echo '<td colspan="7"><div style="text-align:center;color: black;background-color:#f9f9f9;padding:12px;margin:-12px; border-bottom:1px #E5E5E5 solid;"><strong>';
                                echo $Yetpaid_list;
                                echo'</strong></div></td></tr>';
                            }
                            else
                            {
                                echo '<tr class = "YetpaidLists"><TD colspan="7"><br><br>无<br><br></TD>';
                            }
                            ?>
                            <?php
                            foreach ($YetprintList as $K => $V)
                            {
                                echo'<tr class="YetprintList">';
                                echo '<td>' . $V->orderId . '</td>';
                                echo '<td>' . $V->paidMoney . '</td>';
                                echo '<td style="width: 200px;">' . $V->placeOrdertime . '</td>';
                                echo '<td><a class="ViewOrde" style="margin-right: 45px" href="./index.php?r=myFile/myFileDetail&businessid=' . base64_encode($V->businessid) . '">查看订单</td>';
                                echo '</tr>';
                            }

                            if ($Yetprint_list != "")
                            {
                                echo '<tr class="YetprintLists">';
                                echo '<td colspan="7"><div style="text-align:center;color: black;background-color:#f9f9f9;padding:12px;margin:-12px; border-bottom:1px #E5E5E5 solid;"><strong>';
                                echo $Yetprint_list;
                                echo'</strong></div></td></tr>';
                            }
                            else
                            {
                                echo '<tr class="YetprintLists" style="200px;"><TD colspan="7"><br><br>无<br><br></TD>';
                                
                            }
                            ?>
                               
                        </table>
                    </div>
                </div>
            </div>
            <div style="width: 100%;height: 20px;"></div>
            <div class="container" id="footer" style="text-align:center;background-color: #333;width: 100%;color:white;font-size: 15px;height:50px;">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 15px;">
                        <strong>COPYRIGHT © 2015 <a  href="http://www.cqutprint.com/">重庆颇闰科技</a>.</strong> All rights reserved.       
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>