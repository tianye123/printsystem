<?php

/*
  Uploadify
  Copyright (c) 2012 Reactive Apps, Ronnie Garcia
  Released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
 */

// Define a destination
$targetFolder = './uploads'; // Relative to the root

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
    $tempFile = $_FILES['Filedata']['tmp_name'];
    //$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;

    $file = iconv("gb2312", "utf-8", $_FILES['Filedata']['name']);
    $targetPath = $targetFolder;
    $targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];

    // Validate the file type
    $fileTypes = array('doc', 'docx', 'pdf'); // File extensions
    $fileParts = pathinfo($_FILES['Filedata']['name']);

//    $user_model = user::model();


    if (in_array($fileParts['extension'], $fileTypes)) {
        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));
    } else {
        echo 'false';
    }
}
?>