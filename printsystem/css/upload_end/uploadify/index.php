<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>UploadiFive Test</title>
        <script src="jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="jquery.uploadify.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="uploadify.css">
        <style type="text/css">
            body {
                font: 13px Arial, Helvetica, Sans-serif;
            }
        </style>
    </head>

    <body>
        <form>
            <div id="queue"></div>
            <!--<input id="file_upload" name="file_upload" type="file" multiple="true">-->

            <div id="uploadFilemodal">
                <h1 style="font-size: 1.5em">上传文件</h1> 
                <form id="uploadform" style="margin-top: 30px" name="uploadform" method="post" action="index.html">
                    <table style="color: #333333">
                        <tr>
                            <td style="width:405px;height: 172px;background-image: url(../../css/bt/uploadFile.png)">
                                <input id="file_upload" name="file_upload" type="file" multiple="true">
                            </td>
                            <td>
                                <div style="border:2px #E5E5E5 solid;margin-left: 10px;padding-left:15px;padding-right: 15px;width: 300px;height: 386px">
                                    上传列表
                                    <HR style="margin-top: 7px;border-color: #E5E5E5;margin-bottom: 7px" id="fuleee">
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </form>

        <script type="text/javascript">
<?php $timestamp = time(); ?>
            $(function() {
                $('#file_upload').uploadify({
                    'formData': {
                        'timestamp': '<?php echo $timestamp; ?>',
                        'token': '<?php echo md5('unique_salt' . $timestamp); ?>'
                    },
                    'swf': 'uploadify.swf',
                    'uploader': 'uploadify.php'
                });
            });
        </script>
    </body>
</html>