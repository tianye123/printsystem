<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>支付宝即时到账交易接口接口</title>
            <style type="text/css">*{margin:0;padding:0;list-style-type:none;}
                a,img{border:0;}
                body{font:12px/180% Arial, Helvetica, sans-serif ,"新宋体";}
                /* pageLoad */
                #pageLoad{background:#FFF;position:fixed;_position:absolute;left:0;right:0;top:0;bottom:0;z-index:999;}
                #pageLoad div{text-align:center;height:150px;width:345px;padding-left:30px;position:absolute;}
                #pageLoad a,#pageLoad samp{display:block;background:url(../css/bt/loadaf.jpg) no-repeat center 0;height:90px;width:345px;position:absolute;z-index:0;left:0;top:0;}
                #pageLoad p{background:#FFF;width:2px;position:absolute;left:0;top:0;height:60px;z-index:5;}
                #pageLoad samp{z-index:1;overflow:hidden;width:0;}
                #pageLoad samp em{display:block;background:url(../css/bt/loadbf.jpg) no-repeat center 0;height:90px;width:345px;}
                #pageLoad span{display:block;text-align:center;color:#44CCFF;font-weight:bold;font-family:Arial;height:24px;font-size:24px;width:340px;padding-right:10px;position:relative;top:100px;}
            </style>
            <script src = "../css/bt/jquery.min.js" type = "text/javascript"></script>

            <script>
                $(function() {
                    var beforeDate = new Date();
                    //页面加载
                    var $window = $(window),
                            $doc = $(document),
                            $body = $("body"),
                            winWidth = $window.width(),
                            docWidth = $doc.width(),
                            docHeight = $doc.height(),
                            winHeight = $window.height(),
                            speed = 250;
                    $body.append("<div  id=\"pageLoad\" style=\"width:" + docWidth + "px;height:" + docHeight + "px;\"><div style=\"left:" + (winWidth - 350) * 0.5 + "px;top:" + (winHeight - 150) * 0.5 + "px;\"><a href=\"http://www.cqporun.com\"></a><samp><em></em></samp><span>0</span><p></p></div></div>");

                    var afterDate = new Date(),
                            pagePreLoad = (afterDate - beforeDate),
                            time = 10 / pagePreLoad,
                            preImgLen = 35 / pagePreLoad,
                            n = 0,
                            m = 0,
                            play = setInterval(function() {
                                if (Number(n) >= 100 && Number(m) >= 350) {
                                    clearInterval(play);
                                    n = 99;
                                    m = 350;
                                    //页面加载完毕后执行(主线)
//                                            setTimeout(function() {
//                                                $("#pageLoad").fadeOut(400, function() {
//                                                    $(this).remove();
//                                                    //location.href="http://www.17sucai.com/";
//                                                });
//
//                                            }, 200);
                                }
                                ;
                                $("#pageLoad").find("samp").css("width", m);
                                $("#pageLoad").find("span").text(Math.floor(n) + "%");
                                n += time;
                                m += preImgLen;
                            }, 100);
                });
            </script>
    </head>
    <body>
        <?php
        /*         *
         * 功能：即时到账交易接口接入页
         * 版本：3.3
         * 修改日期：2012-07-23
         * 说明：
         * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
         * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。

         * ************************注意*************************
         * 如果您在接口集成过程中遇到问题，可以按照下面的途径来解决
         * 1、商户服务中心（https://b.alipay.com/support/helperApply.htm?action=consultationApply），提交申请集成协助，我们会有专业的技术工程师主动联系您协助解决
         * 2、商户帮助中心（http://help.alipay.com/support/232511-16307/0-16307.htm?sh=Y&info_type=9）
         * 3、支付宝论坛（http://club.alipay.com/read-htm-tid-8681712.html）
         * 如果不想使用扩展功能请把扩展功能参数赋空值。
         */

        require_once("alipay.config.php");
        require_once("lib/alipay_submit.class.php");

        /*         * ************************请求参数************************* */

        //支付类型
        $payment_type = "1";
        //必填，不能修改
        //服务器异步通知页面路径
        $notify_url = "http://www.cqutprint.com/alipay/notify_url.php";
//        $notify_url = "http://183.230.116.223/printsystem/printsystem/alipay/notify_url.php";
        //需http://格式的完整路径，不能加?id=123这类自定义参数
        //页面跳转同步通知页面路径
        $return_url = "http://www.cqutprint.com/alipay/return_url.php";
        //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/
        //商户订单号
        $out_trade_no = $_GET['WIDout_trade_no'];
        //商户网站订单系统中唯一订单号，必填
        //订单名称
        $subject = $_GET['WIDsubject'];
        //必填
        //付款金额
        $total_fee = $_GET['WIDtotal_fee'];
        //必填
        //订单描述

        $body = $_GET['WIDbody'];
        //商品展示地址
        $show_url = $_GET['WIDshow_url'];
        //需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html
        //防钓鱼时间戳
        $anti_phishing_key = "";
        //若要使用请调用类文件submit中的query_timestamp函数
        //客户端的IP地址
        $exter_invoke_ip = "";
        //非局域网的外网IP地址，如：221.0.0.1


        /*         * ********************************************************* */

//构造要请求的参数数组，无需改动
        $parameter = array(
            "service" => "create_direct_pay_by_user",
            "partner" => trim($alipay_config['partner']),
            "seller_email" => trim($alipay_config['seller_email']),
            "payment_type" => $payment_type,
            "notify_url" => $notify_url,
            "return_url" => $return_url,
            "out_trade_no" => $out_trade_no,
            "subject" => $subject,
            "total_fee" => $total_fee,
            "body" => $body,
            "show_url" => $show_url,
            "anti_phishing_key" => $anti_phishing_key,
            "exter_invoke_ip" => $exter_invoke_ip,
            "_input_charset" => trim(strtolower($alipay_config['input_charset']))
        );

//建立请求
        $alipaySubmit = new AlipaySubmit($alipay_config);
        $html_text = $alipaySubmit->buildRequestForm($parameter, "get", "页面跳转中>>>");
        echo $html_text;
        ?>
    </body>
</html>